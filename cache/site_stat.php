	<div class="main_center_spacer"></div>
	<div class="main_center_container">


	    <div class="main_center_title">
		<div class="main_center_title_left"><font style="color:#617f10">SZERVEZETI </font> Statisztika</div>
		<div class="main_center_title_right"><font style="font-size: 9px; font-weight: normal;">a statisztika összeállítva ekkor: </font>2019-08-08 05:19:14</div>
	    </div>
	    <div class="main_center_content_spacer"></div>
	    <div class="main_center_content_left">Felhasználószám:</div><div class="main_center_content_center"><b>2548</b></div><div class="main_center_content_right"></div>
	    <div class="main_center_content_spacer"></div>
	    <div class="main_center_content_left">Aktív felhasználók:</div><div class="main_center_content_center"><b>340</b></div><div class="main_center_content_right">13.34%</div>
	    <div class="main_center_content_spacer"></div>
	    <div class="main_center_content_left">Szervezet szám:</div><div class="main_center_content_center"><b>715</b></div><div class="main_center_content_right"></div>
	    <div class="main_center_content_spacer"></div>

	    <div class="main_center_title">
		<div class="main_center_title_left"><font style="color:#617f10">FREYA</font> Statisztika</div>
		<div class="main_center_title_right"></div>
	    </div>
	    <div class="main_center_content_spacer"></div>
	    <div class="main_center_content_left"></div><div class="main_center_content_center"><b>Utolsó 30 nap<br /></b>(az előző időszakhoz képest)</div><div class="main_center_content_right"><b>2012.01.10 óta</b></div>
	    <div class="main_center_content_spacer"></div>
	    <div class="main_center_content_spacer"></div>
	    <div class="main_center_content_spacer"></div>
	    <div class="main_center_content_left">Egyedi látogatók:</div><div class="main_center_content_center"><b>11</b> (<font style="color:red;">-5</font>)</div><div class="main_center_content_right">20</div>
	    <div class="main_center_content_spacer"></div>
	    <div class="main_center_content_left">Munkamenetet:</div><div class="main_center_content_center"><b>165</b> (<font style="color:red;">-36</font>)</div><div class="main_center_content_right">424</div>
	    <div class="main_center_content_spacer"></div>
	    <div class="main_center_content_left">Oldallatöltés:</div><div class="main_center_content_center"><b>50008</b> (<font style="color:red;">-7316</font>)</div><div class="main_center_content_right">113288</div>
	    <div class="main_center_content_spacer"></div>
	    <div class="main_center_content_left">Átlagos oldalletöltési idő:</div><div class="main_center_content_center"><b>0.15s</b> (<font style="color:red;">+0.02s</font>)</div><div class="main_center_content_right">0.15s</div>  
	    <div class="main_center_content_spacer"></div>
	    <div class="main_center_content_left">Max oldalletöltési idő:</div><div class="main_center_content_center"><b>866.67s</b> (<font style="color:red;">+444.38s</font>)</div><div class="main_center_content_right"></div>  
	    <div class="main_center_content_spacer"></div>
	    <div class="main_center_content_left">Átlagos adatbáziskérés:</div><div class="main_center_content_center"><b>17.47</b> (<font style="color:red;">+1.28</font>)</div><div class="main_center_content_right">16.69</div>
	    <div class="main_center_content_spacer"></div>
	    <div class="main_center_content_left">Végrehajtot keresések:</div><div class="main_center_content_center"><b>18620</b> (<font style="color:red;">-2170</font>)</div><div class="main_center_content_right">41542</div>
		    <div class="main_center_content_spacer"></div>
	    <div class="main_center_content_left">Napi átlag keresés:</div><div class="main_center_content_center"><b>620.67</b> (<font style="color:red;">-36.17</font>)</div><div class="main_center_content_right"></div>
	    <div class="main_center_content_spacer"></div>
	    <div class="main_center_content_left">Átlagos keresési idő:</div><div class="main_center_content_center"><b>0.07s</b> (<font style="color:green;">-0.02s</font>)</div><div class="main_center_content_right">0.08s</div>  
	    <div class="main_center_content_spacer"></div>
	    	    <div class="main_center_content_left">Max keresési idő:</div><div class="main_center_content_center"><b>93.21s</b> (<font style="color:green;">-129.21s</font>)</div><div class="main_center_content_right"></div>  
	    <div class="main_center_content_spacer"></div>
	    <div class="main_center_content_left">Adatbáziskérés keresésnél:</div><div class="main_center_content_center"><b>22.51</b> (<font style="color:red;">+1.21</font>)</div><div class="main_center_content_right">21.76</div>
	    <div class="main_center_content_spacer"></div>
	    
	    <div class="main_center_title">
		<div class="main_center_title_left"><font style="color:#617f10">OLDALLETÖLTÉS </font> Szórása</div>
		<div class="main_center_title_right"><a title="Kifejt" href="#" id="names-toogle" onclick="return false"><span class="ui-state-default ui-corner-all ui-icon ui-icon-help"></span></a></div>
	    </div>
	    <div class="main_center_content_spacer"></div>
	     	    <div class="main_center_content_left">x&LessFullEqual;1</div><div class="main_center_content_center"><b>113026</b></div><div class="main_center_content_right">99.77%</div>
	   <div class="main_center_content_spacer"></div>
	             <div id="main_center_content_names" class="ui-corner-all">
 	    <div class="main_center_content_left">x&LessFullEqual;0.1</div><div class="main_center_content_center"><b>106063</b></div><div class="main_center_content_right">93.62%</div>
	    <div class="main_center_content_spacer"></div>
	    <div class="main_center_content_left">0.1&lt;x&LessFullEqual;0.2:</div><div class="main_center_content_center"><b>5183</b></div><div class="main_center_content_right">4.58%</div>
	    <div class="main_center_content_spacer"></div>
	    	    <div class="main_center_content_left">0.2&lt;x&LessFullEqual;0.3:</div><div class="main_center_content_center"><b>942</b></div><div class="main_center_content_right">0.83%</div>
	    <div class="main_center_content_spacer"></div>
	    	    <div class="main_center_content_left">0.3&lt;x&LessFullEqual;0.4:</div><div class="main_center_content_center"><b>327</b></div><div class="main_center_content_right">0.29%</div>
	    <div class="main_center_content_spacer"></div>
	    	    <div class="main_center_content_left">0.4&lt;x&LessFullEqual;0.5:</div><div class="main_center_content_center"><b>201</b></div><div class="main_center_content_right">0.18%</div>
	    <div class="main_center_content_spacer"></div>
	    	    <div class="main_center_content_left">0.5&lt;x&LessFullEqual;0.6:</div><div class="main_center_content_center"><b>137</b></div><div class="main_center_content_right">0.12%</div>
	    <div class="main_center_content_spacer"></div>
	    	    <div class="main_center_content_left">0.6&lt;x&LessFullEqual;0.7:</div><div class="main_center_content_center"><b>53</b></div><div class="main_center_content_right">0.05%</div>
	    <div class="main_center_content_spacer"></div>
	    	    <div class="main_center_content_left">0.7&lt;x&LessFullEqual;0.8:</div><div class="main_center_content_center"><b>46</b></div><div class="main_center_content_right">0.04%</div>
	    <div class="main_center_content_spacer"></div>
	    	    <div class="main_center_content_left">0.8&lt;x&LessFullEqual;0.9:</div><div class="main_center_content_center"><b>47</b></div><div class="main_center_content_right">0.04%</div>
	    <div class="main_center_content_spacer"></div>
	    	    <div class="main_center_content_left">0.9&lt;x&LessFullEqual;1.0:</div><div class="main_center_content_center"><b>27</b></div><div class="main_center_content_right">0.02%</div>
	    <div class="main_center_content_spacer"></div>
                    </div>
	    <div class="main_center_content_left">1&lt;x&LessFullEqual;2:</div><div class="main_center_content_center"><b>131</b></div><div class="main_center_content_right">0.12%</div>
	    <div class="main_center_content_spacer"></div>
	    <div class="main_center_content_left">2&lt;x&LessFullEqual;3:</div><div class="main_center_content_center"><b>33</b></div><div class="main_center_content_right">0.03%</div>
	    <div class="main_center_content_spacer"></div>
	    <div class="main_center_content_left">3&lt;x&LessFullEqual;4:</div><div class="main_center_content_center"><b>6</b></div><div class="main_center_content_right">0.01%</div>
	    <div class="main_center_content_spacer"></div>
	    <div class="main_center_content_left">4&lt;x&LessFullEqual;5:</div><div class="main_center_content_center"><b>3</b></div><div class="main_center_content_right">0%</div>
	    <div class="main_center_content_spacer"></div>
	    <div class="main_center_content_left">5&lt;x:</div><div class="main_center_content_center"><b>89</b></div><div class="main_center_content_right">0.08%</div>
	    <div class="main_center_content_spacer"></div>
	   
	    
	    <div class="main_center_title">
		<div class="main_center_title_left"><font style="color:#617f10">BÖNGÉSZŐ</font> Statisztika</div>
		<div class="main_center_title_right"></div>
	    </div>
	    <div class="main_center_content_spacer"></div>
	    <div class="main_center_content_left"></div><div class="main_center_content_center"><b>Utolsó 30 nap<br /></b></div><div class="main_center_content_right"><b>2012.01.10 óta</b></div>
	    <div class="main_center_content_spacer"></div>
	    <div class="main_center_content_spacer"></div>
	    <div class="main_center_content_spacer"></div>
	
	        <div class="main_center_content_left">Mozilla Firefox</div><div class="main_center_content_center">41 - 24.85%</div><div class="main_center_content_right">94 - 22.17%</div>

				
	        <div class="main_center_content_left">Microsoft Internet Explorer</div><div class="main_center_content_center">1 - 0.61%</div><div class="main_center_content_right">1 - 0.24%</div>

				
	        <div class="main_center_content_left">Google Chrome</div><div class="main_center_content_center">123 - 74.55%</div><div class="main_center_content_right">329 - 77.59%</div>

				
	        <div class="main_center_content_left">Egyéb</div><div class="main_center_content_center">0 - 0%</div><div class="main_center_content_right">0 - 0%</div>

				
	</div>

	<div class="main_center_spacer"></div>
	