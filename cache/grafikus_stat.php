<?php 
		$chart=array (
  'week' => 
  array (
    2019 => 
    array (
      22 => 
      array (
        'oldalletoltes' => '5956',
        'keresesek' => '2132',
        'nembejelkeresesek' => '1273',
        'egyedi' => '58',
      ),
      23 => 
      array (
        'oldalletoltes' => '11228',
        'keresesek' => '4090',
        'nembejelkeresesek' => '3223',
        'egyedi' => '35',
      ),
      24 => 
      array (
        'oldalletoltes' => '13525',
        'keresesek' => '4974',
        'nembejelkeresesek' => '4110',
        'egyedi' => '50',
      ),
      25 => 
      array (
        'oldalletoltes' => '13115',
        'keresesek' => '4894',
        'nembejelkeresesek' => '4012',
        'egyedi' => '41',
      ),
      26 => 
      array (
        'oldalletoltes' => '16526',
        'keresesek' => '5715',
        'nembejelkeresesek' => '4225',
        'egyedi' => '63',
      ),
      27 => 
      array (
        'oldalletoltes' => '12351',
        'keresesek' => '4658',
        'nembejelkeresesek' => '3710',
        'egyedi' => '51',
      ),
      28 => 
      array (
        'oldalletoltes' => '11040',
        'keresesek' => '4085',
        'nembejelkeresesek' => '3480',
        'egyedi' => '38',
      ),
      29 => 
      array (
        'oldalletoltes' => '10668',
        'keresesek' => '3986',
        'nembejelkeresesek' => '3114',
        'egyedi' => '38',
      ),
      30 => 
      array (
        'oldalletoltes' => '11696',
        'keresesek' => '4451',
        'nembejelkeresesek' => '3477',
        'egyedi' => '35',
      ),
      31 => 
      array (
        'oldalletoltes' => '7143',
        'keresesek' => '2548',
        'nembejelkeresesek' => '2510',
        'egyedi' => '15',
      ),
    ),
  ),
  'day' => 
  array (
    2019 => 
    array (
      6 => 
      array (
        6 => 
        array (
          'oldalletoltes' => '3848',
          'keresesek' => '1244',
          'nembejelkeresesek' => '702',
          'egyedi' => '26',
        ),
        7 => 
        array (
          'oldalletoltes' => '2020',
          'keresesek' => '857',
          'nembejelkeresesek' => '540',
          'egyedi' => '13',
        ),
        8 => 
        array (
          'oldalletoltes' => '88',
          'keresesek' => '31',
          'nembejelkeresesek' => '31',
        ),
        9 => 
        array (
          'oldalletoltes' => '58',
          'keresesek' => '23',
          'nembejelkeresesek' => '17',
          'egyedi' => '3',
        ),
        10 => 
        array (
          'oldalletoltes' => '131',
          'keresesek' => '38',
          'nembejelkeresesek' => '38',
        ),
        11 => 
        array (
          'oldalletoltes' => '3584',
          'keresesek' => '1254',
          'nembejelkeresesek' => '790',
          'egyedi' => '6',
        ),
        12 => 
        array (
          'oldalletoltes' => '2849',
          'keresesek' => '1135',
          'nembejelkeresesek' => '954',
          'egyedi' => '11',
        ),
        13 => 
        array (
          'oldalletoltes' => '2835',
          'keresesek' => '998',
          'nembejelkeresesek' => '896',
          'egyedi' => '8',
        ),
        14 => 
        array (
          'oldalletoltes' => '1705',
          'keresesek' => '619',
          'nembejelkeresesek' => '505',
          'egyedi' => '7',
        ),
        15 => 
        array (
          'oldalletoltes' => '66',
          'keresesek' => '23',
          'nembejelkeresesek' => '23',
        ),
        16 => 
        array (
          'oldalletoltes' => '87',
          'keresesek' => '17',
          'nembejelkeresesek' => '9',
          'egyedi' => '12',
        ),
        17 => 
        array (
          'oldalletoltes' => '4173',
          'keresesek' => '1517',
          'nembejelkeresesek' => '1031',
          'egyedi' => '15',
        ),
        18 => 
        array (
          'oldalletoltes' => '2562',
          'keresesek' => '899',
          'nembejelkeresesek' => '899',
        ),
        19 => 
        array (
          'oldalletoltes' => '2731',
          'keresesek' => '1102',
          'nembejelkeresesek' => '914',
          'egyedi' => '13',
        ),
        20 => 
        array (
          'oldalletoltes' => '2042',
          'keresesek' => '758',
          'nembejelkeresesek' => '744',
          'egyedi' => '2',
        ),
        21 => 
        array (
          'oldalletoltes' => '1792',
          'keresesek' => '632',
          'nembejelkeresesek' => '464',
          'egyedi' => '8',
        ),
        22 => 
        array (
          'oldalletoltes' => '138',
          'keresesek' => '49',
          'nembejelkeresesek' => '49',
        ),
        23 => 
        array (
          'oldalletoltes' => '117',
          'keresesek' => '43',
          'nembejelkeresesek' => '43',
        ),
        24 => 
        array (
          'oldalletoltes' => '2889',
          'keresesek' => '1131',
          'nembejelkeresesek' => '849',
          'egyedi' => '7',
        ),
        25 => 
        array (
          'oldalletoltes' => '2644',
          'keresesek' => '966',
          'nembejelkeresesek' => '966',
        ),
        26 => 
        array (
          'oldalletoltes' => '3318',
          'keresesek' => '1182',
          'nembejelkeresesek' => '843',
          'egyedi' => '14',
        ),
        27 => 
        array (
          'oldalletoltes' => '2370',
          'keresesek' => '912',
          'nembejelkeresesek' => '703',
          'egyedi' => '10',
        ),
        28 => 
        array (
          'oldalletoltes' => '1614',
          'keresesek' => '597',
          'nembejelkeresesek' => '573',
          'egyedi' => '4',
        ),
        29 => 
        array (
          'oldalletoltes' => '163',
          'keresesek' => '63',
          'nembejelkeresesek' => '35',
          'egyedi' => '6',
        ),
        30 => 
        array (
          'oldalletoltes' => '128',
          'keresesek' => '27',
          'nembejelkeresesek' => '27',
        ),
        5 => 
        array (
          'egyedi' => '19',
        ),
      ),
      7 => 
      array (
        1 => 
        array (
          'oldalletoltes' => '5179',
          'keresesek' => '1586',
          'nembejelkeresesek' => '1044',
          'egyedi' => '11',
        ),
        2 => 
        array (
          'oldalletoltes' => '2985',
          'keresesek' => '1036',
          'nembejelkeresesek' => '897',
          'egyedi' => '16',
        ),
        3 => 
        array (
          'oldalletoltes' => '3063',
          'keresesek' => '1208',
          'nembejelkeresesek' => '906',
          'egyedi' => '9',
        ),
        4 => 
        array (
          'oldalletoltes' => '2457',
          'keresesek' => '936',
          'nembejelkeresesek' => '760',
          'egyedi' => '10',
        ),
        5 => 
        array (
          'oldalletoltes' => '2480',
          'keresesek' => '845',
          'nembejelkeresesek' => '534',
          'egyedi' => '12',
        ),
        6 => 
        array (
          'oldalletoltes' => '234',
          'keresesek' => '77',
          'nembejelkeresesek' => '57',
          'egyedi' => '5',
        ),
        7 => 
        array (
          'oldalletoltes' => '113',
          'keresesek' => '39',
          'nembejelkeresesek' => '33',
          'egyedi' => '4',
        ),
        8 => 
        array (
          'oldalletoltes' => '2817',
          'keresesek' => '1078',
          'nembejelkeresesek' => '1028',
          'egyedi' => '8',
        ),
        9 => 
        array (
          'oldalletoltes' => '2950',
          'keresesek' => '1063',
          'nembejelkeresesek' => '726',
          'egyedi' => '6',
        ),
        10 => 
        array (
          'oldalletoltes' => '2480',
          'keresesek' => '924',
          'nembejelkeresesek' => '790',
          'egyedi' => '7',
        ),
        11 => 
        array (
          'oldalletoltes' => '2482',
          'keresesek' => '920',
          'nembejelkeresesek' => '706',
          'egyedi' => '10',
        ),
        12 => 
        array (
          'oldalletoltes' => '1340',
          'keresesek' => '596',
          'nembejelkeresesek' => '405',
          'egyedi' => '7',
        ),
        13 => 
        array (
          'oldalletoltes' => '169',
          'keresesek' => '38',
          'nembejelkeresesek' => '22',
          'egyedi' => '9',
        ),
        14 => 
        array (
          'oldalletoltes' => '76',
          'keresesek' => '27',
          'nembejelkeresesek' => '27',
        ),
        15 => 
        array (
          'oldalletoltes' => '2491',
          'keresesek' => '933',
          'nembejelkeresesek' => '811',
          'egyedi' => '9',
        ),
        16 => 
        array (
          'oldalletoltes' => '2515',
          'keresesek' => '979',
          'nembejelkeresesek' => '724',
          'egyedi' => '9',
        ),
        17 => 
        array (
          'oldalletoltes' => '2196',
          'keresesek' => '775',
          'nembejelkeresesek' => '747',
          'egyedi' => '5',
        ),
        18 => 
        array (
          'oldalletoltes' => '2100',
          'keresesek' => '754',
          'nembejelkeresesek' => '735',
          'egyedi' => '7',
        ),
        19 => 
        array (
          'oldalletoltes' => '1579',
          'keresesek' => '599',
          'nembejelkeresesek' => '418',
          'egyedi' => '8',
        ),
        20 => 
        array (
          'oldalletoltes' => '83',
          'keresesek' => '18',
          'nembejelkeresesek' => '18',
        ),
        21 => 
        array (
          'oldalletoltes' => '204',
          'keresesek' => '88',
          'nembejelkeresesek' => '88',
        ),
        22 => 
        array (
          'oldalletoltes' => '2202',
          'keresesek' => '909',
          'nembejelkeresesek' => '631',
          'egyedi' => '9',
        ),
        23 => 
        array (
          'oldalletoltes' => '2415',
          'keresesek' => '931',
          'nembejelkeresesek' => '707',
          'egyedi' => '9',
        ),
        24 => 
        array (
          'oldalletoltes' => '1960',
          'keresesek' => '611',
          'nembejelkeresesek' => '607',
          'egyedi' => '2',
        ),
        25 => 
        array (
          'oldalletoltes' => '2126',
          'keresesek' => '738',
          'nembejelkeresesek' => '647',
          'egyedi' => '7',
        ),
        26 => 
        array (
          'oldalletoltes' => '1639',
          'keresesek' => '661',
          'nembejelkeresesek' => '394',
          'egyedi' => '8',
        ),
        27 => 
        array (
          'oldalletoltes' => '122',
          'keresesek' => '48',
          'nembejelkeresesek' => '40',
          'egyedi' => '3',
        ),
        28 => 
        array (
          'oldalletoltes' => '106',
          'keresesek' => '39',
          'nembejelkeresesek' => '39',
        ),
        29 => 
        array (
          'oldalletoltes' => '2298',
          'keresesek' => '842',
          'nembejelkeresesek' => '840',
          'egyedi' => '1',
        ),
        30 => 
        array (
          'oldalletoltes' => '2655',
          'keresesek' => '1024',
          'nembejelkeresesek' => '767',
          'egyedi' => '7',
        ),
        31 => 
        array (
          'oldalletoltes' => '2818',
          'keresesek' => '1129',
          'nembejelkeresesek' => '658',
          'egyedi' => '10',
        ),
      ),
      8 => 
      array (
        1 => 
        array (
          'oldalletoltes' => '2149',
          'keresesek' => '762',
          'nembejelkeresesek' => '654',
          'egyedi' => '6',
        ),
        2 => 
        array (
          'oldalletoltes' => '1544',
          'keresesek' => '602',
          'nembejelkeresesek' => '478',
          'egyedi' => '9',
        ),
        3 => 
        array (
          'oldalletoltes' => '126',
          'keresesek' => '53',
          'nembejelkeresesek' => '41',
          'egyedi' => '2',
        ),
        4 => 
        array (
          'oldalletoltes' => '202',
          'keresesek' => '58',
          'nembejelkeresesek' => '48',
          'egyedi' => '7',
        ),
        5 => 
        array (
          'oldalletoltes' => '2331',
          'keresesek' => '796',
          'nembejelkeresesek' => '774',
          'egyedi' => '4',
        ),
        6 => 
        array (
          'oldalletoltes' => '2420',
          'keresesek' => '837',
          'nembejelkeresesek' => '833',
          'egyedi' => '3',
        ),
        7 => 
        array (
          'oldalletoltes' => '2190',
          'keresesek' => '857',
          'nembejelkeresesek' => '855',
          'egyedi' => '1',
        ),
      ),
    ),
  ),
) ?>	
	<div class="main_center_spacer"></div>
	<div class="main_center_container">


	    <div class="main_center_title">
		<div class="main_center_title_left"><font style="color:#617f10">GRAFIKUS </font> Statisztika</div>
		<div class="main_center_title_right">
		    <form action="<?php $config["site"]["absolutepath"] . "/admin/grafikus-statisztika"; ?>" method="post">
			<select name="datum">
			    <?php	
			if($_POST["datum"]){
			    $x=explode("-",$_POST["datum"]);
			}
			else { $x[0]="week"; $x[1]=date("Y"); }
			    ?>
			    <?php
			    foreach($chart["day"] as $key => $value)
				{
				print "<option value=\"week-$key\"";
				    if (($x[0]."-".$x[1])==("day-$key")){
				    print " selected=\"selected\"";
				    }
				    print ">$key év - heti statisztika</option>";
				foreach($value as $subkey => $subvalue)
				    {
				    print "<option value=\"day-$key-$subkey\"";
				    if (($x[0]."-".$x[1]."-".$x[2])==("day-$key-$subkey")){
				    print " selected=\"selected\"";
				    }
				    print ">$key-$subkey hó - napi statisztika</option>";
				    }
				} ?>			</select>
			<input class="ui-state-default ui-corner-all" type="submit" name="szukitsd" value="szűkítsd" />
		    </form>
		</div>
	    </div>
	    
	    <script src="js/amcharts/amcharts.js" type="text/javascript"></script>
	    <script src="js/amcharts/raphael.js" type="text/javascript"></script>

	    <script type="text/javascript">
		var chart;

		var chartData = [<?php
	if ($x[2]){
	  foreach ($chart[$x[0]][$x[1]][$x[2]] as $key => $value) {
		$i["day"]--;
		if (!$value["egyedi"])
		    $value["egyedi"] = 0;
		if (!$value["nembejelkeresesek"])
		    $value["nembejelkeresesek"] = 0;
		if (!$value["keresesek"])
		    $value["keresesek"] = 0;
		print ("{
			 day: $key,
			 egyedi: " . $value["egyedi"] . ",
			 kereses: " . $value["keresesek"] . ",
			 nembejelkereses: " . $value["nembejelkeresesek"] . "
			}");
		    print ",";
	    }  
	}
	else {
	foreach ($chart[$x[0]][$x[1]] as $key => $value) {
		$i["week"]--;
		if (!$value["egyedi"])
		    $value["egyedi"] = 0;
		if (!$value["nembejelkeresesek"])
		    $value["nembejelkeresesek"] = 0;
		if (!$value["keresesek"])
		    $value["keresesek"] = 0;
		print ("{
			 day: $key,
			 egyedi: " . $value["egyedi"] . ",
			 kereses: " . $value["keresesek"] . ",
			 nembejelkereses: " . $value["nembejelkeresesek"] . "
			}");
		    print ",";
	    }
	}
	?>];
		   
		    AmCharts.ready(function () {
			// SERIAL CHART
			chart = new AmCharts.AmSerialChart();
			chart.dataProvider = chartData;
			chart.categoryField = "day";
			chart.startDuration = 0.5;
			chart.balloon.color = "#000000";

			// AXES
			// category
			var categoryAxis = chart.categoryAxis;
			categoryAxis.fillAlpha = 1;
			categoryAxis.fillColor = "#FAFAFA";
			categoryAxis.gridAlpha = 0;
			categoryAxis.axisAlpha = 0;
			categoryAxis.gridPosition = "start";
			categoryAxis.position = "bottom";

			// value
			var valueAxis = new AmCharts.ValueAxis();
			valueAxis.title = "Érték";
			valueAxis.dashLength = 5;
			valueAxis.axisAlpha = 0;
			valueAxis.minimum = 0;
			valueAxis.integersOnly = true;
			valueAxis.gridCount = 20;
			chart.addValueAxis(valueAxis);

			// GRAPHS
		        					            		
			var graph = new AmCharts.AmGraph();
			graph.title = "Egyedi látogatók";
			graph.valueField = "egyedi";
			graph.balloonText = "Látogatók akik bejelentkeztek: [[value]]";
			graph.bullet = "round";
			chart.addGraph(graph);

			var graph = new AmCharts.AmGraph();
			graph.title = "Keresés nem bejelentkezve";
			graph.valueField = "nembejelkereses";
			graph.balloonText = "Keresések nem bejelentkezve: [[value]]";
			graph.bullet = "round";
			chart.addGraph(graph);
			
			var graph = new AmCharts.AmGraph();
			graph.title = "Összes Keresés";
			graph.valueField = "kereses";
			graph.balloonText = "Keresések: [[value]]";
			graph.bullet = "round";
			chart.addGraph(graph);

			// LEGEND
			var legend = new AmCharts.AmLegend();
			legend.markerType = "circle";
			chart.addLegend(legend);

			// WRITE
			chart.write("chartdiv");
		    });
	    </script>
	    <div id="chartdiv" style="width: 100%; height: 600px;"></div>
	    <div class="main_center_content_spacer"></div>

	</div>
		