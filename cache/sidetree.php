
	<script type="text/javascript">
                    $(function(){
                        $("ul#szervezetlista").simpletreeview({
                            open: '<span style="color: #900;">&darr;</span>',
                            close: '<span style="color: #090;">&rarr;</span>',
                            slide: true,
                            speed: 'fast',
                            collapsed: false
                        });

                    });
                </script>  
 <?php 
	if ($_SESSION["user"]["id"] && $q[0] == "szervezetek" && !$q[2]) {
	    ?>	    <div class="main_left_container">
	        <div class="main_left_title"><font style="color:#617f10">SZERVEZET</font> Gyorsmenü</div>
	        <ul id="szervezetlista" class="treeview_root" style="font-size: 9px;">
	    	<li class="treeview_root"><a href="http://freya.develop:8080/szervezetek/">Magyarország Rendőrsége</a>
			<ul class="treeview">
<li class="treeview">
<a href="http://freya.develop:8080/szervezetek/1">Országos RFK</a>
<ul class="treeview">
<li class="treeview">&rarr; <a href="http://freya.develop:8080/szervezetek/388">Rendészeti Főigazgatóság</a></li>
<li class="treeview">&rarr; <a href="http://freya.develop:8080/szervezetek/389">Bűnügyi Főigazgatóság</a></li>
<li class="treeview">&rarr; <a href="http://freya.develop:8080/szervezetek/4">Gazdasági Főigazgatóság</a></li>
<li class="treeview">&rarr; <a href="http://freya.develop:8080/szervezetek/416">Hivatal</a></li>
<li class="treeview">&rarr; <a href="http://freya.develop:8080/szervezetek/417">Ellenőrzési Szolgálat</a></li>
<li class="treeview">&rarr; <a href="http://freya.develop:8080/szervezetek/418">Titkársági Főosztály</a></li>
<li class="treeview">&rarr; <a href="http://freya.develop:8080/szervezetek/428">Költségvetési Ellenőrzési Iroda</a></li>
<li class="treeview">&rarr; <a href="http://freya.develop:8080/szervezetek/447">Humánigazgatási Szolgálat</a></li>
<li class="treeview">&rarr; <a href="http://freya.develop:8080/szervezetek/652">Általános Helyettesi Szerv</a></li>
<li class="treeview">&rarr; <a href="http://freya.develop:8080/szervezetek/685">RFK Jogállású Szervek</a></li>
<li class="treeview">
<a href="http://freya.develop:8080/szervezetek/686">Területi Szervek</a>
<ul class="treeview">
<li class="treeview">&rarr; <a href="http://freya.develop:8080/szervezetek/11">Baranya MRFK</a></li>
<li class="treeview">&rarr; <a href="http://freya.develop:8080/szervezetek/12">Somogy MRFK</a></li>
<li class="treeview">&rarr; <a href="http://freya.develop:8080/szervezetek/13">Tolna MRFK</a></li>
<li class="treeview">&rarr; <a href="http://freya.develop:8080/szervezetek/144">Jász-Nagykun-Szolnok MRFK</a></li>
<li class="treeview">&rarr; <a href="http://freya.develop:8080/szervezetek/145">Borsod-Abaúj-Zemplén MRFK</a></li>
<li class="treeview">&rarr; <a href="http://freya.develop:8080/szervezetek/146">Békés MRFK</a></li>
<li class="treeview">&rarr; <a href="http://freya.develop:8080/szervezetek/147">Bács-Kiskun MRFK</a></li>
<li class="treeview">&rarr; <a href="http://freya.develop:8080/szervezetek/148">Csongrád MRFK</a></li>
<li class="treeview">&rarr; <a href="http://freya.develop:8080/szervezetek/149">Fejér MRFK</a></li>
<li class="treeview">&rarr; <a href="http://freya.develop:8080/szervezetek/150">Győr-Moson-Sopron MRFK</a></li>
<li class="treeview">&rarr; <a href="http://freya.develop:8080/szervezetek/151">Hajdú-Bihar MRFK</a></li>
<li class="treeview">&rarr; <a href="http://freya.develop:8080/szervezetek/152">Heves MRFK</a></li>
<li class="treeview">&rarr; <a href="http://freya.develop:8080/szervezetek/153">Komárom-Esztegom MRFK</a></li>
<li class="treeview">&rarr; <a href="http://freya.develop:8080/szervezetek/154">Nógrád MRFK</a></li>
<li class="treeview">&rarr; <a href="http://freya.develop:8080/szervezetek/155">Pest MRFK</a></li>
<li class="treeview">&rarr; <a href="http://freya.develop:8080/szervezetek/156">Szabolcs-Szatmár-Bereg MRFK</a></li>
<li class="treeview">&rarr; <a href="http://freya.develop:8080/szervezetek/157">Vas MRFK</a></li>
<li class="treeview">&rarr; <a href="http://freya.develop:8080/szervezetek/158">Zala MRFK</a></li>
<li class="treeview">&rarr; <a href="http://freya.develop:8080/szervezetek/159">Veszprém MRFK</a></li>
<li class="treeview">&rarr; <a href="http://freya.develop:8080/szervezetek/177">Budapesti RFK</a></li>
</ul>
</li>
</ul>
</li>
<li class="treeview">&rarr; <a href="http://freya.develop:8080/szervezetek/500">Terrorelhárító Központ</a></li>
<li class="treeview">&rarr; <a href="http://freya.develop:8080/szervezetek/624">Nemzeti Védelmi Szolgálat</a></li>
</ul>
	    	</li>
	        </ul>
	    </div>
	    <div class="main_left_spacer"></div>
	     <?php 
	}
		    ?> <?php 
	if ($_SESSION["user"]["id"] && $q[0] == "sajat_vilag" && $q[2] == "szervezet-szerkesztese") {
	     ?>            
	    <div class="main_left_container">
	        <div class="main_left_title"><font style="color:#617f10">SZERVEZET</font> Gyorsmenü</div>
	        <ul id="szervezetlista" class="treeview_root" style="font-size: 9px;">
	    	<li class="treeview_root"><a href="http://freya.develop:8080/szervezetek/">Magyarország Rendőrsége</a>
			<ul class="treeview">
<li class="treeview">
<a href="http://freya.develop:8080/sajat-vilag/adatlapom/szervezet-szerkesztese/1">Országos RFK</a>
<ul class="treeview">
<li class="treeview">&rarr; <a href="http://freya.develop:8080/sajat-vilag/adatlapom/szervezet-szerkesztese/388">Rendészeti Főigazgatóság</a></li>
<li class="treeview">&rarr; <a href="http://freya.develop:8080/sajat-vilag/adatlapom/szervezet-szerkesztese/389">Bűnügyi Főigazgatóság</a></li>
<li class="treeview">&rarr; <a href="http://freya.develop:8080/sajat-vilag/adatlapom/szervezet-szerkesztese/4">Gazdasági Főigazgatóság</a></li>
<li class="treeview">&rarr; <a href="http://freya.develop:8080/sajat-vilag/adatlapom/szervezet-szerkesztese/416">Hivatal</a></li>
<li class="treeview">&rarr; <a href="http://freya.develop:8080/sajat-vilag/adatlapom/szervezet-szerkesztese/417">Ellenőrzési Szolgálat</a></li>
<li class="treeview">&rarr; <a href="http://freya.develop:8080/sajat-vilag/adatlapom/szervezet-szerkesztese/418">Titkársági Főosztály</a></li>
<li class="treeview">&rarr; <a href="http://freya.develop:8080/sajat-vilag/adatlapom/szervezet-szerkesztese/428">Költségvetési Ellenőrzési Iroda</a></li>
<li class="treeview">&rarr; <a href="http://freya.develop:8080/sajat-vilag/adatlapom/szervezet-szerkesztese/447">Humánigazgatási Szolgálat</a></li>
<li class="treeview">&rarr; <a href="http://freya.develop:8080/sajat-vilag/adatlapom/szervezet-szerkesztese/652">Általános Helyettesi Szerv</a></li>
<li class="treeview">&rarr; <a href="http://freya.develop:8080/sajat-vilag/adatlapom/szervezet-szerkesztese/685">RFK Jogállású Szervek</a></li>
<li class="treeview">
<a href="http://freya.develop:8080/sajat-vilag/adatlapom/szervezet-szerkesztese/686">Területi Szervek</a>
<ul class="treeview">
<li class="treeview">&rarr; <a href="http://freya.develop:8080/sajat-vilag/adatlapom/szervezet-szerkesztese/11">Baranya MRFK</a></li>
<li class="treeview">&rarr; <a href="http://freya.develop:8080/sajat-vilag/adatlapom/szervezet-szerkesztese/12">Somogy MRFK</a></li>
<li class="treeview">&rarr; <a href="http://freya.develop:8080/sajat-vilag/adatlapom/szervezet-szerkesztese/13">Tolna MRFK</a></li>
<li class="treeview">&rarr; <a href="http://freya.develop:8080/sajat-vilag/adatlapom/szervezet-szerkesztese/144">Jász-Nagykun-Szolnok MRFK</a></li>
<li class="treeview">&rarr; <a href="http://freya.develop:8080/sajat-vilag/adatlapom/szervezet-szerkesztese/145">Borsod-Abaúj-Zemplén MRFK</a></li>
<li class="treeview">&rarr; <a href="http://freya.develop:8080/sajat-vilag/adatlapom/szervezet-szerkesztese/146">Békés MRFK</a></li>
<li class="treeview">&rarr; <a href="http://freya.develop:8080/sajat-vilag/adatlapom/szervezet-szerkesztese/147">Bács-Kiskun MRFK</a></li>
<li class="treeview">&rarr; <a href="http://freya.develop:8080/sajat-vilag/adatlapom/szervezet-szerkesztese/148">Csongrád MRFK</a></li>
<li class="treeview">&rarr; <a href="http://freya.develop:8080/sajat-vilag/adatlapom/szervezet-szerkesztese/149">Fejér MRFK</a></li>
<li class="treeview">&rarr; <a href="http://freya.develop:8080/sajat-vilag/adatlapom/szervezet-szerkesztese/150">Győr-Moson-Sopron MRFK</a></li>
<li class="treeview">&rarr; <a href="http://freya.develop:8080/sajat-vilag/adatlapom/szervezet-szerkesztese/151">Hajdú-Bihar MRFK</a></li>
<li class="treeview">&rarr; <a href="http://freya.develop:8080/sajat-vilag/adatlapom/szervezet-szerkesztese/152">Heves MRFK</a></li>
<li class="treeview">&rarr; <a href="http://freya.develop:8080/sajat-vilag/adatlapom/szervezet-szerkesztese/153">Komárom-Esztegom MRFK</a></li>
<li class="treeview">&rarr; <a href="http://freya.develop:8080/sajat-vilag/adatlapom/szervezet-szerkesztese/154">Nógrád MRFK</a></li>
<li class="treeview">&rarr; <a href="http://freya.develop:8080/sajat-vilag/adatlapom/szervezet-szerkesztese/155">Pest MRFK</a></li>
<li class="treeview">&rarr; <a href="http://freya.develop:8080/sajat-vilag/adatlapom/szervezet-szerkesztese/156">Szabolcs-Szatmár-Bereg MRFK</a></li>
<li class="treeview">&rarr; <a href="http://freya.develop:8080/sajat-vilag/adatlapom/szervezet-szerkesztese/157">Vas MRFK</a></li>
<li class="treeview">&rarr; <a href="http://freya.develop:8080/sajat-vilag/adatlapom/szervezet-szerkesztese/158">Zala MRFK</a></li>
<li class="treeview">&rarr; <a href="http://freya.develop:8080/sajat-vilag/adatlapom/szervezet-szerkesztese/159">Veszprém MRFK</a></li>
<li class="treeview">&rarr; <a href="http://freya.develop:8080/sajat-vilag/adatlapom/szervezet-szerkesztese/177">Budapesti RFK</a></li>
</ul>
</li>
</ul>
</li>
<li class="treeview">&rarr; <a href="http://freya.develop:8080/sajat-vilag/adatlapom/szervezet-szerkesztese/500">Terrorelhárító Központ</a></li>
<li class="treeview">&rarr; <a href="http://freya.develop:8080/sajat-vilag/adatlapom/szervezet-szerkesztese/624">Nemzeti Védelmi Szolgálat</a></li>
</ul>
	    	</li>
	        </ul>
	    </div>
	    <div class="main_left_spacer"></div>

  <?php 
	}
		    ?> <?php
	if ($_SESSION["user"]["id"] && $q[0] == "felhasznalok" && $q[3] == "szervezeti-besorolas-szerkesztese") {
	     ?>	    <div class="main_left_container">
	        <div class="main_left_title"><font style="color:#617f10">SZERVEZET</font> Gyorsmenü</div>
	        <ul id="szervezetlista" class="treeview_root" style="font-size: 9px;">
	    	<li class="treeview_root"><a href="http://freya.develop:8080/szervezetek/">Magyarország Rendőrsége</a>
			<ul class="treeview">
<li class="treeview">
<a href="http://freya.develop:8080/felhasznalok/<?php print "$q[1]"; ?>/adatlap/szervezeti-besorolas-szerkesztese/1">Országos RFK</a>
<ul class="treeview">
<li class="treeview">&rarr; <a href="http://freya.develop:8080/felhasznalok/<?php print "$q[1]"; ?>/adatlap/szervezeti-besorolas-szerkesztese/388">Rendészeti Főigazgatóság</a></li>
<li class="treeview">&rarr; <a href="http://freya.develop:8080/felhasznalok/<?php print "$q[1]"; ?>/adatlap/szervezeti-besorolas-szerkesztese/389">Bűnügyi Főigazgatóság</a></li>
<li class="treeview">&rarr; <a href="http://freya.develop:8080/felhasznalok/<?php print "$q[1]"; ?>/adatlap/szervezeti-besorolas-szerkesztese/4">Gazdasági Főigazgatóság</a></li>
<li class="treeview">&rarr; <a href="http://freya.develop:8080/felhasznalok/<?php print "$q[1]"; ?>/adatlap/szervezeti-besorolas-szerkesztese/416">Hivatal</a></li>
<li class="treeview">&rarr; <a href="http://freya.develop:8080/felhasznalok/<?php print "$q[1]"; ?>/adatlap/szervezeti-besorolas-szerkesztese/417">Ellenőrzési Szolgálat</a></li>
<li class="treeview">&rarr; <a href="http://freya.develop:8080/felhasznalok/<?php print "$q[1]"; ?>/adatlap/szervezeti-besorolas-szerkesztese/418">Titkársági Főosztály</a></li>
<li class="treeview">&rarr; <a href="http://freya.develop:8080/felhasznalok/<?php print "$q[1]"; ?>/adatlap/szervezeti-besorolas-szerkesztese/428">Költségvetési Ellenőrzési Iroda</a></li>
<li class="treeview">&rarr; <a href="http://freya.develop:8080/felhasznalok/<?php print "$q[1]"; ?>/adatlap/szervezeti-besorolas-szerkesztese/447">Humánigazgatási Szolgálat</a></li>
<li class="treeview">&rarr; <a href="http://freya.develop:8080/felhasznalok/<?php print "$q[1]"; ?>/adatlap/szervezeti-besorolas-szerkesztese/652">Általános Helyettesi Szerv</a></li>
<li class="treeview">&rarr; <a href="http://freya.develop:8080/felhasznalok/<?php print "$q[1]"; ?>/adatlap/szervezeti-besorolas-szerkesztese/685">RFK Jogállású Szervek</a></li>
<li class="treeview">
<a href="http://freya.develop:8080/felhasznalok/<?php print "$q[1]"; ?>/adatlap/szervezeti-besorolas-szerkesztese/686">Területi Szervek</a>
<ul class="treeview">
<li class="treeview">&rarr; <a href="http://freya.develop:8080/felhasznalok/<?php print "$q[1]"; ?>/adatlap/szervezeti-besorolas-szerkesztese/11">Baranya MRFK</a></li>
<li class="treeview">&rarr; <a href="http://freya.develop:8080/felhasznalok/<?php print "$q[1]"; ?>/adatlap/szervezeti-besorolas-szerkesztese/12">Somogy MRFK</a></li>
<li class="treeview">&rarr; <a href="http://freya.develop:8080/felhasznalok/<?php print "$q[1]"; ?>/adatlap/szervezeti-besorolas-szerkesztese/13">Tolna MRFK</a></li>
<li class="treeview">&rarr; <a href="http://freya.develop:8080/felhasznalok/<?php print "$q[1]"; ?>/adatlap/szervezeti-besorolas-szerkesztese/144">Jász-Nagykun-Szolnok MRFK</a></li>
<li class="treeview">&rarr; <a href="http://freya.develop:8080/felhasznalok/<?php print "$q[1]"; ?>/adatlap/szervezeti-besorolas-szerkesztese/145">Borsod-Abaúj-Zemplén MRFK</a></li>
<li class="treeview">&rarr; <a href="http://freya.develop:8080/felhasznalok/<?php print "$q[1]"; ?>/adatlap/szervezeti-besorolas-szerkesztese/146">Békés MRFK</a></li>
<li class="treeview">&rarr; <a href="http://freya.develop:8080/felhasznalok/<?php print "$q[1]"; ?>/adatlap/szervezeti-besorolas-szerkesztese/147">Bács-Kiskun MRFK</a></li>
<li class="treeview">&rarr; <a href="http://freya.develop:8080/felhasznalok/<?php print "$q[1]"; ?>/adatlap/szervezeti-besorolas-szerkesztese/148">Csongrád MRFK</a></li>
<li class="treeview">&rarr; <a href="http://freya.develop:8080/felhasznalok/<?php print "$q[1]"; ?>/adatlap/szervezeti-besorolas-szerkesztese/149">Fejér MRFK</a></li>
<li class="treeview">&rarr; <a href="http://freya.develop:8080/felhasznalok/<?php print "$q[1]"; ?>/adatlap/szervezeti-besorolas-szerkesztese/150">Győr-Moson-Sopron MRFK</a></li>
<li class="treeview">&rarr; <a href="http://freya.develop:8080/felhasznalok/<?php print "$q[1]"; ?>/adatlap/szervezeti-besorolas-szerkesztese/151">Hajdú-Bihar MRFK</a></li>
<li class="treeview">&rarr; <a href="http://freya.develop:8080/felhasznalok/<?php print "$q[1]"; ?>/adatlap/szervezeti-besorolas-szerkesztese/152">Heves MRFK</a></li>
<li class="treeview">&rarr; <a href="http://freya.develop:8080/felhasznalok/<?php print "$q[1]"; ?>/adatlap/szervezeti-besorolas-szerkesztese/153">Komárom-Esztegom MRFK</a></li>
<li class="treeview">&rarr; <a href="http://freya.develop:8080/felhasznalok/<?php print "$q[1]"; ?>/adatlap/szervezeti-besorolas-szerkesztese/154">Nógrád MRFK</a></li>
<li class="treeview">&rarr; <a href="http://freya.develop:8080/felhasznalok/<?php print "$q[1]"; ?>/adatlap/szervezeti-besorolas-szerkesztese/155">Pest MRFK</a></li>
<li class="treeview">&rarr; <a href="http://freya.develop:8080/felhasznalok/<?php print "$q[1]"; ?>/adatlap/szervezeti-besorolas-szerkesztese/156">Szabolcs-Szatmár-Bereg MRFK</a></li>
<li class="treeview">&rarr; <a href="http://freya.develop:8080/felhasznalok/<?php print "$q[1]"; ?>/adatlap/szervezeti-besorolas-szerkesztese/157">Vas MRFK</a></li>
<li class="treeview">&rarr; <a href="http://freya.develop:8080/felhasznalok/<?php print "$q[1]"; ?>/adatlap/szervezeti-besorolas-szerkesztese/158">Zala MRFK</a></li>
<li class="treeview">&rarr; <a href="http://freya.develop:8080/felhasznalok/<?php print "$q[1]"; ?>/adatlap/szervezeti-besorolas-szerkesztese/159">Veszprém MRFK</a></li>
<li class="treeview">&rarr; <a href="http://freya.develop:8080/felhasznalok/<?php print "$q[1]"; ?>/adatlap/szervezeti-besorolas-szerkesztese/177">Budapesti RFK</a></li>
</ul>
</li>
</ul>
</li>
<li class="treeview">&rarr; <a href="http://freya.develop:8080/felhasznalok/<?php print "$q[1]"; ?>/adatlap/szervezeti-besorolas-szerkesztese/500">Terrorelhárító Központ</a></li>
<li class="treeview">&rarr; <a href="http://freya.develop:8080/felhasznalok/<?php print "$q[1]"; ?>/adatlap/szervezeti-besorolas-szerkesztese/624">Nemzeti Védelmi Szolgálat</a></li>
</ul>
	    	</li>
	        </ul>
	    </div>
	    <div class="main_left_spacer"></div>
  <?php 
	}
		    ?> <?php
 	if ($_SESSION["user"]["id"] && $q[0] == "szervezetek" && $q[2] == "szervezeti-besorolas-szerkesztese") {
	     ?>	    <div class="main_left_container">
	        <div class="main_left_title"><font style="color:#617f10">SZERVEZET</font> Gyorsmenü</div>
	        <ul id="szervezetlista" class="treeview_root" style="font-size: 9px;">
	    	<li class="treeview_root"><a href="http://freya.develop:8080/szervezetek/">Magyarország Rendőrsége</a>
			<ul class="treeview">
<li class="treeview">
<a href="http://freya.develop:8080/szervezetek/<?php print "$q[1]"; ?>/szervezeti-besorolas-szerkesztese/1">Országos RFK</a>
<ul class="treeview">
<li class="treeview">&rarr; <a href="http://freya.develop:8080/szervezetek/<?php print "$q[1]"; ?>/szervezeti-besorolas-szerkesztese/388">Rendészeti Főigazgatóság</a></li>
<li class="treeview">&rarr; <a href="http://freya.develop:8080/szervezetek/<?php print "$q[1]"; ?>/szervezeti-besorolas-szerkesztese/389">Bűnügyi Főigazgatóság</a></li>
<li class="treeview">&rarr; <a href="http://freya.develop:8080/szervezetek/<?php print "$q[1]"; ?>/szervezeti-besorolas-szerkesztese/4">Gazdasági Főigazgatóság</a></li>
<li class="treeview">&rarr; <a href="http://freya.develop:8080/szervezetek/<?php print "$q[1]"; ?>/szervezeti-besorolas-szerkesztese/416">Hivatal</a></li>
<li class="treeview">&rarr; <a href="http://freya.develop:8080/szervezetek/<?php print "$q[1]"; ?>/szervezeti-besorolas-szerkesztese/417">Ellenőrzési Szolgálat</a></li>
<li class="treeview">&rarr; <a href="http://freya.develop:8080/szervezetek/<?php print "$q[1]"; ?>/szervezeti-besorolas-szerkesztese/418">Titkársági Főosztály</a></li>
<li class="treeview">&rarr; <a href="http://freya.develop:8080/szervezetek/<?php print "$q[1]"; ?>/szervezeti-besorolas-szerkesztese/428">Költségvetési Ellenőrzési Iroda</a></li>
<li class="treeview">&rarr; <a href="http://freya.develop:8080/szervezetek/<?php print "$q[1]"; ?>/szervezeti-besorolas-szerkesztese/447">Humánigazgatási Szolgálat</a></li>
<li class="treeview">&rarr; <a href="http://freya.develop:8080/szervezetek/<?php print "$q[1]"; ?>/szervezeti-besorolas-szerkesztese/652">Általános Helyettesi Szerv</a></li>
<li class="treeview">&rarr; <a href="http://freya.develop:8080/szervezetek/<?php print "$q[1]"; ?>/szervezeti-besorolas-szerkesztese/685">RFK Jogállású Szervek</a></li>
<li class="treeview">
<a href="http://freya.develop:8080/szervezetek/<?php print "$q[1]"; ?>/szervezeti-besorolas-szerkesztese/686">Területi Szervek</a>
<ul class="treeview">
<li class="treeview">&rarr; <a href="http://freya.develop:8080/szervezetek/<?php print "$q[1]"; ?>/szervezeti-besorolas-szerkesztese/11">Baranya MRFK</a></li>
<li class="treeview">&rarr; <a href="http://freya.develop:8080/szervezetek/<?php print "$q[1]"; ?>/szervezeti-besorolas-szerkesztese/12">Somogy MRFK</a></li>
<li class="treeview">&rarr; <a href="http://freya.develop:8080/szervezetek/<?php print "$q[1]"; ?>/szervezeti-besorolas-szerkesztese/13">Tolna MRFK</a></li>
<li class="treeview">&rarr; <a href="http://freya.develop:8080/szervezetek/<?php print "$q[1]"; ?>/szervezeti-besorolas-szerkesztese/144">Jász-Nagykun-Szolnok MRFK</a></li>
<li class="treeview">&rarr; <a href="http://freya.develop:8080/szervezetek/<?php print "$q[1]"; ?>/szervezeti-besorolas-szerkesztese/145">Borsod-Abaúj-Zemplén MRFK</a></li>
<li class="treeview">&rarr; <a href="http://freya.develop:8080/szervezetek/<?php print "$q[1]"; ?>/szervezeti-besorolas-szerkesztese/146">Békés MRFK</a></li>
<li class="treeview">&rarr; <a href="http://freya.develop:8080/szervezetek/<?php print "$q[1]"; ?>/szervezeti-besorolas-szerkesztese/147">Bács-Kiskun MRFK</a></li>
<li class="treeview">&rarr; <a href="http://freya.develop:8080/szervezetek/<?php print "$q[1]"; ?>/szervezeti-besorolas-szerkesztese/148">Csongrád MRFK</a></li>
<li class="treeview">&rarr; <a href="http://freya.develop:8080/szervezetek/<?php print "$q[1]"; ?>/szervezeti-besorolas-szerkesztese/149">Fejér MRFK</a></li>
<li class="treeview">&rarr; <a href="http://freya.develop:8080/szervezetek/<?php print "$q[1]"; ?>/szervezeti-besorolas-szerkesztese/150">Győr-Moson-Sopron MRFK</a></li>
<li class="treeview">&rarr; <a href="http://freya.develop:8080/szervezetek/<?php print "$q[1]"; ?>/szervezeti-besorolas-szerkesztese/151">Hajdú-Bihar MRFK</a></li>
<li class="treeview">&rarr; <a href="http://freya.develop:8080/szervezetek/<?php print "$q[1]"; ?>/szervezeti-besorolas-szerkesztese/152">Heves MRFK</a></li>
<li class="treeview">&rarr; <a href="http://freya.develop:8080/szervezetek/<?php print "$q[1]"; ?>/szervezeti-besorolas-szerkesztese/153">Komárom-Esztegom MRFK</a></li>
<li class="treeview">&rarr; <a href="http://freya.develop:8080/szervezetek/<?php print "$q[1]"; ?>/szervezeti-besorolas-szerkesztese/154">Nógrád MRFK</a></li>
<li class="treeview">&rarr; <a href="http://freya.develop:8080/szervezetek/<?php print "$q[1]"; ?>/szervezeti-besorolas-szerkesztese/155">Pest MRFK</a></li>
<li class="treeview">&rarr; <a href="http://freya.develop:8080/szervezetek/<?php print "$q[1]"; ?>/szervezeti-besorolas-szerkesztese/156">Szabolcs-Szatmár-Bereg MRFK</a></li>
<li class="treeview">&rarr; <a href="http://freya.develop:8080/szervezetek/<?php print "$q[1]"; ?>/szervezeti-besorolas-szerkesztese/157">Vas MRFK</a></li>
<li class="treeview">&rarr; <a href="http://freya.develop:8080/szervezetek/<?php print "$q[1]"; ?>/szervezeti-besorolas-szerkesztese/158">Zala MRFK</a></li>
<li class="treeview">&rarr; <a href="http://freya.develop:8080/szervezetek/<?php print "$q[1]"; ?>/szervezeti-besorolas-szerkesztese/159">Veszprém MRFK</a></li>
<li class="treeview">&rarr; <a href="http://freya.develop:8080/szervezetek/<?php print "$q[1]"; ?>/szervezeti-besorolas-szerkesztese/177">Budapesti RFK</a></li>
</ul>
</li>
</ul>
</li>
<li class="treeview">&rarr; <a href="http://freya.develop:8080/szervezetek/<?php print "$q[1]"; ?>/szervezeti-besorolas-szerkesztese/500">Terrorelhárító Központ</a></li>
<li class="treeview">&rarr; <a href="http://freya.develop:8080/szervezetek/<?php print "$q[1]"; ?>/szervezeti-besorolas-szerkesztese/624">Nemzeti Védelmi Szolgálat</a></li>
</ul>
	    	</li>
	        </ul>
	    </div>
	    <div class="main_left_spacer"></div>
  <?php 
	}
		    ?>