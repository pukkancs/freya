<?php

class szervezetek {

    function __construct() {
        global $q;
        include_once("content/content_szervezetek.php");
        $this->content = new szervezetek_content();

        switch ($q[2]) {
            case "":
            case "adatlap": $this->adatlap_kiir();
                break;
            case "uj-felhasznalo-hozzaadasa": $this->uj_felhasznalo_hozzaad();
                break;
	    case "szervezet-altalanos-adatainak-szerkesztese": $this->altalanos_adatok_szerkesztese();
                break;
	    case "szervezet-elerhetosegek-szerkesztese": $this->szervezet_elerhetosegek_szerkesztese();
                break;
	    case "szervezeti-besorolas-szerkesztese": $this->szervezet_modosit();
                break;
	    case "szervezet-torlese": $this->szervezet_torlese();
                break;
	    case "uj-alarendelt-szerv-hozzaadasa": $this->szervezet_hozzaadasa();
                break;
            default: $this->adatlap_kiir();
        }
    }

    /*     * ****************************************************** */

    function adatlap_kiir() {
        $this->content->adatlap_kiir();
    }

    /*     * ****************************************************** */

    function uj_felhasznalo_hozzaad() {
    	global $lang, $odin, $sql_query_count, $config, $siteloadlog, $q;
	$error = false;
	//VISSZALÉPÉS
	if ($_POST["hozzaad"] == $lang["gomb"]["megse"] || $q[1]=="1") {
	   header("location:" . $config["site"]["absolutepath"] . "/szervezetek/".$q[1]."");
	   exit();	   
	}
	//ELLENŐRZÉS
	    if (!$odin->admin_this("user","new",$q[1])){ $error["kiir"].="Nincs jogod ezt a felhasználót szerkeszteni!<br />"; exit(); }
	    $sql = "SELECT email FROM felhasznalok WHERE 1";
	    $check = mysql_query($sql);
	    $sql_query_count++;
	    while ($sor = mysql_fetch_assoc($check)) {
		if (!strcasecmp($sor["email"], $_POST["email"])) {
		    $error["kiir"].="Már létezik ilyen GroupWise cím!<br />";
		}
	    }
	if ($_POST["hozzaad"] == $lang["gomb"]["hozzaad"]) {
	    if (strlen($_POST["vezeteknev"]) < 3)
		$error["kiir"].="Hibás vezetéknév!<br />";
	    if (strlen($_POST["keresztnev"]) < 3)
		$error["kiir"].="Hibás keresztnev!<br />";
	    if (strlen($_POST["email"]) < 4 || !stristr($_POST["email"], "@") || !stristr($_POST["email"], ".")) {
		$error["kiir"].="Hibás GroupWise cím!<br />"; }
	    if (strlen($_POST["beosztas"]) < 3 && $_POST["beosztas"] && !$_POST["beosztas_id"])
		$error["kiir"].="Hibás beosztásnév!<br />";
	    if (strlen($_POST["rendfokozat"]) < 3 && $_POST["rendfokozat"] && !$_POST["rendfokozat_id"])
		$error["kiir"].="Hibás rendfokozat!<br />";
	    if (!$_POST["beosztas_id"] && !$_POST["beosztas"]) {
		$error["kiir"].="Adja meg kézileg a beosztást!<br />";
		$error["beosztas"] = true;
	    }
	    if (!$_POST["rendfokozat_id"] && !$_POST["rendfokozat"]) {
		$error["kiir"].="Adja meg kézileg a renfokozatot!<br />";
		$error["rendfokozat"] = true;
	    }
	    if ($_POST["telephely_irsz"] && (strlen($_POST["telephely_irsz"]) != 4 || !is_numeric($_POST["telephely_irsz"]))) {

		$error["kiir"].="Hibásan megadott telephely irányítószám! Helyes formátum: 7600<br />";
	    }
	    if ($_POST["levelezesi_irsz"] && (strlen($_POST["levelezesi_irsz"]) != 4 || !is_numeric($_POST["levelezesi_irsz"]))) {

		$error["kiir"].="Hibásan megadott levelezési irányítószám! Helyes formátum: 7600<br />";
	    }
	    if ($_POST["vezetoi"] && (strlen($_POST["vezetoi"]) != 6 || !is_numeric($_POST["vezetoi"]))) {

		$error["kiir"].="Hibásan megadott vezetői szám! Helyes formátum: 231111<br />";
	    }
	    
	    // strlen($_POST["kozvetlen"]) != 6 || 
	    if ($_POST["kozvetlen"] && (!is_numeric($_POST["kozvetlen"]))) {

		$error["kiir"].="Hibásan megadott közvetlen szám! Helyes formátum: 231111<br />";
	    }
	    if ($_POST["2helyi"] && (strlen($_POST["2helyi"]) != 6 || !is_numeric($_POST["2helyi"]))) {

		$error["kiir"].="Hibásan megadott 2. helyi szám! Helyes formátum: 231111<br />";
	    }
	    if ($_POST["fax"] && (strlen($_POST["fax"]) != 6 || !is_numeric($_POST["fax"]))) {

		$error["kiir"].="Hibásan megadott fax szám! Helyes formátum: 231111<br />";
	    }
	    if ($_POST["tetra"] && (strlen($_POST["tetra"]) != 7 || !is_numeric($_POST["tetra"]))) {

		$error["kiir"].="Hibásan megadott tetra szám! Helyes formátum: 3230000<br />";
	    }
	    if ($_POST["mobil"] && (strlen($_POST["mobil"]) != 9 || !is_numeric($_POST["mobil"]))) {

		$error["kiir"].="Hibásan megadott mobil telefonszám! Helyes formátum: 209888777<br />";
	    }
	    if ($_POST["vez_kozvetlen"] && (strlen($_POST["vez_kozvetlen"]) < 8 || !is_numeric($_POST["vez_kozvetlen"]))) {

		$error["kiir"].="Hibásan megadott vezetékes közvetlen telefonszám! Helyes formátum: 72999888<br />";
	    }
	    if ($_POST["vez_fax"] && (strlen($_POST["vez_fax"]) < 8 || !is_numeric($_POST["vez_fax"]))) {

		$error["kiir"].="Hibásan megadott vezetékes fax telefonszám! Helyes formátum: 72999888<br />";
	    }
	}
	if ($error["kiir"]) {
	    $this->content->uj_felhasznalo_hozzaad($error);
	} elseif ($_POST["hozzaad"] == $lang["gomb"]["hozzaad"]) {
	    $time = date('Y-m-d H:i:s', time());
	    if ($_POST["beosztas"]) {
		$sql = "SELECT * FROM beosztasok";
		$check = mysql_query($sql);
		$sql_query_count++;
		while ($sor = mysql_fetch_assoc($check)) {
		    if (!strcasecmp($sor["beosztasnev"], $_POST["beosztas"])) {
			unset($_POST["beosztas"]);
			$_POST["beosztas_id"] = $sor["id"];
		    }
		}
	    }
	    if ($_POST["rendfokozat"]) {
		$sql = "SELECT * FROM rendfokozatok";
		$check = mysql_query($sql);
		$sql_query_count++;
		while ($sor = mysql_fetch_assoc($check)) {
		    if (!strcasecmp($sor["megnevezes"], $_POST["rendfokozat"])) {
			unset($_POST["rendfokozat"]);
			$_POST["rendfokozat_id"] = $sor["id"];
		    }
		}
	    }
	    if ($_POST["beosztas"]) {
		$sql = "INSERT INTO beosztasok (beosztasnev) VALUES ('" . $_POST["beosztas"] . "')";
		mysql_query($sql) or die("MySQL hibaüzenet:" . mysql_error());
		$siteloadlog["event"] = 8;
		$siteloadlog["what"] = $_POST["beosztas"];
		$odin->addsiteload();
		$sql = "SELECT id FROM beosztasok WHERE beosztasnev='" . $_POST["beosztas"] . "'";
		$check = mysql_query($sql);
		$sql_query_count++;
		while ($sor = mysql_fetch_assoc($check)) {
		    $_POST["beosztas_id"] = $sor["id"];
		}
	    }
	    if ($_POST["rendfokozat"]) {
		$sql = "INSERT INTO rendfokozatok (megnevezes) VALUES ('" . $_POST["rendfokozat"] . "')";
		mysql_query($sql) or die("MySQL hibaüzenet:" . mysql_error());
		$siteloadlog["event"] = 11;
		$siteloadlog["what"] = $_POST["rendfokozat"];
		$odin->addsiteload();
		$sql = "SELECT id FROM rendfokozatok WHERE megnevezes='" . $_POST["rendfokozat"] . "'";
		$check = mysql_query($sql);
		$sql_query_count++;
		while ($sor = mysql_fetch_assoc($check)) {
		    $_POST["rendfokozat_id"] = $sor["id"];
		}
	    }
	    $_POST=$odin->values_to_null($_POST);
	    $azonosito=$odin->generate_azonosito();
	    $startat = rand(0, 24);
	    $newpasskey = substr(md5(rand(0, 100000)), $startat, 8);
	    $sql = "INSERT INTO felhasznalok (`azonosito`, `passkey`, `szervezet_id`, `modositva_ekkor`, `modositva_altal`, `allomany_statusz`, `titulus`, `vezeteknev`,`kozepsonev`,`keresztnev`,`email`,`beosztas_id`,`rendfokozat_id`,`beosztas_megjegyzes`, `nicknev`,titulus_2)
	    VALUES ($azonosito,\"$newpasskey\",".$q[1].",'$time',\"" . $_SESSION["user"]["id"] . "\",\"" . $_POST["allomany_statusz"] . "\",\"" . $_POST["titulus"] . "\",\"" . $_POST["vezeteknev"] . "\",\"" . $_POST["kozepsonev"] . "\",\"" . $_POST["keresztnev"] . "\",\"" . $_POST["email"] . "\",\"" . $_POST["beosztas_id"] . "\",\"" . $_POST["rendfokozat_id"] . "\",\"" . $_POST["beosztas_megjegyzes"] . "\",\"" . $_POST["nicknev"] . "\",\"" . $_POST["titulus_2"] . "\")";
	    mysql_query($sql) or die("MySQL hibaüzenet:" . mysql_error());
	    $sql = "SELECT id FROM felhasznalok WHERE azonosito=$azonosito";
	    $result=mysql_query($sql);
	    while ($sor=mysql_fetch_assoc($result))
	    {
		$forMail=$odin->generate_new_password($sor["id"],$azonosito);
		$id=$sor["id"];
		$sql = "UPDATE felhasznalok SET `modositva_ekkor`= '$time', ";

	    $_POST["telephely"] = $_POST["telephely_irsz"] . " " . $_POST["telephely_varos"] . ", " . $_POST["telephely_cim"] . " " . $_POST["telephely_szam"];
	    if (strlen($_POST["telephely"]) == 4) {
		$_POST["telephely_varos"] = "";
	    };
	    if ($_POST["telephely"] && $details["telephely"] != $_POST["telephely"]) {
		$sql.=" `telephely_irsz`='" . $_POST["telephely_irsz"] . "', `telephely_varos`='" . $_POST["telephely_varos"] . "', `telephely_utca`='" . $_POST["telephely_cim"] . "', `telephely_cim`='" . $_POST["telephely_szam"] . "',";
	    } else {
		$sql.=" `telephely_irsz`=NULL, `telephely_varos`=NULL, `telephely_utca`=NULL, `telephely_cim`=NULL,";
	    }
	    if ($_POST["telephely_epulet"]) {
		$sql.=" `telephely_epulet`='" . $_POST["telephely_epulet"] . "',";
	    } else {
		$sql.="`telephely_epulet`=NULL,";
	    }
	    if ($_POST["telephely_emelet"]) {
		$sql.=" `telephely_emelet`='" . $_POST["telephely_emelet"] . "',";
	    } else {
		$sql.="`telephely_emelet`=NULL,";
	    }
	    if ($_POST["telephely_iroda"]) {
		$sql.=" `telephely_iroda`='" . $_POST["telephely_iroda"] . "',";
	    } else {
		$sql.="`telephely_iroda`=NULL,";
	    }
	    if ($_POST["vezetoi"]) {
		$sql.=" `vezetoi`='" . $_POST["vezetoi"] . "',";
	    } else {
		$sql.="`vezetoi`=NULL,";
	    }
	    if ($_POST["kozvetlen"]) {
		$sql.=" `kozvetlen`='" . $_POST["kozvetlen"] . "',";
	    } else {
		$sql.="`kozvetlen`=NULL,";
	    }
	    if ($_POST["2helyi"]) {
		$sql.=" `2helyi`='" . $_POST["2helyi"] . "',";
	    } else {
		$sql.="`2helyi`=NULL,";
	    }
	    if ($_POST["fax"] && $details["fax"] != $_POST["fax"]) {
		$sql.=" `fax`='" . $_POST["fax"] . "',";
	    } else {
		$sql.="`fax`=NULL,";
	    }
	    if ($_POST["mobil"]) {
		$sql.=" `mobil`='" . $_POST["mobil"] . "',";
	    } else {
		$sql.="`mobil`=NULL,";
	    }
	    if ($_POST["vez_kozvetlen"]) {
		$sql.=" `vez_kozvetlen`='" . $_POST["vez_kozvetlen"] . "',";
	    } else {
		$sql.="`vez_kozvetlen`=NULL,";
	    }
	    if ($_POST["vez_fax"] && $details["vez_fax"] != $_POST["vez_fax"]) {
		$sql.=" `vez_fax`='" . $_POST["vez_fax"] . "',";
	    } else {
		$sql.="`vez_fax`=NULL,";
	    }
	    if ($_POST["tetra"]) {
		$sql.=" `tetra`='" . $_POST["tetra"] . "',";
	    } else {
		$sql.="`tetra`=NULL,";
	    }
	    $sql.="`modositva_altal`= '" . $_SESSION["user"]["id"] . "'";
	    $sql.=" WHERE id='" . $sor["id"] . "'";
	    mysql_query($sql) or die("MySQL hibaüzenet:" . mysql_error());
	    $sql = "UPDATE felhasznalok SET `modositva_ekkor`= '$time', `modositva_altal`= \"" . $_SESSION["user"]["id"] . "\", `megjegyzes`='" . $_POST["megjegyzes"] . "' WHERE id=\"" . $sor["id"] . "\"";
	    mysql_query($sql) or die("MySQL hibaüzenet:" . mysql_error());
	    }
	    $sql = "UPDATE modositva SET `ekkor`= '$time' WHERE id=1";
	    mysql_query($sql) or die("MySQL hibaüzenet:" . mysql_error());
	    $siteloadlog["event"] = 1;
	    $siteloadlog["who"] = $id;
	    $siteloadlog["what"] = "Felhasználó hozzáadva";
	    $odin->send_mail($_POST["email"], "TELEFONKÖNYV", "Ez egy automatikus üzenet, ne válaszolj rá.\n\nEgy adminisztrátor létrehozott számodra egy felhasználói fiókot.\n\n - A telefonkönyv elérhetősége: ".$config["site"]["absolutepath"]."\n - - Ideiglenes azonosítód: " . $forMail["username"] ."\n - - Ideiglenes jelszavad:  " . $forMail["pass"] ."\n\n");
	    $odin->addsiteload();
	    header("Location:" . $config["site"]["absolutepath"] . "/szervezetek/".$q[1]."");
	    exit();
	}
	else
	    $this->content->uj_felhasznalo_hozzaad();
    }
    /*     * ****************************************************** */
        function altalanos_adatok_szerkesztese() {
	global $lang, $odin, $loki, $sql_query_count, $config, $siteloadlog, $q;
	$error = false;
	$time = date('Y-m-d H:i:s', time());
	//VISSZALÉPÉS
	if ($_POST["modosit"] == $lang["gomb"]["megse"]) {
	    header("location:" . $config["site"]["absolutepath"] . "/szervezetek/".$q[1]."");
	}
	//ELLENŐRZÉS
	if ($_POST["modosit"] == $lang["gomb"]["modosit"]) {
	    if ($_POST["ugyelet"] && (strlen($_POST["ugyelet"]) != 6 || !is_numeric($_POST["ugyelet"]))) {

		$error["kiir"].="Hibásan megadott ügyeleti szám! Helyes formátum: 231111<br />";
	    } 
	    if ($_POST["hirkozpont"] && (strlen($_POST["hirkozpont"]) != 6 || !is_numeric($_POST["hirkozpont"]))) {

		$error["kiir"].="Hibásan megadott hírközpont szám! Helyes formátum: 231111<br />";
	    } 
	}
	if (!$odin->admin_this("szervezet","edit",$q[1])){ $error["kiir"].="Nincs jogod ezt a szervezetet szerkeszteni!<br />"; exit(); }
	if ($error) {
	    $this->content->altalanos_adatok_szerkesztese($error);
	} elseif ($_POST["modosit"] == $lang["gomb"]["modosit"]) {
	    $details=$odin->get_szervezet_details($q[1]);
	    $sql = "UPDATE szervezetek SET ";
	    $_POST["telephely"] = $_POST["telephely_irsz"] . " " . $_POST["telephely_varos"] . ", " . $_POST["telephely_cim"] . " " . $_POST["telephely_szam"];
	    if ($_POST["ugyelet"] && ($details["ugyelet"]!=$_POST["ugyelet"] || $_POST["ugyelet"]==$details["eredeti"]["ugyelet"])) {
		$sql.=" `ugyelet`='" . $_POST["ugyelet"] . "',";
	    } else {
		$sql.="`ugyelet`=NULL,";
	    }
	    if ($_POST["hirkozpont"] && ($details["hirkozpont"]!=$_POST["hirkozpont"]|| $_POST["hirkozpont"]==$details["eredeti"]["hirkozpont"])) {
		$sql.=" `hirkozpont`='" . $_POST["hirkozpont"] . "'";
	    } else {
		$sql.="`hirkozpont`=NULL";
	    }
	    $sql.=" WHERE id='" . $q[1] . "'";
	    mysql_query($sql) or die("$sql.<br />MySQL hibaüzenet:" . mysql_error());
	    $sql = "UPDATE modositva SET `ekkor`= '$time' WHERE id=1";
	    mysql_query($sql) or die("MySQL hibaüzenet:" . mysql_error());
	    $siteloadlog["event"] = 6;
	    $siteloadlog["who"] = $q[1];
	    $siteloadlog["what"] = "Általános adatok módsítva";
    	    $loki->cron();
	    $odin->addsiteload();
	    header("Location:" . $config["site"]["absolutepath"] . "/szervezetek/".$q[1]."");
	    exit();
	}
	else
	    $this->content->altalanos_adatok_szerkesztese($error);
    }

    /*     * ****************************************************** */
        function szervezet_elerhetosegek_szerkesztese() {
	global $lang, $odin, $loki, $sql_query_count, $config, $siteloadlog, $q;
	$error = false;
	$time = date('Y-m-d H:i:s', time());
	//VISSZALÉPÉS
	if ($_POST["modosit"] == $lang["gomb"]["megse"]) {
	    header("location:" . $config["site"]["absolutepath"] . "/szervezetek/".$q[1]."");
	}
	//ELLENŐRZÉS
	if ($_POST["modosit"] == $lang["gomb"]["modosit"]) {
	    if ($_POST["nev"] && strlen($_POST["nev"]) < 4 ) {
		$error["kiir"].="Hibásan megadott szervezet név! A szevezet neve tartalmazzon legalább 4 karaktert!<br />";
	    }
	    if (!$_POST["szervezet_tipus"]) {
		$error["kiir"].="A szervezet típusát meg kell adni!<br />";
	    }
	    if (!$_POST["szervezet_tipus"]==1 && $_POST["oroklodes"]) {
		$error["kiir"].="Ennél a szervezet típusnál nem lehet ilyen örökődési beállítást alkalmazni!<br />";
		unset($_POST["oroklodes"]);		
	    }
	    if ($_POST["telephely_irsz"] && (strlen($_POST["telephely_irsz"]) != 4 || !is_numeric($_POST["telephely_irsz"]))) {

		$error["kiir"].="Hibásan megadott telephely irányítószám! Helyes formátum: 7600<br />";
	    }
	    if ($_POST["levelezesi_irsz"] && (strlen($_POST["levelezesi_irsz"]) != 4 || !is_numeric($_POST["levelezesi_irsz"]))) {

		$error["kiir"].="Hibásan megadott levelezési irányítószám! Helyes formátum: 7600<br />";
	    }
	    if ($_POST["email"] && (strlen($_POST["email"]) < 4 || !stristr($_POST["email"], "@") || !stristr($_POST["email"], "."))) {
		$error["kiir"].="Hibás GroupWise cím!<br />"; }
	    if ($_POST["fax"] && (strlen($_POST["fax"]) != 6 || !is_numeric($_POST["fax"]))) {
		$error["kiir"].="Hibásan megadott fax szám! Helyes formátum: 231111<br />";
	    }
	    if ($_POST["vez_kozvetlen"] && (strlen($_POST["vez_kozvetlen"]) < 8 || !is_numeric($_POST["vez_kozvetlen"]))) {
		$error["kiir"].="Hibásan megadott vezetékes közvetlen telefonszám! Helyes formátum: 72999888<br />";
	    }
	    if ($_POST["vez_fax"] && (strlen($_POST["vez_fax"]) < 8 || !is_numeric($_POST["vez_fax"]))) {
		$error["kiir"].="Hibásan megadott vezetékes fax telefonszám! Helyes formátum: 72999888<br />";
	    }
	}
	if (!$odin->admin_this("szervezet","edit",$q[1])){ $error["kiir"].="Nincs jogod ezt a szervezetet szerkeszteni!<br />"; }
	if ($error) {
	    $this->content->szervezet_elerhetosegek_szerkesztese($error);
	} elseif ($_POST["modosit"] == $lang["gomb"]["modosit"]) {
	    $details=$odin->get_szervezet_details($q[1]);
	    $sql = "UPDATE szervezetek SET ";
	    $_POST["telephely"] = $_POST["telephely_irsz"] . " " . $_POST["telephely_varos"] . ", " . $_POST["telephely_cim"] . " " . $_POST["telephely_szam"];
	    $_POST["levelezesi"] = $_POST["levelezesi_irsz"] . " " . $_POST["levelezesi_varos"] . ", " . $_POST["levelezesi_cim"];
	    if (strlen($_POST["telephely"]) == 4) {
		unset($_POST["telephely"]);
	    };
	    if (strlen($_POST["levelezesi"]) == 3) {
		unset ($_POST["levelezesi"]);
	    };
	    if ($_POST["nev"]) {
		$sql.=" `nev`='" . $_POST["nev"] . "',";
	    } else {
		$sql.=" `nev`=NULL,";
	    }
	    if ($_POST["szervezet_tipus"]==1){
		$sql.=" `email`=NULL,";
	    }
	    elseif ($_POST["email"]) {
		$sql.=" `email`='" . $_POST["email"] . "',";
	    } else {
		$sql.=" `email`=NULL,";
	    }
	    if ($_POST["szervezet_tipus"]) {
		$sql.=" `szervezet_tipus`='" . $_POST["szervezet_tipus"] . "',";
	    }
	    if ($_POST["oroklodes"]) {
		$sql.=" `oroklodes`='" . $_POST["oroklodes"] . "',";
	    } else {
		$sql.=" `oroklodes`=NULL,";
	    }
	    if ($_POST["szervezet_tipus"]==1){
		$sql.=" `telephely_irsz`=NULL, `telephely_varos`=NULL, `telephely_cim`=NULL, `telephely_szam`=NULL,";
	    }
	    elseif ($_POST["telephely"] && ($detials["telephely"] != $_POST["telephely"] || $_POST["telephely"]==$details["eredeti"]["telephely"])) {
		$sql.=" `telephely_irsz`='" . $_POST["telephely_irsz"] . "', `telephely_varos`='" . $_POST["telephely_varos"] . "', `telephely_cim`='" . $_POST["telephely_cim"] . "', `telephely_szam`='" . $_POST["telephely_szam"] . "',";
	    } else {
		$sql.=" `telephely_irsz`=NULL, `telephely_varos`=NULL, `telephely_cim`=NULL, `telephely_szam`=NULL,";
	    }
	    if ($_POST["szervezet_tipus"]==1){
		$sql.=" `levelezesi_irsz`=NULL, `levelezesi_varos`=NULL, `levelezesi_cim`=NULL,";
	    }
	    elseif ($_POST["levelezesi"] && ($detials["levelezesi"] != $_POST["levelezesi"] || $_POST["levelezesi"]==$details["eredeti"]["levelezesi"])) {
		$sql.=" `levelezesi_irsz`='" . $_POST["levelezesi_irsz"] . "', `levelezesi_varos`='" . $_POST["levelezesi_varos"] . "', `levelezesi_cim`='" . $_POST["levelezesi_cim"] . "',";
	    } else {
		$sql.=" `levelezesi_irsz`=NULL, `levelezesi_varos`=NULL, `levelezesi_cim`=NULL,";
	    }
	    if ($_POST["szervezet_tipus"]==1){
		$sql.="`fax`=NULL,";
	    }
	    elseif ($_POST["fax"] && ($details["fax"] != $_POST["fax"] || $_POST["fax"]==$details["eredeti"]["fax"])) {
		$sql.=" `fax`='" . $_POST["fax"] . "',";
	    } else {
		$sql.="`fax`=NULL,";
	    }
	    if ($_POST["szervezet_tipus"]==1){
		$sql.="`vez_kozvetlen`=NULL,";
	    }
	    elseif ($_POST["vez_kozvetlen"] && ($details["vez_kozvetlen"] != $_POST["vez_kozvetlen"] || $_POST["vez_kozvetlen"]==$details["eredeti"]["vez_kozvetlen"])) {
		$sql.=" `vez_kozvetlen`='" . $_POST["vez_kozvetlen"] . "',";
	    } else {
		$sql.="`vez_kozvetlen`=NULL,";
	    }
	    if ($_POST["szervezet_tipus"]==1){
		$sql.="`vez_fax`=NULL";
	    }
	    elseif ($_POST["vez_fax"] && ($details["vez_fax"] != $_POST["vez_fax"] || $_POST["vez_fax"]==$details["eredeti"]["vez_fax"])) {
		$sql.=" `vez_fax`='" . $_POST["vez_fax"] . "'";
	    } else {
		$sql.="`vez_fax`=NULL";
	    }
	    $sql.=" WHERE `id`='" . $q[1] . "'";
	    mysql_query($sql) or die("MySQL hibaüzenet:" . mysql_error());
	    $sql = "UPDATE modositva SET `ekkor`= '$time' WHERE id=1";
	    mysql_query($sql) or die("MySQL hibaüzenet:" . mysql_error());
	    $siteloadlog["event"] = 6;
	    $siteloadlog["who"] = $q[1];
	    $siteloadlog["what"] = "Szervezet elérhetőségek módosítva";
    	    $loki->cron();
	    $odin->addsiteload();
	    header("Location:" . $config["site"]["absolutepath"] . "/szervezetek/".$q[1]."");
	    exit();
	}
	else
	    $this->content->szervezet_elerhetosegek_szerkesztese($error);
    }

    /*     * ****************************************************** */
       function szervezet_modosit() {
	global $lang, $odin, $loki, $sql_query_count, $config, $siteloadlog, $q;
	$error = false;
	$details=$odin->get_szervezet_details($_POST["id"]);
	$details2=$odin->get_szervezet_details($q[1]);
	$time = date('Y-m-d H:i:s', time());
	//VISSZALÉPÉS
	if ($_POST["modosit"] == $lang["gomb"]["megse"]) {
	    header("location:".$config["site"]["absolutepath"]."/szervezetek/".$q[1]."");
	    exit();	  
	}
	//ELLENŐRZÉS
	if (!$odin->admin_this("szervezet","edit",$q[1])){ $error["kiir"].="Nincs jogod ezt a felhasználót szerkeszteni!<br />"; }
	if ($_POST["modosit"] == $lang["gomb"]["athelyez"]) {
	    if ($POST["id"] && !mysql_num_rows(mysql_query("SELECT id FROM szervezetek WHERE id = '" . $_POST["id"] . "'"))) {
		$error["kiir"].="Nem létező célszerv!<br />";
	    }
	    if (substr_count($details["teljes_id"], $details2["teljes_id"])) {
		$error["kiir"].="Szervezet nem helyezhető át saját magába, illetve alárendelt szerveibe!<br />";
	    }
	}
	if ($error) {
	    $this->content->szervezet_modosit($error);
	} elseif ($_POST["modosit"] == $lang["gomb"]["athelyez"]) {
	    $sql = "UPDATE szervezetek SET `parent_id`='" . $_POST["id"] . "' WHERE id=\"" . $q[1] . "\"";
	    mysql_query($sql) or die("MySQL hibaüzenet:" . mysql_error());
	    $sql = "UPDATE modositva SET `ekkor`= '$time' WHERE id=1";
	    mysql_query($sql) or die("MySQL hibaüzenet:" . mysql_error());
	    $siteloadlog["event"] = 6;
	    $siteloadlog["who"] = $q[1];
	    $siteloadlog["what"] = "Szervezeti besorolás módosítáva";
    	    $loki->cron();
	    $odin->addsiteload();
	    header("Location:" . $config["site"]["absolutepath"] . "/szervezetek/".$q[1]."");
	    exit();
	}
	else
	    $this->content->szervezet_modosit($error);
    }


    /*     * ****************************************************** */
        function szervezet_torlese() {
	global $lang, $odin, $loki, $sql_query_count, $config, $siteloadlog, $q;
	$error = false;
	$time = date('Y-m-d H:i:s', time());
	//VISSZALÉPÉS
	if ($_POST["torol"] == $lang["gomb"]["megse"]) {
	    header("location:" . $config["site"]["absolutepath"] . "/szervezetek/".$q[1]);
	}
	//ELLENŐRZÉS
	$details=$odin->get_szervezet_details($q[1]);
	if ($_POST["torol"] == $lang["gomb"]["torold"]) {
	}
	if (!$odin->admin_this("szervezet","delete",$q[1])){ $error["kiir"].="Nincs jogod ezt a szervezetet törölni, vagy a szervezet nem üres!<br />";}
	if ($error) {
	    $this->content->szervezet_torlese($error);
	}
	elseif ($_POST["torol"] == $lang["gomb"]["torold"]) {
	    $sql = "DELETE FROM szervezetek WHERE `id`=\"".$q[1]."\"";
	    mysql_query($sql) or die("MySQL hibaüzenet:" . mysql_error());
	    $siteloadlog["event"] = 7;
	    $siteloadlog["who"] = $q[1];
	    $siteloadlog["what"] = $details["nev"];
	    $loki->cron();
	    $odin->addsiteload();
	    header("Location:" . $config["site"]["absolutepath"] . "/szervezetek/".$details["parent_id"]."");
	    exit();
	}
	else
	    $this->content->szervezet_torlese($error);
    }

    /*     * ****************************************************** */
        function szervezet_hozzaadasa() {
	global $lang, $odin, $loki, $sql_query_count, $config, $siteloadlog, $q;
	$error = false;
	$time = date('Y-m-d H:i:s', time());
	$details=$odin->get_szervezet_details($q[1]);
	//VISSZALÉPÉS
	if ($_POST["hozzaad"] == $lang["gomb"]["megse"]) {
	    header("location:" . $config["site"]["absolutepath"] . "/szervezetek/".$q[1]."");
	}
	//ELLENŐRZÉS
	if ($_POST["hozzaad"] == $lang["gomb"]["hozzaad"]) {
	    if ($_POST["nev"] && strlen($_POST["nev"]) < 4 ) {
		$error["kiir"].="Hibásan megadott szervezet név! A szevezet neve tartalmazzon legalább 4 karaktert!<br />";
	    }
	    if (!$_POST["szervezet_tipus"]) {
		$error["kiir"].="A szervezet típusát meg kell adni!<br />";
	    }
	    if (!$_POST["szervezet_tipus"]==1 && $_POST["oroklodes"]) {
		$error["kiir"].="Ennél a szervezet típusnál nem lehet ilyen örökődési beállítást alkalmazni!<br />";
		unset($_POST["oroklodes"]);		
	    }
	    if ($_POST["telephely_irsz"] && (strlen($_POST["telephely_irsz"]) != 4 || !is_numeric($_POST["telephely_irsz"]))) {

		$error["kiir"].="Hibásan megadott telephely irányítószám! Helyes formátum: 7600<br />";
	    }
	    if ($_POST["levelezesi_irsz"] && (strlen($_POST["levelezesi_irsz"]) != 4 || !is_numeric($_POST["levelezesi_irsz"]))) {

		$error["kiir"].="Hibásan megadott levelezési irányítószám! Helyes formátum: 7600<br />";
	    }
	    if ($_POST["email"] && (strlen($_POST["email"]) < 4 || !stristr($_POST["email"], "@") || !stristr($_POST["email"], "."))) {
		$error["kiir"].="Hibás GroupWise cím!<br />"; }
	    if ($_POST["fax"] && (strlen($_POST["fax"]) != 6 || !is_numeric($_POST["fax"]))) {
		$error["kiir"].="Hibásan megadott fax szám! Helyes formátum: 231111<br />";
	    }
	    if ($_POST["vez_kozvetlen"] && (strlen($_POST["vez_kozvetlen"]) < 8 || !is_numeric($_POST["vez_kozvetlen"]))) {
		$error["kiir"].="Hibásan megadott vezetékes közvetlen telefonszám! Helyes formátum: 72999888<br />";
	    }
	    if ($_POST["vez_fax"] && (strlen($_POST["vez_fax"]) < 8 || !is_numeric($_POST["vez_fax"]))) {
		$error["kiir"].="Hibásan megadott vezetékes fax telefonszám! Helyes formátum: 72999888<br />";
	    }
	    if ($_POST["ugyelet"] && (strlen($_POST["ugyelet"]) != 6 || !is_numeric($_POST["ugyelet"]))) {

		$error["kiir"].="Hibásan megadott ügyeleti szám! Helyes formátum: 231111<br />";
	    } 
	    if ($_POST["hirkozpont"] && (strlen($_POST["hirkozpont"]) != 6 || !is_numeric($_POST["hirkozpont"]))) {

		$error["kiir"].="Hibásan megadott hírközpont szám! Helyes formátum: 231111<br />";
	    } 
	}
	if (!$odin->admin_this("szervezet","new",$q[1])){ $error["kiir"].="Nincs jogod ezt a szervezetet szerkeszteni!<br />"; }
	if ($error) {
	    $this->content->szervezet_hozzaadasa($error);
	    
	} elseif ($_POST["hozzaad"] == $lang["gomb"]["hozzaad"]) {
	    $sql = "INSERT INTO szervezetek (nev, parent_id, email, szervezet_tipus, oroklodes, telephely_irsz, telephely_varos, telephely_cim, telephely_szam, levelezesi_irsz, levelezesi_varos, levelezesi_cim, fax, vez_kozvetlen, vez_fax, ugyelet, hirkozpont)
	    VALUES ('".$_POST["nev"]."',";
	    $_POST["telephely"] = $_POST["telephely_irsz"] . " " . $_POST["telephely_varos"] . ", " . $_POST["telephely_cim"] . " " . $_POST["telephely_szam"];
	    $_POST["levelezesi"] = $_POST["levelezesi_irsz"] . " " . $_POST["levelezesi_varos"] . ", " . $_POST["levelezesi_cim"];
	    if (strlen($_POST["telephely"]) == 4) {
		unset($_POST["telephely"]);
	    };
	    if (strlen($_POST["levelezesi"]) == 3) {
		unset ($_POST["levelezesi"]);
	    };
	    if ($q[1]) {
		$sql.=" '" . $details["id"] . "',";
	    } else {
		$sql.=" NULL,";
	    }
	    if ($_POST["szervezet_tipus"]==1){
		$sql.=" NULL,";
	    }
	    elseif ($_POST["email"] && ($details["email"] != $_POST["email"] || $_POST["oroklodes"])) {
		$sql.=" '" . $_POST["email"] . "',";
	    } else {
		$sql.=" NULL,";
	    }
	    if ($_POST["szervezet_tipus"]) {
		$sql.=" '" . $_POST["szervezet_tipus"] . "',";
	    }
	    if ($_POST["oroklodes"]) {
		$sql.=" '" . $_POST["oroklodes"] . "',";
	    } else {
		$sql.=" NULL,";
	    }
	    if ($_POST["szervezet_tipus"]==1){
		$sql.=" NULL, NULL, NULL, NULL,";
	    }
	    elseif ($_POST["telephely"] && ($details["telephely"] != $_POST["telephely"] || $_POST["oroklodes"] )) {
		$sql.=" '" . $_POST["telephely_irsz"] . "', '" . $_POST["telephely_varos"] . "', '" . $_POST["telephely_cim"] . "', '" . $_POST["telephely_szam"] . "',";
	    } else {
		$sql.=" NULL, NULL, NULL, NULL,";
	    }
	    if ($_POST["szervezet_tipus"]==1){
		$sql.=" NULL, NULL, NULL,";
	    }
	    elseif ($_POST["levelezesi"] && ($details["levelezesi"] != $_POST["levelezesi"] || $_POST["oroklodes"] )) {
		$sql.=" '" . $_POST["levelezesi_irsz"] . "', '" . $_POST["levelezesi_varos"] . "', '" . $_POST["levelezesi_cim"] . "',";
	    } else {
		$sql.=" NULL, NULL, NULL,";
	    }
	    if ($_POST["szervezet_tipus"]==1){
		$sql.=" NULL,";
	    }
	    elseif ($_POST["fax"] && ($details["fax"] != $_POST["fax"] || $_POST["oroklodes"] )) {
		$sql.=" '" . $_POST["fax"] . "',";
	    } else {
		$sql.=" NULL,";
	    }
	    if ($_POST["szervezet_tipus"]==1){
		$sql.=" NULL,";
	    }
	    elseif ($_POST["vez_kozvetlen"] && ($details["vez_kozvetlen"] != $_POST["vez_kozvetlen"] || $_POST["oroklodes"] )) {
		$sql.=" '" . $_POST["vez_kozvetlen"] . "',";
	    } else {
		$sql.=" NULL,";
	    }
	    if ($_POST["szervezet_tipus"]==1){
		$sql.=" NULL,";
	    }
	    elseif ($_POST["vez_fax"] && ($details["vez_fax"] != $_POST["vez_fax"] || $_POST["oroklodes"] )) {
		$sql.=" '" . $_POST["vez_fax"] . "',";
	    } else {
		$sql.=" NULL,";
	    }
	    if ($_POST["szervezet_tipus"]==1){
		$sql.=" NULL,";
	    }
	    elseif ($_POST["ugyelet"] && (($details["ugyelet"]!=$_POST["ugyelet"] || $_POST["ugyelet"]==$details["eredeti"]["ugyelet"]) || $_POST["oroklodes"])) {
		$sql.=" '" . $_POST["ugyelet"] . "',";
	    } else {
		$sql.=" NULL,";
	    }
	    if ($_POST["szervezet_tipus"]==1){
		$sql.=" NULL";
	    }
	    elseif ($_POST["hirkozpont"] && (($details["hirkozpont"]!=$_POST["hirkozpont"]|| $_POST["hirkozpont"]==$details["eredeti"]["hirkozpont"])  || $_POST["oroklodes"])) {
		$sql.=" '" . $_POST["hirkozpont"] . "'";
	    } else {
		$sql.=" NULL";
	    }
	    $sql.=")";
	    print $sql."<br /><br />";
	    mysql_query($sql) or die("MySQL hibaüzenet:" . mysql_error());
	    $sql = "UPDATE modositva SET `ekkor`= '$time' WHERE id=1";
	    mysql_query($sql) or die("MySQL hibaüzenet:" . mysql_error());
	    $sql= "SELECT max(id) AS maxid FROM szervezetek WHERE 1";
	    $result=mysql_query($sql) or die("MySQL hibaüzenet:" . mysql_error());
	    $row=mysql_fetch_assoc($result);
	    $siteloadlog["event"] = 5;
	    $siteloadlog["who"] = $row["maxid"];
	    $siteloadlog["what"] = "Szervezet sikeresen létrehozva";
    	    $loki->cron();
	    $odin->addsiteload();
	    header("Location:" . $config["site"]["absolutepath"] . "/szervezetek/".$row["maxid"]."");
	    exit();
	}
	else
	    $this->content->szervezet_hozzaadasa($error);
    }

    /*     * ****************************************************** */
}

?>
