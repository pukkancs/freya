<?php

class beosztasok {

    function __construct() {
        global $q;
        include_once("content/content_beosztasok.php");
        $this->content = new beosztasok_content();

        switch ($q[2]) {
		    case "szerkeszt": $this->beosztas_szerkeszt();
			break;
		    case "torol": $this->beosztas_torol();
			break;
		    default: $this->adatlap_kiir();
			break;
        }
    }

    /*     * ****************************************************** */

    function adatlap_kiir() {
        $this->content->adatlap_kiir();
    }
    /*     * ****************************************************** */
    function beosztas_szerkeszt() {
     	global $lang, $odin, $sql_query_count, $config, $siteloadlog, $q;
	$error = false;
	$time = date('Y-m-d H:i:s', time());
	//VISSZALÉPÉS
	if ($_POST["modosit"] == $lang["gomb"]["megse"]) {
	    header("location:" . $config["site"]["absolutepath"] . "/beosztasok/".$q[1]."");
	}
	//ELLENŐRZÉS
	if ($_POST["modosit"] == $lang["gomb"]["modosit"]) {
	   if (strlen($_POST["beosztasnev"]) < 3) {
		$error["kiir"].="Túl rövid beosztásnév!";  
	   }
	}
	if (!$odin->admin_this("beosztas","edit",$q[1])){ $error["kiir"].="Nincs jogod ezt a beosztást szerkeszteni!<br />"; }
	if ($error) {
	    $this->content->beosztas_szerkeszt($error);
	} elseif ($_POST["modosit"] == $lang["gomb"]["modosit"]) {
	    $_POST=$odin->values_to_null($_POST);
	    $_POST["beosztasnev"] = $_POST["beosztasnev"];
	    $sql = "UPDATE beosztasok SET `beosztasnev`='" . $_POST["beosztasnev"] . "' WHERE id=\"" . $q[1] . "\"";
	    mysql_query($sql) or die("MySQL hibaüzenet:" . mysql_error());
	    $sql = "UPDATE modositva SET `ekkor`= '$time' WHERE id=1";
	    mysql_query($sql) or die("MySQL hibaüzenet:" . mysql_error());
	    $siteloadlog["event"] = 9;
	    $siteloadlog["who"] = $q[1];
	    $siteloadlog["what"] = "Beosztás megnevezése módosítva";
	    $odin->addsiteload();
	    header("Location:" . $config["site"]["absolutepath"] . "/beosztasok/".$q[1]."");
	    exit();
	}
	else
	    $this->content->beosztas_szerkeszt($error);
    }
    /*     * ****************************************************** */
 function beosztas_torol() {
	global $lang, $odin, $sql_query_count, $config, $siteloadlog, $q;
	$error = false;
	$time = date('Y-m-d H:i:s', time());
	//VISSZALÉPÉS
	if ($_POST["torol"] == $lang["gomb"]["megse"]) {
	    header("location:" . $config["site"]["absolutepath"] . "/beosztasok/".$q[1]."/torol");
	}
	//ELLENŐRZÉS

	if (!$odin->admin_this("beosztas","delete",$q[1])){ $error["kiir"].="Nincs jogod ezt a beosztast törölni!<br />"; }
	$sql = "SELECT beosztasnev, id FROM beosztasok WHERE id = ".$q["1"]."";
		   $result = mysql_query($sql);
                    while ($sor = mysql_fetch_assoc($result)) {
			$what=$sor["beosztasnev"];
		    }
	$sql2 = "SELECT count(*) as count FROM felhasznalok WHERE beosztas_id = ".$q[1]."";
		    $result2 = mysql_query($sql2);
		    while ($sor2 = mysql_fetch_assoc($result2)) {
		    if ($sor2["count"]>0) $error["kiir"]="Nem törölhető olyan beosztás amihez tartozik felhasználó!";
		    }
	if ($error) {
	    $this->content->beosztas_torol($error);
	} elseif ($_POST["torol"] == $lang["gomb"]["torold"]) {
	    $sql = "DELETE FROM beosztasok WHERE `id`=\"".$q[1]."\"";
	    mysql_query($sql) or die("MySQL hibaüzenet:" . mysql_error());
	    $siteloadlog["event"] = 10;
	    $siteloadlog["who"] = $q[1];
	    $siteloadlog["what"] = $what;
	    $odin->addsiteload();
	    header("Location:" . $config["site"]["absolutepath"] . "/admin/beosztasok-kezelese");
	    exit();
	}
	else
	    $this->content->beosztas_torol($error);
    }

    /*     * ****************************************************** */
}

?>
