<?php

class felhasznalok {

    function __construct() {
        global $q, $error_404;
        include_once("content/content_felhasznalok.php");
        $this->content = new felhasznalok_content();
	if ($_SESSION["user"]["id"]){
        switch ($q[2]) {
            case "": $this->adatlap_kiir();
                break;
            case "adatlap": 
		switch ($q[3]) {
		    case "szemelyes-adatok-szerkesztese": $this->szemelyes_adatok_modosit();
			break;
		    case "megjegyzes-szerkesztese": $this->megjegyzes_modosit();
			break;
		    case "ceges-elerhetosegek-szerkesztese": $this->ceges_elerhetosegek_modosit();
			break;
		    case "szervezeti-besorolas-szerkesztese": $this->szervezet_modosit();
			break;
		    case "jogosultsagok": $this->jogosutlsagok_kiir();
			break;
		    default: $this->adatlap_kiir();
			break;
		    }
                break;
            case "privat-adatlap": $this->privat_adatlap_kiir();
                break;
            case "fiok-adatok": if ($_SESSION["user"]["root"]) {
                   switch ($q[3]) {
		    case "felhasznalo-szerkesztese": $this->felhasznalo_szerkeszt();
			break;   
		    case "felhasznalo-torlese": $this->felhasznalo_torlese();
			break;
		    default: $this->fiok_adatok_kiir();
			break;
		    }
                } else {
                    $this->adatlap_kiir();
                } break;
            case "jogosultsagok": if ($_SESSION["user"]["root"]) {
                    $this->jogosultsagok_kiir();
                } else {
                    $this->adatlap_kiir();
                } break;
            default: $this->adatlap_kiir();
                break;
        }
	} else {
            switch ($q[2]) {
	    default: $this->adatlap_kiir();
                break;
	    }
	}
	
    }

    /*     * ****************************************************** */

    function adatlap_kiir() {
        $this->content->adatlap_kiir();
    }

    /*     * ****************************************************** */

    function privat_adatlap_kiir() {
        global $siteloadlog, $q;
        if ($_POST["kiir"]) {
            $siteloadlog["event"] = 14;
            $siteloadlog["who"] = $q[1];
            $this->content->privat_adatlap_kiir();
        }else
            $this->content->privat_adatlap_ellenoriz();
    }

    /*     * ****************************************************** */

    function fiok_adatok_kiir() {
        $this->content->fiok_adatok_kiir();
    }

    /*     * ****************************************************** */

    function jogosultsagok_kiir() {
        $this->content->jogosultsagok_kiir();
    }

    /*     * ****************************************************** */
    
    function szemelyes_adatok_modosit() {
	global $lang, $odin, $sql_query_count, $config, $siteloadlog, $q;
	$error = false;
	//VISSZALÉPÉS
	if ($_POST["modosit"] == $lang["gomb"]["megse"] || $q[1]=="1") {
	   header("location:" . $config["site"]["absolutepath"] . "/felhasznalok/".$q[1]."");
	   exit();	   
	}
	//ELLENŐRZÉS
	    if (!$odin->admin_this("user","edit",$q[1])){ $error["kiir"].="Nincs jogod ezt a felhasználót szerkeszteni!<br />"; exit(); }
	    $sql = "SELECT email FROM felhasznalok WHERE `id` <> \"" . $q[1] . "\"";
	    $check = mysql_query($sql);
	    $sql_query_count++;
	    while ($sor = mysql_fetch_assoc($check)) {
		if (!strcasecmp($sor["email"], $_POST["email"])) {
		    $error["kiir"].="Már létezik ilyen GroupWise cím!<br />";
		}
	    }
	
	if ($_POST["modosit"] == $lang["gomb"]["modosit"]) {
	    if (strlen($_POST["vezeteknev"]) < 3)
		$error["kiir"].="Hibás vezetéknév!<br />";
	    if (strlen($_POST["keresztnev"]) < 3)
		$error["kiir"].="Hibás keresztnev!<br />";
	    if (strlen($_POST["email"]) < 4 || !stristr($_POST["email"], "@") || !stristr($_POST["email"], ".")) {
		$error.="Hibás GroupWise cím!<br />"; }
	    if (strlen($_POST["beosztas"]) < 3 && $_POST["beosztas"] && !$_POST["beosztas_id"])
		$error["kiir"].="Hibás beosztásnév!<br />";
	    if (strlen($_POST["rendfokozat"]) < 3 && $_POST["rendfokozat"] && !$_POST["rendfokozat_id"])
		$error["kiir"].="Hibás rendfokozat!<br />";
	    if (!$_POST["beosztas_id"] && !$_POST["beosztas"]) {
		$error["kiir"].="Adja meg kézileg a beosztást!<br />";
		$error["beosztas"] = true;
	    }
	    if (!$_POST["rendfokozat_id"] && !$_POST["rendfokozat"]) {
		$error["kiir"].="Adja meg kézileg a renfokozatot!<br />";
		$error["rendfokozat"] = true;
	    }
	}
	if ($error["kiir"]) {
	    $this->content->szemelyes_adatok_modosit($error);
	} elseif ($_POST["modosit"] == $lang["gomb"]["modosit"]) {
	    $time = date('Y-m-d H:i:s', time());
	    if ($_POST["beosztas"]) {
		$sql = "SELECT * FROM beosztasok";
		$check = mysql_query($sql);
		$sql_query_count++;
		while ($sor = mysql_fetch_assoc($check)) {
		    if (!strcasecmp($sor["beosztasnev"], $_POST["beosztas"])) {
			unset($_POST["beosztas"]);
			$_POST["beosztas_id"] = $sor["id"];
		    }
		}
	    }
	    if ($_POST["rendfokozat"]) {
		$sql = "SELECT * FROM rendfokozatok";
		$check = mysql_query($sql);
		$sql_query_count++;
		while ($sor = mysql_fetch_assoc($check)) {
		    if (!strcasecmp($sor["megnevezes"], $_POST["rendfokozat"])) {
			unset($_POST["rendfokozat"]);
			$_POST["rendfokozat_id"] = $sor["id"];
		    }
		}
	    }
	    if ($_POST["beosztas"]) {
		$sql = "INSERT INTO beosztasok (beosztasnev) VALUES ('" . $_POST["beosztas"] . "')";
		mysql_query($sql) or die("MySQL hibaüzenet:" . mysql_error());
		$siteloadlog["event"] = 8;
		$siteloadlog["what"] = $_POST["beosztas"];
		$odin->addsiteload();
		$sql = "SELECT id FROM beosztasok WHERE beosztasnev='" . $_POST["beosztas"] . "'";
		$check = mysql_query($sql);
		$sql_query_count++;
		while ($sor = mysql_fetch_assoc($check)) {
		    $_POST["beosztas_id"] = $sor["id"];
		}
	    }
	    if ($_POST["rendfokozat"]) {
		$sql = "INSERT INTO rendfokozatok (megnevezes) VALUES ('" . $_POST["rendfokozat"] . "')";
		mysql_query($sql) or die("MySQL hibaüzenet:" . mysql_error());
		$siteloadlog["event"] = 11;
		$siteloadlog["what"] = $_POST["rendfokozat"];
		$odin->addsiteload();
		$sql = "SELECT id FROM rendfokozatok WHERE megnevezes='" . $_POST["rendfokozat"] . "'";
		$check = mysql_query($sql);
		$sql_query_count++;
		while ($sor = mysql_fetch_assoc($check)) {
		    $_POST["rendfokozat_id"] = $sor["id"];
		}
	    }	    
	    $_POST=$odin->values_to_null($_POST);
	    $sql = "UPDATE felhasznalok SET `modositva_ekkor`= '$time', `modositva_altal`= \"" . $_SESSION["user"]["id"] . "\", `allomany_statusz`= \"" . $_POST["allomany_statusz"] . "\", `titulus`= \"" . $_POST["titulus"] . "\", `titulus_2`= \"" . $_POST["titulus_2"] . "\", `vezeteknev`= \"" . $_POST["vezeteknev"] . "\",`kozepsonev`= \"" . $_POST["kozepsonev"] . "\",`keresztnev`= \"" . $_POST["keresztnev"] . "\",`nicknev`= \"" . $_POST["nicknev"] . "\",`email`= \"" . $_POST["email"] . "\",`beosztas_id`= \"" . $_POST["beosztas_id"] . "\",`rendfokozat_id`= \"" . $_POST["rendfokozat_id"] . "\",`beosztas_megjegyzes`= \"" . $_POST["beosztas_megjegyzes"] . "\", `nicknev`= \"" . $_POST["nicknev"] . "\" WHERE id=\"" . $q[1] . "\"";
	    mysql_query($sql) or die("MySQL hibaüzenet:" . mysql_error());
	    $sql = "UPDATE modositva SET `ekkor`= '$time' WHERE id=1";
	    mysql_query($sql) or die("MySQL hibaüzenet:" . mysql_error());
	    $siteloadlog["event"] = 3;
	    $siteloadlog["who"] = $q[1];
	    $siteloadlog["what"] = "Szemelyes adatok módosítva";
	    $odin->addsiteload();
	    header("Location:" . $config["site"]["absolutepath"] . "/felhasznalok/".$q[1]."");
	    exit();
	}
	else
	    $this->content->szemelyes_adatok_modosit();
    }

    /*     * ****************************************************** */

    function megjegyzes_modosit() {
	global $lang, $odin, $sql_query_count, $config, $siteloadlog, $q;
	$error = false;
	$time = date('Y-m-d H:i:s', time());
	//VISSZALÉPÉS
	if ($_POST["modosit"] == $lang["gomb"]["megse"]) {
	    header("location:" . $config["site"]["absolutepath"] . "/felhasznalok/".$q[1]."");
	}
	//ELLENŐRZÉS
	if ($_POST["modosit"] == $lang["gomb"]["modosit"]) {
	    
	}
	if (!$odin->admin_this("user","edit",$q[1])){ $error["kiir"].="Nincs jogod ezt a felhasználót szerkeszteni!<br />"; exit(); }
	if ($error) {
	    $this->content->megjegyzes_modosit($error);
	} elseif ($_POST["modosit"] == $lang["gomb"]["modosit"]) {
	    $_POST=$odin->values_to_null($_POST);
	    $_POST["megjegyzes"] = $_POST["megjegyzes"];
	    $sql = "UPDATE felhasznalok SET `modositva_ekkor`= '$time', `modositva_altal`= \"" . $_SESSION["user"]["id"] . "\", `megjegyzes`='" . $_POST["megjegyzes"] . "' WHERE id=\"" . $q[1] . "\"";
	    mysql_query($sql) or die("MySQL hibaüzenet:" . mysql_error());
	    $sql = "UPDATE modositva SET `ekkor`= '$time' WHERE id=1";
	    mysql_query($sql) or die("MySQL hibaüzenet:" . mysql_error());
	    $siteloadlog["event"] = 3;
	    $siteloadlog["who"] = $q[1];
	    $siteloadlog["what"] = "Megjegyzés módosítva";
	    $odin->addsiteload();
	    header("Location:" . $config["site"]["absolutepath"] . "/felhasznalok/".$q[1]."");
	    exit();
	}
	else
	    $this->content->megjegyzes_modosit($error);
    }

    /*     * ****************************************************** */

    function szervezet_modosit() {
	global $lang, $odin, $sql_query_count, $config, $siteloadlog, $q;
	$error = false;
	$details=$odin->get_szervezet_details($_POST["szervezet_id"]);
	$time = date('Y-m-d H:i:s', time());
	//VISSZALÉPÉS
	if ($_POST["modosit"] == $lang["gomb"]["megse"]) {
	    header("location:".$config["site"]["absolutepath"]."/felhasznalok/".$q[1]."");
	    exit();	  
	}
	//ELLENŐRZÉS
	if (!$odin->admin_this("user","edit",$q[1])){ $error["kiir"].="Nincs jogod ezt a felhasználót szerkeszteni!<br />"; exit(); }
	if ($_POST["modosit"] == $lang["gomb"]["athelyez"] && (!$_POST["szervezet_id"] || $details["szervezet_tipus"]==1)) {
		$error["kiir"].="Ebbe a szervezetbe nem lehet felhasználót áthelyezni!<br />";
	}
	if ($_POST["modosit"] == $lang["gomb"]["athelyez"]) {
	    if (!mysql_num_rows(mysql_query("SELECT id FROM szervezetek WHERE id = '" . $_POST["szervezet_id"] . "'"))) {
		$error["kiir"].="Nem létező célszerv!<br />";
	    }
	}
	if ($error) {
	    $this->content->szervezet_modosit($error);
	} elseif ($_POST["modosit"] == $lang["gomb"]["athelyez"]) {
	    $sql = "UPDATE felhasznalok SET `modositva_ekkor`= '$time', `modositva_altal`= \"" . $_SESSION["user"]["id"] . "\", `szervezet_id`='" . $_POST["szervezet_id"] . "' WHERE id=\"" . $q[1] . "\"";
	    mysql_query($sql) or die("MySQL hibaüzenet:" . mysql_error());
	    $sql = "UPDATE modositva SET `ekkor`= '$time' WHERE id=1";
	    mysql_query($sql) or die("MySQL hibaüzenet:" . mysql_error());
	    $siteloadlog["event"] = 3;
	    $siteloadlog["who"] = $q[1];
	    $siteloadlog["what"] = "Szervezet módosítva";
	    $odin->addsiteload();
	    header("Location:" . $config["site"]["absolutepath"] . "/felhasznalok/".$q[1]."");
	    exit();
	}
	else
	    $this->content->szervezet_modosit($error);
    }


    /*     * ****************************************************** */

    function ceges_elerhetosegek_modosit() {
	global $lang, $odin, $sql_query_count, $config, $siteloadlog, $q;
	$error = false;
	$time = date('Y-m-d H:i:s', time());
	//VISSZALÉPÉS
	if ($_POST["modosit"] == $lang["gomb"]["megse"]) {
	    header("location:" . $config["site"]["absolutepath"] . "/felhasznalok/".$q[1]."");
	}
	if (!$odin->admin_this("user","edit",$q[1])){ $error["kiir"].="Nincs jogod ezt a felhasználót szerkeszteni!<br />"; exit(); }
	//ELLENŐRZÉS
	if ($_POST["modosit"] == $lang["gomb"]["modosit"]) {
	    if ($_POST["telephely_irsz"] && (strlen($_POST["telephely_irsz"]) != 4 || !is_numeric($_POST["telephely_irsz"]))) {

		$error["kiir"].="Hibásan megadott telephely irányítószám! Helyes formátum: 7600<br />";
	    }
	    if ($_POST["levelezesi_irsz"] && (strlen($_POST["levelezesi_irsz"]) != 4 || !is_numeric($_POST["levelezesi_irsz"]))) {

		$error["kiir"].="Hibásan megadott levelezési irányítószám! Helyes formátum: 7600<br />";
	    }
	    if ($_POST["vezetoi"] && (strlen($_POST["vezetoi"]) != 6 || !is_numeric($_POST["vezetoi"]))) {

		$error["kiir"].="Hibásan megadott vezetői szám! Helyes formátum: 231111<br />";
	    }
	    // strlen($_POST["kozvetlen"]) != 6 ||
	    if ($_POST["kozvetlen"] && ( !is_numeric($_POST["kozvetlen"]))) {

		$error["kiir"].="Hibásan megadott közvetlen szám! Helyes formátum: 231111<br />";
	    }
	    if ($_POST["2helyi"] && (strlen($_POST["2helyi"]) != 6 || !is_numeric($_POST["2helyi"]))) {

		$error["kiir"].="Hibásan megadott 2. helyi szám! Helyes formátum: 231111<br />";
	    }
	    if ($_POST["fax"] && (strlen($_POST["fax"]) != 6 || !is_numeric($_POST["fax"]))) {

		$error["kiir"].="Hibásan megadott fax szám! Helyes formátum: 231111<br />";
	    }
	    if ($_POST["tetra"] && (strlen($_POST["tetra"]) != 7 || !is_numeric($_POST["tetra"]))) {

		$error["kiir"].="Hibásan megadott tetra szám! Helyes formátum: 3230000<br />";
	    }
	    if ($_POST["mobil"] && (strlen($_POST["mobil"]) != 9 || !is_numeric($_POST["mobil"]))) {

		$error["kiir"].="Hibásan megadott mobil telefonszám! Helyes formátum: 209888777<br />";
	    }
	    if ($_POST["vez_kozvetlen"] && (strlen($_POST["vez_kozvetlen"]) < 8 || !is_numeric($_POST["vez_kozvetlen"]))) {

		$error["kiir"].="Hibásan megadott vezetékes közvetlen telefonszám! Helyes formátum: 72999888<br />";
	    }
	    if ($_POST["vez_fax"] && (strlen($_POST["vez_fax"]) < 8 || !is_numeric($_POST["vez_fax"]))) {

		$error["kiir"].="Hibásan megadott vezetékes fax telefonszám! Helyes formátum: 72999888<br />";
	    }
	}


	if ($error) {
	    $this->content->ceges_elerhetosegek_modosit($error);
	} elseif ($_POST["modosit"] == $lang["gomb"]["modosit"]) {
	    $sql = "UPDATE felhasznalok SET `modositva_ekkor`= '$time', ";

	    $_POST["telephely"] = $_POST["telephely_irsz"] . " " . $_POST["telephely_varos"] . ", " . $_POST["telephely_cim"] . " " . $_POST["telephely_szam"];
	    if (strlen($_POST["telephely"]) == 4) {
		$_POST["telephely_varos"] = "";
	    };
	    if ($_POST["telephely"] && $_SESSION["user"]["szervtol"]["telephely"] != $_POST["telephely"]) {
		$sql.=" `telephely_irsz`='" . $_POST["telephely_irsz"] . "', `telephely_varos`='" . $_POST["telephely_varos"] . "', `telephely_utca`='" . $_POST["telephely_cim"] . "', `telephely_cim`='" . $_POST["telephely_szam"] . "',";
	    } else {
		$sql.=" `telephely_irsz`=NULL, `telephely_varos`=NULL, `telephely_utca`=NULL, `telephely_cim`=NULL,";
	    }
	    if ($_POST["telephely_epulet"]) {
		$sql.=" `telephely_epulet`='" . $_POST["telephely_epulet"] . "',";
	    } else {
		$sql.="`telephely_epulet`=NULL,";
	    }
	    if ($_POST["telephely_emelet"]) {
		$sql.=" `telephely_emelet`='" . $_POST["telephely_emelet"] . "',";
	    } else {
		$sql.="`telephely_emelet`=NULL,";
	    }
	    if ($_POST["telephely_iroda"]) {
		$sql.=" `telephely_iroda`='" . $_POST["telephely_iroda"] . "',";
	    } else {
		$sql.="`telephely_iroda`=NULL,";
	    }
	    if ($_POST["vezetoi"]) {
		$sql.=" `vezetoi`='" . $_POST["vezetoi"] . "',";
	    } else {
		$sql.="`vezetoi`=NULL,";
	    }
	    if ($_POST["kozvetlen"]) {
		$sql.=" `kozvetlen`='" . $_POST["kozvetlen"] . "',";
	    } else {
		$sql.="`kozvetlen`=NULL,";
	    }
	    if ($_POST["2helyi"]) {
		$sql.=" `2helyi`='" . $_POST["2helyi"] . "',";
	    } else {
		$sql.="`2helyi`=NULL,";
	    }
	    if ($_POST["fax"] && $_SESSION["user"]["szervtol"]["fax"] != $_POST["fax"]) {
		$sql.=" `fax`='" . $_POST["fax"] . "',";
	    } else {
		$sql.="`fax`=NULL,";
	    }
	    if ($_POST["mobil"]) {
		$sql.=" `mobil`='" . $_POST["mobil"] . "',";
	    } else {
		$sql.="`mobil`=NULL,";
	    }
	    if ($_POST["vez_kozvetlen"]) {
		$sql.=" `vez_kozvetlen`='" . $_POST["vez_kozvetlen"] . "',";
	    } else {
		$sql.="`vez_kozvetlen`=NULL,";
	    }
	    if ($_POST["vez_fax"] && $_SESSION["user"]["szervtol"]["vez_fax"] != $_POST["vez_fax"]) {
		$sql.=" `vez_fax`='" . $_POST["vez_fax"] . "',";
	    } else {
		$sql.="`vez_fax`=NULL,";
	    }
	    if ($_POST["tetra"]) {
		$sql.=" `tetra`='" . $_POST["tetra"] . "',";
	    } else {
		$sql.="`tetra`=NULL,";
	    }
	    $sql.="`modositva_altal`= '" . $_SESSION["user"]["id"] . "'";
	    $sql.=" WHERE id='" . $q[1] . "'";
	    mysql_query($sql) or die("MySQL hibaüzenet:" . mysql_error());
	    $sql = "UPDATE modositva SET `ekkor`= '$time' WHERE id=1";
	    mysql_query($sql) or die("MySQL hibaüzenet:" . mysql_error());
	    $siteloadlog["event"] = 3;
	    $siteloadlog["who"] = $q[1];
	    $siteloadlog["what"] = "Céges elérhetőségek";
	    $odin->addsiteload();
	    header("Location:" . $config["site"]["absolutepath"] . "/felhasznalok/".$q[1]."");
	    exit();
	}
	else
	    $this->content->ceges_elerhetosegek_modosit($error);
    }

    /*     * ****************************************************** */
        function felhasznalo_torlese() {
	global $lang, $odin, $sql_query_count, $config, $siteloadlog, $q;
	$error = false;
	$time = date('Y-m-d H:i:s', time());
	//VISSZALÉPÉS
	if ($_POST["torol"] == $lang["gomb"]["megse"]) {
	    header("location:" . $config["site"]["absolutepath"] . "/felhasznalok/".$q[1]."/fiok-adatok");
	}
	//ELLENŐRZÉS
	$details=$odin->get_user_basedetails($q[1]);
	if ($_POST["torol"] == $lang["gomb"]["torold"]) {
	  if ($_POST["ellenorzokod"]!=$details["user"]["passkey"]) {

		$error["kiir"].="Hibás ellenőrzőkód!<br />";
	    }  
	}
	if (!$odin->admin_this("user","delete",$q[1])){ $error["kiir"].="Nincs jogod ezt a felhasználót szerkeszteni!<br />"; exit(); }
	if ($error) {
	    $this->content->felhasznalo_torlese($error);
	} elseif ($_POST["torol"] == $lang["gomb"]["torold"]) {
	    $sql = "DELETE FROM felhasznalok WHERE `id`=\"".$q[1]."\" AND `passkey`=\"".$_POST["ellenorzokod"]."\"";
	    mysql_query($sql) or die("MySQL hibaüzenet:" . mysql_error());
	    $siteloadlog["event"] = 4;
	    $siteloadlog["who"] = $q[1];
	    $siteloadlog["what"] = $details["user"]["teljesnev"];
	    $odin->addsiteload();
	    header("Location:" . $config["site"]["absolutepath"] . "/szervezetek/".$details["user"]["szervezet_id"]."");
	    exit();
	}
	else
	    $this->content->felhasznalo_torlese($error);
    }

    /*     * ****************************************************** */
    function felhasznalo_szerkeszt() {
	global $lang, $odin, $sql_query_count, $config, $q, $siteloadlog;
	$error = false;
	//VISSZALÉPÉS
	if ($_POST["modosit"] == $lang["gomb"]["megse"]) {
	    header("location:" . $config["site"]["absolutepath"] . "/felhasznalok/".$q[1]."/fiok-adatok/");
	}
	//ELLENŐRZÉS
	if ($_POST["modosit"]) {
	    $sql = "SELECT azonosito, email FROM felhasznalok WHERE `id` <> \"" . $q[1] . "\"";
	    $check = mysql_query($sql);
	    $sql_query_count++;
	    while ($sor = mysql_fetch_assoc($check)) {
		if (!strcmp($sor["azonosito"], $_POST["azonosito"])) {
		    $error["kiir"].="Már létezik ilyen Felhasználói azonosító!<br />";
		}
	    }
	    if (strlen($_POST["azonosito"])<4 || strlen($_POST["azonosito"])>32) {
		$error["kiir"].="Hibás felhasználói azonosító! (A felhasználó azonosító bármilyen minimum 4, maximum 32 karakter hosszúságú lehet)<br />";
	    }
	    if (strlen($_POST["jelszo1"]) < 4 || $_POST["jelszo1"] != $_POST["jelszo2"]) {
		$error["kiir"].="Hibás, vagy túl rövid jelszó!";
	    }
	}
	if (!$odin->admin_this("user","edit",$q[1])){ $error["kiir"].="Nincs jogod ezt a felhasználót szerkeszteni!<br />"; exit(); }
	//VÉGREHAJTÁS / MEGJELENÍTÉS
	if ($error) {
	    $this->content->felhasznalo_szerkeszt($error);
	    }
	elseif ($_POST["modosit"]) {
	    $sql = "SELECT azonosito, email FROM felhasznalok WHERE `id` <> \"" . $q[1] . "\"";
	    $check = mysql_query($sql);
	    $sql_query_count++;
	    while ($sor = mysql_fetch_assoc($check)) {
		if (!strcmp($sor["azonosito"], $_POST["azonosito"])) {
		    $error["kiir"].="Már létezik ilyen Felhasználói azonosító!<br />";
		}
	    }
	    if (strlen($_POST["azonosito"])<4 || strlen($_POST["azonosito"])>32) {
		$error["kiir"].="Hibás Felhasználói azonosító!<br />";
	    }
	    if (strlen($_POST["jelszo1"]) < 4) {
		$error["kiir"].="Hibás, vagy túl rövid jelszó!";
	    }
	    $sql = "SELECT azonosito, email FROM felhasznalok WHERE `id` = \"" . $q[1] . "\"";
	    $check = mysql_query($sql);
	    $sql_query_count++;
	    while ($sor = mysql_fetch_assoc($check)) {
		$email=$sor["email"];
	    }
	    if (!$error) {
		    $time = date('Y-m-d H:i:s', time());
		    $azonosito = str_replace(" ", "", $_POST["azonosito"]);
		    $newpass = str_replace(" ", "", $_POST["jelszo1"]);
		    $newpassextension = substr(md5(rand(100, 10000000)), $startat, 8);
		    $passtodb = md5($newpass . "" . substr(md5($newpassextension), 3, 4) . "" . substr(md5($newpassextension), 11, 4));
		    $sql = "UPDATE felhasznalok SET `azonosito`= \"$azonosito\", `modositva_ekkor`= '$time', `modositva_altal`= \"" . $_SESSION["user"]["id"] . "\",`jelszo`= \"$passtodb\", `jelszo_kiegeszito`= \"$newpassextension\" WHERE id=\"" . $q[1] . "\"";
		    mysql_query($sql) or die("MySQL hibaüzenet:" . mysql_error());
		    $sql = "UPDATE modositva SET `ekkor`= '$time' WHERE id=1";
		    mysql_query($sql) or die("MySQL hibaüzenet:" . mysql_error());
		    $odin->send_mail($email, "TELEFONKÖNYV Adatmódosítás", "Ez egy automatikus üzenet! \n\nA felhasználói adatait egy adminisztrátor módosította.\n\nÚj felhasználóneve: $azonosito\n\nÚj jelszava: $newpass");
		    $siteloadlog["event"] = 3;
		    $siteloadlog["who"] = $q[1];
		    $siteloadlog["what"] = "Rendszeradatok módosítva";
		    $odin->addsiteload();
		    header("Location:" . $config["site"]["absolutepath"] . "/felhasznalok/".$q[1]."/fiok-adatok/");
		    exit();
	    } else $this->content->felhasznalo_szerkeszt($error);
	}
	else {
	    $this->content->felhasznalo_szerkeszt();
	} 
    }

    /*     * ****************************************************** */
    
}

?>
