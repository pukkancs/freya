<?php
class admin
{
function __construct()
	{	
    global $q;
    include_once("content/content_admin.php");
	$this->content=new admin_content();
	
    switch ($q[1])
		{
        case "":
	    if ($_SESSION["user"]["root"] || $_SESSION["user"]["rg_log"])
	    { $this->sys_log_kiir();  break; }
	case "online_felhasznalok":
	    if ($_SESSION["user"]["root"] || $_SESSION["user"]["rg_log"])
	    { $this->online_felhasznalok_kiir();
             break; }
	case "verzio_statisztika":
	    if ($_SESSION["user"]["root"] || $_SESSION["user"]["rg_statisztika"])
	    { $this->verzio_statisztika_kiir(); 
             break; }
	case "oldal_statisztika":
	    if ($_SESSION["user"]["root"] || $_SESSION["user"]["rg_statisztika"])
	    { $this->oldal_statisztika_kiir();
            break; }
	case "grafikus_statisztika":
	    if ($_SESSION["user"]["root"] || $_SESSION["user"]["rg_statisztika"])
	    {
	    $this->grafikus_statisztika_kiir();
            break;
	    }
	case "rendszer_log":
	    if ($_SESSION["user"]["root"] || $_SESSION["user"]["rg_log"])
	    { $this->sys_log_kiir();
            break; }
	case "hianyzo_rendfokozatok":
	    if ($_SESSION["user"]["root"])
	    {
	    $this->hianyzo_rendfokozatok_kiir();
            break; 
	    }
	case "beosztasok_kezelese":
	    if ($_SESSION["user"]["root"] || $_SESSION["user"]["rg_beosztasok"])
	    {
	    $this->beosztasok_kezelese_kiir();
            break; 
	    }
	default:
            include_once("system/class_error_404.php"); 
	    $error_404=new error_404();
            break;	
            }
	}
/*********************************************************/
function online_felhasznalok_kiir()
	{
	$this->content->online_felhasznalok_kiir();
	}
/*********************************************************/
function oldal_statisztika_kiir()
	{
	$this->content->oldal_statisztika_kiir();
	}
/*********************************************************/
function grafikus_statisztika_kiir()
	{
	$this->content->grafikus_statisztika_kiir();
	}
/*********************************************************/
function verzio_statisztika_kiir()
	{
	$this->content->verzio_statisztika_kiir();
	}
/*********************************************************/
function sys_log_kiir()
	{
	$this->content->sys_log_kiir();
	}
/*********************************************************/
function hianyzo_rendfokozatok_kiir(){
	$this->content->hianyzo_rendfokozatok_kiir();	    
    }
/*********************************************************/
function beosztasok_kezelese_kiir()
	{
	$this->content->beosztasok_kezelese_kiir();
	}
/*********************************************************/
}
?>
