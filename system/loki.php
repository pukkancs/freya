<?php

// LOKI 1.0 beta
class loki {
    
    function __construct() {
	$run=0;
	
	$yesterday = date("Y")."-".date("m")."-".(date("d")-1);
	$yesterday_end_time = $yesterday . " 23:59:59";
	$yesterday_end_time=strtotime($yesterday_end_time);
	
        $sql="SELECT * FROM modositva WHERE id=1";
        $result=mysql_query($sql);
        while($sor=mysql_fetch_assoc($result)){
        if ((date("H")>18 || date("H")<4) && strtotime($sor["loki"])<(time()-86400)) {
            $this->cron();
            $run++;
	    }
	if (strtotime($sor["sstat"])<($yesterday_end_time) && !$run) {
	    $this->cache_site_stat();
            $run++;
	    }
	if (strtotime($sor["gstat"])<($yesterday_end_time) && !$run) {
            $this->cache_graf_stat();
            $run++;
	    }
	if (strtotime($sor["vstat"])<($yesterday_end_time) && !$run) {
            $this->cache_version_stat();
            $run++;
	    }    
	}
     }

    function cron() {
	$this->rebuild_szervezetek_db();
	$this->check_felhasznalok_db();
	$this->cache_side_tree();
	$this->update_modositva_db();
    }

    function no_user_abort($time) {
	ignore_user_abort(1); // felhasználói megszakítás figyelmen kívül hagyása
	set_time_limit($time); // max 60 másodpercig futhat ez a script.
    }

    function update_modositva_db() {
        $time = date('Y-m-d H:i:s', time());
	$sql = "UPDATE modositva SET `loki`= '$time', `ekkor`= '$time' WHERE id=1";
	mysql_query($sql) or die("MySQL hibaüzenet:" . mysql_error());
    }

// ALAPFUNKCIÓK
    function rebuild_szervezetek_db() {

	$time = date('Y-m-d H:i:s', time());

	$this->no_user_abort(60);

	$result = mysql_query("SELECT * FROM szervezetek ORDER BY id") or die("MySQL hibaüzenet:" . mysql_error());

	while ($sor = mysql_fetch_assoc($result)) {
	    $tree_id = $sor["id"];
	    $j = 0;
	    while ($tree_id) {
		$result99 = mysql_query("SELECT * FROM szervezetek WHERE id = $tree_id");
		while ($sor99 = mysql_fetch_array($result99)) {
		    $j++;
		    $jmax = $j;
		    $tid["$j"] = $sor99["id"];
		    $tnev["$j"] = $sor99["nev"];
		    $tree_id = $sor99["parent_id"];
		}
	    }
	    $teljes_id = "";
	    $teljes_nev = "";
	    while ($j != 0) {
		if ($j == $jmax)
		    $teljes_id = "$teljes_id" . $tid["$j"]; else
		    $teljes_id = "$teljes_id" . "-" . $tid["$j"];
		if ($j == $jmax)
		    $teljes_nev = "$teljes_nev" . $tnev[$j];
		else
		    $teljes_nev = "$teljes_nev" . "/" . $tnev[$j];
		$j--;
	    }
	    mysql_query("UPDATE szervezetek SET `teljes_id`= \"$teljes_id\" WHERE `id` = \"" . $sor["id"] . "\"");
	    mysql_query("UPDATE szervezetek SET `teljes_nev`= \"$teljes_nev\" WHERE `id` = \"" . $sor["id"] . "\"");
	}
    }

    function check_felhasznalok_db() {

	$time = date('Y-m-d H:i:s', time());

	$this->no_user_abort(60);

	$result = mysql_query("SELECT id FROM felhasznalok WHERE allomany_statusz = '7' OR allomany_statusz = '8' AND modositva_ekkor < '" . (time() - 15552000) . "' ORDER BY id") or die("MySQL hibaüzenet:" . mysql_error());
	while ($sor = mysql_fetch_assoc($result)) {
	    // mysql_query("DELETE FROM felhasznalok WHERE id=" . $sor["id"] . "") or die("MySQL hibaüzenet:" . mysql_error());
	}
    }

    function cache_side_tree() {
	global $config, $q, $odin;

	$time = date('Y-m-d H:i:s', time());

	$this->no_user_abort(60);

	$file = 'cache/sidetree.php';
	ob_start();
	?>

	<script type="text/javascript">
                    $(function(){
                        $("ul#szervezetlista").simpletreeview({
                            open: '<span style="color: #900;">&darr;</span>',
                            close: '<span style="color: #090;">&rarr;</span>',
                            slide: true,
                            speed: 'fast',
                            collapsed: false
                        });

                    });
                </script>  
<?php
$search = array(
    0 => "/Rendőrkapitányság/",
    1 => "/Rendőr-főkapitányság/",
    2 => "/Gazdasági Főigazgatóság Területi Szervei/",
    3 => "/Gazdasági Ellátó Igazgatóság/",
    4 => "/Rendőr-Főkapitányságok/",
);
$replace = array(
    0 => "Rk.",
    1 => "RFK",
    2 => "GF Területi Szervei",
    3 => "GEI",
    4 => "RFK-k"
);
$sql_query_count++;
   

	print (" <?php 
	if (\$_SESSION[\"user\"][\"id\"] && \$q[0] == \"szervezetek\" && !\$q[2]) {
	    ?>"); ?>
	    <div class="main_left_container">
	        <div class="main_left_title"><font style="color:#617f10">SZERVEZET</font> Gyorsmenü</div>
	        <ul id="szervezetlista" class="treeview_root" style="font-size: 9px;">
	    	<li class="treeview_root"><a href="<?php print $config["site"]["absolutepath"] . "/szervezetek/"; ?>"><?php print $config["site"]["tree_root_element"]; ?></a>
			<?php
                        $check = mysql_query("SELECT nev, id, teljes_id FROM szervezetek WHERE szervezet_tipus=5 OR id<8 ORDER BY teljes_id");
			while ($sor = mysql_fetch_assoc($check)) {

			    if (strlen($sor["nev"]))
			    $sor["nev"] = preg_replace($search, $replace, $sor["nev"]);
			    $tree[$sor["teljes_id"]] = "<a href=\"" . $config["site"]["absolutepath"] . "/szervezetek/" . $sor["id"] . "\">" . $sor["nev"] . "</a>";
			}
			$tree = $odin->explodeTree($tree, "-", true);
			$odin->array_to_ulli($tree);
			?>
	    	</li>
	        </ul>
	    </div>
	    <div class="main_left_spacer"></div>
	    <?php
	    print (" <?php 
	}
		    ?>");
	print (" <?php 
	if (\$_SESSION[\"user\"][\"id\"] && \$q[0] == \"sajat_vilag\" && \$q[2] == \"szervezet-szerkesztese\") {
	     ?>"); 
        unset($tree);    
        ?>
            
	    <div class="main_left_container">
	        <div class="main_left_title"><font style="color:#617f10">SZERVEZET</font> Gyorsmenü</div>
	        <ul id="szervezetlista" class="treeview_root" style="font-size: 9px;">
	    	<li class="treeview_root"><a href="<?php print $config["site"]["absolutepath"] . "/szervezetek/"; ?>"><?php print $config["site"]["tree_root_element"]; ?></a>
			<?php
			$check = mysql_query("SELECT nev, id, teljes_id FROM szervezetek WHERE szervezet_tipus=5 OR id<8 ORDER BY teljes_id");
			while ($sor = mysql_fetch_assoc($check)) {

			    if (strlen($sor["nev"]))
				$sor["nev"] = preg_replace($search, $replace, $sor["nev"]);
			    $tree[$sor["teljes_id"]] = "<a href=\"" . $config["site"]["absolutepath"] . "/sajat-vilag/adatlapom/szervezet-szerkesztese/" . $sor["id"] . "\">" . $sor["nev"] . "</a>";
			}
			$tree = $odin->explodeTree($tree, "-", true);
			$odin->array_to_ulli($tree);
			?>
	    	</li>
	        </ul>
	    </div>
	    <div class="main_left_spacer"></div>

 <?php
	    print (" <?php 
	}
		    ?>");
	    print (" <?php
	if (\$_SESSION[\"user\"][\"id\"] && \$q[0] == \"felhasznalok\" && \$q[3] == \"szervezeti-besorolas-szerkesztese\") {
	     ?>"); 
            unset($tree);
            ?>
	    <div class="main_left_container">
	        <div class="main_left_title"><font style="color:#617f10">SZERVEZET</font> Gyorsmenü</div>
	        <ul id="szervezetlista" class="treeview_root" style="font-size: 9px;">
	    	<li class="treeview_root"><a href="<?php print $config["site"]["absolutepath"] . "/szervezetek/"; ?>"><?php print $config["site"]["tree_root_element"]; ?></a>
			<?php
			$check = mysql_query("SELECT nev, id, teljes_id FROM szervezetek WHERE szervezet_tipus=5 OR id<8 ORDER BY teljes_id");
			while ($sor = mysql_fetch_assoc($check)) {
			    if (strlen($sor["nev"]))
				$sor["nev"] = preg_replace($search, $replace, $sor["nev"]);
			    $tree[$sor["teljes_id"]] = "<a href=\"" . $config["site"]["absolutepath"] . "/felhasznalok/<?php print \"\$q[1]\"; ?>/adatlap/szervezeti-besorolas-szerkesztese/" . $sor["id"] . "\">" . $sor["nev"] . "</a>";
			}
			$tree = $odin->explodeTree($tree, "-", true);
			$odin->array_to_ulli($tree);
			?>
	    	</li>
	        </ul>
	    </div>
	    <div class="main_left_spacer"></div>
 <?php
	    print (" <?php 
	}
		    ?>");

	    print (" <?php
 	if (\$_SESSION[\"user\"][\"id\"] && \$q[0] == \"szervezetek\" && \$q[2] == \"szervezeti-besorolas-szerkesztese\") {
	     ?>"); 
            unset($tree);
            ?>
	    <div class="main_left_container">
	        <div class="main_left_title"><font style="color:#617f10">SZERVEZET</font> Gyorsmenü</div>
	        <ul id="szervezetlista" class="treeview_root" style="font-size: 9px;">
	    	<li class="treeview_root"><a href="<?php print $config["site"]["absolutepath"] . "/szervezetek/"; ?>"><?php print $config["site"]["tree_root_element"]; ?></a>
			<?php
			$check = mysql_query("SELECT nev, id, teljes_id FROM szervezetek WHERE szervezet_tipus=5 OR id<8 ORDER BY teljes_id");
			while ($sor = mysql_fetch_assoc($check)) {
			    if (strlen($sor["nev"]))
				$sor["nev"] = preg_replace($search, $replace, $sor["nev"]);
			    $tree[$sor["teljes_id"]] = "<a href=\"" . $config["site"]["absolutepath"] . "/szervezetek/<?php print \"\$q[1]\"; ?>/szervezeti-besorolas-szerkesztese/" . $sor["id"] . "\">" . $sor["nev"] . "</a>";
			}
			$tree = $odin->explodeTree($tree, "-", true);
			$odin->array_to_ulli($tree);
			?>
	    	</li>
	        </ul>
	    </div>
	    <div class="main_left_spacer"></div>
 <?php
	    print (" <?php 
	}
		    ?>");



	$current = ob_get_contents();
	ob_end_clean();
	file_put_contents($file, $current);
    }
    
    function cache_graf_stat(){
	global $lang, $odin, $q, $config;
	
	$i["week"]=0;
	$i["day"]=0;
	
	$yesterday = date("Y")."-".date("m")."-".(date("d")-1);
	$yesterday_end_time = $yesterday . " 23:59:59";
	$yesterday_end_time=strtotime($yesterday_end_time);

	$this->no_user_abort(60);

	$file = 'cache/grafikus_stat.php';
	ob_start();
	
	//HETI NÉZETŰ STATISZTIKA
	$sql = "SELECT YEAR( FROM_UNIXTIME( ekkor ) ) AS year, WEEK( FROM_UNIXTIME( ekkor ) ) AS week, COUNT( id ) AS oldalletoltes
		FROM oldalletoltesek
		WHERE `ekkor` < '$yesterday_end_time'
		GROUP BY WEEK( FROM_UNIXTIME( ekkor ) )
		ORDER BY ekkor ASC";
	$result = mysql_query($sql) or die("MySQL hibaüzenet 337:" . mysql_error());
	    while ($sor = mysql_fetch_assoc($result)) {
$chart["week"][$sor["year"]][$sor["week"]]["oldalletoltes"] = $sor["oldalletoltes"];
	    }
	
	$sql = "SELECT YEAR( FROM_UNIXTIME( ekkor ) ) AS year, WEEK( FROM_UNIXTIME( ekkor ) ) AS week, COUNT( id ) AS keresesek
		FROM oldalletoltesek
		WHERE `keres` like \"%kereses%\" AND `ekkor` < '$yesterday_end_time'
		GROUP BY WEEK(FROM_UNIXTIME(ekkor))
		ORDER BY ekkor ASC";
	$result = mysql_query($sql) or die("MySQL hibaüzenet:" . mysql_error());
	    while ($sor = mysql_fetch_assoc($result)) {
$chart["week"][$sor["year"]][$sor["week"]]["keresesek"] = $sor["keresesek"];
	    }
	    
	$sql = "SELECT YEAR( FROM_UNIXTIME( ekkor ) ) AS year, WEEK( FROM_UNIXTIME( ekkor ) ) AS week, COUNT( id ) AS nembejelkeresesek
		FROM oldalletoltesek
		WHERE `keres` like \"%kereses%\" AND user_id=0 AND `ekkor` < '$yesterday_end_time'
		GROUP BY WEEK(FROM_UNIXTIME(ekkor))
		ORDER BY ekkor ASC";
	$result = mysql_query($sql) or die("MySQL hibaüzenet 358:" . mysql_error());
	    while ($sor = mysql_fetch_assoc($result)) {
$chart["week"][$sor["year"]][$sor["week"]]["nembejelkeresesek"] = $sor["nembejelkeresesek"];
	    }
	    
	$sql = "SELECT YEAR( FROM_UNIXTIME( aktiv_ekkor ) ) AS year, WEEK( FROM_UNIXTIME( aktiv_ekkor ) ) AS week, COUNT( id ) AS egyedi
		FROM munkamenetek
		WHERE user_id>0 AND `aktiv_ekkor` < '$yesterday_end_time'
		GROUP BY WEEK( FROM_UNIXTIME( aktiv_ekkor ) )
		ORDER BY aktiv_ekkor ASC";
	$result = mysql_query($sql) or die("MySQL hibaüzenet 368:" . mysql_error());
	    while ($sor = mysql_fetch_assoc($result)) {
$chart["week"][$sor["year"]][$sor["week"]]["egyedi"] = $sor["egyedi"];
	    }
	    
	//HAVI NÉZETŰ STATISZTIKA
	$sql = "SELECT YEAR( FROM_UNIXTIME( ekkor ) ) AS year, MONTH( FROM_UNIXTIME( ekkor ) ) AS month, DAY( FROM_UNIXTIME( ekkor ) ) AS day, COUNT( id ) AS oldalletoltes
		FROM oldalletoltesek
		WHERE `ekkor` < '$yesterday_end_time'
		GROUP BY DATE( FROM_UNIXTIME( ekkor ) )
		ORDER BY ekkor ASC";
	$result = mysql_query($sql) or die("MySQL hibaüzenet 378:" . mysql_error());
	    while ($sor = mysql_fetch_assoc($result)) {
$chart["day"][$sor["year"]][$sor["month"]][$sor["day"]]["oldalletoltes"] = $sor["oldalletoltes"];
	    }
	
	$sql = "SELECT YEAR( FROM_UNIXTIME( ekkor ) ) AS year, MONTH( FROM_UNIXTIME( ekkor ) ) AS month, DAY( FROM_UNIXTIME( ekkor ) ) AS day, COUNT( id ) AS keresesek
		FROM oldalletoltesek
		WHERE `keres` like \"%kereses%\" AND `ekkor` < '$yesterday_end_time'
		GROUP BY DATE(FROM_UNIXTIME(ekkor))
		ORDER BY ekkor ASC";
	$result = mysql_query($sql) or die("MySQL hibaüzenet 389:" . mysql_error());
	    while ($sor = mysql_fetch_assoc($result)) {
$chart["day"][$sor["year"]][$sor["month"]][$sor["day"]]["keresesek"] = $sor["keresesek"];
	    }
	    
	$sql = "SELECT YEAR( FROM_UNIXTIME( ekkor ) ) AS year, MONTH( FROM_UNIXTIME( ekkor ) ) AS month, DAY( FROM_UNIXTIME( ekkor ) ) AS day, COUNT( id ) AS nembejelkeresesek
		FROM oldalletoltesek
		WHERE `keres` like \"%kereses%\" AND user_id=0 AND `ekkor` < '$yesterday_end_time'
		GROUP BY DATE(FROM_UNIXTIME(ekkor))
		ORDER BY ekkor ASC";
	$result = mysql_query($sql) or die("MySQL hibaüzenet 399:" . mysql_error());
	    while ($sor = mysql_fetch_assoc($result)) {
$chart["day"][$sor["year"]][$sor["month"]][$sor["day"]]["nembejelkeresesek"] = $sor["nembejelkeresesek"];
	    }
	    
	$sql = "SELECT YEAR( FROM_UNIXTIME( aktiv_ekkor ) ) AS year, MONTH( FROM_UNIXTIME( aktiv_ekkor ) ) AS month, DAY( FROM_UNIXTIME( aktiv_ekkor ) ) AS day, COUNT( id ) AS egyedi
		FROM munkamenetek
		WHERE user_id>0 AND `aktiv_ekkor` < '$yesterday_end_time'
		GROUP BY DATE( FROM_UNIXTIME( aktiv_ekkor ) )
		ORDER BY aktiv_ekkor ASC";
	$result = mysql_query($sql) or die("MySQL hibaüzenet 409:" . mysql_error());
	    while ($sor = mysql_fetch_assoc($result)) {
$chart["day"][$sor["year"]][$sor["month"]][$sor["day"]]["egyedi"] = $sor["egyedi"];
	    }
	    print "<?php 
		";
	    print "\$chart="; 
	    var_export($chart);
	    print " ?>";
	?>
	
	<div class="main_center_spacer"></div>
	<div class="main_center_container">


	    <div class="main_center_title">
		<div class="main_center_title_left"><font style="color:#617f10">GRAFIKUS </font> Statisztika</div>
		<div class="main_center_title_right">
		    <form action="<?php print "<?php \$config[\"site\"][\"absolutepath\"] . \"/admin/grafikus-statisztika\"; ?>"; ?>" method="post">
			<select name="datum">
			    <?php 
			    print "<?php	
			if(\$_POST[\"datum\"]){
			    \$x=explode(\"-\",\$_POST[\"datum\"]);
			}
			else { \$x[0]=\"week\"; \$x[1]=date(\"Y\"); }
			    ?>
			    "; 
			    print "<?php
			    foreach(\$chart[\"day\"] as \$key => \$value)
				{
				print \"<option value=\\\"week-\$key\\\"\";
				    if ((\$x[0].\"-\".\$x[1])==(\"day-\$key\")){
				    print \" selected=\\\"selected\\\"\";
				    }
				    print \">\$key év - heti statisztika</option>\";
				foreach(\$value as \$subkey => \$subvalue)
				    {
				    print \"<option value=\\\"day-\$key-\$subkey\\\"\";
				    if ((\$x[0].\"-\".\$x[1].\"-\".\$x[2])==(\"day-\$key-\$subkey\")){
				    print \" selected=\\\"selected\\\"\";
				    }
				    print \">\$key-\$subkey hó - napi statisztika</option>\";
				    }
				} ?>";
			?>
			</select>
			<input class="ui-state-default ui-corner-all" type="submit" name="szukitsd" value="szűkítsd" />
		    </form>
		</div>
	    </div>
	    
	    <script src="js/amcharts/amcharts.js" type="text/javascript"></script>
	    <script src="js/amcharts/raphael.js" type="text/javascript"></script>

	    <script type="text/javascript">
		var chart;

		var chartData = [<?php print "<?php
	if (\$x[2]){
	  foreach (\$chart[\$x[0]][\$x[1]][\$x[2]] as \$key => \$value) {
		\$i[\"day\"]--;
		if (!\$value[\"egyedi\"])
		    \$value[\"egyedi\"] = 0;
		if (!\$value[\"nembejelkeresesek\"])
		    \$value[\"nembejelkeresesek\"] = 0;
		if (!\$value[\"keresesek\"])
		    \$value[\"keresesek\"] = 0;
		print (\"{
			 day: \$key,
			 egyedi: \" . \$value[\"egyedi\"] . \",
			 kereses: \" . \$value[\"keresesek\"] . \",
			 nembejelkereses: \" . \$value[\"nembejelkeresesek\"] . \"
			}\");
		    print \",\";
	    }  
	}
	else {
	foreach (\$chart[\$x[0]][\$x[1]] as \$key => \$value) {
		\$i[\"week\"]--;
		if (!\$value[\"egyedi\"])
		    \$value[\"egyedi\"] = 0;
		if (!\$value[\"nembejelkeresesek\"])
		    \$value[\"nembejelkeresesek\"] = 0;
		if (!\$value[\"keresesek\"])
		    \$value[\"keresesek\"] = 0;
		print (\"{
			 day: \$key,
			 egyedi: \" . \$value[\"egyedi\"] . \",
			 kereses: \" . \$value[\"keresesek\"] . \",
			 nembejelkereses: \" . \$value[\"nembejelkeresesek\"] . \"
			}\");
		    print \",\";
	    }
	}
	?>"
	    ?>];
		   
		    AmCharts.ready(function () {
			// SERIAL CHART
			chart = new AmCharts.AmSerialChart();
			chart.dataProvider = chartData;
			chart.categoryField = "day";
			chart.startDuration = 0.5;
			chart.balloon.color = "#000000";

			// AXES
			// category
			var categoryAxis = chart.categoryAxis;
			categoryAxis.fillAlpha = 1;
			categoryAxis.fillColor = "#FAFAFA";
			categoryAxis.gridAlpha = 0;
			categoryAxis.axisAlpha = 0;
			categoryAxis.gridPosition = "start";
			categoryAxis.position = "bottom";

			// value
			var valueAxis = new AmCharts.ValueAxis();
			valueAxis.title = "Érték";
			valueAxis.dashLength = 5;
			valueAxis.axisAlpha = 0;
			valueAxis.minimum = 0;
			valueAxis.integersOnly = true;
			valueAxis.gridCount = 20;
			chart.addValueAxis(valueAxis);

			// GRAPHS
		        					            		
			var graph = new AmCharts.AmGraph();
			graph.title = "Egyedi látogatók";
			graph.valueField = "egyedi";
			graph.balloonText = "Látogatók akik bejelentkeztek: [[value]]";
			graph.bullet = "round";
			chart.addGraph(graph);

			var graph = new AmCharts.AmGraph();
			graph.title = "Keresés nem bejelentkezve";
			graph.valueField = "nembejelkereses";
			graph.balloonText = "Keresések nem bejelentkezve: [[value]]";
			graph.bullet = "round";
			chart.addGraph(graph);
			
			var graph = new AmCharts.AmGraph();
			graph.title = "Összes Keresés";
			graph.valueField = "kereses";
			graph.balloonText = "Keresések: [[value]]";
			graph.bullet = "round";
			chart.addGraph(graph);

			// LEGEND
			var legend = new AmCharts.AmLegend();
			legend.markerType = "circle";
			chart.addLegend(legend);

			// WRITE
			chart.write("chartdiv");
		    });
	    </script>
	    <div id="chartdiv" style="width: 100%; height: 600px;"></div>
	    <div class="main_center_content_spacer"></div>

	</div>
		<?php
	$current = ob_get_contents();
	ob_end_clean();
	file_put_contents($file, $current);
	
	$time = date('Y-m-d H:i:s', time());
	$sql = "UPDATE modositva SET `gstat`= '$time' WHERE id=1";
	mysql_query($sql) or die("MySQL hibaüzenet:" . mysql_error());
    }
    
    function cache_site_stat(){
	global $lang, $odin, $q;
	
	$this->no_user_abort(60);
	
	$time = date('Y-m-d H:i:s', time());
	
	$file = 'cache/site_stat.php';
	ob_start();
	
	$from = strtotime("-30 day");
	$from60 = strtotime("-60 day");
	$stat["usercount"] = mysql_numrows(mysql_query("SELECT id FROM felhasznalok WHERE id>1"));
	;
	$stat["activeusercount"] = mysql_numrows(mysql_query("SELECT id FROM felhasznalok WHERE id>1 AND account_statusz>0"));
	;
	$stat["szervezetcount"] = mysql_numrows(mysql_query("SELECT id FROM szervezetek WHERE id>0 AND szervezet_tipus>1"));
	$stat["visitors"] = mysql_numrows(mysql_query("SELECT user_id FROM munkamenetek WHERE user_id>0 GROUP BY user_id"));
	$stat["visitors30"] = mysql_numrows(mysql_query("SELECT user_id, max(aktiv_ekkor) as lastact FROM munkamenetek WHERE user_id>0 AND aktiv_ekkor>\"$from\" GROUP BY user_id"));
	$stat["visitors60"] = mysql_numrows(mysql_query("SELECT user_id, max(aktiv_ekkor) as lastact FROM munkamenetek WHERE user_id>0 AND aktiv_ekkor<=\"$from\" AND aktiv_ekkor>\"$from60\" GROUP BY user_id"));
	$stat["avgdbq"] = 0;
	$stat["avgdbq30"] = 0;
	$stat["avgdbq60"] = 0;
	$stat["avgdbqs"] = 0;
	$stat["avgdbqs30"] = 0;
	$stat["avgdbqs60"] = 0;
	$stat["munkamenetek"] = 0;
	$stat["munkamenetek30"] = 0;
	$stat["munkamenetek60"] = 0;
	$stat["oldalletoltes"] = 0;
	$stat["oldalletoltes30"] = 0;
	$stat["oldalletoltes60"] = 0;
	$stat["oldalletoltesido"] = 0;
	$stat["oldalletoltesido30"] = 0;
	$stat["oldalletoltesido60"] = 0;
	$stat["peakoldalletoltesido30"] = 0;
	$stat["peakoldalletoltesido60"] = 0;
	$stat["keresesek"] = 0;
	$stat["keresesek30"] = 0;
	$stat["keresesek60"] = 0;
	$stat["keresesekido"] = 0;
	$stat["keresesekido30"] = 0;
	$stat["keresesekido60"] = 0;
	$stat["peakkeresesekido30"] = 0;
	$stat["peakkeresesekido60"] = 0;
	$stat["bongeszo"]["ff"] = 0;
	$stat["bongeszo"]["ie"] = 0;
	$stat["bongeszo"]["ch"] = 0;
	$stat["bongeszo"]["eg"] = 0;
	$stat["bongeszo30"]["ff"] = 0;
	$stat["bongeszo30"]["ie"] = 0;
	$stat["bongeszo30"]["ch"] = 0;
	$stat["bongeszo30"]["eg"] = 0;

	$s[0]=0;
	$s[1]=0;
	$s[2]=0;
	$s[3]=0;
	$s[4]=0;
	$s[5]=0;
	$s0[0]=0;
	$s0[1]=0;
	$s0[2]=0;
	$s0[3]=0;
	$s0[4]=0;
	$s0[5]=0;
	$s0[6]=0;
	$s0[7]=0;
	$s0[8]=0;
	$s0[9]=0;
	
	$sql = "SELECT * FROM oldalletoltesek";
	$result = mysql_query($sql);
	while ($sor = mysql_fetch_assoc($result)) {
	    $stat["oldalletoltes"]++;
	    
	    	if($sor["oldalletoltes"]<=1)
			{
			$s[0]++;
			}
		if($sor["oldalletoltes"]<=0.1)
			{
			$s0[0]++;
			}
		if($sor["oldalletoltes"]>0.1 && $sor["oldalletoltes"]<=0.2)
			{
			$s0[1]++;
			}
		if($sor["oldalletoltes"]>0.2 && $sor["oldalletoltes"]<=0.3)
			{
			$s0[2]++;
			}
		if($sor["oldalletoltes"]>0.3 && $sor["oldalletoltes"]<=0.4)
			{
			$s0[3]++;
			}
		if($sor["oldalletoltes"]>0.4 && $sor["oldalletoltes"]<=0.5)
			{
			$s0[4]++;
			}
		if($sor["oldalletoltes"]>0.5 && $sor["oldalletoltes"]<=0.6)
			{
			$s0[5]++;
			}
		if($sor["oldalletoltes"]>0.6 && $sor["oldalletoltes"]<=0.7)
			{
			$s0[6]++;
			}
		if($sor["oldalletoltes"]>0.7 && $sor["oldalletoltes"]<=0.8)
			{
			$s0[7]++;
			}
		if($sor["oldalletoltes"]>0.8 && $sor["oldalletoltes"]<=0.9)
			{
			$s0[8]++;
			}
		if($sor["oldalletoltes"]>0.9 && $sor["oldalletoltes"]<=1)
			{
			$s0[9]++;
			}
		if($sor["oldalletoltes"]>1 && $sor["oldalletoltes"]<=2)
			{
			$s[1]++;
			}
		if($sor["oldalletoltes"]>2 && $sor["oldalletoltes"]<=3)
			{
			$s[2]++;
			}
		if($sor["oldalletoltes"]>3 && $sor["oldalletoltes"]<=4)
			{
			$s[3]++;
			}
		if($sor["oldalletoltes"]>4 && $sor["oldalletoltes"]<=5)
			{
			$s[4]++;
			}
		if($sor["oldalletoltes"]>5)
			{
			$s[5]++;
			}
	    
	    $stat["avgdbq"] = $stat["avgdbq"] + $sor["adatbaziskeres"];
	    $stat["oldalletoltesido"] = $stat["oldalletoltesido"] + $sor["oldalletoltes"];
	    if ($sor["ekkor"] > ($from)) {
		$stat["avgdbq30"] = $stat["avgdbq30"] + $sor["adatbaziskeres"];
		$stat["oldalletoltes30"]++;
		$stat["oldalletoltesido30"] = $stat["oldalletoltesido30"] + $sor["oldalletoltes"];
		if ($stat["peakoldalletoltesido30"]<$sor["oldalletoltes"]) { $stat["peakoldalletoltesido30"]= $sor["oldalletoltes"]; }
		
	    } elseif ($sor["ekkor"] > ($from60)) {
		$stat["avgdbq60"] = $stat["avgdbq60"] + $sor["adatbaziskeres"];
		$stat["oldalletoltes60"]++;
		$stat["oldalletoltesido60"] = $stat["oldalletoltesido60"] + $sor["oldalletoltes"];
		if ($stat["peakoldalletoltesido60"]<$sor["oldalletoltes"]) { $stat["peakoldalletoltesido60"]= $sor["oldalletoltes"]; }
	    }
	    if (stristr($sor["keres"], "/kereses/")) {
		$stat["keresesek"]++;
		$stat["avgdbqs"] = $stat["avgdbqs"] + $sor["adatbaziskeres"];
		$stat["keresesekido"] = $stat["keresesekido"] + $sor["oldalletoltes"];
		if ($sor["ekkor"] > $from) {
		    $stat["avgdbqs30"] = $stat["avgdbqs30"] + $sor["adatbaziskeres"];
		    $stat["keresesek30"]++;
		    $stat["keresesekido30"] = $stat["keresesekido30"] + $sor["oldalletoltes"];
		    if ($stat["peakkeresesekido30"]<$sor["oldalletoltes"]) { $stat["peakkeresesekido30"]= $sor["oldalletoltes"]; }
		} elseif ($sor["ekkor"] > $from60) {
		    $stat["avgdbqs60"] = $stat["avgdbqs60"] + $sor["adatbaziskeres"];
		    $stat["keresesek60"]++;
		    $stat["keresesekido60"] = $stat["keresesekido60"] + $sor["oldalletoltes"];
		    if ($stat["peakkeresesekido60"]<$sor["oldalletoltes"]) { $stat["peakkeresesekido60"]= $sor["oldalletoltes"]; }
		}
	    }
	}
	$stat["avgdbq"] = round($stat["avgdbq"] / $stat["oldalletoltes"], 2);
	$stat["avgdbq30"] = round($stat["avgdbq30"] / $stat["oldalletoltes30"], 2);
	$stat["avgdbq60"] = round($stat["avgdbq60"] / $stat["oldalletoltes60"], 2);
	$stat["avgdbqs"] = round($stat["avgdbqs"] / $stat["keresesek"], 2);
	$stat["avgdbqs30"] = round($stat["avgdbqs30"] / $stat["keresesek30"], 2);
	$stat["avgdbqs60"] = round($stat["avgdbqs60"] / $stat["keresesek60"], 2);
	$stat["oldalletoltesido"] = round($stat["oldalletoltesido"] / $stat["oldalletoltes"], 2);
	$stat["oldalletoltesido30"] = round($stat["oldalletoltesido30"] / $stat["oldalletoltes30"], 2);
	$stat["oldalletoltesido60"] = round($stat["oldalletoltesido60"] / $stat["oldalletoltes60"], 2);
	$stat["keresesekido"] = round($stat["keresesekido"] / $stat["keresesek"], 2);
	$stat["keresesekido30"] = round($stat["keresesekido30"] / $stat["keresesek30"], 2);
	$stat["keresesekido60"] = round($stat["keresesekido60"] / $stat["keresesek60"], 2);
	
	
	$stat["peakoldalletoltesido30"] = round($stat["peakoldalletoltesido30"], 2);
	$stat["peakoldalletoltesido60"] = round($stat["peakoldalletoltesido60"], 2);
	$stat["peakkeresesekido30"] = round($stat["peakkeresesekido30"], 2);
	$stat["peakkeresesekido60"] = round($stat["peakkeresesekido60"], 2);

	if (!$account["letrehozva"]) {
	    $account["letrehozva"] = "A fiók az \"ELENOR\" rendszerből került átemelésre!";
	}
	if (!$account["elsoaktivalas"] && $_SESSION["user"]["account_statusz"]) {
	    $account["elsoaktivalas"] = "HIBA, HIÁNYZÓ REKORD!";
	}
	if (!$account["elsoaktivalas"] && !$_SESSION["user"]["account_statusz"]) {
	    $account["elsoaktivalas"] = "A felhasználói fiók inaktív!";
	}
	$sql = "SELECT id, aktiv_ekkor, bongeszo FROM munkamenetek";
	$result = mysql_query($sql);
	while ($sor = mysql_fetch_assoc($result)) {
	    $stat["munkamenetek"]++;
	    if ($sor["aktiv_ekkor"] > $from) {
		$stat["munkamenetek30"]++;
	    } elseif ($sor["aktiv_ekkor"] > $from60) {
		$stat["munkamenetek60"]++;
	    }
	    if (stristr($sor["bongeszo"], "firefox")) {
		$stat["bongeszo"]["ff"]++;
		if ($sor["aktiv_ekkor"] > $from) {
		    $stat["bongeszo30"]["ff"]++;
		}
	    } elseif (stristr($sor["bongeszo"], "MSIE")) {
		$stat["bongeszo"]["ie"]++;
		if ($sor["aktiv_ekkor"] > $from) {
		    $stat["bongeszo30"]["ie"]++;
		}
	    } elseif (stristr($sor["bongeszo"], "chrome")) {
		$stat["bongeszo"]["ch"]++;
		if ($sor["aktiv_ekkor"] > $from) {
		    $stat["bongeszo30"]["ch"]++;
		}
	    } else {
		$stat["bongeszo"]["eg"]++;
		if ($sor["aktiv_ekkor"] > $from) {
		    $stat["bongeszo30"]["eg"]++;
		}
	    }
	}
	?>
	<div class="main_center_spacer"></div>
	<div class="main_center_container">


	    <div class="main_center_title">
		<div class="main_center_title_left"><font style="color:#617f10">SZERVEZETI </font> Statisztika</div>
		<div class="main_center_title_right"><font style="font-size: 9px; font-weight: normal;">a statisztika összeállítva ekkor: </font><?php print  $time; ?></div>
	    </div>
	    <div class="main_center_content_spacer"></div>
	    <div class="main_center_content_left">Felhasználószám:</div><div class="main_center_content_center"><?php print ("<b>" . $stat["usercount"] . "</b>"); ?></div><div class="main_center_content_right"></div>
	    <div class="main_center_content_spacer"></div>
	    <div class="main_center_content_left">Aktív felhasználók:</div><div class="main_center_content_center"><?php print ("<b>" . $stat["activeusercount"] . "</b>"); ?></div><div class="main_center_content_right"><?php print "" . round(($stat["activeusercount"] / $stat["usercount"] * 100), 2) . "%"; ?></div>
	    <div class="main_center_content_spacer"></div>
	    <div class="main_center_content_left">Szervezet szám:</div><div class="main_center_content_center"><?php print ("<b>" . $stat["szervezetcount"] . "</b>"); ?></div><div class="main_center_content_right"></div>
	    <div class="main_center_content_spacer"></div>

	    <div class="main_center_title">
		<div class="main_center_title_left"><font style="color:#617f10">FREYA</font> Statisztika</div>
		<div class="main_center_title_right"></div>
	    </div>
	    <div class="main_center_content_spacer"></div>
	    <div class="main_center_content_left"></div><div class="main_center_content_center"><b>Utolsó 30 nap<br /></b>(az előző időszakhoz képest)</div><div class="main_center_content_right"><b>2012.01.10 óta</b></div>
	    <div class="main_center_content_spacer"></div>
	    <div class="main_center_content_spacer"></div>
	    <div class="main_center_content_spacer"></div>
	    <div class="main_center_content_left">Egyedi látogatók:</div><div class="main_center_content_center"><?php
	print ("<b>" . $stat["visitors30"] . "</b>");
	if (($stat["visitors30"] - $stat["visitors60"]) > 0)
	    print (" (<font style=\"color:green;\">+" . ($stat["visitors30"] - $stat["visitors60"]) . "</font>)"); else
	    print (" (<font style=\"color:red;\">" . ($stat["visitors30"] - $stat["visitors60"]) . "</font>)");
	?></div><div class="main_center_content_right"><?php print ($stat["visitors"]); ?></div>
	    <div class="main_center_content_spacer"></div>
	    <div class="main_center_content_left">Munkamenetet:</div><div class="main_center_content_center"><?php
	print ("<b>" . $stat["munkamenetek30"] . "</b>");
	if (($stat["munkamenetek30"] - $stat["munkamenetek60"]) > 0)
	    print (" (<font style=\"color:green;\">+" . ($stat["munkamenetek30"] - $stat["munkamenetek60"]) . "</font>)"); else
	    print (" (<font style=\"color:red;\">" . ($stat["munkamenetek30"] - $stat["munkamenetek60"]) . "</font>)");
	?></div><div class="main_center_content_right"><?php print ($stat["munkamenetek"]); ?></div>
	    <div class="main_center_content_spacer"></div>
	    <div class="main_center_content_left">Oldallatöltés:</div><div class="main_center_content_center"><?php
	print ("<b>" . $stat["oldalletoltes30"] . "</b>");
	if (($stat["oldalletoltes30"] - $stat["oldalletoltes60"]) > 0)
	    print (" (<font style=\"color:green;\">+" . ($stat["oldalletoltes30"] - $stat["oldalletoltes60"]) . "</font>)"); else
	    print (" (<font style=\"color:red;\">" . ($stat["oldalletoltes30"] - $stat["oldalletoltes60"]) . "</font>)");
	?></div><div class="main_center_content_right"><?php print ($stat["oldalletoltes"]); ?></div>
	    <div class="main_center_content_spacer"></div>
	    <div class="main_center_content_left">Átlagos oldalletöltési idő:</div><div class="main_center_content_center"><?php
	print ("<b>" . $stat["oldalletoltesido30"] . "s</b>");
	if (($stat["oldalletoltesido30"] - $stat["oldalletoltesido60"]) >= 0)
	    print (" (<font style=\"color:red;\">+" . ($stat["oldalletoltesido30"] - $stat["oldalletoltesido60"]) . "s</font>)"); else
	    print (" (<font style=\"color:green;\">" . ($stat["oldalletoltesido30"] - $stat["oldalletoltesido60"]) . "s</font>)");
	?></div><div class="main_center_content_right"><?php print ($stat["oldalletoltesido"] . "s"); ?></div>  
	    <div class="main_center_content_spacer"></div>
	    <div class="main_center_content_left">Max oldalletöltési idő:</div><div class="main_center_content_center"><?php
	print ("<b>" . $stat["peakoldalletoltesido30"] . "s</b>");
	if (($stat["peakoldalletoltesido30"] - $stat["peakoldalletoltesido60"]) >= 0)
	    print (" (<font style=\"color:red;\">+" . ($stat["peakoldalletoltesido30"] - $stat["peakoldalletoltesido60"]) . "s</font>)"); else
	    print (" (<font style=\"color:green;\">" . ($stat["peakoldalletoltesido30"] - $stat["peakoldalletoltesido60"]) . "s</font>)");
	?></div><div class="main_center_content_right"></div>  
	    <div class="main_center_content_spacer"></div>
	    <div class="main_center_content_left">Átlagos adatbáziskérés:</div><div class="main_center_content_center"><?php
	print ("<b>" . $stat["avgdbq30"] . "</b>");
	if (($stat["avgdbq30"] - $stat["avgdbq60"]) >= 0)
	    print (" (<font style=\"color:red;\">+" . ($stat["avgdbq30"] - $stat["avgdbq60"]) . "</font>)"); else
	    print (" (<font style=\"color:green;\">" . ($stat["avgdbq30"] - $stat["avgdbq60"]) . "</font>)");
	?></div><div class="main_center_content_right"><?php print ($stat["avgdbq"]); ?></div>
	    <div class="main_center_content_spacer"></div>
	    <div class="main_center_content_left">Végrehajtot keresések:</div><div class="main_center_content_center"><?php
	print ("<b>" . $stat["keresesek30"] . "</b>");
	if (($stat["keresesek30"] - $stat["keresesek60"]) > 0)
	    print (" (<font style=\"color:green;\">+" . ($stat["keresesek30"] - $stat["keresesek60"]) . "</font>)"); else
	    print (" (<font style=\"color:red;\">" . ($stat["keresesek30"] - $stat["keresesek60"]) . "</font>)");
	?></div><div class="main_center_content_right"><?php print ($stat["keresesek"]); ?></div>
		    <div class="main_center_content_spacer"></div>
	    <div class="main_center_content_left">Napi átlag keresés:</div><div class="main_center_content_center"><?php
	print ("<b>" . round(($stat["keresesek30"]/30),2) . "</b>");
	if (round(((round(($stat["keresesek30"]/30),2) - round(($stat["keresesek60"]/30),2)))/2,2) > 0)
	    print (" (<font style=\"color:green;\">+" . round(((round(($stat["keresesek30"]/30),2) - round(($stat["keresesek60"]/30),2)))/2,2) . "</font>)"); else
	    print (" (<font style=\"color:red;\">" . round(((round(($stat["keresesek30"]/30),2) - round(($stat["keresesek60"]/30),2)))/2,2) . "</font>)");
	?></div><div class="main_center_content_right"></div>
	    <div class="main_center_content_spacer"></div>
	    <div class="main_center_content_left">Átlagos keresési idő:</div><div class="main_center_content_center"><?php
	print ("<b>" . $stat["keresesekido30"] . "s</b>");
	if (($stat["keresesekido30"] - $stat["keresesekido60"]) >= 0)
	    print (" (<font style=\"color:red;\">+" . ($stat["keresesekido30"] - $stat["keresesekido60"]) . "s</font>)"); else
	    print (" (<font style=\"color:green;\">" . ($stat["keresesekido30"] - $stat["keresesekido60"]) . "s</font>)");
	?></div><div class="main_center_content_right"><?php print ($stat["keresesekido"] . "s"); ?></div>  
	    <div class="main_center_content_spacer"></div>
	    	    <div class="main_center_content_left">Max keresési idő:</div><div class="main_center_content_center"><?php
	print ("<b>" . $stat["peakkeresesekido30"] . "s</b>");
	if (($stat["peakkeresesekido30"] - $stat["peakkeresesekido60"]) >= 0)
	    print (" (<font style=\"color:red;\">+" . ($stat["peakkeresesekido30"] - $stat["peakkeresesekido60"]) . "s</font>)"); else
	    print (" (<font style=\"color:green;\">" . ($stat["peakkeresesekido30"] - $stat["peakkeresesekido60"]) . "s</font>)");
	?></div><div class="main_center_content_right"></div>  
	    <div class="main_center_content_spacer"></div>
	    <div class="main_center_content_left">Adatbáziskérés keresésnél:</div><div class="main_center_content_center"><?php
	print ("<b>" . $stat["avgdbqs30"] . "</b>");
	if (($stat["avgdbqs30"] - $stat["avgdbqs60"]) >= 0)
	    print (" (<font style=\"color:red;\">+" . ($stat["avgdbqs30"] - $stat["avgdbqs60"]) . "</font>)"); else
	    print (" (<font style=\"color:green;\">" . ($stat["avgdbqs30"] - $stat["avgdbqs60"]) . "</font>)");
	?></div><div class="main_center_content_right"><?php print ($stat["avgdbqs"]); ?></div>
	    <div class="main_center_content_spacer"></div>
	    
	    <div class="main_center_title">
		<div class="main_center_title_left"><font style="color:#617f10">OLDALLETÖLTÉS </font> Szórása</div>
		<div class="main_center_title_right"><a title="Kifejt" href="#" id="names-toogle" onclick="return false"><span class="ui-state-default ui-corner-all ui-icon ui-icon-help"></span></a></div>
	    </div>
	    <div class="main_center_content_spacer"></div>
	     	    <div class="main_center_content_left">x&LessFullEqual;1</div><div class="main_center_content_center"><?php print ("<b>" . $s[0] . "</b>"); ?></div><div class="main_center_content_right"><?php print round(((float)($s[0])*100/(float)($stat["oldalletoltes"])),2)."%"; ?></div>
	   <div class="main_center_content_spacer"></div>
	             <div id="main_center_content_names" class="ui-corner-all">
 	    <div class="main_center_content_left">x&LessFullEqual;0.1</div><div class="main_center_content_center"><?php print ("<b>" . $s0[0] . "</b>"); ?></div><div class="main_center_content_right"><?php print round(((float)($s0[0])*100/(float)($stat["oldalletoltes"])),2)."%"; ?></div>
	    <div class="main_center_content_spacer"></div>
	    <div class="main_center_content_left">0.1&lt;x&LessFullEqual;0.2:</div><div class="main_center_content_center"><?php print ("<b>" . $s0[1] . "</b>"); ?></div><div class="main_center_content_right"><?php print round(((float)($s0[1])*100/(float)($stat["oldalletoltes"])),2)."%"; ?></div>
	    <div class="main_center_content_spacer"></div>
	    	    <div class="main_center_content_left">0.2&lt;x&LessFullEqual;0.3:</div><div class="main_center_content_center"><?php print ("<b>" . $s0[2] . "</b>"); ?></div><div class="main_center_content_right"><?php print round(((float)($s0[2])*100/(float)($stat["oldalletoltes"])),2)."%"; ?></div>
	    <div class="main_center_content_spacer"></div>
	    	    <div class="main_center_content_left">0.3&lt;x&LessFullEqual;0.4:</div><div class="main_center_content_center"><?php print ("<b>" . $s0[3] . "</b>"); ?></div><div class="main_center_content_right"><?php print round(((float)($s0[3])*100/(float)($stat["oldalletoltes"])),2)."%"; ?></div>
	    <div class="main_center_content_spacer"></div>
	    	    <div class="main_center_content_left">0.4&lt;x&LessFullEqual;0.5:</div><div class="main_center_content_center"><?php print ("<b>" . $s0[4] . "</b>"); ?></div><div class="main_center_content_right"><?php print round(((float)($s0[4])*100/(float)($stat["oldalletoltes"])),2)."%"; ?></div>
	    <div class="main_center_content_spacer"></div>
	    	    <div class="main_center_content_left">0.5&lt;x&LessFullEqual;0.6:</div><div class="main_center_content_center"><?php print ("<b>" . $s0[5] . "</b>"); ?></div><div class="main_center_content_right"><?php print round(((float)($s0[5])*100/(float)($stat["oldalletoltes"])),2)."%"; ?></div>
	    <div class="main_center_content_spacer"></div>
	    	    <div class="main_center_content_left">0.6&lt;x&LessFullEqual;0.7:</div><div class="main_center_content_center"><?php print ("<b>" . $s0[6] . "</b>"); ?></div><div class="main_center_content_right"><?php print round(((float)($s0[6])*100/(float)($stat["oldalletoltes"])),2)."%"; ?></div>
	    <div class="main_center_content_spacer"></div>
	    	    <div class="main_center_content_left">0.7&lt;x&LessFullEqual;0.8:</div><div class="main_center_content_center"><?php print ("<b>" . $s0[7] . "</b>"); ?></div><div class="main_center_content_right"><?php print round(((float)($s0[7])*100/(float)($stat["oldalletoltes"])),2)."%"; ?></div>
	    <div class="main_center_content_spacer"></div>
	    	    <div class="main_center_content_left">0.8&lt;x&LessFullEqual;0.9:</div><div class="main_center_content_center"><?php print ("<b>" . $s0[8] . "</b>"); ?></div><div class="main_center_content_right"><?php print round(((float)($s0[8])*100/(float)($stat["oldalletoltes"])),2)."%"; ?></div>
	    <div class="main_center_content_spacer"></div>
	    	    <div class="main_center_content_left">0.9&lt;x&LessFullEqual;1.0:</div><div class="main_center_content_center"><?php print ("<b>" . $s0[9] . "</b>"); ?></div><div class="main_center_content_right"><?php print round(((float)($s0[9])*100/(float)($stat["oldalletoltes"])),2)."%"; ?></div>
	    <div class="main_center_content_spacer"></div>
                    </div>
	    <div class="main_center_content_left">1&lt;x&LessFullEqual;2:</div><div class="main_center_content_center"><?php print ("<b>" . $s[1] . "</b>"); ?></div><div class="main_center_content_right"><?php print round(((float)($s[1])*100/(float)($stat["oldalletoltes"])),2)."%"; ?></div>
	    <div class="main_center_content_spacer"></div>
	    <div class="main_center_content_left">2&lt;x&LessFullEqual;3:</div><div class="main_center_content_center"><?php print ("<b>" . $s[2] . "</b>"); ?></div><div class="main_center_content_right"><?php print round(((float)($s[2])*100/(float)($stat["oldalletoltes"])),2)."%"; ?></div>
	    <div class="main_center_content_spacer"></div>
	    <div class="main_center_content_left">3&lt;x&LessFullEqual;4:</div><div class="main_center_content_center"><?php print ("<b>" . $s[3] . "</b>"); ?></div><div class="main_center_content_right"><?php print round(((float)($s[3])*100/(float)($stat["oldalletoltes"])),2)."%"; ?></div>
	    <div class="main_center_content_spacer"></div>
	    <div class="main_center_content_left">4&lt;x&LessFullEqual;5:</div><div class="main_center_content_center"><?php print ("<b>" . $s[4] . "</b>"); ?></div><div class="main_center_content_right"><?php print round(((float)($s[4])*100/(float)($stat["oldalletoltes"])),2)."%"; ?></div>
	    <div class="main_center_content_spacer"></div>
	    <div class="main_center_content_left">5&lt;x:</div><div class="main_center_content_center"><?php print ("<b>" . $s[5] . "</b>"); ?></div><div class="main_center_content_right"><?php print round(((float)($s[5])*100/(float)($stat["oldalletoltes"])),2)."%"; ?></div>
	    <div class="main_center_content_spacer"></div>
	   
	    
	    <div class="main_center_title">
		<div class="main_center_title_left"><font style="color:#617f10">BÖNGÉSZŐ</font> Statisztika</div>
		<div class="main_center_title_right"></div>
	    </div>
	    <div class="main_center_content_spacer"></div>
	    <div class="main_center_content_left"></div><div class="main_center_content_center"><b>Utolsó 30 nap<br /></b></div><div class="main_center_content_right"><b>2012.01.10 óta</b></div>
	    <div class="main_center_content_spacer"></div>
	    <div class="main_center_content_spacer"></div>
	    <div class="main_center_content_spacer"></div>
	<?php
	foreach ($stat["bongeszo"] as $key => $value) {
	    ?>

	        <div class="main_center_content_left"><?php print ("" . $lang["bongeszo"]["$key"] . ""); ?></div><div class="main_center_content_center"><?php print ($stat["bongeszo30"]["$key"] . " - " . round($stat["bongeszo30"]["$key"] / $stat["munkamenetek30"] * 100, 2) . "%"); ?></div><div class="main_center_content_right"><?php print ($value . " - " . round($value / $stat["munkamenetek"] * 100, 2) . "%"); ?></div>

				<?php
			    }
			    ?>

	</div>

	<div class="main_center_spacer"></div>
	<?php
	$current = ob_get_contents();
	ob_end_clean();
	file_put_contents($file, $current);
	

	$sql = "UPDATE modositva SET `sstat`= '$time' WHERE id=1";
	mysql_query($sql) or die("MySQL hibaüzenet:" . mysql_error());
    }

    function cache_version_stat(){
	global $lang, $odin, $q;
	
	$this->no_user_abort(60);
		
	$sql="SELECT id, `oldalverzio`, count(`id`) AS `oldalletoltes`, sum(`adatbaziskeres`) AS `dbq`, sum(`oldalletoltes`) AS `ido` FROM `oldalletoltesek` WHERE `oldalverzio` LIKE \"%.%._\" GROUP BY `oldalverzio` ORDER BY id";
	$result=mysql_query($sql);
	    while ($sor = mysql_fetch_assoc($result))
		{
		$stat[$sor["oldalverzio"]]["avdbq"]=round($sor["dbq"]/$sor["oldalletoltes"],2);
		$stat[$sor["oldalverzio"]]["avsiteload"]=round($sor["ido"]/$sor["oldalletoltes"],2);
		}
	
	
	$time = date('Y-m-d H:i:s', time());
	
	$file = 'cache/verzio_stat.php';
	ob_start();
	
	?>
	<div class="main_center_spacer"></div>
	<div class="main_center_container">


	    <div class="main_center_title">
		<div class="main_center_title_left"><font style="color:#617f10">Verzió </font> Statisztika</div>
		<div class="main_center_title_right"><font style="font-size: 9px; font-weight: normal;">a statisztika összeállítva ekkor: </font><?php print  $time; ?></div>
	    </div>
	    <div class="main_center_content_spacer"></div>
	    <div class="main_center_content_left"></div><div class="main_center_content_center"><b>Adatbáziskérés<br /></b>(az előző verzióhoz képest)</div><div class="main_center_content_right"><b>Oldalletöltés</b><br />(az előző verzióhoz képest)</div>
	    <div class="main_center_content_spacer"></div>
	    <div class="main_center_content_spacer"></div>
	    <div class="main_center_content_spacer"></div>
	<?php foreach ($stat as $key => $value) { ?>
	        <div class="main_center_content_spacer"></div>
	        <div class="main_center_content_left"><?php print $key; ?>:</div><div class="main_center_content_center"><?php
	    print ("<b>" . $value["avdbq"] . "</b>");
	    if (($value["avdbq"] - $prev["avdbq"]) >= 0)
		print (" (<font style=\"color:red;\">+" . (number_format($value["avdbq"] - $prev["avdbq"], 2)) . "</font>)"); else
		print (" (<font style=\"color:green;\">" . (number_format($value["avdbq"] - $prev["avdbq"], 2)) . "</font>)");
	    ?></div><div class="main_center_content_right"><?php
		       print ("<b>" . number_format($value["avsiteload"], 2) . "s</b>");
		       if (($value["avsiteload"] - $prev["avsiteload"]) >= 0)
			   print (" (<font style=\"color:red;\">+" . (number_format($value["avsiteload"] - $prev["avsiteload"], 2)) . "s</font>)"); else
			   print (" (<font style=\"color:green;\">" . number_format(($value["avsiteload"] - $prev["avsiteload"]), 2) . "s</font>)");
		       ?></div>

					<?php
					$prev["avdbq"] = $value["avdbq"];
					$prev["avsiteload"] = $value["avsiteload"];
				    }
				    ?>
	</div>

	<div class="main_center_spacer"></div>
			    <?php
    	$current = ob_get_contents();
	ob_end_clean();
	file_put_contents($file, $current);
	
	$sql = "UPDATE modositva SET `vstat`= '$time' WHERE id=1";
	mysql_query($sql) or die("MySQL hibaüzenet:" . mysql_error());
			    
    }

}
?>
