<?php

class sajat_vilag {

    function __construct() {
	global $q;
	include_once("content/content_sajat_vilag.php");
	$this->content = new sajat_vilag_content();

	switch ($q[1]) {
	    case "": $this->adatlapom_kiir();
		break;
	    case "adatlapom":
		switch ($q[2]) {
		    case "rendszer-adatok-szerkesztese": $this->rendszer_adatok_modosit();
			break;
		    case "szemelyes-adatok-szerkesztese": $this->szemelyes_adatok_modosit();
			break;
		    case "privat-adatok-szerkesztese": $this->privat_adatok_modosit();
			break;
		    case "megjegyzes-szerkesztese": $this->megjegyzes_modosit();
			break;
		    case "ceges-elerhetosegek-szerkesztese": $this->ceges_elerhetosegek_modosit();
			break;
		    case "szervezet-szerkesztese": $this->szervezet_modosit();
			break;
		    default: $this->adatlapom_kiir();
			break;
		}
		break;
	    case "fiok_adataim": $this->fiok_adataim_kiir();
		break;
	    case "jogosultsagaim": $this->jogosultsagaim_kiir();
		break;
	    case "fiok_logom": $this->fiok_logom_kiir();
		break;
	    default:
		include_once("system/class_error_404.php");
		$error_404 = new error_404();
		break;
	}
    }

    /*     * ****************************************************** */

    function adatlapom_kiir() {
	$this->content->adatlapom_kiir();
    }

    /*     * ****************************************************** */

    function fiok_adataim_kiir() {
	$this->content->fiok_adataim_kiir();
    }

    /*     * ****************************************************** */

    function jogosultsagaim_kiir() {
	$this->content->jogosultsagaim_kiir();
    }

    /*     * ****************************************************** */

    function fiok_logom_kiir() {
	$this->content->fiok_logom_kiir();
    }

    /*     * ****************************************************** */

    function rendszer_adatok_modosit() {
	global $lang, $odin, $sql_query_count, $config, $siteloadlog;
	$error = false;
	//VISSZALÉPÉS
	if ($_POST["modosit"] == $lang["gomb"]["megse"]) {
	    header("location:" . $config["site"]["absolutepath"] . "/sajat-vilag");
	}
	//ELLENŐRZÉS
	if ($_POST["modosit"]) {
	    $sql = "SELECT azonosito, email FROM felhasznalok WHERE `id` <> \"" . $_SESSION["user"]["id"] . "\"";
	    $check = mysql_query($sql);
	    $sql_query_count++;
	    while ($sor = mysql_fetch_assoc($check)) {
		if (!strcmp($sor["azonosito"], $_POST["azonosito"]) || !strcasecmp($sor["email"], $_POST["email"])) {
		    $error.="Már létezik ilyen Felhasználói azonosító, vagy GroupWise cím<br />";
		}
	    }
	    if (strlen($_POST["azonosito"])<4 || strlen($_POST["azonosito"])>32) {
		$error.="Hibás felhasználói azonosító! (A felhasználó azonosító bármilyen minimum 4, maximum 32 karakter hosszúságú lehet)<br />";
	    }
	    if (strlen($_POST["email"]) < 4 || !stristr($_POST["email"], "@") || !stristr($_POST["email"], ".")) {
		$error.="Hibás GroupWise cím!<br />";
	    }
	    if (strlen($_POST["jelszo1"]) < 4 || $_POST["jelszo1"] != $_POST["jelszo2"]) {
		$error.="Hibás, vagy túl rövid jelszó!";
	    }
	}
	if ($_POST["megerosit"]) {
	    $sql = "SELECT * FROM felhasznalok WHERE id=\"" . $_SESSION["user"]["id"] . "\"";
	    $check = mysql_query($sql);
	    $sql_query_count++;
	    while ($sor = mysql_fetch_assoc($check)) {
		$checkpassresult = $odin->checkpass($sor, $_POST["oldpass"]);
	    }
	    if (!$checkpassresult) {
		$error.="Hibás jelszó! <br />";
	    }
	    if ($_POST["passkey"] != $_SESSION["user"]["passkey"]) {
		$error.="Hibás ellenőrzőkód! ".$_SESSION["user"]["passkey"]."";
	    }
	}
	//VÉGREHAJTÁS / MEGJELENÍTÁS
	if ($error) {
	    if ($_POST["modosit"]) {
		$this->content->rendszer_adatok_modosit($error);
	    }
	    if ($_POST["megerosit"]) {
		$this->content->rendszer_adatok_modosit_megerosit($error);
	    }
	} elseif ($_POST["modosit"]) {
	    $odin->send_mail($_POST["email"], "TELEFONKÖNYV Adatmódosítás", "Ez egy automatikus üzenet! \n\nA módosítás végrehajtásához ellenörzőkód szükséges:\n\nAz ellenőrzőkódod: " . $_SESSION["user"]["passkey"]);
	    $this->content->rendszer_adatok_modosit_megerosit($error);
	} elseif ($_POST["megerosit"]) {

	    $sql = "SELECT azonosito, email FROM felhasznalok WHERE `id` <> \"" . $_SESSION["user"]["id"] . "\"";
	    $check = mysql_query($sql);
	    $sql_query_count++;
	    while ($sor = mysql_fetch_assoc($check)) {
		if (!strcmp($sor["azonosito"], $_POST["azonosito"]) || !strcasecmp($sor["email"], $_POST["email"])) {
		    $error.="Már létezik ilyen Felhasználói azonosító, vagy GroupWise cím<br />";
		}
	    }
	    if (strlen($_POST["azonosito"])<4 || strlen($_POST["azonosito"])>32) {
		$error.="Hibás Felhasználói azonosító!<br />";
	    }
	    if (strlen($_POST["email"]) < 4 || !stristr($_POST["email"], "@") || !stristr($_POST["email"], ".")) {
		$error.="Hibás GroupWise cím!<br />";
	    }
	    if (strlen($_POST["jelszo1"]) < 4) {
		$error.="Hibás, vagy túl rövid jelszó!";
	    }
	    if (!$error) {
		if ($_SESSION["user"]["passkey"] == $_POST["passkey"]) {
		    $time = date('Y-m-d H:i:s', time());
		    $azonosito = str_replace(" ", "", $_POST["azonosito"]);
		    $email = str_replace(" ", "", $_POST["email"]);
		    $newpass = str_replace(" ", "", $_POST["jelszo1"]);
		    $newpassextension = substr(md5(rand(100, 10000000)), $startat, 8);
		    $passtodb = md5($newpass . "" . substr(md5($newpassextension), 3, 4) . "" . substr(md5($newpassextension), 11, 4));
		    $sql = "UPDATE felhasznalok SET `azonosito`= \"$azonosito\", `modositva_ekkor`= '$time', `modositva_altal`= \"" . $_SESSION["user"]["id"] . "\", `email`= \"$email\", `jelszo`= \"$passtodb\", `jelszo_kiegeszito`= \"$newpassextension\" WHERE id=\"" . $_SESSION["user"]["id"] . "\"";
		    mysql_query($sql) or die("MySQL hibaüzenet:" . mysql_error());
		    $sql = "UPDATE modositva SET `ekkor`= '$time' WHERE id=1";
		    mysql_query($sql) or die("MySQL hibaüzenet:" . mysql_error());
		    $siteloadlog["event"] = 3;
		    $siteloadlog["who"] = $_SESSION["user"]["id"];
		    $siteloadlog["what"] = "Rendszeradatok módosítva";
		    $odin->addsiteload();
		    header("Location:" . $config["site"]["absolutepath"] . "/sajat-vilag");
		    exit();
		}
	    } else {
		$this->content->rendszer_adatok_modosit($error);
	    }
	} else {
	    $this->content->rendszer_adatok_modosit();
	}
    }

    /*     * ****************************************************** */

    function szemelyes_adatok_modosit() {
	global $lang, $odin, $sql_query_count, $config, $siteloadlog;
	$error = false;
	//VISSZALÉPÉS
	if ($_POST["modosit"] == $lang["gomb"]["megse"]) {
	   header("location:" . $config["site"]["absolutepath"] . "/sajat-vilag");
	}
	//ELLENŐRZÉS
	if ($_POST["modosit"] == $lang["gomb"]["modosit"]) {
	    if (strlen($_POST["vezeteknev"]) < 3)
		$error["kiir"].="Hibás vezetéknév!<br />";
	    if (strlen($_POST["keresztnev"]) < 3)
		$error["kiir"].="Hibás keresztnev!<br />";
	    if (strlen($_POST["beosztas"]) < 3 && $_POST["beosztas"] && !$_POST["beosztas_id"])
		$error["kiir"].="Hibás beosztásnév!<br />";
	    if (strlen($_POST["rendfokozat"]) < 3 && $_POST["rendfokozat"] && !$_POST["rendfokozat_id"])
		$error["kiir"].="Hibás rendfokozat!<br />";
	    if (!$_POST["beosztas_id"] && !$_POST["beosztas"]) {
		$error["kiir"].="Adja meg kézileg a beosztást!<br />";
		$error["beosztas"] = true;
	    }
	    if (!$_POST["rendfokozat_id"] && !$_POST["rendfokozat"]) {
		$error["kiir"].="Adja meg kézileg a renfokozatot!<br />";
		$error["rendfokozat"] = true;
	    }
	}
	if ($error["kiir"]) {
	    $this->content->szemelyes_adatok_modosit($error);
	} elseif ($_POST["modosit"] == $lang["gomb"]["modosit"]) {
	    $time = date('Y-m-d H:i:s', time());
	    if ($_POST["beosztas"]) {
		$sql = "SELECT * FROM beosztasok";
		$check = mysql_query($sql);
		$sql_query_count++;
		while ($sor = mysql_fetch_assoc($check)) {
		    if (!strcasecmp($sor["beosztasnev"], $_POST["beosztas"])) {
			unset($_POST["beosztas"]);
			$_POST["beosztas_id"] = $sor["id"];
		    }
		}
	    }
	    if ($_POST["rendfokozat"]) {
		$sql = "SELECT * FROM rendfokozatok";
		$check = mysql_query($sql);
		$sql_query_count++;
		while ($sor = mysql_fetch_assoc($check)) {
		    if (!strcasecmp($sor["megnevezes"], $_POST["rendfokozat"])) {
			unset($_POST["rendfokozat"]);
			$_POST["rendfokozat_id"] = $sor["id"];
		    }
		}
	    }
	    if ($_POST["beosztas"]) {
		$sql = "INSERT INTO beosztasok (beosztasnev) VALUES ('" . $_POST["beosztas"] . "')";
		mysql_query($sql) or die("MySQL hibaüzenet:" . mysql_error());
		$siteloadlog["event"] = 8;
		$siteloadlog["what"] = $_POST["beosztas"];
		$odin->addsiteload();
		$sql = "SELECT id FROM beosztasok WHERE beosztasnev='" . $_POST["beosztas"] . "'";
		$check = mysql_query($sql);
		$sql_query_count++;
		while ($sor = mysql_fetch_assoc($check)) {
		    $_POST["beosztas_id"] = $sor["id"];
		}
	    }
	    if ($_POST["rendfokozat"]) {
		$sql = "INSERT INTO rendfokozatok (megnevezes) VALUES ('" . $_POST["rendfokozat"] . "')";
		mysql_query($sql) or die("MySQL hibaüzenet:" . mysql_error());
		$siteloadlog["event"] = 11;
		$siteloadlog["what"] = $_POST["rendfokozat"];
		$odin->addsiteload();
		$sql = "SELECT id FROM rendfokozatok WHERE megnevezes='" . $_POST["rendfokozat"] . "'";
		$check = mysql_query($sql);
		$sql_query_count++;
		while ($sor = mysql_fetch_assoc($check)) {
		    $_POST["rendfokozat_id"] = $sor["id"];
		}
	    }
	    $sql = "UPDATE felhasznalok SET `modositva_ekkor`= '$time', `modositva_altal`= \"" . $_SESSION["user"]["id"] . "\", `allomany_statusz`= \"" . $_POST["allomany_statusz"] . "\", `titulus`= \"" . $_POST["titulus"] . "\", `titulus_2`= \"" . $_POST["titulus_2"] . "\", `vezeteknev`= \"" . $_POST["vezeteknev"] . "\",`kozepsonev`= \"" . $_POST["kozepsonev"] . "\",`keresztnev`= \"" . $_POST["keresztnev"] . "\",`beosztas_id`= \"" . $_POST["beosztas_id"] . "\",`rendfokozat_id`= \"" . $_POST["rendfokozat_id"] . "\",`beosztas_megjegyzes`= \"" . $_POST["beosztas_megjegyzes"] . "\", `nicknev`= \"" . $_POST["nicknev"] . "\" WHERE id=\"" . $_SESSION["user"]["id"] . "\"";
	    mysql_query($sql) or die("MySQL hibaüzenet:" . mysql_error());
	    $sql = "UPDATE modositva SET `ekkor`= '$time' WHERE id=1";
	    mysql_query($sql) or die("MySQL hibaüzenet:" . mysql_error());
	    $siteloadlog["event"] = 3;
	    $siteloadlog["who"] = $_SESSION["user"]["id"];
	    $siteloadlog["what"] = "Szemelyes adatok módosítva";
	    $odin->addsiteload();
	    header("Location:" . $config["site"]["absolutepath"] . "/sajat-vilag");
	    exit();
	}
	else
	    $this->content->szemelyes_adatok_modosit();
    }

    /*     * ****************************************************** */

    function privat_adatok_modosit() {
	global $lang, $odin, $sql_query_count, $config, $siteloadlog;
	$error = false;
	$time = date('Y-m-d H:i:s', time());
	//VISSZALÉPÉS
	if ($_POST["modosit"] == $lang["gomb"]["megse"]) {
	    header("location:" . $config["site"]["absolutepath"] . "/sajat-vilag");
	}
	//ELLENŐRZÉS
	if ($_POST["modosit"] == $lang["gomb"]["modosit"]) {
	    $nevnap[0] = $_POST["nevnap"][5] . $_POST["nevnap"][6] . $_POST["nevnap"][8] . $_POST["nevnap"][9];
	    $nevnap[1] = $_POST["nevnap"][5] . $_POST["nevnap"][6];
	    $nevnap[2] = $_POST["nevnap"][8] . $_POST["nevnap"][9];
            $szuletesnap[0]=$_POST["szuletesnap"][0].$_POST["szuletesnap"][1].$_POST["szuletesnap"][2].$_POST["szuletesnap"][3];
            $szuletesnap[1]=$_POST["szuletesnap"][5].$_POST["szuletesnap"][6];
            $szuletesnap[2]=$_POST["szuletesnap"][8].$_POST["szuletesnap"][9];
            if($_POST["szuletesnap"] && (!is_numeric($szuletesnap[0]) || $szuletesnap[1]>12 || $szuletesnap[1]<1 || $szuletesnap[2]>31 || $szuletesnap[2]<1 )) 
            $error["kiir"].="Hibás születésnap!<br />".$szuletesnap[0].$szuletesnap[1].$szuletesnap[2];
	    if ($_POST["nevnap"] && ($nevnap[1] < 1 || $nevnap[1] > 12 || $nevnap[2] < 1 || $nevnap[2] > 31))
		$error["kiir"].="Hibás névnap!<br />";
	    if ($_POST["privat_mobil"] && (strlen($_POST["privat_mobil"]) != 9 || !is_numeric($_POST["privat_mobil"]))) {

		$error["kiir"].="Hibásan megadott mobil telefonszám! Helyes formátum: 209888777<br />";
	    }
	    if ($_POST["privat_vezetekes"] && (strlen($_POST["privat_vezetekes"]) != 8 || !is_numeric($_POST["privat_vezetekes"]))) {

		$error["kiir"].="Hibásan megadott vezetekes telefonszám! Helyes formátum: 72999888<br />";
	    }
	    if ($_POST["privat_fax"] && (strlen($_POST["privat_fax"]) < 8 || strlen($_POST["privat_fax"]) > 9 || !is_numeric($_POST["privat_fax"]))) {

		$error["kiir"].="Hibásan megadott fax telefonszám! Helyes formátum: 72999888 vagy 209888777<br />";
	    }
	    if ($_POST["privat_email"] && (strlen($_POST["privat_email"]) < 4 || !stristr($_POST["privat_email"], "@") || !stristr($_POST["privat_email"], "."))) {

		$error["kiir"].="Hibásan megadott email cím! Helyes formátum: akarmi@domain.hu <br />";
	    }
	}
	if ($error) {
	    $this->content->privat_adatok_modosit($error);
	} elseif ($_POST["modosit"] == $lang["gomb"]["modosit"]) {
	    $sql = "UPDATE felhasznalok SET `nevnap`='" . $nevnap[0] . "',`szuletesnap`='" . $_POST["szuletesnap"] . "', `modositva_ekkor`= '$time', `modositva_altal`= \"" . $_SESSION["user"]["id"] . "\", `privat_mobil`= '" . $_POST["privat_mobil"] . "',`privat_vezetekes`= '" . $_POST["privat_vezetekes"] . "', `privat_fax`= '" . $_POST["privat_fax"] . "', `privat_email`= '" . $_POST["privat_email"] . "'  WHERE id=\"" . $_SESSION["user"]["id"] . "\"";
	    mysql_query($sql) or die("MySQL hibaüzenet:" . mysql_error());
	    $sql = "UPDATE modositva SET `ekkor`= '$time' WHERE id=1";
	    mysql_query($sql) or die("MySQL hibaüzenet:" . mysql_error());
	    $siteloadlog["event"] = 3;
	    $siteloadlog["who"] = $_SESSION["user"]["id"];
	    $siteloadlog["what"] = "Privát adatok módosítva";
	    $odin->addsiteload();
	    header("Location:" . $config["site"]["absolutepath"] . "/sajat-vilag");
	    exit();
	}
	else
	    $this->content->privat_adatok_modosit($error);
    }

    /*     * ****************************************************** */

    function megjegyzes_modosit() {
	global $lang, $odin, $sql_query_count, $config, $siteloadlog;
	$error = false;
	$time = date('Y-m-d H:i:s', time());
	//VISSZALÉPÉS
	if ($_POST["modosit"] == $lang["gomb"]["megse"]) {
	    header("location:" . $config["site"]["absolutepath"] . "/sajat-vilag");
	}
	//ELLENŐRZÉS
	if ($_POST["modosit"] == $lang["gomb"]["modosit"]) {
	    
	}
	if ($error) {
	    $this->content->megjegyzes_modosit($error);
	} elseif ($_POST["modosit"] == $lang["gomb"]["modosit"]) {
	    $_POST["megjegyzes"] = $_POST["megjegyzes"];
	    $sql = "UPDATE felhasznalok SET `modositva_ekkor`= '$time', `modositva_altal`= \"" . $_SESSION["user"]["id"] . "\", `megjegyzes`='" . $_POST["megjegyzes"] . "' WHERE id=\"" . $_SESSION["user"]["id"] . "\"";
	    mysql_query($sql) or die("MySQL hibaüzenet:" . mysql_error());
	    $sql = "UPDATE modositva SET `ekkor`= '$time' WHERE id=1";
	    mysql_query($sql) or die("MySQL hibaüzenet:" . mysql_error());
	    $siteloadlog["event"] = 3;
	    $siteloadlog["who"] = $_SESSION["user"]["id"];
	    $siteloadlog["what"] = "Megjegyzés módosítva";
	    $odin->addsiteload();
	    header("Location:" . $config["site"]["absolutepath"] . "/sajat-vilag");
	    exit();
	}
	else
	    $this->content->megjegyzes_modosit($error);
    }

    /*     * ****************************************************** */

    function szervezet_modosit() {
	global $lang, $odin, $sql_query_count, $config, $siteloadlog, $q;
	$error = false;
	$details=$odin->get_szervezet_details($_POST["szervezet_id"]);
	$time = date('Y-m-d H:i:s', time());
	//VISSZALÉPÉS
	if ($_POST["modosit"] == $lang["gomb"]["megse"]) {
	    header("location:".$config["site"]["absolutepath"]."/sajat-vilag");
	}
	//ELLENŐRZÉS
	if ($_POST["modosit"] == $lang["gomb"]["athelyez"] && (!$_POST["szervezet_id"] || $details["szervezet_tipus"]==1)) {
		$error["kiir"].="Ebbe a szervezetbe nem lehet felhasználót áthelyezni!<br />";
	}
	if ($_POST["modosit"] == $lang["gomb"]["athelyez"]) {
	    if (!mysql_num_rows(mysql_query("SELECT id FROM szervezetek WHERE id = '" . $_POST["szervezet_id"] . "'"))) {
		$error["kiir"].="Nem létező célszerv!<br />";
	    }
	}
	if ($error) {
	    $this->content->szervezet_modosit($error);
	} elseif ($_POST["modosit"] == $lang["gomb"]["athelyez"]) {
	    $sql = "UPDATE felhasznalok SET `modositva_ekkor`= '$time', `modositva_altal`= \"" . $_SESSION["user"]["id"] . "\", `szervezet_id`='" . $_POST["szervezet_id"] . "' WHERE id=\"" . $_SESSION["user"]["id"] . "\"";
	    mysql_query($sql) or die("MySQL hibaüzenet:" . mysql_error());
	    $sql = "UPDATE modositva SET `ekkor`= '$time' WHERE id=1";
	    mysql_query($sql) or die("MySQL hibaüzenet:" . mysql_error());
	    $siteloadlog["event"] = 3;
	    $siteloadlog["who"] = $_SESSION["user"]["id"];
	    $siteloadlog["what"] = "Szervezet módosítva";
	    $odin->addsiteload();
	    header("Location:" . $config["site"]["absolutepath"] . "/sajat-vilag");
	    exit();
	}
	else
	    $this->content->szervezet_modosit($error);
    }

    /*     * ****************************************************** */

    function ceges_elerhetosegek_modosit() {
	global $lang, $odin, $sql_query_count, $config, $siteloadlog;
	$error = false;
	$time = date('Y-m-d H:i:s', time());
	//VISSZALÉPÉS
	if ($_POST["modosit"] == $lang["gomb"]["megse"]) {
	    header("location:" . $config["site"]["absolutepath"] . "/sajat-vilag");
	}
	//ELLENŐRZÉS
	if ($_POST["modosit"] == $lang["gomb"]["modosit"]) {
	    if ($_POST["telephely_irsz"] && (strlen($_POST["telephely_irsz"]) != 4 || !is_numeric($_POST["telephely_irsz"]))) {

		$error["kiir"].="Hibásan megadott telephely irányítószám! Helyes formátum: 7600<br />";
	    }
	    if ($_POST["levelezesi_irsz"] && (strlen($_POST["levelezesi_irsz"]) != 4 || !is_numeric($_POST["levelezesi_irsz"]))) {

		$error["kiir"].="Hibásan megadott levelezési irányítószám! Helyes formátum: 7600<br />";
	    }
	    if ($_POST["vezetoi"] && (strlen($_POST["vezetoi"]) != 6 || !is_numeric($_POST["vezetoi"]))) {

		$error["kiir"].="Hibásan megadott vezetői szám! Helyes formátum: 231111<br />";
	    }
	    if ($_POST["kozvetlen"] && (strlen($_POST["kozvetlen"]) != 6 || !is_numeric($_POST["kozvetlen"]))) {

		$error["kiir"].="Hibásan megadott közvetlen szám! Helyes formátum: 231111<br />";
	    }
	    if ($_POST["2helyi"] && (strlen($_POST["2helyi"]) != 6 || !is_numeric($_POST["2helyi"]))) {

		$error["kiir"].="Hibásan megadott 2. helyi szám! Helyes formátum: 231111<br />";
	    }
	    if ($_POST["fax"] && (strlen($_POST["fax"]) != 6 || !is_numeric($_POST["fax"]))) {

		$error["kiir"].="Hibásan megadott fax szám! Helyes formátum: 231111<br />";
	    }
	    if ($_POST["tetra"] && (strlen($_POST["tetra"]) != 7 || !is_numeric($_POST["tetra"]))) {

		$error["kiir"].="Hibásan megadott tetra szám! Helyes formátum: 3230000<br />";
	    }
	    if ($_POST["mobil"] && (strlen($_POST["mobil"]) != 9 || !is_numeric($_POST["mobil"]))) {

		$error["kiir"].="Hibásan megadott mobil telefonszám! Helyes formátum: 209888777<br />";
	    }
	    if ($_POST["vez_kozvetlen"] && (strlen($_POST["vez_kozvetlen"]) < 8 || !is_numeric($_POST["vez_kozvetlen"]))) {

		$error["kiir"].="Hibásan megadott vezetékes közvetlen telefonszám! Helyes formátum: 72999888<br />";
	    }
	    if ($_POST["vez_fax"] && (strlen($_POST["vez_fax"]) < 8 || !is_numeric($_POST["vez_fax"]))) {

		$error["kiir"].="Hibásan megadott vezetékes fax telefonszám! Helyes formátum: 72999888<br />";
	    }
	}


	if ($error) {
	    $this->content->ceges_elerhetosegek_modosit($error);
	} elseif ($_POST["modosit"] == $lang["gomb"]["modosit"]) {
	    $sql = "UPDATE felhasznalok SET `modositva_ekkor`= '$time', ";

	    $_POST["telephely"] = $_POST["telephely_irsz"] . " " . $_POST["telephely_varos"] . ", " . $_POST["telephely_cim"] . " " . $_POST["telephely_szam"];
	    if (strlen($_POST["telephely"]) == 4) {
		$_POST["telephely_varos"] = "";
	    };
	    if ($_POST["telephely"] && $_SESSION["user"]["szervtol"]["telephely"] != $_POST["telephely"]) {
		$sql.=" `telephely_irsz`='" . $_POST["telephely_irsz"] . "', `telephely_varos`='" . $_POST["telephely_varos"] . "', `telephely_utca`='" . $_POST["telephely_cim"] . "', `telephely_cim`='" . $_POST["telephely_szam"] . "',";
	    } else {
		$sql.=" `telephely_irsz`=NULL, `telephely_varos`=NULL, `telephely_utca`=NULL, `telephely_cim`=NULL,";
	    }
	    if ($_POST["telephely_epulet"]) {
		$sql.=" `telephely_epulet`='" . $_POST["telephely_epulet"] . "',";
	    } else {
		$sql.="`telephely_epulet`=NULL,";
	    }
	    if ($_POST["telephely_emelet"]) {
		$sql.=" `telephely_emelet`='" . $_POST["telephely_emelet"] . "',";
	    } else {
		$sql.="`telephely_emelet`=NULL,";
	    }
	    if ($_POST["telephely_iroda"]) {
		$sql.=" `telephely_iroda`='" . $_POST["telephely_iroda"] . "',";
	    } else {
		$sql.="`telephely_iroda`=NULL,";
	    }
	    if ($_POST["vezetoi"]) {
		$sql.=" `vezetoi`='" . $_POST["vezetoi"] . "',";
	    } else {
		$sql.="`vezetoi`=NULL,";
	    }
	    if ($_POST["kozvetlen"]) {
		$sql.=" `kozvetlen`='" . $_POST["kozvetlen"] . "',";
	    } else {
		$sql.="`kozvetlen`=NULL,";
	    }
	    if ($_POST["2helyi"]) {
		$sql.=" `2helyi`='" . $_POST["2helyi"] . "',";
	    } else {
		$sql.="`2helyi`=NULL,";
	    }
	    if ($_POST["fax"] && $_SESSION["user"]["szervtol"]["fax"] != $_POST["fax"]) {
		$sql.=" `fax`='" . $_POST["fax"] . "',";
	    } else {
		$sql.="`fax`=NULL,";
	    }
	    if ($_POST["mobil"]) {
		$sql.=" `mobil`='" . $_POST["mobil"] . "',";
	    } else {
		$sql.="`mobil`=NULL,";
	    }
	    if ($_POST["vez_kozvetlen"]) {
		$sql.=" `vez_kozvetlen`='" . $_POST["vez_kozvetlen"] . "',";
	    } else {
		$sql.="`vez_kozvetlen`=NULL,";
	    }
	    if ($_POST["vez_fax"] && $_SESSION["user"]["szervtol"]["vez_fax"] != $_POST["vez_fax"]) {
		$sql.=" `vez_fax`='" . $_POST["vez_fax"] . "',";
	    } else {
		$sql.="`vez_fax`=NULL,";
	    }
	    if ($_POST["tetra"]) {
		$sql.=" `tetra`='" . $_POST["tetra"] . "',";
	    } else {
		$sql.="`tetra`=NULL,";
	    }
	    $sql.="`modositva_altal`= '" . $_SESSION["user"]["id"] . "'";
	    $sql.=" WHERE id='" . $_SESSION["user"]["id"] . "'";
	    print $sql;
	    mysql_query($sql) or die("MySQL hibaüzenet:" . mysql_error());
	    $sql = "UPDATE modositva SET `ekkor`= '$time' WHERE id=1";
	    mysql_query($sql) or die("MySQL hibaüzenet:" . mysql_error());
	    $siteloadlog["event"] = 3;
	    $siteloadlog["who"] = $_SESSION["user"]["id"];
	    $siteloadlog["what"] = "Céges elérhetőségek";
	    $odin->addsiteload();
	    header("Location:" . $config["site"]["absolutepath"] . "/sajat-vilag");
	    exit();
	}
	else
	    $this->content->ceges_elerhetosegek_modosit($error);
    }

    /*     * ****************************************************** */
}

?>
