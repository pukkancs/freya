<?php

class heimdallr {

    function __construct() {
	global $sql_query_count, $siteloadlog;
	global $config;
	global $odin;

	if (!isset($_SESSION["user"]["id"])) { //ELLENŐRIZZÜK, HOGY BE VAN-E JELENTKEZVE
	    
	    if (!isset($_COOKIE["Uid"]) && !isset($_POST['bejelentkezes'])) { //NINCS BEJELENTKEZVE, SE COOKIE SE BEJELENTKEZÉSI ADATOK, AKKOR A BEJELENTKEZŐ OLDALRA
		
		$odin->addsiteload();
		header("Location:" . $config["site"]["absolutepath"] . "/bejelentkezes");
		exit();
	    } else if (isset($_COOKIE["Uid"]) && !isset($_POST['bejelentkezes'])) { //Megnézzük, hogy van e létező cookie //Ha érvényes Cookie, akkor beállítjuk az user->id SESSIONT. Ha nem érvényes, visza a bejelentkező oldalra, hibaüzenettel
		
		$uid = $_COOKIE["Uid"];
		$passkey = $_COOKIE["Passkey"];
		$ipcookie = "" . substr(md5($_SERVER['REMOTE_ADDR']), 3, 4) . "" . substr(md5($_SERVER['REMOTE_ADDR']), 11, 4);
		$check = mysql_query("SELECT * FROM felhasznalok WHERE id = '$uid'");
		$sql_query_count++;
		while ($sor = mysql_fetch_assoc($check)) {
		    if ($passkey != $sor['passkey'] || $ipcookie != $_COOKIE["Ipcookie"] || time() > strtotime($sor["passkey_ervenyes"])) { // ha nem érvényes a cookie
			
			$odin->addsiteload();
			unset($_SESSION);
			session_destroy();
			header("Location:" . $config["site"]["absolutepath"] . "/bejelentkezes"); // vissza a bejelentkező oldalra !cookie!
			exit();
		    } else { // ha érvényes a cookie
			
			$siteloadlog["event"]=16;
			$siteloadlog["who"]=0;
			$siteloadlog["what"]="Bejelentkezés Sütivel";
			$odin->add_session_db($uid);
			$this->get_user_details($sor);
		    }
		}
	    } else if (isset($_POST['bejelentkezes'])) { //HA A BEJELENTKEZŐ FORM EL LETT KÜLDVE
		
		if (!$_POST['name'] || !$_POST['pass']) { //Ha nem lett kitöltve a felhasználónév vagy jelszó
		    
		    $odin->addsiteload();
		    header("Location:" . $config["site"]["absolutepath"] . "/bejelentkezes/hiba/hianyos-kitoltes"); // vissza a bejelentkező oldalra + hiba
		    exit();
		}
		$check = mysql_query("SELECT * FROM felhasznalok WHERE azonosito = '" . $_POST['name'] . "'"); //felhasználó lekérése a helyi adatbázisból
		$sql_query_count++;
		
		$check2 = mysql_num_rows($check);
		if ($check2 != 1) { //ellenőrzom h szerepelt-e ( ha 0 akkor nem, 1 akkor szerepelt, ha jól van megírva a felviteli script akkor pedig 1-nél több nem lehet
		    
		    
		    $odin->addsiteload();
		    header("Location:" . $config["site"]["absolutepath"] . "/bejelentkezes/hiba/hibas-felhasznalonev-vagy-jelszo"); //Bejelentkező oldal + hibaüzenet, ha nem szerepel
		    exit();
		} else { // ha szerepelt
		    while ($sor = mysql_fetch_assoc($check)) {
			$checkpass = $this->checkpass($sor);
			if ($sor["azonosito"] != $_POST["name"] || $checkpass != true) {
			    
			    $odin->addsiteload();
			    header("Location:" . $config["site"]["absolutepath"] . "/bejelentkezes/hiba/hibas-felhasznalonev-vagy-jelszo"); //Bejelentkező oldal + hibaüzenet, ha nem szerepel
			    exit();
			} else {
			    
			    $uid = $sor["id"];
			    $until = time() + $config["site"]["cookie_validtime"];
			    $startat = rand(0, 24);
			    $newpasskey = substr(md5(rand(0, 100000)), $startat, 8);
			    $ipcookie = "" . substr(md5($_SERVER['REMOTE_ADDR']), 3, 4) . "" . substr(md5($_SERVER['REMOTE_ADDR']), 11, 4);
			    mysql_query("UPDATE felhasznalok SET passkey = '$newpasskey', passkey_ervenyes = '" . date('Y-m-d H:i:s', $until) . "' WHERE id = '$uid'"); //biztonsági frissítés + a sikeres bejelentkezés miatt új legkésöbbi érvényességet kap a cookie (nembiztos h használja)
			    $sql_query_count++; //hátrány! ha máshol bejelentkezünk, a gépünkön lévő coockie érvényét veszti
			    if (isset($_POST["lowseclogin"])) { // ha bejelölte a csökkentett biztonságot // létrehozom a cookiekat
				setcookie("Uid", "" . $sor['id'] . "", $until);
				setcookie("Passkey", "$newpasskey", $until);
				setcookie("Ipcookie", "$ipcookie", $until); // cookie frissítése
                                $_COOKIE["Uid"]=$sor["id"];
                                $_COOKIE["Passkey"]=$newpasskey;
                                $_COOKIE["Ipcookie"]=$ipcookie;
				
			    }
			    $siteloadlog["event"]=16;
			    $siteloadlog["who"]=$uid;
			    $siteloadlog["what"]="Bejelentkezés felhasználónév / jelszóval";
			    $odin->add_session_db($uid);
			    $this->get_user_details($sor); // lekérem az adatait + frissítem az utolsó aktivitás időpontját   
			    mysql_query("UPDATE munkamenetek SET kijelentkezve=NULL WHERE session_id='" . $_SESSION["user"]["sessionid"] . "' AND user_id = '" . $_SESSION["user"]["id"] . "'") or die("MySQL hibaüzenet:" . mysql_error());
			}
		    }
		}
	    }
	}
	if (isset($_SESSION["user"]["id"])) { //ENNEK LE KELL FUTNIA HA IDŐKÖZBEN BEJELENTKEZTÜNK ( ezért nem ELSE )
	    
	    $odin->update_session_db();
	    $this->check_user_details($_SESSION["user"]["id"]); // ellenőrzöm a user adatait ( változott-e menet közben, jogosult-e a rendszer használatára )
	    $this->check_user_status(); // user státuszának ellenőrzése
	}
    }

    function checkpass($sor) {
	global $odin;
	$getextpass = md5($_POST["pass"] . "" . substr(md5($sor["jelszo_kiegeszito"]), 3, 4) . "" . substr(md5($sor["jelszo_kiegeszito"]), 11, 4));
	if ($sor["jelszo"] == $getextpass) {
	    return true;
	}
    }

    function get_user_details($mysql_result_variable) {
	global $sql_query_count;
	global $config;
	global $odin;
	$sessionid=$_SESSION["user"]["sessionid"];
	unset($_SESSION["user"]);
	$_SESSION["user"]["sessionid"]=$sessionid;
	while (list ($key, $val) = each($mysql_result_variable)) {
		$_SESSION["user"]["" . $key . ""] = $val;
	}
	while (list ($key, $val) = each($mysql_result_variable)) {
		$_SESSION["user"]["original"]["" . $key . ""] = $val;
	}
	$_SESSION["user"]["teljesnev"] = $_SESSION["user"]["titulus"] . " " . $_SESSION["user"]["vezeteknev"] . " " . $_SESSION["user"]["kozepsonev"] . " " . $_SESSION["user"]["keresztnev"];
	/* EZ A FUNKCIÓ ÉRTELMÉT VESZTETTE ---- 
        $_SESSION["user"]["nem"] = substr($_SESSION["user"]["azonosito"], 0, 1);
	$_SESSION["user"]["szuletesnap"] = substr($_SESSION["user"]["azonosito"], 1, 6);
	switch ($_SESSION["user"]["nem"]){
	case 1:
	$_SESSION["user"]["nem"] = 1;
	$_SESSION["user"]["szuletesnap"] = "19" . $_SESSION["user"]["szuletesnap"];
	break;

	case 3:
	$_SESSION["user"]["nem"] = 1;
	$_SESSION["user"]["szuletesnap"] = "20" . $_SESSION["user"]["szuletesnap"];
	break;

	case 5:
	$_SESSION["user"]["nem"] = 1;
	$_SESSION["user"]["szuletesnap"] = "19" . $_SESSION["user"]["szuletesnap"];
	break;

	case 7:
	$_SESSION["user"]["nem"] = 1;
	$_SESSION["user"]["szuletesnap"] = "20" . $_SESSION["user"]["szuletesnap"];
	break;

	case 2:
	$_SESSION["user"]["nem"] = 2;
	$_SESSION["user"]["szuletesnap"] = "19" . $_SESSION["user"]["szuletesnap"];
	break;

	case 4:
	$_SESSION["user"]["nem"] = 2;
	$_SESSION["user"]["szuletesnap"] = "20" . $_SESSION["user"]["szuletesnap"];
	break;
	case 6:
	$_SESSION["user"]["nem"] = 2;
	$_SESSION["user"]["szuletesnap"] = "19" . $_SESSION["user"]["szuletesnap"];
	break;

	case 8:
	$_SESSION["user"]["nem"] = 2;
	$_SESSION["user"]["szuletesnap"] = "20" . $_SESSION["user"]["szuletesnap"];
	break;

	default:
	$_SESSION["user"]["nem"] = "";
	$_SESSION["user"]["szuletesnap"] = "";
	break;
	} 
        */


	$result = mysql_query("SELECT * FROM szervezetek WHERE id = '" . $_SESSION["user"]["szervezet_id"] . "'");
	$sql_query_count++;
	while ($sor = mysql_fetch_assoc($result)) {
	    while (list ($key, $val) = each($sor)) {
		$_SESSION["user"]["szervezet_" . $key . ""] = $val;
	    }
	}
	$_SESSION["user"]["lastchecked"] = time();
        while ($sor = mysql_fetch_assoc($result)) {
            while (list ($key, $val) = each($sor)) {
                $_SESSION["user"]["szervezet_" . $key . ""] = $val;
            }
        }
        unset($_SESSION["user"]["szervezet_idk"]);
	$_SESSION["user"]["szervezet_idk"] = explode("-", $_SESSION["user"]["szervezet_teljes_id"]);
        $i = 0;
        while (list ($key, $val) = each($_SESSION["user"]["szervezet_idk"])) {
            $sql = "SELECT * FROM szervezetek WHERE id=$val";
            $result = mysql_query($sql); //szervezet lekérése a helyi adatbázisból
	    $sql_query_count++;
	    while($sor=mysql_fetch_assoc($result)){
	    $lekeres[$i]=$sor;
	    }
	    $i++;
        }
        $_SESSION["user"]["szervezet_szam"] = $i;
        unset($donoset);
        unset($base_szerv_set);
	while ($i!=-1) {
	    $sor = $lekeres[$i];
	    $_SESSION["user"]["szervezet_nevek"][$i] = $sor["nev"];

	    if (!$donoset && $sor["oroklodes"]!=1) {
		//TELEPHELY
		if (!$_SESSION["user"]["telephely"]) {
		    $_SESSION["user"]["telephely_irsz"] = $sor["telephely_irsz"];
		    $_SESSION["user"]["telephely_varos"] = $sor["telephely_varos"];
		    $_SESSION["user"]["telephely_cim"] = $sor["telephely_cim"];
		    $_SESSION["user"]["telephely_szam"] = $sor["telephely_szam"];
		}
		if (!$_SESSION["user"]["szervtol"]["telephely"]) {
		    $_SESSION["user"]["szervtol"]["telephely_irsz"] = $sor["telephely_irsz"];
		    $_SESSION["user"]["szervtol"]["telephely_varos"] = $sor["telephely_varos"];
		    $_SESSION["user"]["szervtol"]["telephely_cim"] = $sor["telephely_cim"];
		    $_SESSION["user"]["szervtol"]["telephely_szam"] = $sor["telephely_szam"];
		}
		//LEVELEZÉSEI CÍM
		if (!$_SESSION["user"]["levelezesi"]) {
		    $_SESSION["user"]["levelezesi_irsz"] = $sor["levelezesi_irsz"];
		    $_SESSION["user"]["levelezesi_varos"] = $sor["levelezesi_varos"];
		    $_SESSION["user"]["levelezesi_cim"] = $sor["levelezesi_cim"];
		}
		if (!$_SESSION["user"]["szervtol"]["levelezesi"]) {
		    $_SESSION["user"]["szervtol"]["levelezesi_irsz"] = $sor["levelezesi_irsz"];
		    $_SESSION["user"]["szervtol"]["levelezesi_varos"] = $sor["levelezesi_varos"];
		    $_SESSION["user"]["szervtol"]["levelezesi_cim"] = $sor["levelezesi_cim"];
		}
		//FAX
		if (!$_SESSION["user"]["szervtol"]["fax"]) {
		    $_SESSION["user"]["szervtol"]["fax"] = $sor["fax"];
		}
		if (!$_SESSION["user"]["fax"]) {
		    $_SESSION["user"]["fax"] = $sor["fax"];
		}
		//VEZETÉKES FAX
		if (!$_SESSION["user"]["szervtol"]["vez_fax"]) {
		    $_SESSION["user"]["szervtol"]["vez_fax"] = $sor["vez_fax"];
		}
		if (!$_SESSION["user"]["vez_fax"]) {
		    $_SESSION["user"]["vez_fax"] = $sor["vez_fax"];
		}
		//TITKÁRSÁG - ez kicsit extreme lesz :) 
		if (!$_SESSION["user"]["titkarsag"]) {
		    $sql="SELECT * FROM felhasznalok WHERE szervezet_id = '" . $sor["id"] . "' AND allomany_statusz='3'";
		    $result2 = mysql_query($sql) or die("MySQL hibaüzenet:" . mysql_error());
		    $sql_query_count++;
                    while ($sor2 = mysql_fetch_assoc($result2)) {
			$_SESSION["user"]["titkarsag"] = $_SESSION["user"]["titkarsag"] . $odin->fancy_phone_number($sor2["kozvetlen"]) . "<br />";
		    }
		}
		
	    if (!$_SESSION["user"]["telephely"]) $_SESSION["user"]["telephely"] = $_SESSION["user"]["telephely_irsz"] . " " . $_SESSION["user"]["telephely_varos"] . ", " . $_SESSION["user"]["telephely_cim"] . " " . $_SESSION["user"]["telephely_szam"];
	    if (!$_SESSION["user"]["levelezesi"]) $_SESSION["user"]["levelezesi"] = $_SESSION["user"]["levelezesi_irsz"] . " " . $_SESSION["user"]["levelezesi_varos"] . ", " . $_SESSION["user"]["levelezesi_cim"];
	    if (strlen($_SESSION["user"]["telephely"])==4) $_SESSION["user"]["telephely"]="";
	    if (strlen($_SESSION["user"]["levelezesi"])==3) $_SESSION["user"]["levelezesi"]="";
	    
	    if (!$_SESSION["user"]["szervtol"]["telephely"]) $_SESSION["user"]["szervtol"]["telephely"] = $_SESSION["user"]["szervtol"]["telephely_irsz"] . " " . $_SESSION["user"]["szervtol"]["telephely_varos"] . ", " . $_SESSION["user"]["szervtol"]["telephely_cim"] . " " . $_SESSION["user"]["szervtol"]["telephely_szam"];
	    if (!$_SESSION["user"]["szervtol"]["levelezesi"]) $_SESSION["user"]["szervtol"]["levelezesi"] = $_SESSION["user"]["szervtol"]["levelezesi_irsz"] . " " . $_SESSION["user"]["szervtol"]["levelezesi_varos"] . ", " . $_SESSION["user"]["szervtol"]["levelezesi_cim"];
	    if (strlen($_SESSION["user"]["szervtol"]["telephely"])==4) $_SESSION["user"]["szervtol"]["telephely"]="";
	    if (strlen($_SESSION["user"]["szervtol"]["levelezesi"])==3) $_SESSION["user"]["szervtol"]["levelezesi"]="";
	    
	    
	    }
	    if ($sor["szervezet_tipus"] == 5 && !$base_szerv_set) {
		$_SESSION["user"]["foszerv"]=$sor["teljes_id"];
		$base_szerv_set=true;
	    }
	    if ($sor["oroklodes"] > 1) {
		$donoset = true;
	    }
	    $i--;
	}
	if (!$_SESSION["user"]["titkarsag"]) $_SESSION["user"]["titkarsag"]=$odin->fancy_text($_SESSION["user"]["titkarsag"]);
	
	if (!$_SESSION["user"]["iroda"]) $_SESSION["user"]["iroda"] = $_SESSION["user"]["telephely_epulet"] . " " . $_SESSION["user"]["telephely_emelet"] . " " . $_SESSION["user"]["telephely_iroda"];
	if (strlen($_SESSION["user"]["iroda"])==2) $_SESSION["user"]["iroda"]="";
	
        $result = mysql_query("SELECT * FROM beosztasok WHERE id = '" . $_SESSION["user"]["beosztas_id"] . "'");
	$sql_query_count++;
	while ($sor = mysql_fetch_assoc($result)) {
	    $_SESSION["user"]["beosztas"] = $sor["beosztasnev"];
	}
	
        $result = mysql_query("SELECT * FROM rendfokozatok WHERE id = '" . $_SESSION["user"]["rendfokozat_id"] . "'");
	$sql_query_count++;
	while ($sor = mysql_fetch_assoc($result)) {
	    $_SESSION["user"]["rendfokozat"] = $sor["megnevezes"];
	}
//FRISSÍTJUK AZ UTOLJÁRA ITTJÁRT ADATOT
	
	$this->update_aktiv_ekkor($_SESSION["user"]["id"]);
    }

    function check_user_details($id) {
	global $sql_query_count, $siteloadlog;
	global $config;
	global $odin;
	$check = mysql_query("SELECT * FROM felhasznalok WHERE id = '$id'");
	$sql_query_count++;
	while ($sor = mysql_fetch_assoc($check)) {
	    // HA 10 PERCNÉL RÉGEBBEN JÁRT AZ OLDALON
	    if (strtotime($sor["aktiv_ekkor"]) < time() - $config["site"]["session_validtime"]) {
		
		if (isset($_COOKIE["Passkey"])) { // HA VAN COOKIE //
                
		$uid = $_COOKIE["Uid"];
		$passkey = $_COOKIE["Passkey"];
		$ipcookie = "" . substr(md5($_SERVER['REMOTE_ADDR']), 3, 4) . "" . substr(md5($_SERVER['REMOTE_ADDR']), 11, 4);
		$check = mysql_query("SELECT * FROM felhasznalok WHERE id = '$uid'");
		$sql_query_count++;
		while ($sor = mysql_fetch_assoc($check)) {
		    if ($passkey != $sor['passkey'] || $ipcookie != $_COOKIE["Ipcookie"] || time() > strtotime($sor["passkey_ervenyes"])) { // ha nem érvényes a cookie
			
			$siteloadlog["event"]=15;
			$siteloadlog["who"]=$uid;
			$siteloadlog["what"]="Rendszer kiléptetés lejárt süti miatt!";
			$odin->addsiteload();
			unset($_SESSION);
			session_destroy();
			header("Location:" . $config["site"]["absolutepath"] . "/bejelentkezes/hiba/lejart-cookie"); // vissza a bejelentkező oldalra
			exit();
		    } else { // ha érvényes a cookie
			
			$siteloadlog["event"]=16;
			$siteloadlog["who"]=0;
			$siteloadlog["what"]="Bejelentkezés Sütivel";
			$odin->add_session_db($uid);
			$this->get_user_details($sor);
                    }
                }
		} else { // HA NINCS // AKKOR IRÁNY A BEJELENTKEZŐ OLDAL
		    
		    mysql_query("UPDATE munkamenetek SET kijelentkezve='2' WHERE session_id='" . $_SESSION["user"]["sessionid"] . "' AND user_id = '" . $_SESSION["user"]["id"] . "'");
		    $siteloadlog["who"]=$_SESSION["user"]["id"];
		    unset($_SESSION);
		    session_destroy();
		    $siteloadlog["event"]=15;
		    $siteloadlog["what"]="Lejárt időkorlát";
		    $odin->addsiteload();
		    $sql_query_count++;
		    header("Location:" . $config["site"]["absolutepath"] . "/bejelentkezes/hiba/lejart-idokorlat");
		    exit();
		}
	    }
	    // HA MÓDOSÍTVA LETT AZ UTOLSÓ ELLENŐRZÉS ÓTA - !ezt át kell nézni!
	    
	    $result69 = mysql_query("SELECT * FROM modositva WHERE id = 1");
	    $sql_query_count++;
	    while ($sor69 = mysql_fetch_assoc($result69)) {
		if (strtotime($sor69["ekkor"]) > $_SESSION["user"]["lastchecked"]) { // AKKOR ÚJRA LEKÉRJÜK AZ ADATAIT
		
		    $this->get_user_details($sor);
		} else { // HA NEM LETT MODÓSÍTVA
		
		    //AKKOR FRISSITJÜK AZ UTOLSÓ AKTIVITÁS IDŐPONTJÁT
		    $this->update_aktiv_ekkor($id);
		}
	    }
	}
    }

    function update_aktiv_ekkor($id) {
	global $sql_query_count;
	global $config;
	global $odin;
	// UTOLSÓ AKTIVITÁS FRISSÍTÉSE AZ ADATBÁZISBAN
	mysql_query("UPDATE felhasznalok SET aktiv_ekkor = '" . date('Y-m-d H:i:s', time()) . "' WHERE id = '$id'");
	$sql_query_count++;
	
    }

    function check_user_status() {
	global $sql_query_count;
	global $config;
	global $odin;
	
	if ($_SESSION["user"]["allomany_statusz"] > 5 && $_SESSION["user"]["root"] != "1") {
	    
	    setcookie("Uid", '', time() - 3600);
	    setcookie("Passkey", '', time() - 3600);
	    $siteloadlog["event"]=15;
	    $siteloadlog["who"]=0;
	    $siteloadlog["what"]="Inaktív státusz!";
	    $odin->addsiteload();
	    unset($_SESSION);
	    session_destroy();
	    header("Location:" . $config["site"]["absolutepath"] . "/bejelentkezes/hiba/inaktiv-statusz");
	    exit();
	}
	if ($_SESSION["user"]["account_statusz"] == "1" || $_SESSION["user"]["root"] == "1") {
	    
	} else if (is_null($_SESSION["user"]["account_statusz"])) {
	    
	    $odin->addsiteload();
	    header("Location:" . $config["site"]["absolutepath"] . "/aktivacio/" . $_SESSION["user"]["id"] . "/" . md5($_SESSION["user"]["passkey"]) . "");
	    exit();
	} else {
	    
	    setcookie("Uid", '', time() - 3600);
	    setcookie("Passkey", '', time() - 3600);
	    $siteloadlog["event"]=15;
	    $siteloadlog["who"]=0;
	    $siteloadlog["what"]="Felfüggesztett fiók!";
	    $odin->addsiteload();
	    unset($_SESSION);
	    session_destroy();
	    header("Location:" . $config["site"]["absolutepath"] . "/aktivacio/bejelentkezes/hiba/baned");
	    exit();
	}
    }

}

?>