<?php
class hirek
{
function __construct()
	{
	global $q;
	include_once("content/content_hirek.php");
	$this->content=new hirek_content();
	switch ($q[1])
		{
		case "verzionaplo": $this->verzio_hirek(); break;
		case "emlekeztetok": $this->emlekeztetok(); break;
		default:
			$this->freya_hirek();
		break;	
		}
	}
/*********************************************************/
function kiir()
	{
	$this->content->nyito();
	}
/*********************************************************/
function freya_hirek()
	{
	global $q, $lang, $odin, $sql_query_count, $config, $siteloadlog;
	switch ($q[2])
		{
	      /******************************************************/
		case "uj-hir-felvitele": 
		$error = false;
		$time = date('Y-m-d H:i:s', time());
		 //VISSZALÉPÉS
		if ($_POST["hozzaad"] == $lang["gomb"]["megse"]) {
		 header("location:" . $config["site"]["absolutepath"] . "/hirek/rendszer");
		}
		//ELLENŐRZÉS
		if ($_POST["hozzaad"] == $lang["gomb"]["hozzaad"]) {
		    if (strlen($_POST["cim"])<3){ $error["kiir"].="Hiányzó, vagy túl rövid cím!<br />"; }
		    if (strlen($_POST["torzs"])<3){ $error["kiir"].="Hiányzó, vagy túl rövid törzs!<br />"; }
		    if (!$odin->admin_this("hir","new",0)) { $error["kiir"].="Nincs jogod az adott művelethez!<br />"; }		    
		}
		if ($error) {
		    $this->content->uj_hir($error);
		} elseif ($_POST["hozzaad"] == $lang["gomb"]["hozzaad"]) {
		   $kiemelt=$_POST["kiemelt_datum"]." ".$_POST["kiemelt_ido"].":00";
		   $sql = "INSERT INTO hirek (`cim`,`torzs`,`letrehozva`,`szerzo_id`,`kiemelt_eddig`,`kiemelt`)
		    VALUES ('".$_POST["cim"]."','".$_POST["torzs"]."','".$time."','".$_SESSION["user"]["id"]."','".$kiemelt."','".$_POST["kiemelt"]."')";
		   mysql_query($sql) or die("MySQL hibaüzenet:" . mysql_error());
		    
		    $sql = "UPDATE modositva SET `ekkor`= '$time' WHERE id=1";
		    mysql_query($sql) or die("MySQL hibaüzenet:" . mysql_error());
		    $odin->addsiteload();
		    header("Location:" . $config["site"]["absolutepath"] . "/hirek/rendszer");
		    exit();
		}
		else {
		  $this->content->uj_hir($error); 
		  }
		break;
		/******************************************************/
		case "hir-szerkesztese": 
		$error = false;
		$time = date('Y-m-d H:i:s', time());
		 //VISSZALÉPÉS
		if ($_POST["modosit"] == $lang["gomb"]["megse"]) {
		 header("location:" . $config["site"]["absolutepath"] . "/hirek/rendszer");
		}
		//ELLENŐRZÉS
		if ($_POST["modosit"] == $lang["gomb"]["modosit"]) {
		    if (strlen($_POST["cim"])<3){ $error["kiir"].="Hiányzó, vagy túl rövid cím!<br />"; }
		    if (strlen($_POST["torzs"])<3){ $error["kiir"].="Hiányzó, vagy túl rövid törzs!<br />"; }
		    if (!$odin->admin_this("hir","edit",0)) { $error["kiir"].="Nincs jogod az adott művelethez!<br />"; }
		}
		if ($error) {
		    $this->content->hir_szerkesztese($error);
		} elseif ($_POST["modosit"] == $lang["gomb"]["modosit"]) {
		   $kiemelt=$_POST["kiemelt_datum"]." ".$_POST["kiemelt_ido"].":00";
		   if (!$_POST["dontaddmod"]) $_POST["torzs"]=$_POST["torzs"]."[FRISSITVE](frissítve ekkor: $time)[/FRISSITVE]";
		   $sql = "UPDATE hirek SET `cim`='".$_POST["cim"]."',`torzs`='".$_POST["torzs"]."',`kiemelt_eddig`='".$kiemelt."',`kiemelt`='".$_POST["kiemelt"]."' WHERE id = ".$q[3]."";
		   print $sql;
		   mysql_query($sql) or die("MySQL hibaüzenet:" . mysql_error());
		    
		    $sql = "UPDATE modositva SET `ekkor`= '$time' WHERE id=1";
		    mysql_query($sql) or die("MySQL hibaüzenet:" . mysql_error());
		    $odin->addsiteload();
		    header("Location:" . $config["site"]["absolutepath"] . "/hirek/rendszer");
		    exit();
		}
		else {
		  $this->content->hir_szerkesztese($error); 
		  }
		break;
		/******************************************************/
		case "hir-torlese": 
		$error = false;
		$time = date('Y-m-d H:i:s', time());
		 //VISSZALÉPÉS
		if ($_POST["torol"] == $lang["gomb"]["megse"]) {
		 header("location:" . $config["site"]["absolutepath"] . "/hirek/rendszer");
		}
		//ELLENŐRZÉS
		if ($_POST["torol"] == $lang["gomb"]["torold"]) {
		    if (!$odin->admin_this("hir","del",0)) { $error["kiir"].="Nincs jogod az adott művelethez!<br />"; }
		}
		if ($error) {
		    $this->content->hir_torlese($error);
		} elseif ($_POST["torol"] == $lang["gomb"]["torold"]) {
		   $sql = "DELETE from hirek where id = ".$q[3]."";
		   mysql_query($sql) or die("MySQL hibaüzenet:" . mysql_error());
		    $sql = "UPDATE modositva SET `ekkor`= '$time' WHERE id=1";
		    mysql_query($sql) or die("MySQL hibaüzenet:" . mysql_error());
		    $odin->addsiteload();
		    header("Location:" . $config["site"]["absolutepath"] . "/hirek/rendszer");
		    exit();
		}
		else {
		  $this->content->hir_torlese($error); 
		  }
		break;
		/******************************************************/
		default:$this->content->freya_hirek(); break;
		}
	}
/*********************************************************/
function verzio_hirek()
	{
	$this->content->verzio_hirek();
	}
/*********************************************************/
function emlekeztetok()
	{
	$this->content->emlekeztetok();
	}
/*********************************************************/
}
?>
