<?php

class odin {

// ALAPFUNKCIÓK
    function check_magic_quotes_qpc() {
       if (!get_magic_quotes_gpc()) { // szerverbeállítás ellenőrzése, ha nincs engedélyezve
            foreach ($_POST as $a => $b) {
                $_POST["$a"] = addslashes($b);
            }
            foreach ($_GET as $a => $b) {
                $_GET["$a"] = addslashes($b);
            }
        }
    }
/* A FUNCKIÓ SZÜKSÉGTELENNÉ VÁLT
    function writelog($text) {
        global $config;
        global $debuglog;
        if ($config["debug"]["log_to_file"] == "1") {
            $fh = fopen("riport.txt", 'a');
            fwrite($fh, $text);
        }
        if ($config["debug"]["log_on_screen"] == "1") {
            $debuglog = $debuglog . str_replace("\n", "<br />", $text);
        }
    }
*/
    function logout() {
        global $siteloadlog;
        mysql_query("UPDATE munkamenetek SET kijelentkezve=1 WHERE session_id='" . $_SESSION["user"]["sessionid"] . "' AND user_id = '" . $_SESSION["user"]["id"] . "'") or die("MySQL hibaüzenet:" . mysql_error());
        setcookie("Passkey", '', time() - 3600);
        setcookie("Ipcookie", '', time() - 3600);
        $siteloadlog["event"] = 15;
        $siteloadlog["who"] = $_SESSION["user"]["id"];
        $this->addsiteload();
        session_unset();
        session_destroy();
        
        header('location:bejelentkezes');
        exit();
    }
    
    function checkpass($sor,$pass) {
	global $odin;
	$getextpass = md5($pass . "" . substr(md5($sor["jelszo_kiegeszito"]), 3, 4) . "" . substr(md5($sor["jelszo_kiegeszito"]), 11, 4));
	if ($sor["jelszo"] == $getextpass) {
	    return true;
	}
    }

    function connect_to_mysql() {
        global $config, $mysql_kapcsolat;

        if (!isset($mysql_kapcsolat)) {
            $mysql_kapcsolat = mysql_connect($config["sql"]["dbhost"], $config["sql"]["dbuser"], $config["sql"]["dbpass"])
                    or die("Nem sikerült az adatbázisszervehez csatlakozni, MySQL hibaüzenet:" . mysql_error());
            mysql_select_db($config["sql"]["dbname"])
                    or die("Nem sikerult az adatbazist elerni, MySQL hibaüzenet:" . mysql_error());
        }
        mysql_query("SET NAMES utf8");
    }

    function close_mysql_connection() {
        if (isset($mysql_kapcsolat))
            mysql_close($mysql_kapcsolat);
    }

// KERESESEKHEZ FANCY URL
    function post_to_fancy_url() {
        global $config, $lang;
	$q=explode("/",$_GET["query"]);

	if ($q[0]=="illetekesseg" && $q[1]=="kereses" && ($_GET["mehet"]=="Mehet" || $_GET["keresd"]==$lang["gomb"]["keresd"])){
	    $search = array(" ", "/", "%");
            $replace = array("_", "", "");
            $_POST["iranyitoszam"] = str_replace($search, $replace, $_GET["iranyitoszam"]);
            $_POST["telepules"] = str_replace($search, $replace, $_GET["telepules"]);
            $_POST["ittkeresd"] = str_replace($search, $replace, $_GET["ittkeresd"]);
	    $_POST["szokezdet"] = str_replace($search, $replace, $_GET["szokezdet"]);
	    $_POST["fonetikus"] = str_replace($search, $replace, $_GET["fonetikus"]);
	    $_POST["limit"] = str_replace(" ", "", $_GET["limit"]);
            header("Location:" . $config["site"]["absolutepath"] . "/illetekesseg/kereses/iranyitoszam=" . $_POST["iranyitoszam"] . "/telepules=" . $_POST["telepules"] . "/szokezdet=" . $_POST["szokezdet"] . "/lista=" . ($_POST["limit"]));
	} 
	
        if ($q[0]=="telefonkonyv" && $q[1]=="kereses" && ($_GET["mehet"]=="Mehet" || $_GET["keresd"]==$lang["gomb"]["keresd"])){
	    $search = array(" ", "/", "%");
            $replace = array("_", "", "");
            $_POST["nev"] = str_replace($search, $replace, $_GET["nev"]);
            $_POST["beosztas"] = str_replace($search, $replace, $_GET["beosztas"]);
            $_POST["szervezet"] = str_replace($search, $replace, $_GET["szervezet"]);
            $_POST["ittkeresd"] = str_replace($search, $replace, $_GET["ittkeresd"]);
            $_POST["kozvetlen"] = str_replace($search, $replace, $_GET["kozvetlen"]);
	    $_POST["szokezdet"] = str_replace($search, $replace, $_GET["szokezdet"]);
	    $_POST["fonetikus"] = str_replace($search, $replace, $_GET["fonetikus"]);
	    $_POST["limit"] = str_replace(" ", "", $_GET["limit"]);
            header("Location:" . $config["site"]["absolutepath"] . "/telefonkonyv/kereses/kereses-helye=" . $_POST["ittkeresd"] . "/nev=" . $_POST["nev"] . "/beosztas=" . $_POST["beosztas"] . "/szervezet=" . $_POST["szervezet"] . "/kozvetlenszam=" . $_POST["kozvetlen"] . "/szokezdet=" . $_POST["szokezdet"] . "/fonetikus=" . $_POST["fonetikus"] . "/lista=" . ($_POST["limit"]));
	} 
    }
// Fonetikus kereséshez átalakító funkció
    function text_to_fonetic($textcmp)
	{
		$textcmp=str_replace(" ","",$textcmp);
		$textcmp=str_replace("-","",$textcmp);
		$textcmp=strtolower($textcmp);
		$textcmp=str_replace("Ö","o",$textcmp);
		$textcmp=str_replace("Ü","u",$textcmp);
		$textcmp=str_replace("Ó","o",$textcmp);
		$textcmp=str_replace("Ő","o",$textcmp);
		$textcmp=str_replace("Ű","u",$textcmp);
		$textcmp=str_replace("Ú","u",$textcmp);
		$textcmp=str_replace("É","e",$textcmp);
		$textcmp=str_replace("Á","a",$textcmp);
		$textcmp=str_replace("Í","i",$textcmp);
		
		$textcmp=str_replace("ö","o",$textcmp);
		$textcmp=str_replace("ä","e",$textcmp);
		$textcmp=str_replace("ü","u",$textcmp);
		$textcmp=str_replace("ó","o",$textcmp);
		$textcmp=str_replace("u","u",$textcmp);
		$textcmp=str_replace("ő","o",$textcmp);
		$textcmp=str_replace("ű","u",$textcmp);
		$textcmp=str_replace("ú","u",$textcmp);
		$textcmp=str_replace("é","e",$textcmp);
		$textcmp=str_replace("á","a",$textcmp);
		$textcmp=str_replace("í","i",$textcmp);

		$textcmp=str_replace("y","i",$textcmp);
		$textcmp=str_replace("w","v",$textcmp);

		$textcmp=str_replace("sch","s",$textcmp);
		$textcmp=str_replace("ts","cs",$textcmp);
		$textcmp=str_replace("tz","c",$textcmp);
		$textcmp=str_replace("eu","aj",$textcmp);
		$textcmp=str_replace("cz","c",$textcmp);
		$textcmp=str_replace("th","t",$textcmp);
		$textcmp=str_replace("hl","l",$textcmp);
		$textcmp=str_replace("gh","g",$textcmp);
		$textcmp=str_replace("ck","k",$textcmp);
		$textcmp=str_replace("ch","h",$textcmp);
		$textcmp=str_replace("ei","ej",$textcmp);

		$textcmp=str_replace("bb","b",$textcmp);
		$textcmp=str_replace("cc","c",$textcmp);
		$textcmp=str_replace("ff","f",$textcmp);
		$textcmp=str_replace("gg","g",$textcmp);
		$textcmp=str_replace("hh","h",$textcmp);
		$textcmp=str_replace("jj","j",$textcmp);
		$textcmp=str_replace("kk","k",$textcmp);
		$textcmp=str_replace("ll","l",$textcmp);
		$textcmp=str_replace("mm","m",$textcmp);
		$textcmp=str_replace("nn","n",$textcmp);
		$textcmp=str_replace("pp","p",$textcmp);
		$textcmp=str_replace("qq","q",$textcmp);
		$textcmp=str_replace("rr","r",$textcmp);
		$textcmp=str_replace("ss","s",$textcmp);
		$textcmp=str_replace("tt","t",$textcmp);
		$textcmp=str_replace("vv","v",$textcmp);
		$textcmp=str_replace("xx","x",$textcmp);
		$textcmp=str_replace("zz","z",$textcmp);

		$textcmp=str_replace("aa","a",$textcmp);
		$textcmp=str_replace("ee","e",$textcmp);
		$textcmp=str_replace("ii","i",$textcmp);
		$textcmp=str_replace("oo","o",$textcmp);
		$textcmp=str_replace("uu","u",$textcmp);
		
		return $textcmp;
	}
// JELSZÓGENERÁTOR
    function generate_new_password($id, $azonosito) {
        global $sql_query_count;

        $startat = rand(0, 24);
        $newpass = substr(md5(rand(100, 10000000)), $startat, 8);
        $newpassextension = substr(md5(rand(100, 10000000)), $startat, 8);
        $passtodb = md5($newpass . "" . substr(md5($newpassextension), 3, 4) . "" . substr(md5($newpassextension), 11, 4));
        mysql_query("UPDATE felhasznalok SET jelszo = '$passtodb', jelszo_kiegeszito='$newpassextension' WHERE id = '$id'");
        $sql_query_count++;
        $details = array(
            "username" => "$azonosito",
            "pass" => "$newpass"
        );
        return $details;
    }

//MAIL FUNKCIÓ
    function send_mail($to, $subject, $message) {
        global $config;
        
        $headers = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
        if ($config["mail"]["settings"] == 1) {
            mail($to, $subject, $message, $headers);
        }
        if ($config["mail"]["settings"] == 2) {

            $baseMessage = $message;

            $fromName = "Freya";
            $fromEmail = "no-reply@freya";
            $fromMailer = "Freya System";
            $smtp = "10.4.1.20";
            $smtp_port = 25;
            $charset = "utf-8";


            $baseMessage = str_replace(chr(13), "", $baseMessage);
            $baseMessage = str_replace("\r\n.", "\r\n..", str_replace("\n", "\r\n", stripslashes($baseMessage)) . " \r\n");
            ini_set('sendmail_from', $fromEmail);
            $connect = @fsockopen($smtp, $smtp_port, $errno, $errstr, 5);
            if (!$connect) {
                header('location:elfelejtett-jelszo/hiba/nincs-kapcsolat-az-e-mail-szerverrel');
                exit;
            }
            $rcv = fgets($connect, 1024);
            fputs($connect, "HELO 127.0.0.1\r\n");
            $rcv = fgets($connect, 1024);


            fputs($connect, "RSET\r\n");
            $rcv = fgets($connect, 1024);

            fputs($connect, "MAIL FROM:$fromEmail\r\n");
            $rcv = fgets($connect, 1024);
            fputs($connect, "RCPT TO:$to\r\n");
            $rcv = fgets($connect, 1024);
            fputs($connect, "DATA\r\n");
            $rcv = fgets($connect, 1024);

            fputs($connect, "Subject: $subject\r\n");
            fputs($connect, "From: $fromName <$fromEmail>\r\n");
            fputs($connect, "To: $toRcpt\r\n");
            fputs($connect, "X-Sender: <$fromEmail>\r\n");
            fputs($connect, "Return-Path: <$fromEmail>\r\n");
            fputs($connect, "Errors-To: <$fromEmail>\r\n");
            fputs($connect, "Message-Id: <" . md5(uniqid(rand())) . "." . preg_replace("/[^a-z0-9]/i", "", $fromName) . "@$smtp>\r\n");
            fputs($connect, "X-Mailer: PHP - $fromMailer\r\n");
            fputs($connect, "X-Priority: 3\r\n");
            fputs($connect, "Date: " . date("r") . "\r\n");
            fputs($connect, "Content-Type: text/plain; charset=$charset\r\n");
            fputs($connect, "\r\n");
            fputs($connect, $message);
            fputs($connect, "\r\n.\r\n");
            $rcv = fgets($connect, 1024);

            fputs($connect, "QUIT\r\n");
            $rcv = fgets($connect, 1024);
            fclose($connect);
            ini_restore('sendmail_from');
        } else {
            
        }
    }

    function get_user_basedetails($id) {
        global $sql_query_count;
        global $config;
        global $odin;
        $sql = "SELECT * FROM felhasznalok WHERE `id` = $id";
        $result = mysql_query($sql);
        $sql_query_count++;
        $counter = 0;
        while ($sor = mysql_fetch_assoc($result)) {
            $counter++;
            foreach ($sor as $key => $val) {
                $request["user"]["" . $key . ""] = $val;
            }
        }
        if (!$counter) {
            return false;
            exit();
        }
        $request["user"]["teljesnev"] = $request["user"]["titulus"] . " " . $request["user"]["vezeteknev"] . " " . $request["user"]["kozepsonev"] . " " . $request["user"]["keresztnev"];

        return $request;
    }

    function get_user_details($id) {
        global $sql_query_count;
        global $config;
        global $odin;
        $sql = "SELECT * FROM felhasznalok WHERE `id` = $id";
        $result = mysql_query($sql);
        $sql_query_count++;
        $counter = 0;
        while ($sor = mysql_fetch_assoc($result)) {
            $counter++;
            foreach ($sor as $key => $val) {
                $request["user"]["" . $key . ""] = $val;
            }
        }
        if (!$counter) {
            return false;
            exit();
        }
        $request["user"]["teljesnev"] = $request["user"]["titulus"] . " " . $request["user"]["vezeteknev"] . " " . $request["user"]["kozepsonev"] . " " . $request["user"]["keresztnev"];
        $request["user"]["nem"] = substr($request["user"]["azonosito"], 0, 1);
        $request["user"]["szuletesnap"] = substr($request["user"]["azonosito"], 1, 6);


        $result = mysql_query("SELECT * FROM szervezetek WHERE id = '" . $request["user"]["szervezet_id"] . "'");
        $sql_query_count++;
        while ($sor = mysql_fetch_assoc($result)) {
            while (list ($key, $val) = each($sor)) {
                $request["user"]["szervezet_" . $key . ""] = $val;
            }
        }
        unset($request["user"]["szervezet_idk"]);
	$request["user"]["szervezet_idk"] = explode("-", $request["user"]["szervezet_teljes_id"]);
        $i = 0;
        while (list ($key, $val) = each($request["user"]["szervezet_idk"])) {
            $sql = "SELECT * FROM szervezetek WHERE id=$val";
            $result = mysql_query($sql); //szervezet lekérése a helyi adatbázisból
	    $sql_query_count++;
	    while($sor=mysql_fetch_assoc($result)){
	    $lekeres[$i]=$sor;
	    }
	    $i++;
        }
        $request["user"]["szervezet_szam"] = $i;
        unset($donoset);
        while ($i!=-1) {
	    $sor = $lekeres[$i];
            $request["user"]["szervezetek"][$i]["nev"] = $sor["nev"];
            $request["user"]["szervezetek"][$i]["id"] = $sor["id"];
            $request["user"]["szervezetek"][$i]["oroklodes"] = $sor["oroklodes"];
            $request["user"]["szervezetek"][$i]["szervezet_tipus"] = $sor["szervezet_tipus"];
            if (!$donoset && $sor["oroklodes"]!=1) {
                //TELEPHELY
                if (!$request["user"]["telephely"]) {
                    $request["user"]["telephely_irsz"] = $sor["telephely_irsz"];
                    $request["user"]["telephely_varos"] = $sor["telephely_varos"];
                    $request["user"]["telephely_cim"] = $sor["telephely_cim"];
                    $request["user"]["telephely_szam"] = $sor["telephely_szam"];
                }
                //LEVELEZÉSEI CÍM
                if (!$request["user"]["levelezesi"]) {
                    $request["user"]["levelezesi_irsz"] = $sor["levelezesi_irsz"];
                    $request["user"]["levelezesi_varos"] = $sor["levelezesi_varos"];
                    $request["user"]["levelezesi_cim"] = $sor["levelezesi_cim"];
                }

                if (!$request["user"]["fax"]) {
                    $request["user"]["fax"] = $sor["fax"];
	        }
                //TITKÁRSÁG - ez kicsit extreme lesz :)
                if (!$request["user"]["titkarsag"]) {
                    $sql = "SELECT * FROM felhasznalok WHERE szervezet_id = '" . $request["user"]["szervezetek"][$i]["id"] . "' AND allomany_statusz='3'";
                    $result2 = mysql_query($sql) or die("MySQL hibaüzenet:" . mysql_error());
                    $sql_query_count++;
                    while ($sor2 = mysql_fetch_assoc($result2)) {
			$request["user"]["titkarsag_nevek"] = $request["user"]["titkarsag_nevek"] . "<a href=\"" . $config["site"]["absolutepath"] . "/felhasznalok/" . $sor2["id"] . "\">".$sor2["titulus"] . " " . $sor2["vezeteknev"] . " " . $sor2["kozepsonev"] . " " . $sor2["keresztnev"]."</a><br />";
			$request["user"]["titkarsag"] = $request["user"]["titkarsag"] . $odin->fancy_phone_number($sor2["kozvetlen"]) . "<br />";
			$request["user"]["titkarsag_link"] = $request["user"]["titkarsag_link"] . "<a href=\"".$config["site"]["absolutepath"]."/telefonkonyv/kereses/kereses-helye=/nev=/beosztas=/szervezet=/kozvetlenszam=".$sor2["kozvetlen"]."/szokezdet=/fonetikus=/lista=1-10"."\">" . $odin->fancy_phone_number($sor2["kozvetlen"]) . "</a>" . "<br />";
			
		    }
                }

                if (!$request["user"]["telephely"])
                    $request["user"]["telephely"] = $request["user"]["telephely_irsz"] . " " . $request["user"]["telephely_varos"] . ", " . $request["user"]["telephely_cim"] . " " . $request["user"]["telephely_szam"];
                if (!$request["user"]["levelezesi"])
                    $request["user"]["levelezesi"] = $request["user"]["levelezesi_irsz"] . " " . $request["user"]["levelezesi_varos"] . ", " . $request["user"]["levelezesi_cim"];
                if (strlen($request["user"]["telephely"]) == 4)
                    unset($request["user"]["telephely"]);
                if (strlen($request["user"]["levelezesi"]) == 3)
                    unset($request["user"]["levelezesi"]);
            }
            if ($sor["oroklodes"] > 1) {
                $donoset = true;
            }
            $i--;
        }
        if (!$request["user"]["titkarsag"]){
            $request["user"]["titkarsag_link"] = "<a href=\"".$config["site"]["absolutepath"]."/telefonkonyv/kereses/kereses-helye=/nev=/beosztas=/szervezet=/kozvetlenszam=".$request["user"]["titkarsag"]."/szokezdet=/fonetikus=/lista=1-10"."\">" . $odin->fancy_phone_number($request["user"]["titkarsag"]) . "</a>";
	    $request["user"]["titkarsag"] = $odin->fancy_text($request["user"]["titkarsag"]);
    
	}
	if (!$request["user"]["iroda"])
            $request["user"]["iroda"] = $request["user"]["telephely_epulet"] . " " . $request["user"]["telephely_emelet"] . " " . $request["user"]["telephely_iroda"];
        if (strlen($request["user"]["iroda"]) == 2)
            $request["user"]["iroda"] = "";

        $result = mysql_query("SELECT * FROM beosztasok WHERE id = '" . $request["user"]["beosztas_id"] . "'");
        $sql_query_count++;
        while ($sor = mysql_fetch_assoc($result)) {
            $request["user"]["beosztas"] = $sor["beosztasnev"];
        }

        $result = mysql_query("SELECT * FROM rendfokozatok WHERE id = '" . $request["user"]["rendfokozat_id"] . "'");
        $sql_query_count++;
        while ($sor = mysql_fetch_assoc($result)) {
            $request["user"]["rendfokozat"] = $sor["megnevezes"];
        }
        return $request;
    }
    
    function get_user_coc($id) {
        global $sql_query_count;
        global $config;

        $sql = "SELECT id, titulus, vezeteknev, keresztnev, kozepsonev, kozvetlen, allomany_statusz FROM felhasznalok WHERE `szervezet_id` = $id AND `allomany_statusz` < '4' ORDER by `allomany_statusz`";
        $result = mysql_query($sql);
        while ($sor = mysql_fetch_assoc($result)) {
            $teljesnev = $sor["titulus"] . " " . $sor["vezeteknev"] . " " . $sor["kozepsonev"] . " " . $sor["keresztnev"];
            if ($sor["allomany_statusz"] == 1) {
                $request["vezeto"].="<a href=\"" . $config["site"]["absolutepath"] . "/felhasznalok/" . $sor["id"] . "\">" . $teljesnev . "</a><br />";
                $request["vezeto_tel"].="" . $this->fancy_phone_number($sor["kozvetlen"]) . "<br />";
            }
            if ($sor["allomany_statusz"] == 2) {
                $request["vezeto-helyettes"].="<a href=\"" . $config["site"]["absolutepath"] . "/felhasznalok/" . $sor["id"] . "\">" . $teljesnev . "</a><br />";
                $request["vezeto-helyettes_tel"].="" . $this->fancy_phone_number($sor["kozvetlen"]) . "<br />";
            }
            if ($sor["allomany_statusz"] == 3) {
                $request["titkarno"].="<a href=\"" . $config["site"]["absolutepath"] . "/felhasznalok/" . $sor["id"] . "\">" . $teljesnev . "</a><br />";
                $request["titkarno_tel"].="" . $this->fancy_phone_number($sor["kozvetlen"]) . "<br />";
            }
            
	}
	$request["coc"]=true;
	return $request;
    }

    function get_szervezet_details($id) {
        global $config;
        $result = mysql_query("SELECT * FROM szervezetek WHERE id = '$id'");
        $sql_query_count++;
        $counter = 0;
        while ($sor = mysql_fetch_assoc($result)) {
            $counter++;
            while (list ($key, $val) = each($sor)) {
                $request["$key"] = "$val";
		$request["eredeti"]["$key"] = "$val";
            }
        }

                $request["telephely"] = $request["telephely_irsz"] . " " . $request["telephely_varos"] . ", " . $request["telephely_cim"] . " " . $request["telephely_szam"];
                $request["levelezesi"] = $request["levelezesi_irsz"] . " " . $request["levelezesi_varos"] . ", " . $request["levelezesi_cim"];
		if (strlen($request["telephely"]) == 4)
                    unset($request["telephely"]);
                if (strlen($request["levelezesi"]) == 3)
                    unset($request["levelezesi"]);
	
        if (!$counter) {
            return false;
            exit();
        }
        unset($request["idk"]);
        $request["idk"] = explode("-", $request["teljes_id"]);
        $i = 0;
        while (list ($key, $val) = each($request["idk"])) {
            $sql = "SELECT * FROM szervezetek WHERE id=$val";
            $result = mysql_query($sql); //szervezet lekérése a helyi adatbázisból
	    $sql_query_count++;
	    while($sor=mysql_fetch_assoc($result)){
	    $lekeres[$i]=$sor;
	    }
	    $i++;
        }
		
        $request["szervezet_szam"] = $i;
        unset($donoset);
        while ($i!=-1) {
	    $sor = $lekeres[$i];
            $request["valami"][$i]["nev"] = $sor["nev"];
            $request["valami"][$i]["id"] = $sor["id"];
            $request["valami"][$i]["oroklodes"] = $sor["oroklodes"];
            $request["valami"][$i]["szervezet_tipus"] = $sor["szervezet_tipus"];
	    $request["valami"][$i]["vez_fax"] = $sor["vez_fax"];
	    $request["valami"][$i]["vez_kozvetlen"] = $sor["vez_kozvetlen"];
            if (!$donoset && $sor["oroklodes"]!=1) {
                //TELEPHELY
                if (!$request["telephely"]) {
                    $request["telephely_irsz"] = $sor["telephely_irsz"];
                    $request["telephely_varos"] = $sor["telephely_varos"];
                    $request["telephely_cim"] = $sor["telephely_cim"];
                    $request["telephely_szam"] = $sor["telephely_szam"];
                }
                //LEVELEZÉSEI CÍM
                if (!$request["levelezesi"]) {
                    $request["levelezesi_irsz"] = $sor["levelezesi_irsz"];
                    $request["levelezesi_varos"] = $sor["levelezesi_varos"];
                    $request["levelezesi_cim"] = $sor["levelezesi_cim"];
                }
                if (!$request["hirkozpont"]) {
                    $request["hirkozpont"] = $sor["hirkozpont"];
                }
                if (!$request["ugyelet"]) {
                    $request["ugyelet"] = $sor["ugyelet"];
                }
                if (!$request["vez_kozvetlen"]) {
                    $request["vez_kozvetlen"] = $sor["vez_kozvetlen"];
                }

                if (!$request["fax"]) {
                    $request["fax"] = $sor["fax"];
                }
                if (!$request["vez_fax"]) {
                    $request["vez_fax"] = $sor["vez_fax"];
		    $k=true;
                }
		//TITKÁRSÁG - ez kicsit extreme lesz :)
                if (!$request["titkarsag"]) {
                    $sql = "SELECT * FROM felhasznalok WHERE szervezet_id = '" . $request["valami"][$i]["id"] . "' AND allomany_statusz='3'";
                    $result2 = mysql_query($sql) or die("MySQL hibaüzenet:" . mysql_error());
                    $sql_query_count++;
                    while ($sor2 = mysql_fetch_assoc($result2)) {
			$request["titkarsag_nevek"] = $request["titkarsag_nevek"] . "<a href=\"" . $config["site"]["absolutepath"] . "/felhasznalok/" . $sor2["id"] . "\">".$sor2["titulus"] . " " . $sor2["vezeteknev"] . " " . $sor2["kozepsonev"] . " " . $sor2["keresztnev"]."</a><br />";
                        $request["titkarsag"] = $request["titkarsag"] . $this->fancy_phone_number($sor2["kozvetlen"]) . "<br />";
                    }
                }

                if (!$request["telephely"])
                    $request["telephely"] = $request["telephely_irsz"] . " " . $request["telephely_varos"] . ", " . $request["telephely_cim"] . " " . $request["telephely_szam"];
                if (!$request["levelezesi"])
                    $request["levelezesi"] = $request["levelezesi_irsz"] . " " . $request["levelezesi_varos"] . ", " . $request["levelezesi_cim"];
                if (strlen($request["telephely"]) == 4)
                    unset($request["telephely"]);
                if (strlen($request["levelezesi"]) == 3)
                    unset($request["levelezesi"]);
            }
            if ($sor["oroklodes"] > 1) {
                $donoset = true;
		$j=$i;
            }
            $i--;
        }
	if ($request["vez_fax"]==$request["valami"][$j]["vez_fax"] && $request["fax"] && $k) {
	    $request["vez_fax"]=$request["valami"][$j]["vez_kozvetlen"].$request["fax"];
	    }
	
	
        if (!$request["titkarsag"]){
            $request["titkarsag"] = $this->fancy_text($request["titkarsag"]);
      	}
        if (!$request["iroda"])
            $request["iroda"] = $request["telephely_epulet"] . " " . $request["telephely_emelet"] . " " . $request["telephely_iroda"];
        if (strlen($request["iroda"]) == 2)
            $request["iroda"] = "";

        $request["rp"].="Hírkozpont: " . $this->fancy_phone_number($request["hirkozpont"]) . "";
        $request["rp"].="<br />Ügyelet: " . $this->fancy_phone_number($request["ugyelet"]) . "";
	$request["rp"].="<br />Fax: " . $this->fancy_phone_number($request["fax"]) . "";
        $sql = "SELECT id, titulus, vezeteknev, keresztnev, kozepsonev, kozvetlen, allomany_statusz FROM felhasznalok WHERE `szervezet_id` = $id AND `allomany_statusz` < '4' ORDER by `allomany_statusz`";
        $result = mysql_query($sql);
        while ($sor = mysql_fetch_assoc($result)) {
            $teljesnev = $sor["titulus"] . " " . $sor["vezeteknev"] . " " . $sor["kozepsonev"] . " " . $sor["keresztnev"];
            if ($sor["allomany_statusz"] == 1) {
                $request["rp"].="<br /><a href=\"" . $config["site"]["absolutepath"] . "/felhasznalok/" . $sor["id"] . "\">" . $teljesnev . "</a>: " . $this->fancy_phone_number($sor["kozvetlen"]) . "";
            }
            if ($sor["allomany_statusz"] == 1) {
                $request["vezeto"].="<a href=\"" . $config["site"]["absolutepath"] . "/felhasznalok/" . $sor["id"] . "\">" . $teljesnev . "</a><br />";
                $request["vezeto_tel"].="" . $this->fancy_phone_number($sor["kozvetlen"]) . "<br />";
            }
            if ($sor["allomany_statusz"] == 2) {
                $request["rp"].="<br /><a href=\"" . $config["site"]["absolutepath"] . "/felhasznalok/" . $sor["id"] . "\">" . $teljesnev . "</a>: " . $this->fancy_phone_number($sor["kozvetlen"]) . "";
            }
            if ($sor["allomany_statusz"] == 2) {
                $request["vezeto-helyettes"].="<a href=\"" . $config["site"]["absolutepath"] . "/felhasznalok/" . $sor["id"] . "\">" . $teljesnev . "</a><br />";
                $request["vezeto-helyettes_tel"].="" . $this->fancy_phone_number($sor["kozvetlen"]) . "<br />";
            }
            if ($sor["allomany_statusz"] == 3) {
                $request["rp"].="<br /><a href=\"" . $config["site"]["absolutepath"] . "/felhasznalok/" . $sor["id"] . "\">" . $teljesnev . "</a>: " . $this->fancy_phone_number($sor["kozvetlen"]) . "";
            }
            if ($sor["allomany_statusz"] == 3) {
                $request["titkarno"].="<a href=\"" . $config["site"]["absolutepath"] . "/felhasznalok/" . $sor["id"] . "\">" . $teljesnev . "</a><br />";
                $request["titkarno_tel"].="" . $this->fancy_phone_number($sor["kozvetlen"]) . "<br />";
            }
        }

        return $request;
    }

// OLDALLETÖLTÉS HOZZÁADÁSA
    function addsiteload() {
        global $start_time, $sql_query_count, $config, $siteloadlog;
        $time = time();
        $runtime = round(microtime(true) - $start_time, 2);
        mysql_query("INSERT INTO oldalletoltesek (keres, user_id, ekkor, adatbaziskeres, oldalletoltes, oldalverzio, esemeny, erintett, esemenylista)
                VALUES ('" . $_GET["query"] . "', '" . $_SESSION["user"]["id"] . "', $time, $sql_query_count, $runtime, '" . $config["site"]["version"] . "', '" . $siteloadlog["event"] . "', '" . $siteloadlog["who"] . "', '" . $siteloadlog["what"] . "')") or die("MySQL hibaüzenet:" . mysql_error());
        $sql_query_count++;
    }

// MUNKAMENET FUNKCIÓK
    function add_session_db($id) {
        global $sql_query_count, $ua;
        if (isset($_SESSION["user"]["sessionid"])) {
            mysql_query("UPDATE munkamenetek SET kijelentkezve=2 WHERE session_id='" . $_SESSION["user"]["sessionid"] . "' AND user_id = '" . $_SESSION["user"]["id"] . "'") or die("MySQL hibaüzenet:" . mysql_error());
            unset($_SESSION["user"]["sessionid"]);
        }
        if (!isset($_SESSION["user"]["sessionid"])) {
            $_SESSION["user"]["sessionid"] = md5(rand(10000, 1000000000));
        }
        mysql_query("INSERT INTO munkamenetek (user_id, session_id, aktiv_ekkor, oprendszer, bongeszo, bongeszo_verzio)
                VALUES ('" . $id . "', '" . $_SESSION["user"]["sessionid"] . "', '" . time() . "', '" . $ua->Platform . "', '" . $ua->Browser . "', '" . $ua->Version . "')") or die("MySQL hibaüzenet:" . mysql_error());
        
        $sql_query_count++;
    }

    function update_session_db() {
        global $sql_query_count, $ua;
        
        mysql_query("UPDATE munkamenetek SET aktiv_ekkor=" . time() . " WHERE session_id='" . $_SESSION["user"]["sessionid"] . "' AND user_id = '" . $_SESSION["user"]["id"] . "'") or die("MySQL hibaüzenet:" . mysql_error());
        $sql_query_count++;
    }

    // TELEFONSZÁM ÁTALAKÍTÓ
    function fancy_phone_number($number) {
        if (!is_numeric($number) || !$number) {
            $number = "NA / NT";
        } else {
            $lenght = strlen($number);
            switch ($lenght) {
                case "6":
                    if ($number[0] == 1) {
                        $number ="(". $number[0] . ") " . $number[1] . $number[2] . "-" . $number[3] . $number[4] . $number[5];
                    } else {
                        $number ="(". $number[0] . $number[1] . ") " . $number[2] . $number[3] . "-" . $number[4] . $number[5];
                    }
                    break;
                case "7":
//                    $number = "(".$number[0] . $number[1].")" . $number[2] . "-" . $number[3] . $number[4] . $number[5] . $number[6];
                    $number = "(".$number[0] . $number[1].") " . $number[2] .  $number[3] ."-".  $number[4] . $number[5] . $number[6];
                    break;
                case "8":
                    if ($number[0] == 1) {
                        $number = "(".$number[0] . ") " . $number[1] . $number[2] . $number[3] . "-" . $number[4] . $number[5] . $number[6] . $number[7];
                    } else {
                        $number = "(".$number[0] . $number[1] . ") " . $number[2] . $number[3] . $number[4] . "-" . $number[5] . $number[6] . $number[7];
                    }
                    break;
                case "9":
                    $number = "(".$number[0] . $number[1] . ") " . $number[2] . $number[3] . $number[4] . "-" . $number[5] . $number[6] . $number[7] . $number[8];
                    break;
		case "14":
                    if ($number[0] == 1) {
                        $number1 = "(".$number[0] . ") " . $number[1] . $number[2] . $number[3] . "-" . $number[4] . $number[5] . $number[6]. $number[7];
			if ($number[8] == 1) {
			$number1.=" / ".$number[9].$number[10].$number[11]."-".$number[12].$number[13];
			}
			else{
			$number1.=" / ".$number[10].$number[11]."-".$number[12].$number[13];      
			}
		    } else {
                        $number1 ="(".$number[0] . $number[1] . ") " . $number[2] . $number[3] . $number[4] . "-" . $number[5] . $number[6]. $number[7];
                    	if ($number[8] == 1) {
			$number1.=" / ".$number[9].$number[10].$number[11]."-".$number[12].$number[13];
			}
			else{
			$number1.=" / ".$number[10].$number[11]."-".$number[12].$number[13];    
			}
			$number=$number1;
		    }break;
                default: $number = "Hibás szám!";
                    break;
            }
        }
        return $number;
    }

    function fancy_text($text) {
        if (!$text) {
            $text = "NA / NT";
        }
	if (strstr($text, "@") && strstr($text, "@")){
	    $text="<a href=\"mailto:$text\">$text</a>";
	}
        return $text;
    }
    
    function tags_to_html($text) {
	global $config;
        $text=str_ireplace("[b]", "<b>", $text);
	$text=str_ireplace("[/b]", "</b>", $text);
	$text=str_ireplace("[u]", "<u>", $text);
	$text=str_ireplace("[/u]", "</u>", $text);
	$text=str_ireplace("[cim1]", "<font style=\"color:#617f10\">", $text);
	$text=str_ireplace("[/cim1]", "</font>", $text);
	$text=str_ireplace("[frissitve]", "<div style=\"font-size: 8px; text-align:right;\">", $text);
	$text=str_ireplace("[/frissitve]", "</div>", $text);
	$text=str_ireplace("[/link]", "</a>", $text);
	$text=str_ireplace("[/freyalink]", "</a>", $text);
        $text=str_ireplace("[freyalink=", "<a href=\"".$config["site"]["absolutepath"]."/", $text);
	$text=str_ireplace("[link=", "<a href=\"", $text);
        $text=str_ireplace("]", "\">", $text);

        return $text;
    }
    
    function admin_this($type, $subtype, $id) {
	global $config;
	/* HÍREK JOGKEZELÉSE - csak ID1SU minden */
	if (($_SESSION["user"]["root"]==1 && ($type=="beosztas" || $subtype=="new" || $id!=1)) || $_SESSION["user"]["id"]==1){
 	    return true;
	    exit();
	}
	if ($type=="hir"){
	    if ($_SESSION["user"]["id"]==1){
	    return true;
	    exit();
	    }
	}
	else if ($type=="szervezet" || ($type=="user" && $subtype=="new")){
	    if ($config["rights"][$_SESSION["user"]["allomany_statusz"]]["szervezet"] || $_SESSION["user"]["rg_szervezetek"])		{
		$sql="SELECT teljes_id FROM szervezetek WHERE id=$id AND ( teljes_id = '".$_SESSION["user"]["szervezet_teljes_id"]."' OR teljes_id like '".$_SESSION["user"]["szervezet_teljes_id"]."-%')";
		$modthis=mysql_num_rows(mysql_query($sql));
		}
	    if ($_SESSION["user"]["root"]==1 || $_SESSION["user"]["rg_szervezetek"] == 2){
		if ($subtype=="delete"){
		$ucount=mysql_num_rows(mysql_query("SELECT id FROM felhasznalok WHERE szervezet_id=$id")); // SZERVEZETBEN LÉVŐ FELHASZNÁLÓK ELLENŐRZÉSÉHEZ
		$szcount=mysql_num_rows(mysql_query("SELECT id FROM szervezetek WHERE parent_id=$id")); // SZERVEZETBEN LÉVŐ ALÁRENDELT SZERVEK ELLENŐRZÉSÉHEZ
		}
		if ($subtype=="delete" && ($ucount || $szcount)) {
		    return false;
		    exit();
		}
		else {
		    return true;
		    exit();
		}
	    }
	}
	else if ($type=="user" && $id!=1){
	    if (($config["rights"][$_SESSION["user"]["allomany_statusz"]]["felhasznalo"] || $_SESSION["user"]["rg_szervezetek"]) && $subtype!="delete"){ // törölni ne tudjanak, inaktívra állítás elég
		$sql="SELECT felhasznalok.id as id FROM felhasznalok left join szervezetek on felhasznalok.szervezet_id = szervezetek.id WHERE szervezetek.teljes_id = '".$_SESSION["user"]["szervezet_teljes_id"]."' OR szervezetek.teljes_id like '".$_SESSION["user"]["szervezet_teljes_id"]."-%'";
		$result=mysql_query($sql);
		while($sor=mysql_fetch_assoc($result)){
		    if ($sor["id"]==$id){
			$modthis=true;
			break;
		    }
		}
	    }
	    if ($_SESSION["user"]["root"]==1 || $modthis){
		return true;
		exit();
	    }
	}
	return false;
    }
    
   function values_to_null($values) {
       foreach($values as $key => $value)
       {
	   if (!$value){ $values["$key"]=NULL; }
       }
       return $values;
   }
   
   function generate_azonosito() {
       $rendben=false;
       while(!$rendben){
       $i=0;   
       $rand=rand(10000000, 99999999);
       $azonosito="911".$rand;
       $sql="SELECT id FROM felhasznalok WHERE azonosito=$azonosito";
       $result=mysql_query($sql);
       $i=mysql_num_rows($result);
       if (!$i) { $rendben=true; }
       }
       return $azonosito;
   }
  // source: http://kevin.vanzonneveld.net/techblog/article/convert_anything_to_tree_structures_in_php/
  // módosítások: Dobó Tamás - 2012
  function explodeTree($array, $delimiter = '_', $baseval = false)
    {
  if(!is_array($array)) return false;
  $splitRE   = '/' . preg_quote($delimiter, '/') . '/';
  $returnArr = array();
  foreach ($array as $key => $val) {
    // Get parent parts and the current leaf
    $parts  = preg_split($splitRE, $key, -1, PREG_SPLIT_NO_EMPTY);
    $leafPart = array_pop($parts);
 
    // Build parent structure
    // Might be slow for really deep and large structures
    $parentArr = &$returnArr;
    foreach ($parts as $part) {
      if (!isset($parentArr[$part])) {
        $parentArr[$part] = array();
      } elseif (!is_array($parentArr[$part])) {
        if ($baseval) {
          $parentArr[$part] = array('__base_val' => $parentArr[$part]);
        } else {
          $parentArr[$part] = array();
        }
      }
      $parentArr = &$parentArr[$part];
    }
 
    // Add the final part to the structure
    if (empty($parentArr[$leafPart])) {
      $parentArr[$leafPart] = $val;
    } elseif ($baseval && is_array($parentArr[$leafPart])) {
      $parentArr[$leafPart]['__base_val'] = $val;
    }
  }
  return $returnArr;
}

function array_to_ulli($array){
    if (array_key_exists('__base_val', $array)){
	    echo $array["__base_val"]."\n";
            array_search($array["__base_val"], $_SESSION["user"]["idk"]);
    }
    echo "<ul class=\"treeview\">\n";
    foreach($array as $key => $member){
	//check for value member
            if(!is_array($member)){
                if ($key=='__base_val'){}
                else{
                //if value is present, echo it in an li
                echo "<li class=\"treeview\">&rarr; $member</li>\n";
                }
	    }
        else {
            //if the member is another array, start a fresh li
            echo "<li class=\"treeview\">\n";
            //and pass the member back to this function to start a new ul
            $this->array_to_ulli($member);
            //then close the li
            echo "</li>\n";
        }
    }
    print "</ul>\n";
    if (array_key_exists('__base_val', $array)){
    echo "";
    }    
}
   
}

?>