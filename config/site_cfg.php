<?php
// ---- NE BABRÁLD, HA NEM TUDOD MIRE VALÓ ----
//
//AZ OLDAL ALAPBEÁLLÍTÁSA 
$config["site"]["absolutepath"] = "http://freya.develop:8080";
$config["site"]["cookie_validtime"] = 2592000; // itt lehet megadni, hogy hány másodpercig fogadja el a sütit a rendszer
$config["site"]["session_validtime"] = 900; // itt lehet megadni, hogy hány másodpercig fogadja el a munkamenetet a rendszer

$config["site"]["tree_root_element"] = "Magyarország Rendőrsége";

//vezetők alapértelmezett jogosultságai - minden jog saját és alárendelt szervezetre határozható meg
//1 - szerkesztési jog / megtekintési | 2 - szerkesztési és jogadási jog 
$config["rights"][1]["szervezet"]=2;
$config["rights"][1]["felhasznalo"]=2;

//vezető helyettesek alapértelmezett jogosultságai - minden jog saját és alárendelt szervezetre vonatkozik
//1 - szerkesztési jog / megtekintési | 2 - szerkesztési és jogadási jog 
$config["rights"][2]["szervezet"]=1;
$config["rights"][2]["felhasznalo"]=1;

//titkárnők alapértelmezett jogosultságai - minden jog saját és alárendelt szervezetre vonatkozik
//1 - szerkesztési jog / megtekintési | 2 - szerkesztési és jogadási jog 
$config["rights"][3]["szervezet"]=1;
$config["rights"][3]["felhasznalo"]=1;

?>