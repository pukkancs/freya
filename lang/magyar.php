<?php

// UTF-8asítás...
$lang["nant"] = "NA / NT";

$lang["head"] = array(
    "title" => $config["site"]["name"] . " " . $config["site"]["version"],
    "keywords" => "Telefonkönyv",
    "description" => "Telefonkönyv"
);

$lang["main_menu"] = array(
    "bejelentkezes" => "Bejelentkezés",
    "elfelejtett_jelszo" => "Elfelejtett jelszó",
    "hirek" => "Hírek",
    "telefonkonyv" => "Telefonkönyv",
    "illetekesseg" => "Illetékesség",
    "sajat_vilag" => "Saját Világ",
    "admin" => "Admin",
    "kijelentkezes" => "Kijelentkezés"
);

$lang["side_menu"] = array(
    "bejelentkezes" => "Bejelentkezés",
    "elfelejtett_jelszo" => "Elfelejtett jelszó",
    "hirek" => "Hírek",
    "telefonkonyv" => "Telefonkönyv",
    "illetekesseg" => "Illetékesség",
    "sajat_vilag" => "Saját Világ",
    "admin" => "Admin",
    "kijelentkezes" => "Kijelentkezés"
);

$lang["side_menu"]["titles"] = array(
    "sajat_vilag" => "<font style=\"color:#617f10\">SAJÁT ADATOK</font> Menü",
    "hirek" => "<font style=\"color:#617f10\">RENDSZER</font> Hírek",
    "telefonkonyv" => "<font style=\"color:#617f10\">TELEFONKÖNYV</font> Menü",
    "felhasznalok" => "<font style=\"color:#617f10\">FELHASZNÁLÓK</font> Menü",
    "admin" => "<font style=\"color:#617f10\">ADMIN</font> Menü",
    "statisztikak" => "<font style=\"color:#617f10\">STATISZTIKÁK</font> Menü",
    "beosztasok" => "<font style=\"color:#617f10\">BEOSZTÁSOK</font> Menü",
    "egyeb_telefonkonyvek" => "<font style=\"color:#617f10\">EGYÉB</font> Telefonkönyvek",
    "megyei_telefonkonyvek" => "<font style=\"color:#617f10\">MEGYEI</font> Telefonkönyvek",
    "dokumentumok" => "<font style=\"color:#617f10\">TELEFONOS</font> Összesítők",
    "szerver_info" => "<font style=\"color:#617f10\">SZERVER</font> Információk",
    "kliens_info" => "<font style=\"color:#617f10\">KLIENS</font> Információk"
);

$lang["footer"] = array(
    "1" => "[ Töltésidő: ",
    "2" => "s - Adatbáziskérés: ",
    "3" => "db ]"
);

$lang["honapok"] = array(
    "01" => "Január",
    "02" => "Február",
    "03" => "Március",
    "04" => "Április",
    "05" => "Május",
    "06" => "Június",
    "07" => "Július",
    "08" => "Augusztus",
    "09" => "Szeptember",
    "10" => "Október",
    "11" => "November",
    "12" => "December",
);

$lang["bejelentkezes"] = array(
    "cim" => "<font style=\"color:#617f10\">FREYA</font> Bejelentkezés",
    "felhasznalonev" => "Felhasználói azonosító:",
    "jelszo" => "Jelszó:",
    "csokkentett_biztonsag" => "Csökkentett biztonság:",
    "cookie_kell" => "Az oldal megfelelő működéséhez a Cookie-kat és a Javasriptet engedélyezned kell!",
    "elkuld" => "Bejelentkezés",
    "mi_ez" => "súgó",
    "mi_ez_kifejtve" => "<div style=\"text-align:center;\"><a href=\"#\" id=\"login-toogle\" onclick=\"return false\"><span style=\"color: #617f10; font-size:12px; line-height: 30px;\">Súgó a bejelentkezéshez</span></a></div>
			    <div id=\"main_login_login\"><br/>A bejelentkezés felhasználónév-jelszó párossál lehetséges. A rendszer saját azonosítókat használ.<br />
				        <span style=\"color: #617f10; line-height: 30px;\">Teendők ha tudod a felhasználóneved, jelszavad</span>
				        <br />- 1: Írd be a felhasználóneved és jelszavad a megfelelő mezőbe. <br />
					- 2: Döntsd el, hogy kívánod-e használni a \"csökkentett biztonság\" üzemmódot. (leírást lásd lejebb)<br />
					- 3: Klikkelj a bejelentkezés gombra<br />
			            <span style=\"color: #617f10; line-height: 30px;\">Teendők ha nem ismered az azonosítóidat</span>
				        <br />
			- 1: Kattints az \"Elfelejtett jelszó\" menüpontra<br />
			- 2: Add meg a GroupWise címed! ( A @-osat :D ) <br />
			- 3: Pipáld ki a \"biztos, hogy elküldöd\" jelölőnégyetet! <br />
			- 4: Várd meg amíg megkapod az ideiglenes felhasználói azonosítót és jelszót a GroupWise címedre!<br />
			- 5: Jelentkezz be a megadott adatokkal a \"Bejelentkezés\" oldalon.<br />
			- 6: Kövesd az oldalon megjelent útmutatást!<br />
			<span style=\"color: #617f10; line-height: 30px;\">Teendők ha ezek után se boldogulsz az oldallal</span>
				        <br />
			- 1: Hivatali munkaidőben a hívd a 23/15-17 melléket és keres engem. (Dobó Tamás) <br />
			- 2: Munkaidőn túl írj egy levelet a dobota@baranya.police.hu címre. <br />
				        <br /></div>
	<div style=\"text-align:center;\"><a href=\"#\" id=\"lowsec-toogle\" onclick=\"return false\"><span style=\"color: #617f10; font-size:12px; line-height: 30px;\">Tájékoztató a csökkentett biztonságról</span></a></div>
			    <div id=\"main_login_lowsec\"><br/>A csökkentett biztonságú bejelentkezés kiválasztásával a felhasználói azonosítódat a böngészőben egy cookie-ban (sütiben) tároljuk.<br />
				        <span style=\"color: #617f10; line-height: 30px;\">Mi az előnye?</span>
				        <br />A cookie-ban tároljuk a felhasználói azonosítódat, így a böngésző újraindításánál nem kell újra beírnod a felhasználóneved és jelszavad,
				        valamint belépned, vagyis sokkal kényelmesebb az oldal használata. 
				        (Fontos! Ez nem azonos a böngésző jelszó mentési szolgáltatásával!)<br />
			            <span style=\"color: #617f10; line-height: 30px;\">Mi a hátránya?</span>
				        <br />A cookie-ban érzékeny adatok tárolása sajnos nem tekinthető biztonságosnak. 
				        Előfordulhat, hogy illetéktelenek hozzáférnek a cookie tartalmához,
				        ezzel megszerezve az oldalhoz a hozzáférésed.<br />
				        <span style=\"color: #617f10; line-height: 30px;\">Veszélyben van a jelszavam?</span>
				        <br />Nincs! A jelszót a rendszer csak és kizárólag a beléptetőszervernek küldi el, és cookieban soha semmilyen formában nem tárolja. 
                        A cookie-ban egy, az oldal által generált Passkey tárolódik, tehát illektéktelenek legrosszabb esetben is csak ehhez az oldalhoz tudnak hozzáférni.<br /></div>",
    "hibas-felhasznalonev-vagy-jelszo" => "Hibás felhasználónév vagy jelszó!",
    "lejart-cookie" => "A süti érvényessége lejárt! Jelentkezz be újra!",
    "lejart-idokorlat" => "A munkamenet időkorlátja lejárt! Jelentkezz be újra.",
    "inaktiv-statusz" => "Nincs jogosutságod bejelentkezni a rendszerbe! Lépj kapcsolatba a rendszergazdával.",
    "baned" => "Bejelentkezési jogosultságod felfüggesztésre került! Lépj kapcsolatba a rendszergazdával.",
    "hianyos-kitoltes" => "Felhasználónév és jelszó megadása kötelező!",
    "jelszo-kikuldve" => "Az új jelszavad elküldtük az általad megadott címre!"
);

$lang["elfelejtett_jelszo"] = array(
    "cim" => "<font style=\"color:#617f10\">FREYA</font> Elfelejtett jelszó kezelő",
    "email" => "GroupWise cím:",
    "biztos" => "Biztos, hogy elküldöd?",
    "cookie_kell" => "Az oldal megfelelő működéséhez a Cookie-kat és a Javasriptet engedélyezned kell!",
    "elkuld" => "Új jelszó igénylés",
    "reszletek" => "részletek",
    "reszletek_kifejtve" => "A jelszóemlékeztető elküldésével egy új jelszót generálunk neked és a megadott címre elküldjük.<br />
				        <span style=\"color: #617f10; line-height: 30px;\">Miért nem a régit mondja meg a rendszer?</span>
				        <br />A rendszer a jelszavakat titkosított formában tárolja, az eredeti jelszó nem nyerhető ki az adatbázisból.<br />
			            <span style=\"color: #617f10; line-height: 30px;\">Mit fog tartalmazni a nekem küldött levél?</span>
				        <br />A levélben elküldjük a bejelentkezéshez szükséges felhasználónevet és az újonnan generált jelszót.<br />
				        <span style=\"color: #617f10; line-height: 30px;\">Meg tudom változtatni a jelszavam?</span>
				        <br />A bejelenkezést követően a saját adatok menüben van lehetőséged a jelszavad megváltoztatására.<br />",
    "nem-letezo-email-cim" => "Hibásan adtad meg a GroupWise címedet, vagy nem szerepelsz a rendszerben!",
    "nem-erositetted-meg" => "Nem jelölted be a \"Biztos, hogy elküldöd?\" mezőt!",
    "nincs-kapcsolat-az-e-mail-szerverrel" => "Nincs kapcsolat az e-mail szerverrel! Próbáld meg késöbb!"
);

$lang["404"] = array(
    "404" => "<strong>404</strong> A keresett oldal nem található!</p>",
);

$lang["felhasznalok"] = array(
    "azonosito" => "Felhasználói azonosító",
    "jelszo" => "Jelszó",
    "uj_jelszo" => "Új jelszó",
    "uj_jelszo_megerosit" => "Új jelszó ismét",
    "email" => "GroupWise cím",
    "teljesnev" => "Teljes név",
    "titulus" => "Titulus",
    "keresztnev" => "Keresztnév",
    "kozepsonev" => "Középső név",
    "vezeteknev" => "Vezetéknév",
    "nicknev" => "Becenév vagy leánykori név",
    "titulus_2" => "Rendőrségi titulus",
    "rendfokozat" => "Rendfokozat",
    "beosztas" => "Beosztás",
    "beosztas_megjegyzes" => "Beosztás megjegyzés",
    "beosztas_jellege" => "Beosztás jellege",
    "nem" => "Nem",
    "szuletesnap" => "Születésnap",
    "nevnap" => "Névnap",
    "szervezet_id" => "Szervezet azonosító",
    "telephely_irsz" => "Telephely irányítószám",
    "telephely_varos" => "Telephely város",
    "telephely_szam" => "Telephely házszám",
    "telephely_cim" => "Telephely utca",
    "telephely_epulet" => "Telephely épület",
    "telephely_emelet" => "Telephely emelet",
    "telephely_iroda" => "Telephely iroda",
    "levelezesi_irsz" => "Levelezesi irányítószám",
    "levelezesi_varos" => "Levelezesi város",
    "levelezesi_cim" => "Levelezesi cím",
    "szervezet" => "Szervezet",
    "vezetoi" => "Vezetői telefon",
    "kozvetlen" => "Közvetlen telefon",
    "fax" => "Fax szám",
    "mobil" => "Mobil szám",
    "tetra" => "Tetra szám",
    "vez_kozvetlen" => "Vezetékes telefon",
    "vez_fax" => "Vezetékes fax",
    "2helyi" => "2. helyi telefon",
    "titkarsag" => "Titkárság száma",
    "privat_fax" => "Privát fax",
    "privat_vezetekes" => "Privát vezetékes",
    "privat_mobil" => "Privát mobil",
    "privat_email" => "Privát email",
    "megjegyzes" => "Megjegyzés",
    "telephely" => "Telephely",
    "levelezesi" => "Levelezési cím",
    "szuletesnap" => "Születésnap"
);

$lang["illetekesseg"] = array(
    "iranyitoszam" => "Irányítószám",
    "telepules" => "Település",
    "fonetikus" => "Fonetikus keresés",
    "szokezdet" => "Csak ha így kezdődik",
    "limit" => "Találatok száma oldalanként",
    "min2kar" => "Minimum 2 karakter megadása szükséges a kereséshez!",
    "nincstalalat" => "A keresés nem hozott eredményt. Változtasson a keresési feltételeken!",
    "hibas_iranyitoszam" => "Az irányítószám csak szám lehet!"
);

$lang["telefonkonyv"] = array(
    "itt_keresd" => "Itt keresd",
    "nev" => "Név",
    "beosztas" => "Beosztás",
    "szervezet" => "Szervezet",
    "kozvetlen" => "Telefonszám",
    "szokezdet" => "Csak ha így kezdődik",
    "fonetikus" => "Fonetikus keresés",
    "limit" => "Találatok száma oldalanként",
    "min2kar" => "Minimum 2 karakter megadása szükséges a kereséshez!",
    "nincstalalat" => "A keresés nem hozott eredményt. Változtasson a keresési feltételeken!",
);

$lang["aktivacio"] = array(
    "azonosito" => "Felhasználói azonosító",
    "email" => "GroupWise cím",
    "regi_jelszo" => "Régi jelszó",
    "uj_jelszo" => "Új jelszó",
    "uj_jelszo_ujra" => "Új jelszó megerősítése",
    "ellenorzokod" => "Ellenőrzőkód"
);

$lang["nemek"] = array(
    "1" => "Férfi",
    "2" => "Nő",
    "NA / NT" => "NA / NT"
);

$lang["vezenyles"] = array(
    "1" => "Vezényelve!",
    "NA / NT" => "Nincs vezényelve"
);

$lang["gomb"] = array(
    "modosit" => "Módosít",
    "megse" => "Mégse",
    "keresd" => "Keresd!",
    "elkuld" => "Elküld",
    "aktival" => "Aktivál",
    "torold" => "Töröld",
    "athelyez" => "Áthelyez",
    "hozzaad" => "Hozzáad"
);

$lang["beosztas_jellege"] = array(
    "1" => "Rendőrségi állományban, aktív, vezető beosztásban",
    "2" => "Rendőrségi állományban, aktív, vezető helyettes beosztásban",
    "3" => "Rendőrségi állományban, aktív, titkár(nő) / ügykezelő beosztásban",
    "4" => "Rendőrségi állományban, aktív, tanácsadó beosztásban",
    "5" => "Rendőrségi állományban, aktív, általános beosztásban",
    "6" => "Rendőrségi állományban, inaktív (füv, gyes, gyed stb..)",
    "7" => "Rendőrségi nyugállományban, inaktív",
    "8" => "Állományon kívül - (leszerelt, stb...)",
    "9" => "Társszerv állományába, aktív",
    "10" => "Társszerv állományában, aktív"
);

$lang["log"]["esemenyek"] = array(
    "1" => "Fiók létrehozva",
    "2" => "Fiók aktiválva",
    "3" => "Fiók módosítva",
    "4" => "Fiók törölve",
    "5" => "Szervezet létrehozva",
    "6" => "Szervezet módosítva",
    "7" => "Szervezet törölve",
    "8" => "Beosztás létrehozva",
    "9" => "Beosztás módosítva",
    "10" => "Beosztás törölve",
    "11" => "Rendfokozat létrehozva",
    "12" => "Rendfokozat módosítva",
    "13" => "Rendfokozat törölve",
    "14" => "Privát adatok megtekintve",
    "15" => "Kijelentkezés",
    "16" => "Bejelentkezés"
);

$lang["hirek"] = array(
    "cim" => "Hír címe:",
    "nyito" => "Bevezető:",
    "torzs" => "Törzs:",
    "kiemelt" => "Kiemelt eddig:",
    "rendszer" => "Rendszerfelhívás:"
);

$lang["bongeszo"]["ff"]="Mozilla Firefox";
$lang["bongeszo"]["ie"]="Microsoft Internet Explorer";
$lang["bongeszo"]["ch"]="Google Chrome";
$lang["bongeszo"]["eg"]="Egyéb";

$lang["szervezet"] = array(
    "nev" => "Szervezet megenevezese:"
);

$lang["szervezet"]["tipus"] = array(
    "1" => "Rendező bejegyzés",
    "2" => "Egyéb szervezet",
    "3" => "Együttműködő és társ szervezetek",
    "4" => "Tényleges Rendőrségi Szervezetek",
    "5" => "Tényleges Kiemelt Rendőrségi Szervezetek"
);
?>