<?php 

header('Content-type: text/html; charset=UTF-8');
//KONFIGURÁCIÓS FÁJL BETÖLTÉSE
include("config/config.php");
//NYELVI FÁJL BETÖLTÉSE
include("lang/magyar.php");
?>
<head>
        <title> <?php print($lang["head"]["title"]); ?></title>
        <base href="<?php print($config["site"]["absolutepath"]); ?>/" />
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link href="favicon.ico" type="image/x-icon" rel="shortcut icon" />
        <meta name="Keywords" content=" <?php print($lang["head"]["keywords"]); ?>"/>
        <script type="text/javascript" src="js/jquery.js"></script>
        <script type="text/javascript" src="js/jquery.ui.js"></script>
        <script type="text/javascript" src="js/jquery.ui.datepicker-hu.js"></script>
	<style type="text/css">
	body {
    font-family: Lucida Grande,Verdana,Bitstream Vera Sans,Arial,sans-serif;
    font-size: 11px;
    font-style: normal;
    line-height: normal;
    font-weight: normal;
    font-variant: normal;
    text-transform: none;
    text-align: center;
    background-color: #FFF;
    margin: 0 0 0 0;
}
a:link		{text-decoration:none;color:#000; background-color:transparent}
a:visited	{text-decoration:none;color:#000; background-color:transparent}
a:active	{text-decoration:underline; color:#000; background-color:transparent}
a:hover		{text-decoration:underline; color:#000; background-color:transparent}
#site
{
    text-align: left;
    background-color: #FFF;
    width: 100%;
}
.site_spacer
{
    clear: both;
    height: 300px;
}

#top_spacer
{
    background-color: #000;
    height: 10px;    
}
#head_container
{
    background-color: #000;
}
#head
{
    width:1002px;
    background-color: #000;
    height: 40px;
    margin: 0 auto 0 auto;
}
#head_logo
{
    float:left;
    width:200px;
    height: 39px;
    border-bottom: none;
    font-size: 26px;
    font-weight: bold;
    color: #0972a5;
    text-align: left;
}
a.head_logo:link 	{text-decoration:none;color:#0972a5; background-color:transparent}
a.head_logo:visited	{text-decoration:none;color:#0972a5; background-color:transparent}
a.head_logo:active	{text-decoration:none;color:#0972a5; background-color:transparent}
a.head_logo:hover	{text-decoration:none;color:#0972a5; background-color:transparent}

#head_menu_container
{
    float:left;
    width:800px;
    height: 39px;
    border: solid 1px #0972a5;
    border-bottom: none;
    margin: 0 auto 0 auto;
    background-color: #0972a5;
}
.head_menu_spacer
{
    height: 20px;
}
.head_menu
{
    color: #FFF;
    text-align:  right;
    font-weight:bold;
    font-size:12px;
    margin-right: 4px;
    margin-left: 4px;
}
a.head_menu:link 	{text-decoration:none;color:#FFF; background-color:transparent}
a.head_menu:visited	{text-decoration:none;color:#FFF; background-color:transparent}
a.head_menu:active	{text-decoration:underline;color:#FFF; background-color:transparent}
a.head_menu:hover	{text-decoration:underline;color:#FFF; background-color:transparent}

#main
{
    width: 1002px;
    margin: 0 auto 0 auto;
    border-top: none;
}

#main_left
{
    float:left;
    width: 200px;
    background-color: #FFF;
}
.main_left_spacer
{
    height:10px;
}
.main_left_title
{	
    font-weight:bold;
    border-bottom:1px dotted #868686;
    font-size:14px;
    height:20px;
    margin-right: 4px;
    margin-bottom: 4px;
}
.main_left_text
{
    margin-left: 4px;
    margin-right: 4px;
}
main_left_menu
{
    margin-left: 4px;
    margin-right: 4px;
}
a.main_left_menu:link		{text-decoration:none; font-weight: bold; color:#000; background-color:transparent}
a.main_left_menu:visited	{text-decoration:none; font-weight: bold; color:#000; background-color:transparent}
a.main_left_menu:active		{text-decoration:underline; font-weight: bold; color:#617f10; background-color:transparent}
a.main_left_menu:hover		{text-decoration:underline; font-weight: bold;  color:#617f10; background-color:transparent}

#main_center_big_container
{
    width: 802px;
    float: left;
}
#main_center
{
    border: solid 1px #AAA;
    border-top: none;
    background-color: #FFF;
}
.main_center_container
{
    width: 790px;
    clear:both;
    margin: 0px 5px 5px 5px;
}
.main_center_spacer
{
    clear:both;
    height:10px;
}
.main_center_title
{
    font-weight:bold;
    font-size: 14px;
    height:20px;
    border-bottom:1px dotted #868686;
    font-size:14px;
    margin-right: 4px;
    margin-bottom: 4px;
}
.main_center_title_left
{
    float:left;
    height:20px;
    vertical-align: bottom;
    font-size:14px;
    margin-bottom: 4px;
}
.main_center_title_right
{
    float:right;
    height:20px;
    font-size:10px;
    text-align: right;
    margin-right: 4px;
    margin-bottom: 4px;
}
#main_center_content_names
{
    padding-top: 5px;
    margin-left:10px;
    margin-right: 10px;
    background-color: #d4eaf7;
    border:1px dotted #868686;
}
.main_center_content_names_white
{
    padding-top: 5px;
    background-color: #FFF;
    border:1px dotted #868686;
}
.main_center_content_names_blue
{
    padding-top: 5px;
    background-color: #d4eaf7;
    border:1px dotted #868686;
}
.main_center_content_names_gray
{
    padding-top: 5px;
    background-color: #eeeeee;
    border:1px dotted #868686;
}
.main_center_content_left
{
    min-height: 22px;
    float:left;
    font-weight: bold;
    width: 200px;
    padding-left: 5px;
    text-align: left;
}
.main_center_content_center
{
    min-height: 22px;
    float:left;
    width: 380px;  
    text-align: center; 
    margin-left:10px;
    margin-right: 10px; 
}
.main_center_content_right
{
    min-height: 22px;
    float:right; 
    padding-right: 5px;
    text-align: right;
}
.main_center_content_spacer
{
    clear:both; 
    height: 4px;
}
.main_center_text
{
    margin-left: 4px;
    margin-right: 4px;
}
.main_login_container
{
    width: 600px;
    margin: 0 auto 0 auto;
}
.main_login_left
{
    width:240px;
    float: left;
    text-align: right;
    margin-right: 10px;
}
.main_login_right
{
    width:350px;
    float:left;
}
.main_login_spacer
{
    height: 5px;
    clear: both;
}
.main_login_both
{
    text-align: center;
}
#main_login_miez
{
    border-top:1px dotted #868686;
    border-bottom:1px dotted #868686;
    margin: 10px 0 10px 0;
    text-align: justify;         
}
.main_closer_left
{
    font-size:10px;
    float:left;
    width: 240px;
    text-align: left;
}
.main_closer_center
{
    font-size:10px;
    float:left;
    width: 300px;  
    text-align: center; 
    margin-left:10px;
    margin-right: 10px; 
}
.main_closer_right
{
    font-size:10px;
    float:left; 
    width: 240px;
    text-align: right;
}
.break { page-break-before: always; }
	</style>
	<?php
$config["sql"] = array(
    "dbhost" => "10.48.0.9",
    "dbuser" => "dobot",
    "dbpass" => "hurkakolbasz",
    "dbname" => "freya"
);

        if (!isset($mysql_kapcsolat)) {
            $mysql_kapcsolat = mysql_connect($config["sql"]["dbhost"], $config["sql"]["dbuser"], $config["sql"]["dbpass"])
                    or die("Nem sikerült az adatbázisszervehez csatlakozni, MySQL hibaüzenet:" . mysql_error());
            mysql_select_db($config["sql"]["dbname"])
                    or die("Nem sikerult az adatbazist elerni, MySQL hibaüzenet:" . mysql_error());
        }
        mysql_query("SET NAMES utf8");

	
	include("system/odin.php"); $odin=new odin();

	$sql="SELECT * FROM szervezetek WHERE teljes_id LIKE \"".$_GET["id"]."%\" ORDER BY `teljes_id` ASC";
	$result66 = mysql_query($sql);
        while ($sor66 = mysql_fetch_assoc($result66)) {
	$q["1"]=$sor66["id"];
	$details = $odin->get_szervezet_details($q["1"]);
        if ($details["szervezet_tipus"]==5 || $details["szervezet_tipus"]==1 || stristr($details["nev"], "kapitányság")) {
            ?>
	
            <div class="main_center_container">
		<div class="site_spacer"></div>
                <div class="main_center_spacer"></div>
                <div class="ui-widget">
                    <div class="ui-state-highlight ui-corner-all" style="font-size: 24px;"> 
                        <p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
                           <?php print $odin->fancy_text($details["nev"]); ?>
                    </div>
                </div>
            </div>
            <div class="site_spacer"></div>   
	    <div class="break"></div>
            <?php
        }        
	if (!$details && $q[1]) {
            ?>
	
            <div class="main_center_container">
                <div class="main_center_spacer"></div>
                <div class="ui-widget">
                    <div class="ui-state-highlight ui-corner-all"> 
                        <p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
                            Nincs ilyen azonosítójú szervezet!
                    </div>
                </div>
            </div>
            <div class="site_spacer"></div>     
            <?php
        } elseif ($details && $q[1])  {
            ?>
            <div class="main_center_container">

                <?php if ($details["szervezet_tipus"] != 1) { ?>

                    <div class="main_center_title">
                        <div class="main_center_title_left"><font style="color:#617f10"><?php print strtoupper($odin->fancy_text($details["nev"])); ?></font> Elérhetőségei</div>
                        <div class="main_center_title_right"></div>
                    </div>

                    <div class="main_center_content_spacer"></div>
                    <div class="main_center_content_left">Vezető:</div>
                    <div class="main_center_content_center"><?php print $odin->fancy_text($details["vezeto_tel"]); ?></div>
                    <div class="main_center_content_right"><?php print $odin->fancy_text($details["vezeto"]); ?></div>
                    <div class="main_center_content_spacer"></div>
                    <?php if ($details["vezeto-helyettes"]) { ?>
                        <div class="main_center_content_left">Vezető-helyettes:</div>
                        <div class="main_center_content_center"><?php print $odin->fancy_text($details["vezeto-helyettes_tel"]); ?></div>
                        <div class="main_center_content_right"><?php print $odin->fancy_text($details["vezeto-helyettes"]); ?></div>
                        <div class="main_center_content_spacer"></div>
                    <?php } ?>
                    <div class="main_center_content_left">Titkárság:</div>
                    <div class="main_center_content_center"><?php print $odin->fancy_text($details["titkarno_tel"]); ?></div>
                    <div class="main_center_content_right"><?php print $odin->fancy_text($details["titkarno"]); ?></div>
                    <div class="main_center_content_spacer"></div>
                    <div class="main_center_content_left">Fax:</div><div class="main_center_content_center"><?php print $odin->fancy_phone_number($details["fax"]); ?></div>
                    <div class="main_center_content_spacer"></div>
                    <div class="main_center_content_left">Elektronikus levelezési cím:</div><div class="main_center_content_center"><?php print $odin->fancy_text($details["email"]); ?></div>
                    <div class="main_center_content_spacer"></div>
                    <div class="main_center_content_left">Külső telefonszám:</div><div class="main_center_content_center"><?php print $odin->fancy_phone_number($details["vez_kozvetlen"]); ?></div>
                    <div class="main_center_content_spacer"></div>
                    <div class="main_center_content_left">Külső faxszám:</div><div class="main_center_content_center"><?php print $odin->fancy_phone_number($details["vez_fax"]); ?></div>
                    <div class="main_center_content_spacer"></div>
                    <div class="main_center_content_left">Cím:</div><div class="main_center_content_center"><?php print $odin->fancy_text($details["telephely"]); ?></div>
                    <div class="main_center_content_spacer"></div>
                    <div class="main_center_content_left">Levelezési cím:</div><div class="main_center_content_center"><?php print $odin->fancy_text($details["levelezesi"]); ?></div>
                    <div class="main_center_content_spacer"></div>	    

                    <div class="main_center_title">
                        <div class="main_center_title_left"><font style="color:#617f10">ÁLTALÁNOS</font> Adatok</div>
                        <div class="main_center_title_right"></div>
                    </div>
                    <div class="main_center_content_left">Hírközpont:</div><div class="main_center_content_center"><?php print $odin->fancy_phone_number($details["hirkozpont"]); ?></div>
                    <div class="main_center_content_spacer"></div>
                    <div class="main_center_content_left">Ügyelet:</div><div class="main_center_content_center"><?php print $odin->fancy_phone_number($details["ugyelet"]); ?></div>
                    <div class="main_center_content_spacer"></div>


                <div class="main_center_title">
                    <div class="main_center_title_left"><font style="color:#617f10">SZERVEZETI</font> Besorolás</div>
                    <div class="main_center_title_right"><font style="color:#617f10">ALÁRENDELT</font> Szervek</div>
                </div>
                <?php
		foreach ($details["valami"] as $key => $value) {
                    $details["szervezet"]["nev"] = "<a href=\"" . $config["site"]["absolutepath"] . "/szervezetek/" . $value["id"] . "\">" . $value["nev"] . "</a><br />" . $details["szervezet"]["nev"];
                }
                $details["szervezet"]["nev"]="<a href=\"" . $config["site"]["absolutepath"] . "/szervezetek\">Magyarország Rendőrsége</a><br />". $details["szervezet"]["nev"];
		?>

                <div style="width:49%; float:left; text-align: left;"><?php print $details["szervezet"]["nev"]; ?></div>
                <div style="width:2%; float:left;">&nbsp;</div>
                <div style="width:49%; float:left; text-align: right;">
		                    <?php
                $a = 0;
                $sql = "SELECT id, nev, teljes_id FROM szervezetek WHERE teljes_id LIKE '" . $details["teljes_id"] . "%'";
                $result = mysql_query($sql);
                while ($sor = mysql_fetch_assoc($result)) {
                    if (!strcmp($sor["teljes_id"], ($details["teljes_id"] . "-" . $sor["id"]))) {
                        $details["alarendelt_szervezet"]["nev"].="<a href=\"" . $config["site"]["absolutepath"] . "/szervezetek/" . $sor["id"] . "\">" . $sor["nev"] . "</a><br />";
                        $a++;
                    }
                }
		                ?>
		 <?php if ($a) { ?>   
		    <?php print $details["alarendelt_szervezet"]["nev"]; ?>
		  <?php } else { ?> Nincs alárendelt szerv!
		  <?php } ?>
		</div>
		
                <div class="main_center_content_spacer"></div>

                <?php if ($details["szervezet_tipus"] != 1) { ?>
                    <div class="main_center_title">
                        <div class="main_center_title_left"><font style="color:#617f10">ALKALMAZOTTAK</font> Listája</div>
                        <div class="main_center_title_right"></div>
                    </div>
                    <?php
                    $a = 0;
                    $sql = "SELECT id FROM felhasznalok WHERE id > 1 AND szervezet_id='" . $details["id"] . "%' AND (`allomany_statusz`<6 OR `allomany_statusz`>8) ORDER BY allomany_statusz ASC, vezeteknev";
                    $result = mysql_query($sql);
                    while ($sor = mysql_fetch_assoc($result)) {
                        $a++;
                        $details = $odin->get_user_details($sor["id"]);
                        ?>
			<div class="main_center_spacer"></div>
                        <div class="main_center_content_names<?php
			    if (($a) % 2 == 0)
				print "_white"; else
				print "_blue";
			    ?>" class="ui-corner-all">
			<div class="main_center_content_left" style="font-weight:normal; width: 304px;">
			<?php
		print "<b>".$details["user"]["beosztas"]."</b><font  style=\"font-size:9px;\">";
		if ($details["user"]["beosztas_megjegyzes"])
		    print "<br />(" . $details["user"]["beosztas_megjegyzes"] . ")"; 

		    print "</font>";
		?>	
		</div>
			<div class="main_center_content_center" style="font-weight:normal; width: 150px;"><?php
		print ("<b><a href=\"" . $config["site"]["absolutepath"] . "/felhasznalok/" . $details["user"]["id"] . "\">" . $details["user"]["teljesnev"] . "</a></b><br /><font  style=\"font-size:9px;\">" . $details["user"]["rendfokozat"] . "<br />");
					if ($details["user"]["titulus_2"])
		    print $details["user"]["titulus_2"] . "<br />";
		print ("</font>");
			    ?></div>
			<div class="main_center_content_right" style="font-weight:normal; width: 300px;"><?php
		if ($details["user"]["email"] != "NA / NT")
		    print "<a href=\"mailto:" . $details["user"]["email"] . "\">" . $details["user"]["email"] . "</a>";
		else print "e-mail: NA /NT";
		print ("<br /><font style=\"font-size:9px;\">közvetlen: <a href=\"".$config["site"]["absolutepath"]."/telefonkonyv/kereses/kereses-helye=/nev=/beosztas=/szervezet=/kozvetlenszam=".$details["user"]["kozvetlen"]."/szokezdet=/lista=1-10"."\">" . $odin->fancy_phone_number($details["user"]["kozvetlen"]) . "</a><br />fax: " . $odin->fancy_phone_number($details["user"]["fax"]) . "</font>");
			    ?></div>
			<div class="main_center_content_spacer"></div>
		    </div>
                        <?php
                    }
                    if (!$a) {
                        ?>
                        <div class="main_center_content_left"></div>
                        <div class="main_center_content_center">Nincsenek alkalmazottak felvíve!</div>
                        <div class="main_center_content_right"></div>
                        <div class="main_center_content_spacer"></div>
                    <?php } ?>

                <?php } ?>
		<div class="main_center_spacer"></div>	
		<?php if ($details["szervezet_tipus"] != 1) { ?>
                    <div class="main_center_title">
                        <div class="main_center_title_left"><font style="color:#617f10">INAKTÍV</font> Alkalmazottak listája</div>
                        <div class="main_center_title_right"></div>
                    </div>
                    <?php
                    $a = 0;
                    $sql = "SELECT id FROM felhasznalok WHERE id > 1 AND szervezet_id='" . $q[1] . "' AND `allomany_statusz`>5 and `allomany_statusz`<9 ORDER BY allomany_statusz ASC, vezeteknev";
                    $result = mysql_query($sql);
                    while ($sor = mysql_fetch_assoc($result)) {
                        $a++;
                        $details = $odin->get_user_details($sor["id"]);
                        ?>
			<div class="main_center_spacer"></div>
                        <div class="main_center_content_names<?php
			    if (($a) % 2 == 0)
				print "_gray"; else
				print "_gray";
			    ?>" class="ui-corner-all">
			<div class="main_center_content_left" style="font-weight:normal; width: 304px;">
			<?php
		print "<b>".$details["user"]["beosztas"]."</b><font  style=\"font-size:9px;\">";
		if ($details["user"]["beosztas_megjegyzes"])
		    print "<br />(" . $details["user"]["beosztas_megjegyzes"] . ")"; 

		    print "</font>";
		?>	
		</div>
			<div class="main_center_content_center" style="font-weight:normal; width: 150px;"><?php
		print ("<b><a href=\"" . $config["site"]["absolutepath"] . "/felhasznalok/" . $details["user"]["id"] . "\">" . $details["user"]["teljesnev"] . "</a></b><br /><font  style=\"font-size:9px;\">" . $details["user"]["rendfokozat"] . "<br />");
					if ($details["user"]["titulus_2"])
		    print $details["user"]["titulus_2"] . "<br />";
		print ("</font>");
			    ?></div>
			<div class="main_center_content_right" style="font-weight:normal; width: 300px;"><?php
		if ($details["user"]["email"] != "NA / NT")
		    print "<a href=\"mailto:" . $details["user"]["email"] . "\">" . $details["user"]["email"] . "</a>";
		else print "e-mail: NA /NT";
		print ("<br /><font style=\"font-size:9px;\">közvetlen: <a href=\"".$config["site"]["absolutepath"]."/telefonkonyv/kereses/kereses-helye=/nev=/beosztas=/szervezet=/kozvetlenszam=".$details["user"]["kozvetlen"]."/szokezdet=/lista=1-10"."\">" . $odin->fancy_phone_number($details["user"]["kozvetlen"]) . "</a><br />fax: " . $odin->fancy_phone_number($details["user"]["fax"]) . "</font>");
			    ?></div>
			<div class="main_center_content_spacer"></div>
		    </div>
                        <?php
                    }
                    if (!$a) {
                        ?>
                        <div class="main_center_content_left"></div>
                        <div class="main_center_content_center">Nincsenek alkalmazottak felvíve!</div>
                        <div class="main_center_content_right"></div>
                        <div class="main_center_content_spacer"></div>
                    <?php } ?>

                <?php } ?>
			    
            <div class="main_center_spacer"></div>	    <div class="break"></div>
                            <?php } ?>
		<?php
        } else { ?>
<div class="main_center_spacer"></div>

            <div class="main_center_container">


                <div class="main_center_title">
                    <div class="main_center_title_left"><font style="color:#617f10">SZERVEZETI</font> Besorolás</div>
                    <div class="main_center_title_right"></div>
                </div>
                <?php
                $details["szervezet"]["nev"]="<a href=\"" . $config["site"]["absolutepath"] . "/szervezetek\">Magyarország Rendőrsége</a><br />". $details["szervezet"]["nev"];
		?>

                <div class="main_center_content_left"></div>
                <div class="main_center_content_center"><?php print $details["szervezet"]["nev"]; ?></div>
                <div class="main_center_content_right"></div>
                <div class="main_center_content_spacer"></div>


                <div class="main_center_title">
                    <div class="main_center_title_left"><font style="color:#617f10">ALÁRENDELT</font> SZERVEK</div>
                    <div class="main_center_title_right"></div>
                </div>
                <?php
                $a = 0;
                $sql = "SELECT id, nev, teljes_id FROM szervezetek WHERE teljes_id LIKE '%'";
                $result = mysql_query($sql);
                while ($sor = mysql_fetch_assoc($result)) {
                    if (!strcmp($sor["teljes_id"],($sor["id"]))) {
                        $details["alarendelt_szervezet"]["nev"].="<a href=\"" . $config["site"]["absolutepath"] . "/szervezetek/" . $sor["id"] . "\">" . $sor["nev"] . "</a><br />";
                        $a++;
                    }
                }
                ?>

                <?php if ($a) { ?>
                    <div class="main_center_content_left"></div>
                    <div class="main_center_content_center"><?php print $details["alarendelt_szervezet"]["nev"]; ?></div>
                    <div class="main_center_content_right"></div>
                    <div class="main_center_content_spacer"></div>

                <?php } else { ?>
                    <div class="main_center_content_left"></div>
                    <div class="main_center_content_center">Nincs alárendelt szerv!</div>
                    <div class="main_center_content_right"></div>
                    <div class="main_center_content_spacer"></div>
                <?php } ?>


    <div class="main_center_spacer"></div>	    <div class="break"></div>
    <?php	}
    
    ?></div><?php
		}
	
	
	        if (isset($mysql_kapcsolat))
            mysql_close($mysql_kapcsolat);
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>
    </body>
</html>
