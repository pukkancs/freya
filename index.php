<?php 
//INDULÁS PILLANATA
$start_time=microtime(true);
$time=time();
//MUNKAMENET INDÍTÁSA
session_start();
//HEADER BEÁLLÍTÁSA
header('Content-type: text/html; charset=UTF-8');
//KONFIGURÁCIÓS FÁJL BETÖLTÉSE
include("config/config.php");
//NYELVI FÁJL BETÖLTÉSE
include("lang/magyar.php");
include("system/odin.php"); $odin=new odin();

$odin->post_to_fancy_url();

//Kapcsolódás a MYSQL adatbázishoz
$odin->connect_to_mysql();

// Magic quotes védelem ellenőrzése
$odin->check_magic_quotes_qpc();
//A kérés szétrobbantása a / jelek mentén - itt keletkezik a $q[0], $q[1], stb...
foreach ($_POST as $a => $b) {
    $_POST["$a"] = htmlspecialchars($b,ENT_QUOTES,"UTF-8");
   
    }
foreach ($_GET as $a => $b) {
    $_GET["$a"] = htmlspecialchars($b,ENT_QUOTES,"UTF-8");
 
    }
$q=explode("/",$_GET["query"]);
$q[0]=str_replace('-','_',$q[0]); //- kicserélése _ -re a kérésben (classnevek miatt) 
$q[1]=str_replace('-','_',$q[1]); //csak az első kettőben ugyanis ezek hívnak fuggvényeket. a többinél hátrány lenne

include("system/loki.php"); $loki=new loki();

// Kijelentkezés - süti és munkamenet törlése, átirányítás a bejelentkező oldalra
if ($_GET["query"] == "kijelentkezes" ) { $odin->logout(); }
//USERAGENT CLASS BETÖLTÉSE
include("system/useragent.php"); $ua=new UserAgent();

//Felhasználóellenőrzés
if($q[0]=="bejelentkezes" || $q[0]=="elfelejtett_jelszo" || ($q[0]=="aktivacio" && $_SESSION["user"]["id"])){}else{include("system/heimdallr.php"); $heimdallr=new heimdallr();}
//NYITÓLAP BEÁLLÍTÁSA 
if((!isset($q[0]) || $q[0]=="nyitooldal" || $q[0]=="") && !isset($_SESSION["user"]["id"])) { $q[0]="bejelentkezes"; }
if((!isset($q[0]) || $q[0]=="nyitooldal" || $q[0]=="bejelentkezes" || $q[0]=="") && isset($_SESSION["user"]["id"])) { $q[0]="hirek"; }
//404 beállítása abban az esetben ha olyan $q[0] kérés érkezik ami nem létező modulra hivatkozik
if(!isset($_SESSION["user"]["id"]))
    {
    $page = array
        (
        "0" => "bejelentkezes",
        "1" => "elfelejtett_jelszo",
        );
    }
else
    {
    $page = array
       (
        "0" => "hirek",
        "1" => "sajat_vilag",
	"2" => "illetekesseg",
        "3" => "telefonkonyv",
	"4" => "felhasznalok",
	"5" => "aktivacio",
	"6" => "admin",
	"7" => "szervezetek",
	"8" => "beosztasok"
        );    
    }
foreach($page as $counter=>$match)
        {
            if (eregi($match, $q[0]))
            {
                $matchfound=true;
                break;
            }
        }

if (!isset($matchfound)) { $q[0]="error_404"; }
// html cuccok betöltése
include("content/output_html.php"); // ez tartalmazza a htlm cuccokat

// a lényeges dolgok még a kiiratás előtt lefutnak, így az oldalsó kiiartások már frissült adatokkal dolgozhatnak.
ob_start();

   include_once("system/class_".$q[0].".php"); 
   $$q[0]=new $q[0](); 
   $output=ob_get_contents();

ob_end_clean();



$output_html->head_box();
$output_html->left_box();
echo $output;
$output_html->footer_box();

$odin->addsiteload();

//MYSQL kapcsoalt lezárása
if(isset($mysql_kapcsolat))$odin->close_mysql_connection();

?>