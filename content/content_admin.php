<?php

class admin_content {

//*********************************************************************	
    function online_felhasznalok_kiir() {
	global $lang, $odin, $config;
	?>
	<div class="main_center_spacer"></div>

	<?php
	$time = time();
	$sql = "SELECT id, user_id, max(aktiv_ekkor) as lastact, bongeszo, bongeszo_verzio, kijelentkezve FROM munkamenetek WHERE id >0";
	if (!$_POST["feltetel"]) {
	    $time = $time - $config["site"]["session_validtime"];
	    $sql.=" AND aktiv_ekkor>$time AND `kijelentkezve` IS NULL GROUP BY user_id ORDER BY lastact DESC";
	}
	if ($_POST["feltetel"] == "online") {
	    $time = $time - $config["site"]["session_validtime"];
	    $sql.=" AND aktiv_ekkor>$time AND `kijelentkezve` IS NULL GROUP BY user_id ORDER BY lastact DESC";
	}
	if ($_POST["feltetel"] == "ma") {
	    $time = strtotime(date('Y-m-d'));
	    $sql.=" AND aktiv_ekkor>$time GROUP BY user_id ORDER BY lastact DESC";
	}
	if ($_POST["feltetel"] == "24ora") {
	    $time = $time - 86400;
	    $sql.=" AND aktiv_ekkor>$time GROUP BY user_id ORDER BY lastact DESC";
	}
	if ($_POST["feltetel"] == "7nap") {
	    $time = $time - 640800;
	    $sql.=" AND aktiv_ekkor>$time GROUP BY user_id ORDER BY lastact DESC";
	}
	if ($_POST["feltetel"] == "30nap") {
	    $time = $time - 2592000;
	    $sql.=" AND aktiv_ekkor>$time GROUP BY user_id ORDER BY lastact DESC";
	}
	?>
	<div class="main_center_container">
	    <div class="main_center_title">
		<div class="main_center_title_left"><font style="color:#617f10">ONLINE </font> Felhasználók (<?php print mysql_numrows(mysql_query($sql)); ?>)</div>
		<div class="main_center_title_right">
		    <form action="<?php print $config["site"]["absolutepath"] . "/admin/online-felhasznalok"; ?>" method="post">
			<select name="feltetel">
			    <option value="online">Jelenleg online</option>
			    <option value="ma" <?php
	if ($_POST["feltetel"] == "ma") {
	    print "selected=\"selected\"";
	}
	?> >Ma</option>
			    <option value="24ora" <?php
	if ($_POST["feltetel"] == "24ora") {
	    print "selected=\"selected\"";
	}
	?> >Az elmúlt 24 órában</option>
			    <option value="7nap" <?php
			    if ($_POST["feltetel"] == "7nap") {
				print "selected=\"selected\"";
			    }
			    ?> >Elmúlt 7 napban</option>
			    <option value="30nap" <?php
			    if ($_POST["feltetel"] == "30nap") {
				print "selected=\"selected\"";
			    }
	?> >Elmúlt 30 napban</option>
			</select>
			<input class="ui-state-default ui-corner-all" type="submit" name="szukitsd" value="szűkítsd" />
		    </form></div>
	    </div>
	<?php
	$result = mysql_query($sql);
	$sql_query_count++;
	while ($sor = mysql_fetch_assoc($result)) {
	    $details = $odin->get_user_basedetails($sor["user_id"]);
	    ?>

	        <div class="main_center_content_spacer"></div>
	        <div class="main_center_content_left"><?php print "<a href=\"" . $config["site"]["absolutepath"] . "/felhasznalok/" . $details["user"]["id"] . "\">" . $details["user"]["teljesnev"] . "</a>"; ?></div>
	        <div class="main_center_content_center"><?php print "" . date('Y-m-d H:i:s', $sor["lastact"]); ?></div>
	        <div class="main_center_content_right"><?php print $sor["bongeszo"] . " " . $sor["bongeszo_verzio"]; ?></div>
	    <?php
	}
	?>

	</div>

	<div class="main_center_spacer"></div>
	<?php
    }

//*********************************************************************	
    function oldal_statisztika_kiir() {
	include ("cache/site_stat.php");
    }

//*********************************************************************
    function grafikus_statisztika_kiir() {
	include ("cache/grafikus_stat.php"); 
    }

//*********************************************************************
    function verzio_statisztika_kiir() {
	include ("cache/verzio_stat.php");
    }

//*********************************************************************
			function sys_log_kiir() {
			    global $lang, $odin, $q, $config;
			    ?>
	<div class="main_center_spacer"></div>
	<div class="main_center_container">
	    <div class="main_center_title">
		<div class="main_center_title_left"><font style="color:#617f10">RENDSZER </font> Log</div>
		<div class="main_center_title_right">
		    <form action="<?php print $config["site"]["absolutepath"] . "/admin/rendszer-log"; ?>" method="post">
			<input type="hidden" name="from" value="<?php
	if ($_POST["from"])
	    print $_POST["from"]; else
	    print "1";
			    ?>" />
			<input type="hidden" value="<?php
	if ($_POST["to"])
	    print $_POST["to"]; else
	    print "$limit";
			    ?>" />
			<select name="feltetel">
			    <option value="mind">Mind</option>
			    <option value="fontos" <?php
	if ($_POST["feltetel"] == "fontos") {
	    print "selected=\"selected\"";
	}
			    ?> >Csak fontos</option>
			</select>
			<select name="limit">
			    <option value="10">10</option>
			    <option value="25" <?php
	if ($_POST["limit"] == "25") {
	    print "selected=\"selected\"";
	}
			    ?>>25</option>
			    <option value="50" <?php
	if ($_POST["limit"] == "50") {
	    print "selected=\"selected\"";
	}
			    ?>>50</option>
			    <option value="100" <?php
	if ($_POST["limit"] == "100") {
	    print "selected=\"selected\"";
	}
			    ?>>100</option>
			</select>  
			<input class="ui-state-default ui-corner-all" type="submit" name="szukitsd" value="szűkítsd" />
		    </form></div>
	    </div>

	    <div class="main_center_content_left"><b>Végrehajtotta</b></div><div class="main_center_content_center"><b>Esemény</b></div><div class="main_center_content_right"><b>Ekkor</b></div>
	    <?php
	    $counter = 0;
	    if ($_POST["limit"]) {
		$limit = $_POST["limit"];
	    } else {
		$limit = 10;
	    }
	    if ($_POST["lap"]) {
		$b = explode("-", $_POST["lap"]);
		$from = $b[0];
		$to = $b[1];
	    } else {
		$from = 1;
		$to = $limit;
	    }

	    $sqlpart = " WHERE";
	    if (!$_POST["feltetel"]) {
		$feltetel = "mind";
	    } else
		$feltetel = $_POST["feltetel"];
	    if ($_POST["feltetel"] == "mind" || !$_POST["feltetel"]) {
		$sqlpart.=" `esemeny` > '0'";
	    }
	    if ($_POST["feltetel"] == "fontos") {
		$sqlpart.=" (`esemeny` > '0' AND `esemeny` < '15')";
	    }
	    $sqlpart.=" ORDER BY id DESC";


	    $sql = "SELECT count(*) as count FROM oldalletoltesek" . $sqlpart;
	    $result = mysql_query($sql);
	    while ($sor = mysql_fetch_assoc($result)) {
		$count = $sor["count"];
	    }
	    $pagecount = (int) ($count / $limit);
	    if ($count % $limit != 0)
		$pagecount++;


	    while ($sor = mysql_fetch_assoc($result)) {
		$count = $sor["count"];
	    }
	    $sql = "SELECT * FROM oldalletoltesek" . $sqlpart . " LIMIT " . ($from - 1) . ", " . ($limit) . "";
	    $result = mysql_query($sql);
	    while ($sor = mysql_fetch_assoc($result)) {
		?>
	        <div class="main_center_content_spacer"></div>
	        <div class="main_center_content_names<?php
	    if (($counter + 1) % 2 == 0)
		print "_white"; else
		print "_blue";
		?>" class="ui-corner-all">
	    	<div class="main_center_content_spacer"></div>
	    	<div class="main_center_content_left" style="font-weight: normal">
			<?php
			if (!$sor["user_id"])
			    print "Rendszer";
			else {
			    $details = $odin->get_user_basedetails($sor["user_id"]);
			    if ($details) {
				print "<a href=\"" . $config["site"]["absolutepath"] . "/felhasznalok/" . $sor["user_id"] . "\">" . $details["user"]["teljesnev"] . "</a>";
			    } else {
				print "<br />Törölt felhasználó";
			    }
			}
			?></div><div class="main_center_content_center"><?php
	    print $lang["log"]["esemenyek"][$sor["esemeny"]];
	    if ($sor["esemenylista"]) {
		print "<br/>" . $sor["esemenylista"];
	    }
	    if ($sor["erintett"] && ($sor["esemeny"] == 1 || $sor["esemeny"] == 2 || $sor["esemeny"] == 3 || $sor["esemeny"] == 4 || $sor["esemeny"] == 14 || $sor["esemeny"] == 15 || $sor["esemeny"] == 16)) {
		$details = $odin->get_user_basedetails($sor["erintett"]);
		if ($details) {
		    print "<br />Érintett: <a href=\"" . $config["site"]["absolutepath"] . "/felhasznalok/" . $details["user"]["id"] . "\">" . $details["user"]["teljesnev"] . "</a>";
		} else {
		    print "<br />Törölt felhasználó";
		}
	    }
	    if ($sor["erintett"] && ($sor["esemeny"] == 5 || $sor["esemeny"] == 6 || $sor["esemeny"] == 7)) {
		$sql2 = "SELECT nev, id FROM szervezetek WHERE id = " . $sor["erintett"] . "";
		$result2 = mysql_query($sql2);
		while ($sor2 = mysql_fetch_assoc($result2)) {
		    $details2 = $sor2;
		    if ($details2["nev"]) {
			print "<br />Érintett: <a href=\"" . $config["site"]["absolutepath"] . "/szervezetek/" . $details2["id"] . "\">" . $details2["nev"] . "</a>";
		    } else {
			print "<br />Törölt szervezet";
		    }
		}
	    }
	    if ($sor["erintett"] && ($sor["esemeny"] == 8 || $sor["esemeny"] == 9 || $sor["esemeny"] == 10)) {
		$sql2 = "SELECT beosztasnev, id FROM beosztasok WHERE id = " . $sor["erintett"] . "";
		$result2 = mysql_query($sql2);
		while ($sor2 = mysql_fetch_assoc($result2)) {
		    $details2 = $sor2;
		}
		if ($details2["beosztasnev"]) {
		    print "<br />Érintett: <a href=\"" . $config["site"]["absolutepath"] . "/beosztasok/" . $details2["id"] . "\">" . $details2["beosztasnev"] . "</a>";
		} else {
		    print "<br />Törölt beosztas";
		}
	    }
			?></div><div class="main_center_content_right"><?php print date('Y-m-d H:i:s', $sor["ekkor"]); ?></div>
	    	<div class="main_center_content_spacer"></div>
	        </div>


	    <?php
	    $counter++;
	}

	if ($pagecount > 1) {
	    ?>
	        <div class="main_center_content_spacer"></div>
	        <div class="main_center_content_names<?php
	    if (($counter + 1) % 2 == 0)
		print "_white"; else
		print "_blue";
	    ?>" class="ui-corner-all">
	    	<form style=" min-height: 22px; padding-left:5px; padding-right:5px;text-align: center;" action="<?php print $config["site"]["absolutepath"] . "/admin/rendszer-log"; ?>" method="post">
	    	    <input type="hidden" name="limit" value="<?php print $limit; ?>" />
	    	    <input type="hidden" name="feltetel" value="<?php print $feltetel; ?>" />
	    	    Találatok:
	    	    <select name="lap">
	    		<option value="<?php print "1 - " . $limit ?>"><?php print "1 - " . $limit ?></option>
		<?php $counter = 1;
		while ($counter != $pagecount) { ?>
				<option value="<?php print (($limit * $counter) + 1) . " - " . ($limit * ($counter + 1)) ?>" <?php
		if ($_POST["lap"] == ("" . ($limit * $counter) + 1) . " - " . ($limit * ($counter + 1) . "")) {
		    print "selected=\"selected\"";
		}
		    ?>><?php print (($limit * $counter) + 1) . " - " . ($limit * ($counter + 1)) ?></option>
		    <?php $counter++;
		} ?>
	    	    </select>
	    	    <input class="ui-state-default ui-corner-all" type="submit" name="mehet" value="Mehet" />
	    	</form>   
	    	<div class="main_center_content_spacer"></div>
	        </div>
		<?php
	    }
	    if ($counter == 0) {
		?>
	        <div class="ui-state-highlight ui-corner-all" style="margin-top: 5px; padding: 0 .7em;"> 
	    	<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
	    	    Nincs megjeleníthető esemény!</p></div>
	    <?php
	}
	?>
	</div>

	<div class="main_center_spacer"></div>
	<?php
    }

//********************************************************************* 	
    function hianyzo_rendfokozatok_kiir() {
	global $lang, $odin, $q, $config;
	?>
	<div class="main_center_spacer"></div>
	<div class="main_center_container">


	    <div class="main_center_title">
		<div class="main_center_title_left"><font style="color:#617f10">HIÁNYZÓ </font> Rendfokozatok</div>
		<div class="main_center_title_right"></div>
	    </div>
	    <div class="main_center_content_spacer"></div>
			    <?php
			    $counter = 0;
			    $sql = "SELECT id FROM `felhasznalok` WHERE rendfokozat_id=18 ORDER BY vezeteknev, keresztnev";
			    $result = mysql_query($sql);
			    while ($sor = mysql_fetch_assoc($result)) {
				$details = $odin->get_user_basedetails($sor["id"]);
				$counter++;
				?>
	        <div class="main_center_content_left"></div><div class="main_center_content_center"><?php print "<a href=\"" . $config["site"]["absolutepath"] . "/felhasznalok/" . $details["user"]["id"] . "\">" . $details["user"]["teljesnev"] . "</a>"; ?></div><div class="main_center_content_right"></div>
	        <div class="main_center_content_spacer"></div>	    
	    <?php
	}
	if (!$counter) {
	    ?>
	        <div class="main_center_content_left"></div><div class="main_center_content_center">Nincsenek hiányzó rendfokozatok!</div><div class="main_center_content_right"></div>
	        <div class="main_center_content_spacer"></div>
	    <?php }
	    ?>
	</div>

	<div class="main_center_spacer"></div>
	    <?php
	}

	//*********************************************************************  
	function beosztasok_kezelese_kiir() {
	    global $lang, $odin, $q, $config;
	    ?>
	<div class="main_center_spacer"></div>
	<div class="main_center_container">
	    <div class="main_center_title">
		<div class="main_center_title_left"><font style="color:#617f10">BEOSZTÁSOK </font> Kezelése</div>
		<div class="main_center_title_right">
		    <form action="<?php print $config["site"]["absolutepath"] . "/admin/beosztasok-kezelese"; ?>" method="post">
			<input type="hidden" name="from" value="<?php
	if ($_POST["from"])
	    print $_POST["from"]; else
	    print "1";
	    ?>" />
			<input type="hidden" value="<?php
	if ($_POST["to"])
	    print $_POST["to"]; else
	    print "$limit";
	    ?>" />
			<select name="limit">
			    <option value="10">10</option>
			    <option value="25" <?php
	if ($_POST["limit"] == "25") {
	    print "selected=\"selected\"";
	}
	    ?>>25</option>
			    <option value="50" <?php
	if ($_POST["limit"] == "50") {
	    print "selected=\"selected\"";
	}
	    ?>>50</option>
			    <option value="100" <?php
	if ($_POST["limit"] == "100") {
	    print "selected=\"selected\"";
	}
	?>>100</option>
			</select>  
			<input class="ui-state-default ui-corner-all" type="submit" name="szukitsd" value="szűkítsd" />
		    </form></div>
	    </div>

	    <div class="main_center_content_left"><b>Beosztás név</b></div><div class="main_center_content_center"><b>Felhasználószám</b></div><div class="main_center_content_right"><b>Tevékenység</b></div>
		    <?php
		    $counter = 0;
		    if ($_POST["limit"]) {
			$limit = $_POST["limit"];
		    } else {
			$limit = 10;
		    }
		    if ($_POST["lap"]) {
			$b = explode("-", $_POST["lap"]);
			$from = $b[0];
			$to = $b[1];
		    } else {
			$from = 1;
			$to = $limit;
		    }

		    $sqlpart = " WHERE 1";

		    $sqlpart.=" ORDER BY beosztasnev";


		    $sql = "SELECT count(*) as count FROM beosztasok" . $sqlpart;
		    $result = mysql_query($sql);
		    while ($sor = mysql_fetch_assoc($result)) {
			$count = $sor["count"];
		    }
		    $pagecount = (int) ($count / $limit);
		    if ($count % $limit != 0)
			$pagecount++;


		    while ($sor = mysql_fetch_assoc($result)) {
			$count = $sor["count"];
		    }
		    $sql = "SELECT * FROM beosztasok" . $sqlpart . " LIMIT " . ($from - 1) . ", " . ($limit) . "";
		    $result = mysql_query($sql);
		    while ($sor = mysql_fetch_assoc($result)) {
			?>
	        <div class="main_center_content_spacer"></div>
	        <div class="main_center_content_names<?php
			if (($counter + 1) % 2 == 0)
			    print "_white"; else
			    print "_blue";
			?>" class="ui-corner-all">
	    	<div class="main_center_content_spacer"></div>
	    	<div class="main_center_content_left" style="font-weight: normal">
	    <?php print "<a href=\"" . $config["site"]["absolutepath"] . "/beosztasok/" . $sor["id"] . "\">" . $sor["beosztasnev"] . "</a>"; ?>		
	    	</div>
	    	<div class="main_center_content_center">
		<?php
		$sql2 = "SELECT count(*) as count FROM felhasznalok where beosztas_id = " . $sor["id"] . "";
		$result2 = mysql_query($sql2);
		while ($sor2 = mysql_fetch_assoc($result2)) {
		    $ucount = $sor2["count"];
		    print $sor2["count"];
		}
		?>
	    	</div>
	    	<div class="main_center_content_right"><?php if ($ucount == 0 && $odin->admin_this("beosztas", "delete", $q[1])) { ?>
			    <a style="float:right;" title="beosztás törlése" href="<?php print $config["site"]["absolutepath"] . "/beosztasok/" . $sor["id"] . "/torol"; ?>"><span class="ui-state-default ui-corner-all ui-icon ui-icon-circle-close"></span></a>
	    <?php } ?>	    <?php if ($odin->admin_this("beosztas", "edit", $q[1])) { ?>
			    <a style="float:right;" title="beosztás módosítása" href="<?php print $config["site"]["absolutepath"] . "/beosztasok/" . $sor["id"] . "/szerkeszt"; ?>"><span class="ui-state-default ui-corner-all ui-icon ui-icon-pencil"></span></a>
	    <?php } ?></div>
	    	<div class="main_center_content_spacer"></div>
	        </div>


	    <?php
	    $counter++;
	}

	if ($pagecount > 1) {
	    ?>
	        <div class="main_center_content_spacer"></div>
	        <div class="main_center_content_names<?php
	    if (($counter + 1) % 2 == 0)
		print "_white"; else
		print "_blue";
	    ?>" class="ui-corner-all">
	    	<form style=" min-height: 22px; padding-left:5px; padding-right:5px;text-align: center;" action="<?php print $config["site"]["absolutepath"] . "/admin/beosztasok-kezelese"; ?>" method="post">
	    	    <input type="hidden" name="limit" value="<?php print $limit; ?>" />
	    	    Találatok:
	    	    <select name="lap">
	    		<option value="<?php print "1 - " . $limit ?>"><?php print "1 - " . $limit ?></option>
	    <?php $counter = 1;
	    while ($counter != $pagecount) { ?>
				<option value="<?php print (($limit * $counter) + 1) . " - " . ($limit * ($counter + 1)) ?>" <?php
		if ($_POST["lap"] == ("" . ($limit * $counter) + 1) . " - " . ($limit * ($counter + 1) . "")) {
		    print "selected=\"selected\"";
		}
		?>><?php print (($limit * $counter) + 1) . " - " . ($limit * ($counter + 1)) ?></option>
		<?php $counter++;
	    } ?>
	    	    </select>
	    	    <input class="ui-state-default ui-corner-all" type="submit" name="mehet" value="Mehet" />
	    	</form>   
	    	<div class="main_center_content_spacer"></div>
	        </div>
	    <?php
	}
	if ($counter == 0) {
	    ?>
	        <div class="ui-state-highlight ui-corner-all" style="margin-top: 5px; padding: 0 .7em;"> 
	    	<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
	    	    Nincs megjeleníthető esemény!</p></div>
	    <?php
	}
	?>
	</div>

	<div class="main_center_spacer"></div>
	<?php
    }

//********************************************************************* 
}
?>