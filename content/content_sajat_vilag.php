<?php

class sajat_vilag_content {

//*********************************************************************	
    function adatlapom_kiir() {
	global $lang, $odin, $config;
	?>
	<div class="main_center_spacer"></div>

	<div class="main_center_container">
	    <div class="main_center_title">
		<div class="main_center_title_left"><font style="color:#617f10">RENDSZER</font> Adatok</div>
		<div class="main_center_title_right"><a title="szerkeszt" href="<?php print $config["site"]["absolutepath"] . "/sajat-vilag/adatlapom/rendszer-adatok-szerkesztese"; ?>"><span class="ui-state-default ui-corner-all ui-icon ui-icon-pencil"></span></a></div>
	    </div>

	    <div class="main_center_content_spacer"></div>
	    <div class="main_center_content_left"><?php print ($lang["felhasznalok"]["azonosito"] . ":"); ?></div><div class="main_center_content_center"><?php
	    print $odin->fancy_text($_SESSION["user"]["azonosito"]); 
	?></div>
	    <div class="main_center_content_spacer"></div>
	    <div class="main_center_content_left"><?php print ($lang["felhasznalok"]["jelszo"] . ":"); ?></div><div class="main_center_content_center">* * * * * * *</div>
	    <div class="main_center_content_spacer"></div>
	    <div class="main_center_content_left"><?php print ($lang["felhasznalok"]["email"] . ":"); ?></div><div class="main_center_content_center"><?php print $odin->fancy_text($_SESSION["user"]["email"]); ?></div>
	    <div class="main_center_content_spacer"></div>

	    <div class="main_center_title">
		<div class="main_center_title_left"><font style="color:#617f10">SZEMÉLYES ADATOK</font> Adatok</div>
		<div class="main_center_title_right"><a title="szerkeszt" href="<?php print $config["site"]["absolutepath"] . "/sajat-vilag/adatlapom/szemelyes-adatok-szerkesztese"; ?>"><span class="ui-state-default ui-corner-all ui-icon ui-icon-pencil"></span></a></div>
	    </div>
	    <div class="main_center_content_left"><?php print ($lang["felhasznalok"]["teljesnev"] . ":"); ?></div><div class="main_center_content_center"><?php print $odin->fancy_text($_SESSION["user"]["teljesnev"]); ?></div><div class="main_center_content_right"><a title="Kifejt" href="#" id="names-toogle" onclick="return false"><span class="ui-state-default ui-corner-all ui-icon ui-icon-help"></span></a></div>
	    <div class="main_center_content_spacer"></div>
	    <div id="main_center_content_names" class="ui-corner-all">
		<div class="main_center_content_left"><?php print ($lang["felhasznalok"]["titulus"] . ":"); ?></div><div class="main_center_content_center"><?php print $odin->fancy_text($_SESSION["user"]["titulus"]); ?></div>
		<div class="main_center_content_spacer"></div>
		<div class="main_center_content_left"><?php print ($lang["felhasznalok"]["vezeteknev"] . ":"); ?></div><div class="main_center_content_center"><?php print $odin->fancy_text($_SESSION["user"]["vezeteknev"]); ?></div>
		<div class="main_center_content_spacer"></div>
		<div class="main_center_content_left"><?php print ($lang["felhasznalok"]["kozepsonev"] . ":"); ?></div><div class="main_center_content_center"><?php print $odin->fancy_text($_SESSION["user"]["kozepsonev"]); ?></div>
		<div class="main_center_content_spacer"></div>
		<div class="main_center_content_left"><?php print ($lang["felhasznalok"]["keresztnev"] . ":"); ?></div><div class="main_center_content_center"><?php print $odin->fancy_text($_SESSION["user"]["keresztnev"]); ?></div>
		<div class="main_center_content_spacer"></div>
	    </div>
	    <div class="main_center_content_left"><?php print ($lang["felhasznalok"]["rendfokozat"] . ":"); ?></div><div class="main_center_content_center"><?php print $odin->fancy_text($_SESSION["user"]["rendfokozat"]); ?></div>
	    <div class="main_center_content_spacer"></div>
	    <div class="main_center_content_left"><?php print ($lang["felhasznalok"]["titulus_2"] . ":"); ?></div><div class="main_center_content_center"><?php print $odin->fancy_text($_SESSION["user"]["titulus_2"]); ?></div>
	    <div class="main_center_content_spacer"></div>
	    <div class="main_center_content_left"><?php print ($lang["felhasznalok"]["beosztas"] . ":"); ?></div><div class="main_center_content_center"><?php print $odin->fancy_text($_SESSION["user"]["beosztas"]); ?></div>
	    <div class="main_center_content_spacer"></div>
	    <div class="main_center_content_left"><?php print ($lang["felhasznalok"]["beosztas_megjegyzes"] . ":"); ?></div><div class="main_center_content_center"><?php print $odin->fancy_text($_SESSION["user"]["beosztas_megjegyzes"]); ?></div>
	    <div class="main_center_content_spacer"></div>
	    <div class="main_center_content_left"><?php print ($lang["felhasznalok"]["beosztas_jellege"] . ":"); ?></div><div class="main_center_content_center"><?php print $odin->fancy_text($lang["beosztas_jellege"]["" . $_SESSION["user"]["allomany_statusz"] . ""]); ?></div>
	    <div class="main_center_content_spacer"></div>
	    <div class="main_center_content_left"><?php print ($lang["felhasznalok"]["nicknev"] . ":"); ?></div><div class="main_center_content_center"><?php print $odin->fancy_text($_SESSION["user"]["nicknev"]); ?></div>
	    <div class="main_center_content_spacer"></div>

	    <div class="main_center_title">
		<div class="main_center_title_left"><font style="color:#617f10">SZERVEZET</font> Adatok</div>
		<div class="main_center_title_right"><a title="szerkeszt" href="<?php print $config["site"]["absolutepath"] . "/sajat-vilag/adatlapom/szervezet-szerkesztese/" . $_SESSION["user"]["szervezet_id"] . ""; ?>"><span class="ui-state-default ui-corner-all ui-icon ui-icon-pencil"></span></a></div>
	    </div>
	    <div class="main_center_content_left"><?php print ($lang["felhasznalok"]["szervezet"] . ":"); ?></div><div class="main_center_content_center">
		<?php
		$i = -1;
		while ($i != $_SESSION["user"]["szervezet_szam"]) {
		    $i++;
		    ?>
		    <?php print ($_SESSION["user"]["szervezet_nevek"]["$i"]); ?><br />
		    <?php
		}
		?>
	    </div>
	    <div class="main_center_content_spacer"></div>
	    <div class="main_center_title">
		<div class="main_center_title_left"><font style="color:#617f10">CÉGES</font> Elérhetőségek</div>
		<div class="main_center_title_right"><a title="szerkeszt" href="<?php print $config["site"]["absolutepath"] . "/sajat-vilag/adatlapom/ceges-elerhetosegek-szerkesztese"; ?>"><span class="ui-state-default ui-corner-all ui-icon ui-icon-pencil"></span></a></div>
	    </div>
	    <div class="main_center_content_spacer"></div>
	    <div class="main_center_content_left"><?php print ($lang["felhasznalok"]["telephely"] . ":"); ?></div><div class="main_center_content_center"><?php print $odin->fancy_text($_SESSION["user"]["telephely"] . "<br />" . $_SESSION["user"]["iroda"]); ?></div>
	    <div class="main_center_content_spacer"></div>
	    <div class="main_center_content_left"><?php print ($lang["felhasznalok"]["levelezesi"] . ":"); ?></div><div class="main_center_content_center"><?php print $odin->fancy_text($_SESSION["user"]["levelezesi"]); ?></div>
	    <div class="main_center_content_spacer"></div>
	    <div class="main_center_content_left"><?php print ($lang["felhasznalok"]["vezetoi"] . ":"); ?></div><div class="main_center_content_center"><?php print ($odin->fancy_phone_number($_SESSION["user"]["vezetoi"])); ?></div>
	    <div class="main_center_content_spacer"></div>
	    <div class="main_center_content_left"><?php print ($lang["felhasznalok"]["kozvetlen"] . ":"); ?></div><div class="main_center_content_center"><?php print ($odin->fancy_phone_number($_SESSION["user"]["kozvetlen"])); ?></div>
	    <div class="main_center_content_spacer"></div>
	    <div class="main_center_content_left"><?php print ($lang["felhasznalok"]["2helyi"] . ":"); ?></div><div class="main_center_content_center"><?php print ($odin->fancy_phone_number($_SESSION["user"]["2helyi"])); ?></div>  
	    <div class="main_center_content_spacer"></div>
	    <div class="main_center_content_left"><?php print ($lang["felhasznalok"]["fax"] . ":"); ?></div><div class="main_center_content_center"><?php print $odin->fancy_phone_number($_SESSION["user"]["fax"]); ?></div>
	    <div class="main_center_content_spacer"></div>
	    <div class="main_center_content_left"><?php print ($lang["felhasznalok"]["titkarsag"] . ":"); ?></div><div class="main_center_content_center"><?php print ($_SESSION["user"]["titkarsag"]); ?></div>
	    <div class="main_center_content_spacer"></div>
	    <div class="main_center_content_left"><?php print ($lang["felhasznalok"]["mobil"] . ":"); ?></div><div class="main_center_content_center"><?php print $odin->fancy_phone_number($_SESSION["user"]["mobil"]); ?></div>
	    <div class="main_center_content_spacer"></div>
	    <div class="main_center_content_left"><?php print ($lang["felhasznalok"]["vez_kozvetlen"] . ":"); ?></div><div class="main_center_content_center"><?php print $odin->fancy_phone_number($_SESSION["user"]["vez_kozvetlen"]); ?></div>
	    <div class="main_center_content_spacer"></div>
	    <div class="main_center_content_left"><?php print ($lang["felhasznalok"]["vez_fax"] . ":"); ?></div><div class="main_center_content_center"><?php print $odin->fancy_phone_number($_SESSION["user"]["vez_fax"]); ?></div>
	    <div class="main_center_content_spacer"></div>
	    <div class="main_center_content_left"><?php print ($lang["felhasznalok"]["tetra"] . ":"); ?></div><div class="main_center_content_center"><?php print $odin->fancy_phone_number($_SESSION["user"]["tetra"]); ?></div>
	    <div class="main_center_content_spacer"></div>

	    <div class="main_center_title">
		<div class="main_center_title_left"><font style="color:#617f10">PRIVÁT</font> Elérhetőségek</div>
		<div class="main_center_title_right"><a title="szerkeszt" href="<?php print $config["site"]["absolutepath"] . "/sajat-vilag/adatlapom/privat-adatok-szerkesztese"; ?>"><span class="ui-state-default ui-corner-all ui-icon ui-icon-pencil"></span></a></div>
	    </div>
	    <div class="main_center_content_left"><?php print ($lang["felhasznalok"]["privat_mobil"] . ":"); ?></div><div class="main_center_content_center"><?php print $odin->fancy_phone_number($_SESSION["user"]["privat_mobil"]); ?></div>
	    <div class="main_center_content_spacer"></div>
	    <div class="main_center_content_left"><?php print ($lang["felhasznalok"]["privat_vezetekes"] . ":"); ?></div><div class="main_center_content_center"><?php print $odin->fancy_phone_number($_SESSION["user"]["privat_vezetekes"]); ?></div>
	    <div class="main_center_content_spacer"></div>
	    <div class="main_center_content_left"><?php print ($lang["felhasznalok"]["privat_fax"] . ":"); ?></div><div class="main_center_content_center"><?php print $odin->fancy_phone_number($_SESSION["user"]["privat_fax"]); ?></div>
	    <div class="main_center_content_spacer"></div>
	    <div class="main_center_content_left"><?php print ($lang["felhasznalok"]["privat_email"] . ":"); ?></div><div class="main_center_content_center"><?php print $odin->fancy_text($_SESSION["user"]["privat_email"]); ?></div>
	    <div class="main_center_content_spacer"></div>
	    <div class="main_center_content_left"><?php print ($lang["felhasznalok"]["szuletesnap"] . ":"); ?></div><div class="main_center_content_center"><?php
	if ($_SESSION["user"]["szuletesnap"] && $_SESSION["user"]["szuletesnap"]!="0000-00-00")
	    print $_SESSION["user"]["szuletesnap"];
	else print "NA / NT";
	?></div>
	    <div class="main_center_content_right"></div>
	    <div class="main_center_content_spacer"></div>
	    <div class="main_center_content_left"><?php print ($lang["felhasznalok"]["nevnap"] . ":"); ?></div><div class="main_center_content_center"><?php
	if (!$_SESSION["user"]["nevnap"])
	    print "NA / NT"; else
	    print ($lang["honapok"][$_SESSION["user"]["nevnap"][0] . $_SESSION["user"]["nevnap"][1]] . " " . $_SESSION["user"]["nevnap"][2] . $_SESSION["user"]["nevnap"][3] . ".");
	?></div>
	    <div class="main_center_content_spacer"></div>
	    <div class="main_center_title">
		<div class="main_center_title_left"><font style="color:#617f10">MEGJEGYZÉSEK</font></div>
		<div class="main_center_title_right"><a title="szerkeszt" href="<?php print $config["site"]["absolutepath"] . "/sajat-vilag/adatlapom/megjegyzes-szerkesztese"; ?>"><span class="ui-state-default ui-corner-all ui-icon ui-icon-pencil"></span></a></div>
	    </div>
	    <div class="main_center_content_spacer"></div>
	    <div class="main_center_content_left"><?php print ($lang["felhasznalok"]["megjegyzes"] . ":"); ?></div><div class="main_center_content_center"><?php print $odin->fancy_text(nl2br($odin->tags_to_html($_SESSION["user"]["megjegyzes"]))); ?></div>
	</div>

	<div class="main_center_spacer"></div>
	<?php
    }

//*********************************************************************
    function rendszer_adatok_modosit($error) {
	global $lang;
	?>
	<div class="main_center_spacer"></div>

	<div class="main_center_container">
	    <div class="main_center_title">
		<div class="main_center_title_left"><font style="color:#617f10">RENDSZER</font> Adatok módosítása</div>
	    </div>
		    <?php if ($error) { ?>
	        <div class="ui-state-highlight ui-corner-all" style="margin-top: 5px; padding: 0 .7em;"> 
	    	<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<?php print $error; ?>
	    	</p>
	        </div>
	<?php } ?>
	    <div class="main_login_spacer"></div>

	    <form action="sajat-vilag/adatlapom/rendszer-adatok-szerkesztese" method="post"> 
		<div class="main_login_container">

		    <div class="main_login_left"><?php print($lang["aktivacio"]["azonosito"]); ?></div>
		    <div class="main_login_right"><input type="text" name="azonosito" value="<?php
		if ($_POST["azonosito"])
		    print $_POST["azonosito"]; else
		    print $_SESSION["user"]["azonosito"];
		?>" size="32" maxlength="64" /></div>
		    <div class="main_login_spacer"></div>
		    <div class="main_login_left"><?php print($lang["aktivacio"]["email"]); ?></div>
		    <div class="main_login_right"><input type="text" name="email" value="<?php
		if ($_POST["email"])
		    print $_POST["email"]; else
		    print $_SESSION["user"]["email"];
		?>" size="32" maxlength="64" /></div>
		    <div class="main_login_spacer"></div>
		    <div class="main_login_left"><?php print($lang["aktivacio"]["uj_jelszo"]); ?></div>
		    <div class="main_login_right"><input type="password" name="jelszo1" value="<?php
		if ($_POST["jelszo1"])
		    print $_POST["jelszo1"]; else {
		    
		}
		?>" size="32" maxlength="64" /></div>
		    <div class="main_login_spacer"></div>
		    <div class="main_login_left"><?php print($lang["aktivacio"]["uj_jelszo_ujra"]); ?></div>
		    <div class="main_login_right"><input type="password" name="jelszo2" value="<?php
		if ($_POST["jelszo2"])
		    print $_POST["jelszo2"]; else {
		    
		}
		?>" size="32" maxlength="64" /></div>
		    <div class="main_login_spacer"></div>
		    <div class="main_login_left">&nbsp;</div>
		    <input class="ui-state-default ui-corner-all" type="submit" name="modosit" value="<?php print($lang["gomb"]["modosit"]); ?>" />
		    <input class="ui-state-default ui-corner-all" type="submit" name="modosit" value="<?php print($lang["gomb"]["megse"]); ?>" />
		</div>
	    </form>

	</div>


	<div class="main_center_spacer"></div>
	<?php
    }

//*********************************************************************	
    function rendszer_adatok_modosit_megerosit($error) {
	global $lang;
	?>
	<div class="main_center_spacer"></div>

	<div class="main_center_container">
	    <div class="main_center_title">
		<div class="main_center_title_left"><font style="color:#617f10">RENDSZER</font> Adatok módosítása</div>
	    </div>
		    <?php if ($error) { ?>
	        <div class="ui-state-highlight ui-corner-all" style="margin-top: 5px; padding: 0 .7em;"> 
	    	<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<?php print $error; ?>
	    	</p>
	        </div>
	<?php } ?>
	    <div class="main_login_spacer"></div>

	    <form action="sajat-vilag/adatlapom/rendszer-adatok-szerkesztese" method="post"> 
		<div class="main_login_container">
		    <input type="hidden" name="azonosito" value="<?php print $_POST["azonosito"]; ?>" />
		    <input type="hidden" name="email" value="<?php print $_POST["email"]; ?>" />
		    <input type="hidden" name="jelszo1" value="<?php print $_POST["jelszo1"]; ?>" />
		    <div class="main_login_left"><?php print($lang["aktivacio"]["regi_jelszo"]); ?></div>
		    <div class="main_login_right"><input type="password" name="oldpass" value="<?php if ($_POST["oldpass"])
						     print $_POST["oldpass"]; ?>" size="32" maxlength="64" /></div>
		    <div class="main_login_left"><?php print($lang["aktivacio"]["ellenorzokod"]); ?></div>
		    <div class="main_login_right"><input type="text" name="passkey" value="<?php if ($_POST["passkey"])
						     print $_POST["passkey"]; ?>" size="32" maxlength="64" /></div>
		    <div class="main_login_spacer"></div>
		    <div class="main_login_left">&nbsp;</div>
		    <div class="main_login_right"><input class="ui-state-default ui-corner-all" type="submit" name="megerosit" value="Megerősít" />
		    </div>


		</div>
	    </form>

	</div>


	<div class="main_center_spacer"></div>
	<?php
    }

//*********************************************************************
    function szemelyes_adatok_modosit($error) {
	global $lang, $odin, $q;
	?>
	<div class="main_center_spacer"></div>

	<div class="main_center_container">
	    <div class="main_center_title">
		<div class="main_center_title_left"><font style="color:#617f10">SZEMÉLYES</font> Adatok módosítása</div>
	    </div>
		    <?php if ($error) { ?>
	        <div class="ui-state-highlight ui-corner-all" style="margin-top: 5px; padding: 0 .7em;"> 
	    	<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<?php print $error["kiir"]; ?>
	    	</p>
	        </div>
	<?php } ?>
	    <div class="main_login_spacer"></div>

	    <form action="sajat-vilag/adatlapom/szemelyes-adatok-szerkesztese" method="post"> 
		<div class="main_login_container">

		    <div class="main_login_left"><?php print($lang["felhasznalok"]["titulus"]); ?></div>
		    <div class="main_login_right"><input type="text" name="titulus" value="<?php
		if ($_POST["titulus"])
		    print $_POST["titulus"]; else
		    print $_SESSION["user"]["titulus"];
		?>" size="32" maxlength="32" /></div>
		    <div class="main_login_spacer"></div>
		    <div class="main_login_left"><?php print($lang["felhasznalok"]["vezeteknev"]); ?></div>
		    <div class="main_login_right"><input type="text" name="vezeteknev" value="<?php
		if ($_POST["vezeteknev"])
		    print $_POST["vezeteknev"]; else
		    print $_SESSION["user"]["vezeteknev"];
		?>" size="32" maxlength="64" /></div>
		    <div class="main_login_spacer"></div>
		    <div class="main_login_left"><?php print($lang["felhasznalok"]["kozepsonev"]); ?></div>
		    <div class="main_login_right"><input type="text" name="kozepsonev" value="<?php
		if ($_POST["kozepsonev"])
		    print $_POST["kozepsonev"]; else
		    print $_SESSION["user"]["kozepsonev"];
		?>" size="32" maxlength="64" /></div>
		    <div class="main_login_spacer"></div>
		    <div class="main_login_left"><?php print($lang["felhasznalok"]["keresztnev"]); ?></div>
		    <div class="main_login_right"><input type="text" name="keresztnev" value="<?php
		if ($_POST["keresztnev"])
		    print $_POST["keresztnev"]; else
		    print $_SESSION["user"]["keresztnev"];
	?>" size="32" maxlength="64" /></div>
		    <div class="main_login_spacer"></div>
	<?php if (!$error["rendfokozat"]) { ?>

	    	    <div class="main_login_left"><?php print($lang["felhasznalok"]["rendfokozat"]); ?></div>
	    	    <div class="main_login_right">
	    		<select name="rendfokozat_id">
	    		    <option value="<?php print $_SESSION["user"]["rendfokozat_id"]; ?>"><?php print $_SESSION["user"]["rendfokozat"]; ?></option>
	    		    <option value="0">Nem szerepel a listában</option>
				<?php
				$sql = "SELECT * FROM rendfokozatok ORDER BY id";
				$result = mysql_query($sql);
				$sql_query_count++;
				while ($sor = mysql_fetch_assoc($result)) {
				    ?>
				    <option value="<?php print $sor["id"]; ?>" <?php if ($_POST["rendfokozat_id"] == $sor["id"])
				print "selected=\"selected\"" ?>><?php print $sor["megnevezes"]; ?></option>  
	    <?php } ?>
	    		    <option value="0">Nem szerepel a listában</option>
	    		</select>
	    	    </div>
	<?php }else { ?>
	    	    <div class="main_login_left"><?php print($lang["felhasznalok"]["rendfokozat"]); ?></div>
	    	    <div class="main_login_right"><input type="text" name="rendfokozat" value="<?php
	    if ($_POST["rendfokozat"])
		print $_POST["rendfokozat"]; else
		print $_SESSION["user"]["rendfokozat"];
	    ?>" size="32" maxlength="64" /></div>  
			    <?php
			}
			?>
		    <div class="main_login_spacer"></div>
	<div class="main_login_left"><?php print($lang["felhasznalok"]["titulus_2"]); ?></div>
		    <div class="main_login_right"><input type="text" name="titulus_2" value="<?php
		if ($_POST["titulus_2"])
		    print $_POST["titulus_2"]; else
		    print $_SESSION["user"]["titulus_2"];
		?>" size="32" maxlength="32" /></div>
		    <div class="main_login_spacer"></div>
	<?php if (!$error["beosztas"]) { ?>
	    	    <div class="main_login_left"><?php print($lang["felhasznalok"]["beosztas"]); ?></div>
	    	    <div class="main_login_right">
	    		<select name="beosztas_id">
	    		    <option value="<?php print $_SESSION["user"]["beosztas_id"]; ?>"><?php print $_SESSION["user"]["beosztas"]; ?></option>
	    		    <option value="0">Nem szerepel a listában</option>
				<?php
				$sql = "SELECT * FROM beosztasok ORDER BY beosztasnev";
				$result = mysql_query($sql);
				$sql_query_count++;
				while ($sor = mysql_fetch_assoc($result)) {
				    ?>
				    <option value="<?php print $sor["id"]; ?>"<?php if ($_POST["beosztas_id"] == $sor["id"])
				print "selected=\"selected\"" ?>><?php print $sor["beosztasnev"]; ?></option>  
					<?php } ?>
	    		    <option value="0">Nem szerepel a listában</option>
	    		</select>
	    	    </div>
		    <?php }else { ?>
	    	    <div class="main_login_left"><?php print($lang["felhasznalok"]["beosztas"]); ?></div>
	    	    <div class="main_login_right"><input type="text" name="beosztas" value="<?php
	    if ($_POST["beosztas"])
		print $_POST["beosztas"]; else
		print $_SESSION["user"]["beosztas"];
			?>" size="32" maxlength="64" /></div>   
			    <?php
			}
			?>
		    <div class="main_login_spacer"></div>


		    <div class="main_login_left"><?php print($lang["felhasznalok"]["beosztas_megjegyzes"]); ?></div>
		    <div class="main_login_right"><input type="text" name="beosztas_megjegyzes" value="<?php
		if ($_POST["beosztas_megjegyzes"])
		    print $_POST["beosztas_megjegyzes"]; else
		    print $_SESSION["user"]["beosztas_megjegyzes"];
			?>" size="32" maxlength="64" /></div>
		    <div class="main_login_spacer"></div>
		    <div class="main_login_left"><?php print($lang["felhasznalok"]["beosztas_jellege"]); ?></div>
		    <div class="main_login_right">
			<select name="allomany_statusz">
			    <?php
			    if (!$_POST["allomany_statusz"])
				$_POST["allomany_statusz"] = $_SESSION["user"]["allomany_statusz"];
			    foreach ($lang["beosztas_jellege"] as $key => $value) {
				?>
	    		    <option value="<?php print $key; ?>" <?php if ($key == $_POST["allomany_statusz"])
			print "selected=\"selected\""; ?>><?php print $value; ?></option>
				    <?php } ?>
			</select>
		    </div>
		    <div class="main_login_spacer"></div>
		    <div class="main_login_left"><?php print($lang["felhasznalok"]["nicknev"]); ?></div>
		    <div class="main_login_right"><input type="text" name="nicknev" value="<?php
			    if ($_POST["nicknev"])
				print $_POST["nicknev"]; else
				print $_SESSION["user"]["nicknev"];
				    ?>" size="32" maxlength="64" /></div>
		    <div class="main_login_spacer"></div>


		    <div class="main_login_left">&nbsp;</div>
		    <input class="ui-state-default ui-corner-all" type="submit" name="modosit" value="<?php print($lang["gomb"]["modosit"]); ?>" />
		    <input class="ui-state-default ui-corner-all" type="submit" name="modosit" value="<?php print($lang["gomb"]["megse"]); ?>" />
		</div>
	    </form>

	</div>


	<div class="main_center_spacer"></div>
	<?php
    }

//*********************************************************************	
    function privat_adatok_modosit($error) {
	global $lang;
	?>
	<div class="main_center_spacer"></div>

	<div class="main_center_container">
	    <div class="main_center_title">
		<div class="main_center_title_left"><font style="color:#617f10">SZEMÉLYES</font> Adatok módosítása</div>
	    </div>
	    <?php if ($error) { ?>
	        <div class="ui-state-highlight ui-corner-all" style="margin-top: 5px; padding: 0 .7em;"> 
	    	<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
			<?php print $error["kiir"]; ?>
	    	</p>
	        </div>
	    <?php } else { ?>
	        <div class="ui-state-highlight ui-corner-all" style="margin-top: 5px; padding: 0 .7em;"> 
	    	<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
	    	    A privát elérhetőségek megadása önkéntes! Megadás esetén minden regisztrált felhasználó által megtekinthető! <br />A rendszer a privát adatok megtekintését naplózza.
	    	</p>
	        </div>
	    <?php } ?>
	    <div class="main_login_spacer"></div>

	    <form action="sajat-vilag/adatlapom/privat-adatok-szerkesztese" method="post"> 
		<div class="main_login_container">

		    <div class="main_login_left"><?php print($lang["felhasznalok"]["privat_mobil"]); ?></div>
		    <div class="main_login_right"><input type="text" name="privat_mobil" value="<?php
	if ($_POST["privat_mobil"]) {
	    print $_POST["privat_mobil"];
	} else if (!$_SESSION["user"]["privat_mobil"]) {
	    
	} else {
	    print $_SESSION["user"]["privat_mobil"];
	}
	    ?>" size="32" maxlength="9" /></div> 
		    <div class="main_login_spacer"></div>

		    <div class="main_login_left"><?php print($lang["felhasznalok"]["privat_vezetekes"]); ?></div>
		    <div class="main_login_right"><input type="text" name="privat_vezetekes" value="<?php
						 if ($_POST["privat_vezetekes"]) {
						     print $_POST["privat_vezetekes"];
						 } elseif (!$_SESSION["user"]["privat_vezetekes"]) {
						     
						 } else {
						     print $_SESSION["user"]["privat_vezetekes"];
						 }
	    ?>" size="32" maxlength="8" /></div>
		    <div class="main_login_spacer"></div>
		    <div class="main_login_left"><?php print($lang["felhasznalok"]["privat_fax"]); ?></div>
		    <div class="main_login_right"><input type="text" name="privat_fax" value="<?php
						 if ($_POST["privat_fax"])
						     print $_POST["privat_fax"]; elseif (!$_SESSION["user"]["privat_fax"]) {
						     
						 } else
						     print $_SESSION["user"]["privat_fax"];
	    ?>" size="32" maxlength="9" /></div>
		    <div class="main_login_spacer"></div>
		    <div class="main_login_left"><?php print($lang["felhasznalok"]["privat_email"]); ?></div>
		    <div class="main_login_right"><input type="text" name="privat_email" value="<?php
						 if ($_POST["privat_email"])
						     print $_POST["privat_email"]; elseif (!$_SESSION["user"]["privat_email"]) {
						     
						 } else
						     print $_SESSION["user"]["privat_email"];
	    ?>" size="32" maxlength="64" /></div>

		    <div class="main_login_spacer"></div>
		    <div class="main_login_left"><?php print($lang["felhasznalok"]["szuletesnap"]); ?></div>
		    <div class="main_login_right"><input id="szuletesnap" type="text" name="szuletesnap" value="<?php
						 if ($_POST["szuletesnap"]) {
						     print $_POST["szuletesnap"]; } elseif (!$_SESSION["user"]["szuletesnap"]) {
						     
						 } else
						     print $_SESSION["user"]["szuletesnap"];
						
	    ?>"/></div>
		    <div class="main_login_spacer"></div>
		    <div class="main_login_left"><?php print($lang["felhasznalok"]["nevnap"]); ?></div>
		    <div class="main_login_right"><input id="nevnap" type="text" name="nevnap" value="<?php
						 if ($_POST["nevnap"]) {
						     print $_POST["nevnap"];
						 } elseif (!$_SESSION["user"]["nevnap"]) {
						     
						 } else {
						     print "2011-" . $_SESSION["user"]["nevnap"][0] . $_SESSION["user"]["nevnap"][1] . "-" . $_SESSION["user"]["nevnap"][2] . $_SESSION["user"]["nevnap"][3];
						 }
	    ?>" size="32" maxlength="64" /></div>

		    <div class="main_login_spacer"></div>
		    <div class="main_login_left">&nbsp;</div>
		    <input class="ui-state-default ui-corner-all" type="submit" name="modosit" value="<?php print($lang["gomb"]["modosit"]); ?>" />
		    <input class="ui-state-default ui-corner-all" type="submit" name="modosit" value="<?php print($lang["gomb"]["megse"]); ?>" />
		</div>
	    </form>

	</div>


	<div class="main_center_spacer"></div>
	<?php
    }

//*********************************************************************	 
    function megjegyzes_modosit($error) {
	global $lang, $odin;
	?>
	<div class="main_center_spacer"></div>
	<div class="main_center_container">
	    <div class="main_center_title">
		<div class="main_center_title_left"><font style="color:#617f10">MEGJEGYZÉS</font> módosítása</div>
	    </div>
	    <?php if ($error) { ?>
	        <div class="ui-state-highlight ui-corner-all" style="margin-top: 5px; padding: 0 .7em;"> 
	    	<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
			<?php print $error["kiir"]; ?>
	    	</p>
	        </div>
	    <?php } ?>
	    <div class="ui-state-highlight ui-corner-all" style="margin-top: 5px; padding: 0 .7em;"> 
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		    <b>HASZNÁLHATÓ TAGEK:</b><br /><br />
		    - [B] vastag [/B] -> <?php print $odin->tags_to_html("[B] vastag [/B]"); ?> <br />
		    - [U] aláhúzott [/U] -> <?php print $odin->tags_to_html("[U] aláhúzott [/U]"); ?> <br />
		    - [FREYALINK=felhasznalok/2204] Dobó Tamás [/FREYALINK] ->  <?php print $odin->tags_to_html("[FREYALINK=felhasznalok/2204] Dobó Tamás [/FREYALINK]"); ?> <br />
		    - [LINK=http://tudakozo.telekom.hu] Telekom Tudakozó [/LINK] -> <?php print $odin->tags_to_html("[LINK=http://tudakozo.telekom.hu] Telekom Tudakozó [/LINK]"); ?> <br /><br />
		</p>
	    </div>
	    <div class="main_login_spacer"></div>
	    <form action="sajat-vilag/adatlapom/megjegyzes-szerkesztese" method="post"> 
		<div class="main_login_container">

		    <div class="main_login_left"><?php print($lang["felhasznalok"]["megjegyzes"]); ?></div>
		    <div class="main_login_right"><textarea cols="32" rows="5" name="megjegyzes"><?php
	if ($_POST["megjegyzes"]) {
	    print $_POST["megjegyzes"];
	} else if (!$_SESSION["user"]["megjegyzes"]) {
	    
	} else {
	    print $_SESSION["user"]["megjegyzes"];
	}
	    ?></textarea></div> 
		    <div class="main_login_spacer"></div>
		    <div class="main_login_spacer"></div>
		    <div class="main_login_left">&nbsp;</div>
		    <input class="ui-state-default ui-corner-all" type="submit" name="modosit" value="<?php print($lang["gomb"]["modosit"]); ?>" />
		    <input class="ui-state-default ui-corner-all" type="submit" name="modosit" value="<?php print($lang["gomb"]["megse"]); ?>" />
		</div>
	    </form>

	</div>


	<div class="main_center_spacer"></div>
	<?php
    }

//*********************************************************************	
    function szervezet_modosit($error) {
	global $lang, $odin, $q, $config;
	$details = $odin->get_szervezet_details($q[3]);
	
	if (!mysql_num_rows(mysql_query("SELECT id FROM szervezetek WHERE id = '" . $q[3] . "'")) && $q[3]) {
		?>
	<div class="main_center_spacer"></div>
	<div class="main_center_container">
	        <div class="ui-state-error ui-corner-all" style="margin-top: 20px; padding: 0 .7em;"> 
	    	<p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span>
		Nincs ilyen szervezet! A saját szervezethez való visszatéréshez kattintson <?php print "<a href=\"" . $config["site"]["absolutepath"] . "/sajat-vilag/adatlapom/szervezet-szerkesztese/" . $_SESSION["user"]["szervezet_id"] . "\">ide</a>"; ?>!
	    	</p>
	        </div>
	<div class="site_spacer"></div>
	</div>
	<div class="main_center_spacer"></div>
	    <?php
	    } else {
	    ?>
	<?php if (!$q[3]) { ?>
		<div class="main_center_spacer"></div>
	<div class="main_center_container">
	    <div class="main_center_title">
		<form action="sajat-vilag/adatlapom/szervezet-szerkesztese/<?php print $q[3]; ?>" method="post"> 
		    <input type="hidden" name="szervezet_id" value="<?php print $q[3]; ?>" /> 
		    <div class="main_center_title_left"><font style="color:#617f10">ÁTHELYZÉS </font> ebbe a szervezetbe</div>
		    <div class="main_center_title_right">
			<input class="ui-state-default ui-corner-all" type="submit" name="modosit" value="<?php print($lang["gomb"]["athelyez"]); ?>" />
			<input class="ui-state-default ui-corner-all" type="submit" name="modosit" value="<?php print($lang["gomb"]["megse"]); ?>" /></div>
		</form>
	    </div>
	    <?php 
	    	if ($error) { ?>
	        <div class="ui-state-highlight ui-corner-all" style="margin-top: 5px; padding: 0 .7em;"> 
	    	<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
			<?php print $error["kiir"]; ?>
	    	</p>
	        </div>
		<div class="main_center_content_spacer"></div>
	    <?php }
	    foreach ($details["valami"] as $key => $value) {
		$details["szervezet"]["nev"] = "<a href=\"" . $config["site"]["absolutepath"] . "/sajat-vilag/adatlapom/szervezet-szerkesztese/" . $value["id"] . "\">" . $value["nev"] . "</a><br />" . $details["szervezet"]["nev"];
	    }
	    ?>

	    <div class="main_center_content_left"></div>
	    <div class="main_center_content_center"><?php print "<a href=\"" . $config["site"]["absolutepath"] . "/sajat-vilag/adatlapom/szervezet-szerkesztese/0\">".$config["site"]["tree_root_element"]."</a><br />".$details["szervezet"]["nev"]; ?></div>
	    <div class="main_center_content_right"></div>
	    <div class="main_center_content_spacer"></div>


	    <?php
	    $a = 0;
	    $sql = "SELECT id, nev, teljes_id FROM szervezetek WHERE teljes_id LIKE '%'";
	    $result = mysql_query($sql);
	    while ($sor = mysql_fetch_assoc($result)) {
		if (!strcmp($sor["teljes_id"], ($sor["id"]))) {
		    $details["alarendelt_szervezet"]["nev"].="<a href=\"" . $config["site"]["absolutepath"] . "/sajat-vilag/adatlapom/szervezet-szerkesztese/" . $sor["id"] . "\">" . $sor["nev"] . "</a><br />";
		    $a++;
		}
	    }
	    ?>

	    <div class="main_center_title">
		<div class="main_center_title_left"><font style="color:#617f10">ALÁRENDELT </font> szervek</div>
	    </div>

	    <?php if ($a) { ?>
	        <div class="main_center_content_left"></div>
	        <div class="main_center_content_center"><?php print $details["alarendelt_szervezet"]["nev"]; ?></div>
	        <div class="main_center_content_right"></div>
	        <div class="main_center_content_spacer"></div>

	    <?php } else { ?>
	        <div class="main_center_content_left"></div>
	        <div class="main_center_content_center">Nincs alárendelt szerv!</div>
	        <div class="main_center_content_right"></div>
	        <div class="main_center_content_spacer"></div>
	    <?php } ?>
	    <div class="site_spacer"></div>
	    <div class="site_spacer"></div>

	</div>


	<div class="main_center_spacer"></div>
	    
	<?php } else { ?>
	<div class="main_center_spacer"></div>
	<div class="main_center_container">
	    <div class="main_center_title">
		<form action="sajat-vilag/adatlapom/szervezet-szerkesztese/<?php print $q[3]; ?>" method="post"> 
		    <input type="hidden" name="szervezet_id" value="<?php print $q[3]; ?>" /> 
		    <div class="main_center_title_left"><font style="color:#617f10">ÁTHELYZÉS </font> ebbe a szervezetbe</div>
		    <div class="main_center_title_right">
			<input class="ui-state-default ui-corner-all" type="submit" name="modosit" value="<?php print($lang["gomb"]["athelyez"]); ?>" />
			<input class="ui-state-default ui-corner-all" type="submit" name="modosit" value="<?php print($lang["gomb"]["megse"]); ?>" /></div>
		</form>
	    </div>
	    	    <?php 
	    	if ($error) { ?>
	        <div class="ui-state-highlight ui-corner-all" style="margin-top: 5px; padding: 0 .7em;"> 
	    	<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
			<?php print $error["kiir"]; ?>
	    	</p>
	        </div>
		<div class="main_center_content_spacer"></div>
	    <?php }

	    foreach ($details["valami"] as $key => $value) {
		$details["szervezet"]["nev"] = "<a href=\"" . $config["site"]["absolutepath"] . "/sajat-vilag/adatlapom/szervezet-szerkesztese/" . $value["id"] . "\">" . $value["nev"] . "</a><br />" . $details["szervezet"]["nev"];
	    }
	    ?>

	    <div class="main_center_content_left"></div>
	    <div class="main_center_content_center"><?php print "<a href=\"" . $config["site"]["absolutepath"] . "/sajat-vilag/adatlapom/szervezet-szerkesztese/0\">".$config["site"]["tree_root_element"]."</a><br />".$details["szervezet"]["nev"]; ?></div>
	    <div class="main_center_content_right"></div>
	    <div class="main_center_content_spacer"></div>


	    <?php
	    $a = 0;
	    $sql = "SELECT id, nev, teljes_id FROM szervezetek WHERE teljes_id LIKE '" . $details["teljes_id"] . "%'";
	    $result = mysql_query($sql);
	    while ($sor = mysql_fetch_assoc($result)) {
		if (!strcmp($sor["teljes_id"], ($details["teljes_id"] . "-" . $sor["id"]))) {
		    $details["alarendelt_szervezet"]["nev"].="<a href=\"" . $config["site"]["absolutepath"] . "/sajat-vilag/adatlapom/szervezet-szerkesztese/" . $sor["id"] . "\">" . $sor["nev"] . "</a><br />";
		    $a++;
		}
	    }
	    ?>

	    <div class="main_center_title">
		<div class="main_center_title_left"><font style="color:#617f10">ALÁRENDELT </font> szervek</div>
	    </div>

	    <?php if ($a) { ?>
	        <div class="main_center_content_left"></div>
	        <div class="main_center_content_center"><?php print $details["alarendelt_szervezet"]["nev"]; ?></div>
	        <div class="main_center_content_right"></div>
	        <div class="main_center_content_spacer"></div>

	    <?php } else { ?>
	        <div class="main_center_content_left"></div>
	        <div class="main_center_content_center">Nincs alárendelt szerv!</div>
	        <div class="main_center_content_right"></div>
	        <div class="main_center_content_spacer"></div>
	    <?php } ?>
	    <div class="site_spacer"></div>
	    <div class="site_spacer"></div>

	</div>


	<div class="main_center_spacer"></div>
	<?php
		}
	    }
    }

//*********************************************************************	
    function ceges_elerhetosegek_modosit($error) {
	global $lang, $odin, $q;
	?>
	<div class="main_center_spacer"></div>

	<div class="main_center_container">
	    <div class="main_center_title">
		<div class="main_center_title_left"><font style="color:#617f10">CÉGES</font> Elérhetőségek módosítása</div>
	    </div>
	    <?php if ($error) { ?>
	        <div class="ui-state-highlight ui-corner-all" style="margin-top: 5px; padding: 0 .7em;"> 
	    	<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
			<?php print $error["kiir"]; ?>
	    	</p>
	        </div>
	    <?php } ?>
	    <div class="main_login_spacer"></div>

	    <form action="sajat-vilag/adatlapom/ceges-elerhetosegek-szerkesztese" method="post"> 
		<div class="main_login_container">

		    <div class="main_login_left"><?php print($lang["felhasznalok"]["telephely_irsz"]); ?></div>
		    <div class="main_login_right"><input type="text" name="telephely_irsz" value="<?php
	    if ($_POST["telephely_irsz"]) {
		print $_POST["telephely_irsz"];
	    } else if (!$_SESSION["user"]["telephely_irsz"]) {
		
	    } else {
		print $_SESSION["user"]["telephely_irsz"];
	    }
	    ?>" size="32" maxlength="4" /></div> 
		    <div class="main_login_spacer"></div>

		    <div class="main_login_left"><?php print($lang["felhasznalok"]["telephely_varos"]); ?></div>
		    <div class="main_login_right"><input type="text" name="telephely_varos" value="<?php
		if ($_POST["telephely_varos"]) {
		    print $_POST["telephely_varos"];
		} else if (!$_SESSION["user"]["telephely_varos"]) {
		    
		} else {
		    print $_SESSION["user"]["telephely_varos"];
		}
	    ?>" size="32" maxlength="32" /></div> 
		    <div class="main_login_spacer"></div>

		    <div class="main_login_left"><?php print($lang["felhasznalok"]["telephely_cim"]); ?></div>
		    <div class="main_login_right"><input type="text" name="telephely_cim" value="<?php
		if ($_POST["telephely_cim"]) {
		    print $_POST["telephely_cim"];
		} else if (!$_SESSION["user"]["telephely_cim"]) {
		    
		} else {
		    print $_SESSION["user"]["telephely_cim"];
		}
	    ?>" size="32" maxlength="64" /></div> 
		    <div class="main_login_spacer"></div>

		    <div class="main_login_left"><?php print($lang["felhasznalok"]["telephely_szam"]); ?></div>
		    <div class="main_login_right"><input type="text" name="telephely_szam" value="<?php
		if ($_POST["telephely_szam"]) {
		    print $_POST["telephely_szam"];
		} else if (!$_SESSION["user"]["telephely_szam"]) {
		    
		} else {
		    print $_SESSION["user"]["telephely_szam"];
		}
	    ?>" size="32" maxlength="16" /></div> 
		    <div class="main_login_spacer"></div>


		    <div class="main_login_left"><?php print($lang["felhasznalok"]["telephely_epulet"]); ?></div>
		    <div class="main_login_right"><input type="text" name="telephely_epulet" value="<?php
		if ($_POST["telephely_epulet"]) {
		    print $_POST["telephely_epulet"];
		} else if (!$_SESSION["user"]["telephely_epulet"]) {
		    
		} else {
		    print $_SESSION["user"]["telephely_epulet"];
		}
	    ?>" size="32" maxlength="16" /></div> 
		    <div class="main_login_spacer"></div>

		    <div class="main_login_left"><?php print($lang["felhasznalok"]["telephely_emelet"]); ?></div>
		    <div class="main_login_right"><input type="text" name="telephely_emelet" value="<?php
		if ($_POST["telephely_emelet"]) {
		    print $_POST["telephely_emelet"];
		} else if (!$_SESSION["user"]["telephely_emelet"]) {
		    
		} else {
		    print $_SESSION["user"]["telephely_emelet"];
		}
	    ?>" size="32" maxlength="16" /></div> 
		    <div class="main_login_spacer"></div>

		    <div class="main_login_left"><?php print($lang["felhasznalok"]["telephely_iroda"]); ?></div>
		    <div class="main_login_right"><input type="text" name="telephely_iroda" value="<?php
		if ($_POST["telephely_iroda"]) {
		    print $_POST["telephely_iroda"];
		} else if (!$_SESSION["user"]["telephely_iroda"]) {
		    
		} else {
		    print $_SESSION["user"]["telephely_iroda"];
		}
	    ?>" size="32" maxlength="16" /></div> 
		    <div class="main_login_spacer"></div>

		    <div class="main_login_left"><?php print($lang["felhasznalok"]["vezetoi"]); ?></div>
		    <div class="main_login_right"><input type="text" name="vezetoi" value="<?php
		if ($_POST["vezetoi"]) {
		    print $_POST["vezetoi"];
		} else if (!$_SESSION["user"]["vezetoi"]) {
		    
		} else {
		    print $_SESSION["user"]["vezetoi"];
		}
	    ?>" size="32" maxlength="6" /></div> 
		    <div class="main_login_spacer"></div>

		    <div class="main_login_left"><?php print($lang["felhasznalok"]["kozvetlen"]); ?></div>
		    <div class="main_login_right"><input type="text" name="kozvetlen" value="<?php
		if ($_POST["kozvetlen"]) {
		    print $_POST["kozvetlen"];
		} else if (!$_SESSION["user"]["kozvetlen"]) {
		    
		} else {
		    print $_SESSION["user"]["kozvetlen"];
		}
	    ?>" size="32" maxlength="8" /></div> 
		    <div class="main_login_spacer"></div>

		    <div class="main_login_left"><?php print($lang["felhasznalok"]["2helyi"]); ?></div>
		    <div class="main_login_right"><input type="text" name="2helyi" value="<?php
		if ($_POST["2helyi"]) {
		    print $_POST["2helyi"];
		} else if (!$_SESSION["user"]["2helyi"]) {
		    
		} else {
		    print $_SESSION["user"]["2helyi"];
		}
	    ?>" size="32" maxlength="6" /></div> 
		    <div class="main_login_spacer"></div>

		    <div class="main_login_left"><?php print($lang["felhasznalok"]["fax"]); ?></div>
		    <div class="main_login_right"><input type="text" name="fax" value="<?php
		if ($_POST["fax"]) {
		    print $_POST["fax"];
		} else if (!$_SESSION["user"]["fax"]) {
		    
		} else {
		    print $_SESSION["user"]["fax"];
		}
	    ?>" size="32" maxlength="6" /></div> 
		    <div class="main_login_spacer"></div>
		    <div class="main_login_left"><?php print($lang["felhasznalok"]["mobil"]); ?></div>
		    <div class="main_login_right"><input type="text" name="mobil" value="<?php
		if ($_POST["mobil"]) {
		    print $_POST["mobil"];
		} else if (!$_SESSION["user"]["mobil"]) {
		    
		} else {
		    print $_SESSION["user"]["mobil"];
		}
	    ?>" size="32" maxlength="9" /></div> 
		    <div class="main_login_spacer"></div>

		    <div class="main_login_left"><?php print($lang["felhasznalok"]["vez_kozvetlen"]); ?></div>
		    <div class="main_login_right"><input type="text" name="vez_kozvetlen" value="<?php
		if ($_POST["vez_kozvetlen"]) {
		    print $_POST["vez_kozvetlen"];
		} else if (!$_SESSION["user"]["vez_kozvetlen"]) {
		    
		} else {
		    print $_SESSION["user"]["vez_kozvetlen"];
		}
	    ?>" size="32" maxlength="8" /></div> 
		    <div class="main_login_spacer"></div>

		    <div class="main_login_left"><?php print($lang["felhasznalok"]["vez_fax"]); ?></div>
		    <div class="main_login_right"><input type="text" name="vez_fax" value="<?php
		if ($_POST["vez_fax"]) {
		    print $_POST["vez_fax"];
		} else if (!$_SESSION["user"]["vez_fax"]) {
		    
		} else {
		    print $_SESSION["user"]["vez_fax"];
		}
	    ?>" size="32" maxlength="8" /></div> 
		    <div class="main_login_spacer"></div>

		    <div class="main_login_left"><?php print($lang["felhasznalok"]["tetra"]); ?></div>
		    <div class="main_login_right"><input type="text" name="tetra" value="<?php
		if ($_POST["tetra"]) {
		    print $_POST["tetra"];
		} else if (!$_SESSION["user"]["tetra"]) {
		    
		} else {
		    print $_SESSION["user"]["tetra"];
		}
	    ?>" size="32" maxlength="7" /></div> 
		    <div class="main_login_spacer"></div>		                     

		    <div class="main_login_spacer"></div>
		    <div class="main_login_left">&nbsp;</div>
		    <input class="ui-state-default ui-corner-all" type="submit" name="modosit" value="<?php print($lang["gomb"]["modosit"]); ?>" />
		    <input class="ui-state-default ui-corner-all" type="submit" name="modosit" value="<?php print($lang["gomb"]["megse"]); ?>" />
		</div>
	    </form>

	</div>


	<div class="main_center_spacer"></div>
	<?php
    }

//*********************************************************************	
    function fiok_adataim_kiir() {
	global $lang, $odin, $q, $config;

		$details=$odin->get_user_basedetails($_SESSION["user"]["id"]);
	$from=strtotime("-30 day");
	$from60=strtotime("-60 day");
	$stat["munkamenetek"] = 0;
	$stat["munkamenetek30"] = 0;
	$stat["munkamenetek60"] = 0;
	$stat["oldalletoltes"] = 0;
	$stat["oldalletoltes30"] = 0;
	$stat["oldalletoltes60"] = 0;
	$stat["oldalletoltesido"] = 0;
	$stat["oldalletoltesido30"] = 0;
	$stat["oldalletoltesido60"] = 0;
	$stat["keresesek"] = 0;
	$stat["keresesek30"] = 0;
	$stat["keresesek60"] = 0;
	$stat["keresesekido"] = 0;
	$stat["keresesekido60"] = 0;

        $sql = "SELECT * FROM oldalletoltesek WHERE `erintett` = " . $_SESSION["user"]["id"] . "";
        $result = mysql_query($sql);
        while ($sor = mysql_fetch_assoc($result)) {
            if ($sor["esemeny"] == 1) {
                $account["letrehozva"] = $sor["user_id"] . " által, ekkor: " . $sor["ekkor"] . "";
            }
            if ($sor["esemeny"] == 2 && !$account["elsoaktivalas"]) {
                $account["elsoaktivalas"] = $sor["ekkor"] . "";
            }

            if ($sor["esemeny"] < 5) {
                $account["utolsomod"]["ekkor"] = $sor["ekkor"];
                $account["utolsomod"]["byid"] = $sor["user_id"];
                $account["utolsomod"]["esemeny"] = $sor["esemeny"];
            }
        }
        $altal = $odin->get_user_basedetails($account["utolsomod"]["byid"]);

        $sql = "SELECT * FROM oldalletoltesek WHERE `user_id` = " . $_SESSION["user"]["id"] . "";
	$result = mysql_query($sql);
	while ($sor = mysql_fetch_assoc($result)) {
	    $stat["oldalletoltes"]++;
	    $stat["oldalletoltesido"]=$stat["oldalletoltesido"]+$sor["oldalletoltes"];
	    if ($sor["ekkor"]>($from)){
		    $stat["oldalletoltes30"]++;
		    $stat["oldalletoltesido30"]=$stat["oldalletoltesido30"]+$sor["oldalletoltes"];
		}
	    elseif ($sor["ekkor"]>($from60)){
		    $stat["oldalletoltes60"]++;
		    $stat["oldalletoltesido60"]=$stat["oldalletoltesido60"]+$sor["oldalletoltes"];
		}
	    if (stristr($sor["keres"], "kereses")) {
		$stat["keresesek"]++;
		$stat["keresesekido"]=$stat["keresesekido"]+$sor["oldalletoltes"];
		if ($sor["ekkor"]>$from){
		    $stat["keresesek30"]++;
		    $stat["keresesekido30"]=$stat["keresesekido30"]+$sor["oldalletoltes"];
		}
		elseif ($sor["ekkor"]>$from60){
		    $stat["keresesek60"]++;
		    $stat["keresesekido60"]=$stat["keresesekido60"]+$sor["oldalletoltes"];
		}
	    }
	}

	$stat["oldalletoltesido"]=round($stat["oldalletoltesido"]/$stat["oldalletoltes"],2);
	$stat["oldalletoltesido30"]=round($stat["oldalletoltesido30"]/$stat["oldalletoltes30"],2);
	$stat["oldalletoltesido60"]=round($stat["oldalletoltesido60"]/$stat["oldalletoltes60"],2);
	$stat["keresesekido"]=round($stat["keresesekido"]/$stat["keresesek"],2);
	$stat["keresesekido30"]=round($stat["keresesekido30"]/$stat["keresesek30"],2);
	$stat["keresesekido60"]=round($stat["keresesekido60"]/$stat["keresesek60"],2);
	
	$sql = "SELECT id, aktiv_ekkor, bongeszo FROM munkamenetek WHERE user_id=".$_SESSION["user"]["id"]."";
	$result = mysql_query($sql);
	while ($sor = mysql_fetch_assoc($result)) {
	    $stat["munkamenetek"]++;
	    if ($sor["aktiv_ekkor"]>$from){
		    $stat["munkamenetek30"]++;
		}
	    elseif ($sor["aktiv_ekkor"]>$from60){
		    $stat["munkamenetek60"]++;
		}
	}
	
        if (!$account["letrehozva"]) {
            $account["letrehozva"] = "A fiók az \"ELENOR\" rendszerből került átemelésre!";
        }
        if (!$account["elsoaktivalas"]) {
            $account["elsoaktivalas"] = "A felhasználói fiók inaktív!";
        }
        ?>
        <div class="main_center_spacer"></div>
        <div class="main_center_container">
            <div class="main_center_title">
                <div class="main_center_title_left"><font style="color:#617f10">ACCOUNT</font> Adatok</div>
                <div class="main_center_title_right">
			    <?php if ($odin->admin_this("user","delete",$_SESSION["user"]["id"])){ ?>
			    <a title="felhasznaló törlése" href="<?php print $config["site"]["absolutepath"] . "/felhasznalok/".$_SESSION["user"]["id"]."/adatlap/fiok-adatok"; ?>"><span class="ui-state-default ui-corner-all ui-icon ui-icon-circle-close"></span></a>
			    <?php } ?>
			</div>
            </div>
            <div class="main_center_content_spacer"></div>
	    <div class="main_center_content_left">Account id:</div><div class="main_center_content_center"><?php print ($details["user"]["id"]); ?></div>
            <div class="main_center_content_spacer"></div>
	    <div class="main_center_content_left">Account név:</div><div class="main_center_content_center"><?php print ($details["user"]["teljesnev"]); ?></div>
            <div class="main_center_content_spacer"></div>
            <div class="main_center_content_left">Account létrehozva:</div><div class="main_center_content_center"><?php print ($account["letrehozva"]); ?></div>
            <div class="main_center_content_spacer"></div>
            <div class="main_center_content_left">Account először aktiválva:</div><div class="main_center_content_center"><?php
        if (is_numeric($account["elsoaktivalas"]))
            print (date('Y-m-d H:i:s', $account["elsoaktivalas"])); else
            print ($account["elsoaktivalas"]);
        ?></div>
            <div class="main_center_content_spacer"></div>
            <?php if (date('Y-m-d H:i:s', $account["utolsomod"]["ekkor"]) == "1970-01-01 01:00:00") { ?>
                <div class="main_center_content_left">Account utoljára módosítva</div><div class="main_center_content_center">Nincs módosítás</div><div class="main_center_content_right"></div>
                <div class="main_center_content_spacer"></div>
                <?php
            } else {
                ?>

                <div class="main_center_content_left">Account utoljára módosítva</div><div class="main_center_content_center"><?php print date('Y-m-d H:i:s', $account["utolsomod"]["ekkor"]); ?></div><div class="main_center_content_right"><a title="Kifejt" href="#" id="names-toogle" onclick="return false"><span class="ui-state-default ui-corner-all ui-icon ui-icon-help"></span></a></div>
                <div class="main_center_content_spacer"></div>
                <div id="main_center_content_names" class="ui-corner-all">
                    <div class="main_center_content_left">Végrehajtotta:</div><div class="main_center_content_center"><?php print "<a href=\"" . $config["site"]["absolutepath"] . "/felhasznalok/" . $account["utolsomod"]["byid"] . "\">" . $altal["user"]["teljesnev"] . "</a>"; ?></div>
                    <div class="main_center_content_spacer"></div>
                    <div class="main_center_content_left">Ekkor:</div><div class="main_center_content_center"><?php print date('Y-m-d H:i:s', $account["utolsomod"]["ekkor"]); ?></div>
                    <div class="main_center_content_spacer"></div>
                    <div class="main_center_content_left">Esemény:</div><div class="main_center_content_center"><?php print $lang["log"]["esemenyek"]["" . $account["utolsomod"]["esemeny"]]; ?></div>
                    <div class="main_center_content_spacer"></div>
                </div>
            <?php } ?>

            	    <div class="main_center_title">
		<div class="main_center_title_left"><font style="color:#617f10">FREYA</font> Statisztika</div>
		<div class="main_center_title_right"></div>
	    </div>
	    <div class="main_center_content_spacer"></div>
	    <div class="main_center_content_left"></div><div class="main_center_content_center"><b>Utolsó 30 nap<br /></b>(az előző időszakhoz képest)</div><div class="main_center_content_right"><b>2012.01.10 óta</b></div>
	    <div class="main_center_content_spacer"></div>
	    <div class="main_center_content_spacer"></div>
	    <div class="main_center_content_spacer"></div>
	    <div class="main_center_content_left">Munkamenetet:</div><div class="main_center_content_center"><?php print ("<b>".$stat["munkamenetek30"]."</b>"); if (($stat["munkamenetek30"]-$stat["munkamenetek60"])>0) print (" (<font style=\"color:green;\">+".($stat["munkamenetek30"]-$stat["munkamenetek60"])."</font>)"); else print (" (<font style=\"color:red;\">".($stat["munkamenetek30"]-$stat["munkamenetek60"])."</font>)");?></div><div class="main_center_content_right"><?php print ($stat["munkamenetek"]); ?></div>
	    <div class="main_center_content_spacer"></div>
	    <div class="main_center_content_left">Oldallatöltés:</div><div class="main_center_content_center"><?php print ("<b>".$stat["oldalletoltes30"]."</b>"); if (($stat["oldalletoltes30"]-$stat["oldalletoltes60"])>0) print (" (<font style=\"color:green;\">+".($stat["oldalletoltes30"]-$stat["oldalletoltes60"])."</font>)"); else print (" (<font style=\"color:red;\">".($stat["oldalletoltes30"]-$stat["oldalletoltes60"])."</font>)");?></div><div class="main_center_content_right"><?php print ($stat["oldalletoltes"]); ?></div>
	    <div class="main_center_content_spacer"></div>
	    <div class="main_center_content_left">Átlagos oldalletöltési idő:</div><div class="main_center_content_center"><?php print ("<b>".$stat["oldalletoltesido30"]."s</b>"); if (($stat["oldalletoltesido30"]-$stat["oldalletoltesido60"])>0) print (" (<font style=\"color:red;\">+".($stat["oldalletoltesido30"]-$stat["oldalletoltesido60"])."s</font>)"); else print (" (<font style=\"color:green;\">".($stat["oldalletoltesido30"]-$stat["oldalletoltesido60"])."s</font>)");?></div><div class="main_center_content_right"><?php print ($stat["oldalletoltesido"]."s"); ?></div>  
	    <div class="main_center_content_spacer"></div>
	    <div class="main_center_content_left">Végrehajtot keresések:</div><div class="main_center_content_center"><?php print ("<b>".$stat["keresesek30"]."</b>"); if (($stat["keresesek30"]-$stat["keresesek60"])>0) print (" (<font style=\"color:green;\">+".($stat["keresesek30"]-$stat["keresesek60"])."</font>)"); else print (" (<font style=\"color:red;\">".($stat["keresesek30"]-$stat["keresesek60"])."</font>)");?></div><div class="main_center_content_right"><?php print ($stat["keresesek"]); ?></div>
	    <div class="main_center_content_spacer"></div>
	    <div class="main_center_content_left">Átlagos keresési idő:</div><div class="main_center_content_center"><?php print ("<b>".$stat["keresesekido30"]."s</b>"); if (($stat["keresesekido30"]-$stat["keresesekido60"])>0) print (" (<font style=\"color:red;\">+".($stat["keresesekido30"]-$stat["keresesekido60"])."s</font>)"); else print (" (<font style=\"color:green;\">".($stat["keresesekido30"]-$stat["keresesekido60"])."s</font>)");?></div><div class="main_center_content_right"><?php print ($stat["keresesekido"]."s"); ?></div>  
	    <div class="main_center_content_spacer"></div>
        </div>

        <div class="main_center_spacer"></div>
        <?php
    }

//*********************************************************************
    function jogosultsagaim_kiir() {
	global $lang, $odin, $q;
	?>
	<div class="main_center_spacer"></div>
	<div class="main_center_container">
	    <div class="main_center_title">
		<div class="main_center_title_left"><font style="color:#617f10">ÁLTALÁNOS </font> Jogosultságok</div>
		<div class="main_center_title_right"></div>
	    </div>

	    <div class="main_center_content_spacer"></div>
	    <div class="main_center_content_left">Bejelentkezési jog:</div><div class="main_center_content_center"><?php
	if ($_SESSION["user"]["account_statusz"] < 2 && (($_SESSION["user"]["allomany_statusz"] > 0 && $_SESSION["user"]["allomany_statusz"] < 6) || $_SESSION["user"]["allomany_statusz"] < 6))
	    print "IGAZ"; else
	    print "HAMIS";
	?></div><div class="main_center_content_right"></div>
	    <div class="main_center_content_spacer"></div>
	    <div class="main_center_content_left">Account státusz:</div><div class="main_center_content_center"><?php
	if ($_SESSION["user"]["account_statusz"] == 1)
	    print "AKTIV"; elseif ($_SESSION["user"]["account_statusz"] == 2)
	    print "BANOLT"; else
	    print "INAKTÍV";
	?></div><div class="main_center_content_right"></div>
	    <div class="main_center_content_spacer"></div>
	    <div class="main_center_content_left">Superuser jog:</div><div class="main_center_content_center"><?php
	if ($_SESSION["user"]["root"] == 1)
	    print "IGAZ"; else
	    print "HAMIS";
	?></div><div class="main_center_content_right"></div>
	</div>

	<div class="main_center_spacer"></div>
	<?php
    }

//********************************************************************* 
    function fiok_logom_kiir() {
	global $lang, $odin, $q, $config;
	?>
	<div class="main_center_spacer"></div>
	<div class="main_center_container">
	    <div class="main_center_title">
		<div class="main_center_title_left"><font style="color:#617f10">FIÓK </font> Logom</div>
		<div class="main_center_title_right">
		    <form action="<?php print $config["site"]["absolutepath"] . "/sajat-vilag/fiok-logom"; ?>" method="post">
			<input type="hidden" name="from" value="<?php
	if ($_POST["from"])
	    print $_POST["from"]; else
	    print "1";
	?>" />
			<input type="hidden" value="<?php
		if ($_POST["to"])
		    print $_POST["to"]; else
		    print "$limit";
	?>" />
			<select name="feltetel">
			    <option value="mind">Mind</option>
			    <option value="fontos" <?php
		if ($_POST["feltetel"] == "fontos") {
		    print "selected=\"selected\"";
		}
	?> >Csak fontos</option>
			</select>
			<select name="limit">
			    <option value="10">10</option>
			    <option value="25" <?php
		    if ($_POST["limit"] == "25") {
			print "selected=\"selected\"";
		    }
	?>>25</option>
			    <option value="50" <?php
		    if ($_POST["limit"] == "50") {
			print "selected=\"selected\"";
		    }
	?>>50</option>
			    <option value="100" <?php
		    if ($_POST["limit"] == "100") {
			print "selected=\"selected\"";
		    }
	?>>100</option>
			</select>  
			<input class="ui-state-default ui-corner-all" type="submit" name="szukitsd" value="szűkítsd" />
		    </form></div>
	    </div>

	    <div class="main_center_content_left"><b>Végrehajtotta</b></div><div class="main_center_content_center"><b>Esemény</b></div><div class="main_center_content_right"><b>Ekkor</b></div>
	    <div class="main_center_content_spacer"></div>
	    <?php
	    $counter = 0;
	    if ($_POST["limit"]) {
		$limit = $_POST["limit"];
	    } else {
		$limit = 10;
	    }
	    if ($_POST["lap"]) {
		$b = explode("-", $_POST["lap"]);
		$from = $b[0];
		$to = $b[1];
	    } else {
		$from = 1;
		$to = $limit;
	    }

	    $sqlpart = " WHERE";
	    if (!$_POST["feltetel"]) {
		$feltetel = "mind";
	    } else
		$feltetel = $_POST["feltetel"];
	    if ($_POST["feltetel"] == "mind" || !$_POST["feltetel"]) {
		$sqlpart.=" (`erintett` = " . $_SESSION["user"]["id"] . " OR  `user_id` = " . $_SESSION["user"]["id"] . " ) AND `esemeny` > '0'";
	    }
	    if ($_POST["feltetel"] == "fontos") {
		$sqlpart.=" (`erintett` = " . $_SESSION["user"]["id"] . " OR  `user_id` = " . $_SESSION["user"]["id"] . " ) AND (`esemeny` = 14 OR (`esemeny` > '0' AND `esemeny` < '5'))";
	    }
	    $sqlpart.=" ORDER BY id DESC";


	    $sql = "SELECT count(*) as count FROM oldalletoltesek" . $sqlpart;
	    $result = mysql_query($sql);
	    while ($sor = mysql_fetch_assoc($result)) {
		$count = $sor["count"];
	    }
	    $pagecount = (int) ($count / $limit);
	    if ($count % $limit != 0)
		$pagecount++;


	    while ($sor = mysql_fetch_assoc($result)) {
		$count = $sor["count"];
	    }
	    $sql = "SELECT * FROM oldalletoltesek" . $sqlpart . " LIMIT " . ($from - 1) . ", " . ($limit) . "";
$result = mysql_query($sql);
	    $result = mysql_query($sql);
	    while ($sor = mysql_fetch_assoc($result)) {
		?>
	        <div class="main_center_content_spacer"></div>
	        <div class="main_center_content_names<?php
	    if (($counter + 1) % 2 == 0)
		print "_white"; else
		print "_blue";
		?>" class="ui-corner-all">
	    	<div class="main_center_content_spacer"></div>
	    	<div class="main_center_content_left" style="font-weight: normal">
			<?php
			if (!$sor["user_id"])
			    print "Rendszer";
			else {
			    $details = $odin->get_user_basedetails($sor["user_id"]);
			    if ($details) {
				print "<a href=\"" . $config["site"]["absolutepath"] . "/felhasznalok/" . $sor["user_id"] . "\">" . $details["user"]["teljesnev"] . "</a>";
			    } else {
				print "<br />Törölt felhasználó";
			    }
			}
			?></div><div class="main_center_content_center"><?php
	    print $lang["log"]["esemenyek"][$sor["esemeny"]];
	    if ($sor["esemenylista"]) {
		print "<br/>" . $sor["esemenylista"];
	    }
	    if ($sor["erintett"] && ($sor["esemeny"] == 1 || $sor["esemeny"] == 2 || $sor["esemeny"] == 3 || $sor["esemeny"] == 4 || $sor["esemeny"] == 14 || $sor["esemeny"] == 15 || $sor["esemeny"] == 16)) {
		$details = $odin->get_user_basedetails($sor["erintett"]);
		if ($details) {
		    print "<br />Érintett: <a href=\"" . $config["site"]["absolutepath"] . "/felhasznalok/" . $details["user"]["id"] . "\">" . $details["user"]["teljesnev"] . "</a>";
		} else {
		    print "<br />Törölt felhasználó";
		}
	    }
	    if ($sor["erintett"] && ($sor["esemeny"] == 5 || $sor["esemeny"] == 6 || $sor["esemeny"] == 7)) {
		$sql2 = "SELECT nev, id FROM szervezetek WHERE id = " . $sor["erintett"] . "";
		$result2 = mysql_query($sql2);
		while ($sor2 = mysql_fetch_assoc($result2)) {
		    $details2 = $sor2;
		    if ($details2["nev"]) {
			print "<br />Érintett: <a href=\"" . $config["site"]["absolutepath"] . "/szervezetek/" . $details2["id"] . "\">" . $details2["nev"] . "</a>";
		    } else {
			print "<br />Törölt szervezet";
		    }
		}
	    }
	    if ($sor["erintett"] && ($sor["esemeny"] == 8 || $sor["esemeny"] == 9 || $sor["esemeny"] == 10)) {
		$sql2 = "SELECT beosztasnev, id FROM beosztasok WHERE id = " . $sor["erintett"] . "";
		$result2 = mysql_query($sql2);
		while ($sor2 = mysql_fetch_assoc($result2)) {
		    $details2 = $sor2;
		}
		if ($details2["beosztasnev"]) {
		    print "<br />Érintett: <a href=\"" . $config["site"]["absolutepath"] . "/beosztasok/" . $details2["id"] . "\">" . $details2["beosztasnev"] . "</a>";
		} else {
		    print "<br />Törölt beosztas";
		}
	    }
			?></div><div class="main_center_content_right"><?php print date('Y-m-d H:i:s', $sor["ekkor"]); ?></div>
	    	<div class="main_center_content_spacer"></div>
	        </div>


	    <?php
	    $counter++;
	}

	if ($pagecount > 1) {
	    ?>
	        <div class="main_center_content_spacer"></div>
	        <div class="main_center_content_names<?php
	    if (($counter + 1) % 2 == 0)
		print "_white"; else
		print "_blue";
	    ?>" class="ui-corner-all">
	    	<form style=" min-height: 22px; padding-left:5px; padding-right:5px;text-align: center;" action="<?php print $config["site"]["absolutepath"] . "/sajat-vilag/fiok-logom"; ?>" method="post">
	    	    <input type="hidden" name="limit" value="<?php print $limit; ?>" />
	    	    <input type="hidden" name="feltetel" value="<?php print $feltetel; ?>" />
	    	    Találatok:
	    	    <select name="lap">
	    		<option value="<?php print "1 - " . $limit ?>"><?php print "1 - " . $limit ?></option>
		<?php $counter = 1;
		while ($counter != $pagecount) { ?>
				<option value="<?php print (($limit * $counter) + 1) . " - " . ($limit * ($counter + 1)) ?>" <?php
		if ($_POST["lap"] == ("" . ($limit * $counter) + 1) . " - " . ($limit * ($counter + 1) . "")) {
		    print "selected=\"selected\"";
		}
		    ?>><?php print (($limit * $counter) + 1) . " - " . ($limit * ($counter + 1)) ?></option>
		    <?php $counter++;
		} ?>
	    	    </select>
	    	    <input class="ui-state-default ui-corner-all" type="submit" name="mehet" value="Mehet" />
	    	</form>   
	    	<div class="main_center_content_spacer"></div>
	        </div>
		<?php
	    }
	    if ($counter == 0) {
		?>
	        <div class="ui-state-highlight ui-corner-all" style="margin-top: 5px; padding: 0 .7em;"> 
	    	<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
	    	    Nincs megjeleníthető esemény!</p></div>
	    <?php
	}
	?>
	</div>

	<div class="main_center_spacer"></div>
	<?php
    }

//********************************************************************* 
}
?>