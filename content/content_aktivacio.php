<?php

class aktivacio_content {

//*********************************************************************	
    function nyito($talalat) {
	global $lang, $config, $q;
	?>
	<div class="main_center_spacer"></div>

	<div class="main_center_container">
	    <div class="main_center_title">
		<div class="main_center_title_left"><font style="color:#617f10">FELHASZNÁLÓI FIÓK</font> Aktiválása</div>
		<div class="main_center_title_right"></div>
	    </div>
	    <?php
	    if($_POST["elkuld"])
                                        {
                                        ?>
                                        <div class="ui-state-error ui-corner-all" style="margin-top: 20px; padding: 0 .7em;"> 
				                            <p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span>
								<b>HIBA A BEVITT ADATOKBAN!</b><br /><br />
		<?php 
	 
	 if ($talalat) { print "- Már létező Felhasználói azonosító, vagy Groupwise cím!<br />"; }
	 if (strlen($_POST["azonosito"])<4 || strlen($_POST["azonosito"])>32) { print "- Hibás felhasználói azonosító! (A felhasználó azonosító minimum 4, maximum 32 karakter hosszú lehet)<br />"; }
	 if (strlen($_POST["email"])<4 || !stristr($_POST["email"],"@") || !stristr($_POST["email"],".")) { print "- Hibás e-mail cím!<br />"; }
	 if (strlen($_POST["jelszo1"])<4 || $_POST["jelszo1"]!=$_POST["jelszo2"]) { print "- Hibás jelszó<br />"; }
	 ?></p>
			                             </div>
                                         <div class="main_login_spacer"></div>
	                                    <?php
                                        }else {?>
	     <div class="ui-state-highlight ui-corner-all" style="margin-top: 5px; padding: 0 .7em;"> 
                <p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		    <b>ÚTMUTATÁS:</b><br /><br />- 1: Adj meg egy Felhasználói azonosítót! (Bármilyen minimum 4, maximum 32 karekter hosszú azonosító lehet.)<br />
			- 2: Add meg a GroupWise címed! ( Csak, ha nem jól jelent meg. ) <br />
			- 3: Adj meg egy minimum 4 karakter hosszú jelszót!<br />
			- 4: Add meg újra a jelszó!<br />
                </p>
            </div>
					 <div class="main_login_spacer"></div>
	    <?php }
?>
	    <form action="<?php print ("" . $config["site"]["absolutepath"] . "/aktivacio/" . $_SESSION["user"]["id"] . "/" . md5($_SESSION["user"]["passkey"])); ?>" method="post">
		<div class="main_login_container">
		    <input type="hidden" name="aktivacio" value="aktivacio" />

		    <div class="main_login_left"><?php print($lang["aktivacio"]["azonosito"]); ?></div>
		    <div class="main_login_right"><input type="text" name="azonosito" value="<?php if ($_POST["azonosito"]) print $_POST["azonosito"]; else print $_SESSION["user"]["azonosito"]; ?>" size="32" maxlength="64" /></div>
		    <div class="main_login_spacer"></div>
		    <div class="main_login_left"><?php print($lang["aktivacio"]["email"]); ?></div>
		    <div class="main_login_right"><input type="text" name="email" value="<?php if ($_POST["email"]) print $_POST["email"]; else print $_SESSION["user"]["email"]; ?>" size="32" maxlength="64" /></div>
		    <div class="main_login_spacer"></div>
		    <div class="main_login_left"><?php print($lang["aktivacio"]["uj_jelszo"]); ?></div>
		    <div class="main_login_right"><input type="password" name="jelszo1" value="<?php if ($_POST["jelszo1"]) print $_POST["jelszo1"]; else {} ?>" size="32" maxlength="64" /></div>
		    <div class="main_login_spacer"></div>
		    <div class="main_login_left"><?php print($lang["aktivacio"]["uj_jelszo_ujra"]); ?></div>
		    <div class="main_login_right"><input type="password" name="jelszo2" value="<?php if ($_POST["jelszo2"]) print $_POST["jelszo2"]; else {} ?>" size="32" maxlength="64" /></div>
		    <div class="main_login_spacer"></div>
		    <div class="main_login_left">&nbsp;</div>
		    <div class="main_login_right"><input class="ui-state-default ui-corner-all" type="submit" name="elkuld" value="<?php print($lang["gomb"]["elkuld"]); ?>" />
		    </div>


		</div>
	    </form>  
	    <div class="main_center_spacer"></div>
	</div>
	<?php
    }
//*********************************************************************	
    function megerosit() {
	global $lang, $config, $q;
	?>
	<div class="main_center_spacer"></div>

	<div class="main_center_container">
	    <div class="main_center_title">
		<div class="main_center_title_left"><font style="color:#617f10">FELHASZNÁLÓI FIÓK</font> Aktiválása</div>
		<div class="main_center_title_right"></div>
	    </div>
	    <?php
	    if($_POST["aktival"])
                                        {
                                        ?>
                                        <div class="ui-state-error ui-corner-all" style="margin-top: 20px; padding: 0 .7em;"> 
				                            <p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span>
	 <?php 
	 print "Hibás ellenőrző kód!";
	 ?></p>
			                             </div>
                                         <div class="main_login_spacer"></div>
					 <div class="main_login_spacer"></div>
	                                    <?php
                                        }else
					{
					                                         ?>
                                        <div class="ui-state-highlight ui-corner-all" style="margin-top: 5px; padding: 0 .7em;"> 
				                            <p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
								<b>AKTIVÁCIÓS KÓD ELÜLDVE!</b><br /><br />
							    - Az aktivációhoz szükséges ellenőrzőkódot a megadott GroupWise címedre elküldtem! <br />- Várj egy percet, majd ellenőrizd a postafiókod!</p>
			                             </div>
                                         <div class="main_login_spacer"></div>
					 <div class="main_login_spacer"></div>
	                                    <?php   
					} ?>
 	    <form action="<?php print ("" . $config["site"]["absolutepath"] . "/aktivacio/" . $_SESSION["user"]["id"] . "/" . md5($_SESSION["user"]["passkey"])); ?>" method="post">
		<div class="main_login_container">
		    <input type="hidden" name="aktivacio" value="aktivacio" />
		    <input type="hidden" name="azonosito" value="<?php print $_POST["azonosito"]; ?>"></input>
		    <input type="hidden" name="email" value="<?php print $_POST["email"]; ?>"></input>
		    <input type="hidden" name="jelszo1" value="<?php print $_POST["jelszo1"]; ?>"></input>
		    <div class="main_login_left"><?php print($lang["aktivacio"]["ellenorzokod"]); ?></div>
		    <div class="main_login_right"><input type="text" name="passkey" value="<?php if ($_POST["passkey"]) print $_POST["passkey"]; ?>" size="32" maxlength="64" /></div>
		    <div class="main_login_spacer"></div>
		    <div class="main_login_left">&nbsp;</div>
		    <div class="main_login_right"><input class="ui-state-default ui-corner-all" type="submit" name="aktival" value="<?php print($lang["gomb"]["aktival"]); ?>" />
		    </div>


		</div>
	    </form> 
	    <div class="main_center_spacer"></div>
	</div>
	<?php
    }
 //*********************************************************************	   
}
?>
