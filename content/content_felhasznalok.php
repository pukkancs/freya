<?php

class felhasznalok_content {

//*********************************************************************	
    function adatlap_kiir() {
	global $lang, $odin, $q, $config;

	if ($q[1] == 1 && $_SESSION["user"]["id"] != 1) {
	    ?>

	    <div class="main_center_container">
	        <div class="main_center_spacer"></div>
	        <div class="ui-widget">
	    	<div class="ui-state-highlight ui-corner-all"> 
	    	    <p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
	    		Én vagyok itt az atyaúristen. Ne akarj engem nézegetni!
	    	</div>
	        </div>
	    </div>
	    <div class="site_spacer"></div>


	    <?php
	} else {
	    $details = $odin->get_user_details($q["1"]);
	    $details["user"] = array_merge($details["user"], $odin->get_user_coc($details["user"]["szervezet_id"]));
	    if (!$details) {
		?>
		<div class="main_center_container">
		    <div class="main_center_spacer"></div>
		    <div class="ui-widget">
			<div class="ui-state-highlight ui-corner-all"> 
			    <p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
				Nincs ilyen azonosítójú felhasználó!
			</div>
		    </div>
		</div>
		<div class="site_spacer"></div>     
		<?php
	    } else {
		?>
		<div class="main_center_spacer"></div>

		<div class="main_center_container">
		    <div class="main_center_title">
			<div class="main_center_title_left"><font style="color:#617f10">SZEMÉLYES</font> Adatok</div>
			<div class="main_center_title_right">
			    <?php if ($odin->admin_this("user", "edit", $q[1])) { ?>
		    	    <a title="szerkeszt" href="<?php print $config["site"]["absolutepath"] . "/felhasznalok/" . $q[1] . "/adatlap/szemelyes-adatok-szerkesztese"; ?>"><span class="ui-state-default ui-corner-all ui-icon ui-icon-pencil"></span></a>
			    <?php } ?>
			</div>
		    </div>

		    <div class="main_center_content_spacer"></div>
		    <div class="main_center_content_left"><?php print ($lang["felhasznalok"]["teljesnev"] . ":"); ?></div><div class="main_center_content_center"><b><?php print $odin->fancy_text($details["user"]["teljesnev"]); ?></b></div><div class="main_center_content_right"><a title="Kifejt" href="#" id="names-toogle" onclick="return false"><span class="ui-state-default ui-corner-all ui-icon ui-icon-help"></span></a></div>
		    <div class="main_center_content_spacer"></div>
		    <div id="main_center_content_names" class="ui-corner-all">
			<div class="main_center_content_left"><?php print ($lang["felhasznalok"]["titulus"] . ":"); ?></div><div class="main_center_content_center"><?php print $odin->fancy_text($details["user"]["titulus"]); ?></div>
			<div class="main_center_content_spacer"></div>
			<div class="main_center_content_left"><?php print ($lang["felhasznalok"]["vezeteknev"] . ":"); ?></div><div class="main_center_content_center"><?php print $odin->fancy_text($details["user"]["vezeteknev"]); ?></div>
			<div class="main_center_content_spacer"></div>
			<div class="main_center_content_left"><?php print ($lang["felhasznalok"]["kozepsonev"] . ":"); ?></div><div class="main_center_content_center"><?php print $odin->fancy_text($details["user"]["kozepsonev"]); ?></div>
			<div class="main_center_content_spacer"></div>
			<div class="main_center_content_left"><?php print ($lang["felhasznalok"]["keresztnev"] . ":"); ?></div><div class="main_center_content_center"><?php print $odin->fancy_text($details["user"]["keresztnev"]); ?></div>
			<div class="main_center_content_spacer"></div>
		    </div>
		    <?php if ($details["user"]["nicknev"]) { ?>
		        <div class="main_center_content_left"><?php print ($lang["felhasznalok"]["nicknev"] . ":"); ?></div><div class="main_center_content_center"><?php print $odin->fancy_text($details["user"]["nicknev"]); ?></div>
		        <div class="main_center_content_spacer"></div>
		    <?php } ?>
		    <div class="main_center_content_left"><?php print ($lang["felhasznalok"]["email"] . ":"); ?></div><div class="main_center_content_center"><?php print ($odin->fancy_text($details["user"]["email"])); ?></div>
		    <div class="main_center_content_spacer"></div>
		    <div class="main_center_content_left"><?php print ($lang["felhasznalok"]["rendfokozat"] . ":"); ?></div><div class="main_center_content_center"><?php print $odin->fancy_text($details["user"]["rendfokozat"]); ?></div>
		    <div class="main_center_content_spacer"></div>
		    <div class="main_center_content_left"><?php print ($lang["felhasznalok"]["titulus_2"] . ":"); ?></div><div class="main_center_content_center"><?php print $odin->fancy_text($details["user"]["titulus_2"]); ?></div>
		    <div class="main_center_content_spacer"></div>
		    <div class="main_center_content_left"><?php print ($lang["felhasznalok"]["beosztas"] . ":"); ?></div><div class="main_center_content_center"><?php print "<a href=\"" . $config["site"]["absolutepath"] . "/beosztasok/" . $details["user"]["beosztas_id"] . "\">" . $odin->fancy_text($details["user"]["beosztas"]) . "</a>"; ?></div>
		    <div class="main_center_content_spacer"></div>
		    <?php if ($details["user"]["beosztas_megjegyzes"]) { ?>
		        <div class="main_center_content_left"><?php print ($lang["felhasznalok"]["beosztas_megjegyzes"] . ":"); ?></div><div class="main_center_content_center"><?php print $odin->fancy_text($details["user"]["beosztas_megjegyzes"]); ?></div>
		        <div class="main_center_content_spacer"></div>
		    <?php } ?>
		    <div class="main_center_content_left"><?php print ($lang["felhasznalok"]["beosztas_jellege"] . ":"); ?></div><div class="main_center_content_center"><?php print $odin->fancy_text($lang["beosztas_jellege"]["" . $details["user"]["allomany_statusz"] . ""]); ?></div>
		    <div class="main_center_content_spacer"></div>

		    <div class="main_center_title">
			<div class="main_center_title_left"><font style="color:#617f10">SZERVEZET</font> Adatok</div>
			<div class="main_center_title_right">
			    <?php if ($odin->admin_this("user", "edit", $q[1])) { ?>
		    	    <a title="szerkeszt" href="<?php print $config["site"]["absolutepath"] . "/felhasznalok/" . $q[1] . "/adatlap/szervezeti-besorolas-szerkesztese/" . $details["user"]["szervezet_id"] . ""; ?>"><span class="ui-state-default ui-corner-all ui-icon ui-icon-pencil"></span></a>
			    <?php } ?>
			</div>
		    </div>
		    <div class="main_center_content_left"><?php print ($lang["felhasznalok"]["szervezet"] . ":"); ?></div><div class="main_center_content_center">
			<?php
			$i = -1;
			while ($i != $details["user"]["szervezet_szam"]) {
			    $i++;
			    ?>
			    <?php
			    if ($details["user"]["szervezetek"]["$i"]["szervezet_tipus"] > 1) {
				print "<a href=\"szervezetek/" . $details["user"]["szervezetek"][$i]["id"] . "\">" . $details["user"]["szervezetek"]["$i"]["nev"] . "</a>";
				?><br />
				<?php
			    }
			}
			?>
		    </div>
		    <div class="main_center_content_spacer"></div>
		    <div class="main_center_content_spacer"></div>
		    <div class="main_center_content_left">Vezető:</div>
		    <div class="main_center_content_center"><?php print $odin->fancy_text($details["user"]["vezeto_tel"]); ?></div>
		    <div class="main_center_content_right"><?php print $odin->fancy_text($details["user"]["vezeto"]); ?></div>
		    <div class="main_center_content_spacer"></div>
		    <?php if ($details["user"]["vezeto-helyettes"]) { ?>
		        <div class="main_center_content_left">Vezető-helyettes:</div>
		        <div class="main_center_content_center"><?php print $odin->fancy_text($details["user"]["vezeto-helyettes_tel"]); ?></div>
		        <div class="main_center_content_right"><?php print $odin->fancy_text($details["user"]["vezeto-helyettes"]); ?></div>
		        <div class="main_center_content_spacer"></div>
		    <?php } ?>
		    <div class="main_center_content_left">Titkárság:</div>
		    <div class="main_center_content_center"><?php print $odin->fancy_text($details["user"]["titkarno_tel"]); ?></div>
		    <div class="main_center_content_right"><?php print $odin->fancy_text($details["user"]["titkarno"]); ?></div>
		    <div class="main_center_content_spacer"></div>
		    <?php
		    if (!$details["user"]["titkarno"] && $details["user"]["titkarsag"]) {
			?>
		        <div class="main_center_content_left">Felettes szerv titkársága:</div>
		        <div class="main_center_content_center"><?php print $details["user"]["titkarsag"]; ?></div>
		        <div class="main_center_content_right"><?php print $details["user"]["titkarsag_nevek"]; ?></div>
		        <div class="main_center_content_spacer"></div>
			<?php
		    }
		    ?>

		    <div class="main_center_title">
			<div class="main_center_title_left"><font style="color:#617f10">CÉGES</font> Elérhetőségek</div>
			<div class="main_center_title_right">
			    <?php if ($odin->admin_this("user", "edit", $q[1])) { ?>
		    	    <a title="szerkeszt" href="<?php print $config["site"]["absolutepath"] . "/felhasznalok/" . $q[1] . "/adatlap/ceges-elerhetosegek-szerkesztese"; ?>"><span class="ui-state-default ui-corner-all ui-icon ui-icon-pencil"></span></a>
			    <?php } ?>
			</div>
		    </div>
		    <div class="main_center_content_spacer"></div>
		    <div class="main_center_content_left"><?php print ($lang["felhasznalok"]["telephely"] . ":"); ?></div><div class="main_center_content_center"><?php print $odin->fancy_text($details["user"]["telephely"] . "<br />" . $details["user"]["iroda"]); ?></div>
		    <div class="main_center_content_spacer"></div>
		    <div class="main_center_content_left"><?php print ($lang["felhasznalok"]["levelezesi"] . ":"); ?></div><div class="main_center_content_center"><?php print $odin->fancy_text($details["user"]["levelezesi"]); ?></div>
		    <div class="main_center_content_spacer"></div>
		    <div class="main_center_content_left"><?php print ($lang["felhasznalok"]["vezetoi"] . ":"); ?></div><div class="main_center_content_center"><?php print ($odin->fancy_phone_number($details["user"]["vezetoi"])); ?></div>
		    <div class="main_center_content_spacer"></div>
		    <div class="main_center_content_left"><?php print ($lang["felhasznalok"]["kozvetlen"] . ":"); ?></div><div class="main_center_content_center"><?php print "<a href=\"" . $config["site"]["absolutepath"] . "/telefonkonyv/kereses/kereses-helye=/nev=/beosztas=/szervezet=/kozvetlenszam=" . $details["user"]["kozvetlen"] . "/szokezdet=/fonetikus=/lista=1-10" . "\">" . $odin->fancy_phone_number($details["user"]["kozvetlen"]) . "</a>"; ?></div>
		    <div class="main_center_content_spacer"></div>
		    <div class="main_center_content_left"><?php print ($lang["felhasznalok"]["2helyi"] . ":"); ?></div><div class="main_center_content_center"><?php print ($odin->fancy_phone_number($details["user"]["2helyi"])); ?></div>  
		    <div class="main_center_content_spacer"></div>
		    <div class="main_center_content_left"><?php print ($lang["felhasznalok"]["fax"] . ":"); ?></div><div class="main_center_content_center"><?php print $odin->fancy_phone_number($details["user"]["fax"]); ?></div>
		    <div class="main_center_content_spacer"></div>
		    <div class="main_center_content_left"><?php print ($lang["felhasznalok"]["mobil"] . ":"); ?></div><div class="main_center_content_center"><?php print $odin->fancy_phone_number($details["user"]["mobil"]); ?></div>
		    <div class="main_center_content_spacer"></div>
		    <div class="main_center_content_left"><?php print ($lang["felhasznalok"]["vez_kozvetlen"] . ":"); ?></div><div class="main_center_content_center"><?php print $odin->fancy_phone_number($details["user"]["vez_kozvetlen"]); ?></div>
		    <div class="main_center_content_spacer"></div>
		    <div class="main_center_content_left"><?php print ($lang["felhasznalok"]["vez_fax"] . ":"); ?></div><div class="main_center_content_center"><?php print $odin->fancy_phone_number($details["user"]["vez_fax"]); ?></div>
		    <div class="main_center_content_spacer"></div>
		    <div class="main_center_content_left"><?php print ($lang["felhasznalok"]["tetra"] . ":"); ?></div><div class="main_center_content_center"><?php print $odin->fancy_phone_number($details["user"]["tetra"]); ?></div>
		    <div class="main_center_content_spacer"></div>

		    <div class="main_center_title">
			<div class="main_center_title_left"><font style="color:#617f10">MEGJEGYZÉSEK</font></div>
			<div class="main_center_title_right">
			    <?php if ($odin->admin_this("user", "edit", $q[1])) { ?>
		    	    <a title="szerkeszt" href="<?php print $config["site"]["absolutepath"] . "/felhasznalok/" . $q[1] . "/adatlap/megjegyzes-szerkesztese"; ?>"><span class="ui-state-default ui-corner-all ui-icon ui-icon-pencil"></span></a>
			    <?php } ?>
			</div>
		    </div>
		    <div class="main_center_content_spacer"></div>
		    <div class="main_center_content_left"><?php print ($lang["felhasznalok"]["megjegyzes"] . ":"); ?></div><div class="main_center_content_center"><?php print $odin->fancy_text(nl2br($odin->tags_to_html($details["user"]["megjegyzes"]))); ?></div>
		</div>

		<div class="main_center_spacer"></div>
		<?php
	    }
	}
    }

//*********************************************************************	
    function fiok_adatok_kiir() {
	global $lang, $odin, $q, $config;

	$details = $odin->get_user_basedetails($q[1]);
	$from = strtotime("-30 day");
	$from60 = strtotime("-60 day");
	$stat["munkamenetek"] = 0;
	$stat["munkamenetek30"] = 0;
	$stat["munkamenetek60"] = 0;
	$stat["oldalletoltes"] = 0;
	$stat["oldalletoltes30"] = 0;
	$stat["oldalletoltes60"] = 0;
	$stat["oldalletoltesido"] = 0;
	$stat["oldalletoltesido30"] = 0;
	$stat["oldalletoltesido60"] = 0;
	$stat["keresesek"] = 0;
	$stat["keresesek30"] = 0;
	$stat["keresesek60"] = 0;
	$stat["keresesekido"] = 0;
	$stat["keresesekido60"] = 0;

	$sql = "SELECT * FROM oldalletoltesek WHERE `erintett` = " . $q[1] . "";
	$result = mysql_query($sql);
	while ($sor = mysql_fetch_assoc($result)) {
	    if ($sor["esemeny"] == 1) {
		$letrehozo = $odin->get_user_basedetails($sor["user_id"]);
		$account["letrehozva"] = "<a href=\"" . $config["site"]["absolutepath"] . "/felhasznalok/" . $letrehozo["user"]["id"] . "\">" . $letrehozo["user"]["teljesnev"] . "</a> által, ekkor: " . date("Y:m:d - H:i", $sor["ekkor"]) . "";
	    }
	    if ($sor["esemeny"] == 2 && !$account["elsoaktivalas"]) {
		$account["elsoaktivalas"] = $sor["ekkor"] . "";
	    }

	    if ($sor["esemeny"] < 5) {
		$account["utolsomod"]["ekkor"] = $sor["ekkor"];
		$account["utolsomod"]["byid"] = $sor["user_id"];
		$account["utolsomod"]["esemeny"] = $sor["esemeny"];
	    }
	}
	$altal = $odin->get_user_basedetails($account["utolsomod"]["byid"]);

	$sql = "SELECT * FROM oldalletoltesek WHERE `user_id` = " . $q[1] . " ORDER BY ekkor";
	$result = mysql_query($sql);
	while ($sor = mysql_fetch_assoc($result)) {
	    $stat["oldalletoltes"]++;
	    $stat["oldalletoltesido"] = $stat["oldalletoltesido"] + $sor["oldalletoltes"];
	    if ($sor["ekkor"] > ($from)) {
		$stat["oldalletoltes30"]++;
		$stat["oldalletoltesido30"] = $stat["oldalletoltesido30"] + $sor["oldalletoltes"];
	    } elseif ($sor["ekkor"] > ($from60)) {
		$stat["oldalletoltes60"]++;
		$stat["oldalletoltesido60"] = $stat["oldalletoltesido60"] + $sor["oldalletoltes"];
	    }
	    if (stristr($sor["keres"], "kereses")) {
		$stat["keresesek"]++;
		$stat["keresesekido"] = $stat["keresesekido"] + $sor["oldalletoltes"];
		if ($sor["ekkor"] > $from) {
		    $stat["keresesek30"]++;
		    $stat["keresesekido30"] = $stat["keresesekido30"] + $sor["oldalletoltes"];
		} elseif ($sor["ekkor"] > $from60) {
		    $stat["keresesek60"]++;
		    $stat["keresesekido60"] = $stat["keresesekido60"] + $sor["oldalletoltes"];
		}
	    }
	    $account["utolsoaktivitas"] = $sor["ekkor"];
	}

	$stat["oldalletoltesido"] = round($stat["oldalletoltesido"] / $stat["oldalletoltes"], 2);
	$stat["oldalletoltesido30"] = round($stat["oldalletoltesido30"] / $stat["oldalletoltes30"], 2);
	$stat["oldalletoltesido60"] = round($stat["oldalletoltesido60"] / $stat["oldalletoltes60"], 2);
	$stat["keresesekido"] = round($stat["keresesekido"] / $stat["keresesek"], 2);
	$stat["keresesekido30"] = round($stat["keresesekido30"] / $stat["keresesek30"], 2);
	$stat["keresesekido60"] = round($stat["keresesekido60"] / $stat["keresesek60"], 2);

	$sql = "SELECT id, aktiv_ekkor, bongeszo FROM munkamenetek WHERE user_id=" . $q[1] . "";
	$result = mysql_query($sql);
	while ($sor = mysql_fetch_assoc($result)) {
	    $stat["munkamenetek"]++;
	    if ($sor["aktiv_ekkor"] > $from) {
		$stat["munkamenetek30"]++;
	    } elseif ($sor["aktiv_ekkor"] > $from60) {
		$stat["munkamenetek60"]++;
	    }
	}

	if (!$account["letrehozva"]) {
	    $account["letrehozva"] = "A fiók az \"ELENOR\" rendszerből került átemelésre!";
	}
	if (!$account["elsoaktivalas"]) {
	    $account["elsoaktivalas"] = "A felhasználói fiók inaktív!";
	}
	?>
	<div class="main_center_spacer"></div>
	<div class="main_center_container">
	    <div class="main_center_title">
		<div class="main_center_title_left"><font style="color:#617f10">ACCOUNT</font> Adatok</div>
		<div class="main_center_title_right">
	<?php if ($odin->admin_this("user", "delete", $q[1])) { ?>
	    	    <a style="float:right;" title="felhasznaló törlése" href="<?php print $config["site"]["absolutepath"] . "/felhasznalok/" . $q[1] . "/fiok-adatok/felhasznalo-torlese"; ?>"><span class="ui-state-default ui-corner-all ui-icon ui-icon-circle-close"></span></a>
		    <?php } ?>	    <?php if ($odin->admin_this("user", "edit", $q[1])) { ?>
	    	    <a style="float:right;" title="felhasznaló módosítása" href="<?php print $config["site"]["absolutepath"] . "/felhasznalok/" . $q[1] . "/fiok-adatok/felhasznalo-szerkesztese"; ?>"><span class="ui-state-default ui-corner-all ui-icon ui-icon-pencil"></span></a>
		    <?php } ?>
		</div>
	    </div>
	    <div class="main_center_content_spacer"></div>
	    <div class="main_center_content_left">Account id:</div><div class="main_center_content_center"><?php print ($details["user"]["id"]); ?></div>
	    <div class="main_center_content_spacer"></div>
	    <div class="main_center_content_left">Account név:</div><div class="main_center_content_center"><?php print ($details["user"]["teljesnev"]); ?></div>
	    <div class="main_center_content_spacer"></div>
	    <div class="main_center_content_left">Account azonosító:</div><div class="main_center_content_center"><?php print ($details["user"]["azonosito"]); ?></div>
	    <div class="main_center_content_spacer"></div>
	    <div class="main_center_content_left">Account jelszó:</div><div class="main_center_content_center">* * * * * *</div>
	    <div class="main_center_content_spacer"></div>
	    <div class="main_center_content_left">Account létrehozva:</div><div class="main_center_content_center"><?php print ($account["letrehozva"]); ?></div>
	    <div class="main_center_content_spacer"></div>
	    <div class="main_center_content_left">Account először aktiválva:</div><div class="main_center_content_center"><?php
		    if (is_numeric($account["elsoaktivalas"]))
			print (date('Y-m-d H:i:s', $account["elsoaktivalas"])); else
			print ($account["elsoaktivalas"]);
		    ?></div>
	    <div class="main_center_content_spacer"></div>
		<?php if (date('Y-m-d H:i:s', $account["utolsomod"]["ekkor"]) == "1970-01-01 01:00:00") { ?>
	        <div class="main_center_content_left">Account utoljára módosítva</div><div class="main_center_content_center">Nincs módosítás</div><div class="main_center_content_right"></div>
	        <div class="main_center_content_spacer"></div>
		<?php
	    } else {
		?>

	        <div class="main_center_content_left">Account utoljára módosítva</div><div class="main_center_content_center"><?php print date('Y-m-d H:i:s', $account["utolsomod"]["ekkor"]); ?></div><div class="main_center_content_right"><a title="Kifejt" href="#" id="names-toogle" onclick="return false"><span class="ui-state-default ui-corner-all ui-icon ui-icon-help"></span></a></div>
	        <div class="main_center_content_spacer"></div>
	        <div id="main_center_content_names" class="ui-corner-all">
	    	<div class="main_center_content_left">Végrehajtotta:</div><div class="main_center_content_center"><?php print "<a href=\"" . $config["site"]["absolutepath"] . "/felhasznalok/" . $account["utolsomod"]["byid"] . "\">" . $altal["user"]["teljesnev"] . "</a>"; ?></div>
	    	<div class="main_center_content_spacer"></div>
	    	<div class="main_center_content_left">Ekkor:</div><div class="main_center_content_center"><?php print date('Y-m-d H:i:s', $account["utolsomod"]["ekkor"]); ?></div>
	    	<div class="main_center_content_spacer"></div>
	    	<div class="main_center_content_left">Esemény:</div><div class="main_center_content_center"><?php print $lang["log"]["esemenyek"]["" . $account["utolsomod"]["esemeny"]]; ?></div>
	    	<div class="main_center_content_spacer"></div>
	        </div>
	<?php } ?>
	    <div class="main_center_content_left">Utoljára aktív:</div><div class="main_center_content_center"><?php print date('Y-m-d H:i:s', $account["utolsoaktivitas"]); ?></div>
	    <div class="main_center_content_spacer"></div>

	    <div class="main_center_title">
		<div class="main_center_title_left"><font style="color:#617f10">FREYA</font> Statisztika</div>
		<div class="main_center_title_right"></div>
	    </div>
	    <div class="main_center_content_spacer"></div>
	    <div class="main_center_content_left"></div><div class="main_center_content_center"><b>Utolsó 30 nap<br /></b>(az előző időszakhoz képest)</div><div class="main_center_content_right"><b>2012.01.10 óta</b></div>
	    <div class="main_center_content_spacer"></div>
	    <div class="main_center_content_spacer"></div>
	    <div class="main_center_content_spacer"></div>
	    <div class="main_center_content_left">Munkamenetet:</div><div class="main_center_content_center"><?php print ("<b>" . $stat["munkamenetek30"] . "</b>");
	if (($stat["munkamenetek30"] - $stat["munkamenetek60"]) > 0)
	    print (" (<font style=\"color:green;\">+" . ($stat["munkamenetek30"] - $stat["munkamenetek60"]) . "</font>)"); else
	    print (" (<font style=\"color:red;\">" . ($stat["munkamenetek30"] - $stat["munkamenetek60"]) . "</font>)"); ?></div><div class="main_center_content_right"><?php print ($stat["munkamenetek"]); ?></div>
	    <div class="main_center_content_spacer"></div>
	    <div class="main_center_content_left">Oldallatöltés:</div><div class="main_center_content_center"><?php print ("<b>" . $stat["oldalletoltes30"] . "</b>");
	if (($stat["oldalletoltes30"] - $stat["oldalletoltes60"]) > 0)
	    print (" (<font style=\"color:green;\">+" . ($stat["oldalletoltes30"] - $stat["oldalletoltes60"]) . "</font>)"); else
	    print (" (<font style=\"color:red;\">" . ($stat["oldalletoltes30"] - $stat["oldalletoltes60"]) . "</font>)"); ?></div><div class="main_center_content_right"><?php print ($stat["oldalletoltes"]); ?></div>
	    <div class="main_center_content_spacer"></div>
	    <div class="main_center_content_left">Átlagos oldalletöltési idő:</div><div class="main_center_content_center"><?php print ("<b>" . $stat["oldalletoltesido30"] . "s</b>");
	if (($stat["oldalletoltesido30"] - $stat["oldalletoltesido60"]) > 0)
	    print (" (<font style=\"color:red;\">+" . ($stat["oldalletoltesido30"] - $stat["oldalletoltesido60"]) . "s</font>)"); else
	    print (" (<font style=\"color:green;\">" . ($stat["oldalletoltesido30"] - $stat["oldalletoltesido60"]) . "s</font>)"); ?></div><div class="main_center_content_right"><?php print ($stat["oldalletoltesido"] . "s"); ?></div>  
	    <div class="main_center_content_spacer"></div>
	    <div class="main_center_content_left">Végrehajtot keresések:</div><div class="main_center_content_center"><?php print ("<b>" . $stat["keresesek30"] . "</b>");
	if (($stat["keresesek30"] - $stat["keresesek60"]) > 0)
	    print (" (<font style=\"color:green;\">+" . ($stat["keresesek30"] - $stat["keresesek60"]) . "</font>)"); else
	    print (" (<font style=\"color:red;\">" . ($stat["keresesek30"] - $stat["keresesek60"]) . "</font>)"); ?></div><div class="main_center_content_right"><?php print ($stat["keresesek"]); ?></div>
	    <div class="main_center_content_spacer"></div>
	    <div class="main_center_content_left">Átlagos keresési idő:</div><div class="main_center_content_center"><?php print ("<b>" . $stat["keresesekido30"] . "s</b>");
	if (($stat["keresesekido30"] - $stat["keresesekido60"]) > 0)
	    print (" (<font style=\"color:red;\">+" . ($stat["keresesekido30"] - $stat["keresesekido60"]) . "s</font>)"); else
	    print (" (<font style=\"color:green;\">" . ($stat["keresesekido30"] - $stat["keresesekido60"]) . "s</font>)"); ?></div><div class="main_center_content_right"><?php print ($stat["keresesekido"] . "s"); ?></div>  
	    <div class="main_center_content_spacer"></div>
	</div>

	<div class="main_center_spacer"></div>
	<?php
    }

//*********************************************************************	
    function privat_adatlap_kiir() {
	global $lang, $odin, $q;

	if ($q[1] == 1 && $_SESSION["user"]["id"] != 1) {
	    ?>

	    <div class="main_center_container">
	        <div class="main_center_spacer"></div>
	        <div class="ui-widget">
	    	<div class="ui-state-highlight ui-corner-all"> 
	    	    <p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
	    		Én vagyok itt az atyaúristen. Ne akarj engem nézegetni!
	    	</div>
	        </div>
	    </div>
	    <div class="site_spacer"></div>


	    <?php
	} else {
	    $details = $odin->get_user_basedetails($q["1"]);
	    if (!$details) {
		?>
		<div class="main_center_container">
		    <div class="main_center_spacer"></div>
		    <div class="ui-widget">
			<div class="ui-state-highlight ui-corner-all"> 
			    <p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
				Nincs ilyen azonosítójú felhasználó!
			</div>
		    </div>
		</div>
		<div class="site_spacer"></div>     
		    <?php
		} else {
		    ?>
		<div class="main_center_spacer"></div>

		<div class="main_center_title">
		    <div class="main_center_title_left"><font style="color:#617f10">PRIVÁT</font> Elérhetőségek</div>
		    <div class="main_center_title_right"></div>
		</div>
		<div class="main_center_content_left"><?php print ($lang["felhasznalok"]["teljesnev"] . ":"); ?></div><div class="main_center_content_center"><b><?php print $odin->fancy_text($details["user"]["teljesnev"]); ?></b></div><div class="main_center_content_right"></a></div>
		<div class="main_center_content_spacer"></div>
		<div class="main_center_content_left"><?php print ($lang["felhasznalok"]["szuletesnap"] . ":"); ?></div><div class="main_center_content_center"><?php
		if ($details["user"]["szuletesnap"] && $details["user"]["szuletesnap"] != "0000-00-00")
		    print $details["user"]["szuletesnap"];
		else
		    print "NA / NT";
		?></div>
		<div class="main_center_content_spacer"></div>
		<div class="main_center_content_left"><?php print ($lang["felhasznalok"]["nevnap"] . ":"); ?></div><div class="main_center_content_center"><?php
		if (!$details["user"]["nevnap"])
		    print "NA / NT"; else
		    print ($lang["honapok"][$details["user"]["nevnap"][0] . $details["user"]["nevnap"][1]] . " " . $details["user"]["nevnap"][2] . $details["user"]["nevnap"][3] . ".");
		    ?></div>
		<div class="main_center_content_spacer"></div>
		<div class="main_center_content_left"><?php print ($lang["felhasznalok"]["privat_mobil"] . ":"); ?></div><div class="main_center_content_center"><?php print $odin->fancy_phone_number($details["user"]["privat_mobil"]); ?></div>
		<div class="main_center_content_spacer"></div>
		<div class="main_center_content_left"><?php print ($lang["felhasznalok"]["privat_vezetekes"] . ":"); ?></div><div class="main_center_content_center"><?php print $odin->fancy_phone_number($details["user"]["privat_vezetekes"]); ?></div>
		<div class="main_center_content_spacer"></div>
		<div class="main_center_content_left"><?php print ($lang["felhasznalok"]["privat_fax"] . ":"); ?></div><div class="main_center_content_center"><?php print $odin->fancy_phone_number($details["user"]["privat_fax"]); ?></div>
		<div class="main_center_content_spacer"></div>
		<div class="main_center_content_left"><?php print ($lang["felhasznalok"]["privat_email"] . ":"); ?></div><div class="main_center_content_center"><?php print $odin->fancy_text($details["user"]["privat_email"]); ?></div>
		<div class="main_center_content_spacer"></div>

		<div class="main_center_spacer"></div>
		<?php
	    }
	}
    }

//*********************************************************************
    function privat_adatlap_ellenoriz() {
	global $lang, $config, $q;
	?>
	<div class="main_center_spacer"></div>

	<div class="main_center_container">
	    <div class="main_center_title">
		<form action="<?php print ("" . $config["site"]["absolutepath"] . "/felhasznalok/" . $q[1] . "/privat-adatlap"); ?>" method="post">
		    <div class="main_center_title_left"><font style="color:#617f10">PRIVÁT</font> Adatok megtekintése</div>
		    <div class="main_center_title_right"><input class="ui-state-default ui-corner-all" type="submit" name="kiir" value="mutasd" /></div>
		</form> 
	    </div>
	    <div class="main_content_spacer"></div>  
	    <div class="ui-state-highlight ui-corner-all" style="margin-top: 5px; padding: 0 .7em;"> 
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		    <b>A rendszer minden privát adatlap megtekintést naplóz!</b>
		</p>
	    </div>
	    <div class="site_spacer"></div>                                

	</div>
	    <?php
	}

//*********************************************************************	
	function szemelyes_adatok_modosit($error) {
	    global $lang, $odin, $q;
	    $details = $odin->get_user_details($q["1"]);
	    ?>
	<div class="main_center_spacer"></div>

	<div class="main_center_container">
	    <div class="main_center_title">
		<div class="main_center_title_left"><font style="color:#617f10">SZEMÉLYES</font> Adatok módosítása</div>
	    </div>
			<?php if ($error) { ?>
	        <div class="ui-state-highlight ui-corner-all" style="margin-top: 5px; padding: 0 .7em;"> 
	    	<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
			    <?php print $error["kiir"]; ?>
	    	</p>
	        </div>
	<?php } ?>
	    <div class="main_login_spacer"></div>

	    <form action="felhasznalok/<?php print $q[1]; ?>/adatlap/szemelyes-adatok-szerkesztese" method="post"> 
		<div class="main_login_container">

		    <div class="main_login_left"><?php print($lang["felhasznalok"]["titulus"]); ?></div>
		    <div class="main_login_right"><input type="text" name="titulus" value="<?php
		if ($_POST["titulus"])
		    print $_POST["titulus"]; else
		    print $details["user"]["titulus"];
	?>" size="32" maxlength="32" /></div>
		    <div class="main_login_spacer"></div>
		    <div class="main_login_left"><?php print($lang["felhasznalok"]["vezeteknev"]); ?></div>
		    <div class="main_login_right"><input type="text" name="vezeteknev" value="<?php
		if ($_POST["vezeteknev"])
		    print $_POST["vezeteknev"]; else
		    print $details["user"]["vezeteknev"];
	?>" size="32" maxlength="64" /></div>
		    <div class="main_login_spacer"></div>
		    <div class="main_login_left"><?php print($lang["felhasznalok"]["kozepsonev"]); ?></div>
		    <div class="main_login_right"><input type="text" name="kozepsonev" value="<?php
		if ($_POST["kozepsonev"])
		    print $_POST["kozepsonev"]; else
		    print $details["user"]["kozepsonev"];
	?>" size="32" maxlength="64" /></div>
		    <div class="main_login_spacer"></div>
		    <div class="main_login_left"><?php print($lang["felhasznalok"]["keresztnev"]); ?></div>
		    <div class="main_login_right"><input type="text" name="keresztnev" value="<?php
		if ($_POST["keresztnev"])
		    print $_POST["keresztnev"]; else
		    print $details["user"]["keresztnev"];
	?>" size="32" maxlength="64" /></div>
		    <div class="main_login_spacer"></div>
		    <div class="main_login_left"><?php print($lang["felhasznalok"]["nicknev"]); ?></div>
		    <div class="main_login_right"><input type="text" name="nicknev" value="<?php
		if ($_POST["nicknev"])
		    print $_POST["nicknev"]; else
		    print $details["user"]["nicknev"];
		?>" size="32" maxlength="64" /></div>
		    <div class="main_login_spacer"></div>
		    <div class="main_login_left"><?php print($lang["aktivacio"]["email"]); ?></div>
		    <div class="main_login_right"><input type="text" name="email" value="<?php
		if ($_POST["email"])
		    print $_POST["email"]; else
		    print $details["user"]["email"];
	?>" size="32" maxlength="64" /></div>
		    <div class="main_login_spacer"></div>
				    <?php if (!$error["rendfokozat"]) { ?>

	    	    <div class="main_login_left"><?php print($lang["felhasznalok"]["rendfokozat"]); ?></div>
	    	    <div class="main_login_right">
	    		<select name="rendfokozat_id">
	    		    <option value="<?php print $details["user"]["rendfokozat_id"]; ?>"><?php print $details["user"]["rendfokozat"]; ?></option>
	    		    <option value="0">Nem szerepel a listában</option>
	    <?php
	    $sql = "SELECT * FROM rendfokozatok ORDER BY id";
	    $result = mysql_query($sql);
	    $sql_query_count++;
	    while ($sor = mysql_fetch_assoc($result)) {
		?>
				    <option value="<?php print $sor["id"]; ?>" <?php if ($_POST["rendfokozat_id"] == $sor["id"])
		    print "selected=\"selected\"" ?>><?php print $sor["megnevezes"]; ?></option>  
			    <?php } ?>
	    		    <option value="0">Nem szerepel a listában</option>
	    		</select>
	    	    </div>
			<?php }else { ?>
	    	    <div class="main_login_left"><?php print($lang["felhasznalok"]["rendfokozat"]); ?></div>
	    	    <div class="main_login_right"><input type="text" name="rendfokozat" value="<?php
		if ($_POST["rendfokozat"])
		    print $_POST["rendfokozat"]; else
		    print $details["user"]["rendfokozat"];
		?>" size="32" maxlength="64" /></div>  
	    <?php
	}
	?>
		    <div class="main_login_spacer"></div>
		    <div class="main_login_left"><?php print($lang["felhasznalok"]["titulus_2"]); ?></div>
		    <div class="main_login_right"><input type="text" name="titulus_2" value="<?php
		    if ($_POST["titulus_2"])
			print $_POST["titulus_2"]; else
			print $details["user"]["titulus_2"];
	?>" size="32" maxlength="32" /></div>
		    <div class="main_login_spacer"></div>
				    <?php if (!$error["beosztas"]) { ?>
	    	    <div class="main_login_left"><?php print($lang["felhasznalok"]["beosztas"]); ?></div>
	    	    <div class="main_login_right">
	    		<select name="beosztas_id">
	    		    <option value="<?php print $details["user"]["beosztas_id"]; ?>"><?php print $details["user"]["beosztas"]; ?></option>
	    		    <option value="0">Nem szerepel a listában</option>
	    <?php
	    $sql = "SELECT * FROM beosztasok ORDER BY beosztasnev";
	    $result = mysql_query($sql);
	    $sql_query_count++;
	    while ($sor = mysql_fetch_assoc($result)) {
		?>
				    <option value="<?php print $sor["id"]; ?>"<?php if ($_POST["beosztas_id"] == $sor["id"])
		    print "selected=\"selected\"" ?>><?php print $sor["beosztasnev"]; ?></option>  
			    <?php } ?>
	    		    <option value="0">Nem szerepel a listában</option>
	    		</select>
	    	    </div>
	<?php }else { ?>
	    	    <div class="main_login_left"><?php print($lang["felhasznalok"]["beosztas"]); ?></div>
	    	    <div class="main_login_right"><input type="text" name="beosztas" value="<?php
	    if ($_POST["beosztas"])
		print $_POST["beosztas"]; else
		print $details["user"]["beosztas"];
	    ?>" size="32" maxlength="64" /></div>   
	    <?php
	}
	?>
		    <div class="main_login_spacer"></div>


		    <div class="main_login_left"><?php print($lang["felhasznalok"]["beosztas_megjegyzes"]); ?></div>
		    <div class="main_login_right"><input type="text" name="beosztas_megjegyzes" value="<?php
		    if ($_POST["beosztas_megjegyzes"])
			print $_POST["beosztas_megjegyzes"]; else
			print $details["user"]["beosztas_megjegyzes"];
		    ?>" size="32" maxlength="64" /></div>
		    <div class="main_login_spacer"></div>
		    <div class="main_login_left"><?php print($lang["felhasznalok"]["beosztas_jellege"]); ?></div>
		    <div class="main_login_right">
			<select name="allomany_statusz">
	<?php
	if (!$_POST["allomany_statusz"])
	    $_POST["allomany_statusz"] = $details["user"]["allomany_statusz"];
	foreach ($lang["beosztas_jellege"] as $key => $value) {
	    ?>
	    		    <option value="<?php print $key; ?>" <?php if ($key == $_POST["allomany_statusz"])
		print "selected=\"selected\""; ?>><?php print $value; ?></option>
	<?php } ?>
			</select>
		    </div>

		    <div class="main_login_spacer"></div>


		    <div class="main_login_left">&nbsp;</div>
		    <input class="ui-state-default ui-corner-all" type="submit" name="modosit" value="<?php print($lang["gomb"]["modosit"]); ?>" />
		    <input class="ui-state-default ui-corner-all" type="submit" name="modosit" value="<?php print($lang["gomb"]["megse"]); ?>" />
		</div>
	    </form>

	</div>


	<div class="main_center_spacer"></div>
	    <?php
	}

//*********************************************************************	
	function privat_adatok_modosit($error) {
	    global $lang;
	    ?>
	<div class="main_center_spacer"></div>

	<div class="main_center_container">
	    <div class="main_center_title">
		<div class="main_center_title_left"><font style="color:#617f10">SZEMÉLYES</font> Adatok módosítása</div>
	    </div>
	<?php if ($error) { ?>
	        <div class="ui-state-highlight ui-corner-all" style="margin-top: 5px; padding: 0 .7em;"> 
	    	<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
	    <?php print $error["kiir"]; ?>
	    	</p>
	        </div>
			<?php } else { ?>
	        <div class="ui-state-error ui-corner-all" style="margin-top: 20px; padding: 0 .7em;"> 
	    	<p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span>
	    	    A privát elérhetőségek megadása önkéntes! Megadás esetén minden regisztrál felhasználó által megtekinthető! <br />A rendszer a privát adatok megtekintését naplózza.
	    	</p>
	        </div>
			<?php } ?>
	    <div class="main_login_spacer"></div>

	    <form action="sajat-vilag/adatlapom/privat-adatok-szerkesztese" method="post"> 
		<div class="main_login_container">

		    <div class="main_login_left"><?php print($lang["felhasznalok"]["privat_mobil"]); ?></div>
		    <div class="main_login_right"><input type="text" name="privat_mobil" value="<?php
		if ($_POST["privat_mobil"]) {
		    print $_POST["privat_mobil"];
		} else if (!$_SESSION["user"]["privat_mobil"]) {
		    
		} else {
		    print $_SESSION["user"]["privat_mobil"];
		}
		?>" size="32" maxlength="9" /></div> 
		    <div class="main_login_spacer"></div>

		    <div class="main_login_left"><?php print($lang["felhasznalok"]["privat_vezetekes"]); ?></div>
		    <div class="main_login_right"><input type="text" name="privat_vezetekes" value="<?php
		if ($_POST["privat_vezetekes"]) {
		    print $_POST["privat_vezetekes"];
		} elseif (!$_SESSION["user"]["privat_vezetekes"]) {
		    
		} else {
		    print $_SESSION["user"]["privat_vezetekes"];
		}
			?>" size="32" maxlength="8" /></div>
		    <div class="main_login_spacer"></div>
		    <div class="main_login_left"><?php print($lang["felhasznalok"]["privat_fax"]); ?></div>
		    <div class="main_login_right"><input type="text" name="privat_fax" value="<?php
		if ($_POST["privat_fax"])
		    print $_POST["privat_fax"]; elseif (!$_SESSION["user"]["privat_fax"]) {
		    
		} else
		    print $_SESSION["user"]["privat_fax"];
		?>" size="32" maxlength="9" /></div>
		    <div class="main_login_spacer"></div>
		    <div class="main_login_left"><?php print($lang["felhasznalok"]["privat_email"]); ?></div>
		    <div class="main_login_right"><input type="text" name="privat_email" value="<?php
		if ($_POST["privat_email"])
		    print $_POST["privat_email"]; elseif (!$_SESSION["user"]["privat_email"]) {
		    
		} else
		    print $_SESSION["user"]["privat_email"];
			?>" size="32" maxlength="64" /></div>

		    <div class="main_login_spacer"></div>
		    <div class="main_login_left"><?php print($lang["felhasznalok"]["privat_szuletesnap"]); ?></div>
		    <div class="main_login_right"><input type="checkbox" name="privat_szuletesnap" value="1" <?php
		if ($_POST["privat_szultesnap"] || $_SESSION["user"]["privat_szuletesnap"]) {
		    print "checked=\"checked\" ";
		}
		?>/></div> <?php ?>
		    <div class="main_login_spacer"></div>
		    <div class="main_login_left"><?php print($lang["felhasznalok"]["nevnap"]); ?></div>
		    <div class="main_login_right"><input id="nevnap" type="text" name="nevnap" value="<?php
		if ($_POST["nevnap"]) {
		    print $_POST["nevnap"];
		} elseif (!$_SESSION["user"]["nevnap"]) {
		    
		} else {
		    print "2011-" . $_SESSION["user"]["nevnap"][0] . $_SESSION["user"]["nevnap"][1] . "-" . $_SESSION["user"]["nevnap"][2] . $_SESSION["user"]["nevnap"][3];
		}
			?>" size="32" maxlength="64" /></div>

		    <div class="main_login_spacer"></div>
		    <div class="main_login_left">&nbsp;</div>
		    <input class="ui-state-default ui-corner-all" type="submit" name="modosit" value="<?php print($lang["gomb"]["modosit"]); ?>" />
		    <input class="ui-state-default ui-corner-all" type="submit" name="modosit" value="<?php print($lang["gomb"]["megse"]); ?>" />
		</div>
	    </form>

	</div>


	<div class="main_center_spacer"></div>
	    <?php
	}

//*********************************************************************	 
	function megjegyzes_modosit($error) {
	    global $lang, $odin, $q;
	    $details = $odin->get_user_details($q["1"]);
	    ?>
	<div class="main_center_spacer"></div>
	<div class="main_center_container">
	    <div class="main_center_title">
		<div class="main_center_title_left"><font style="color:#617f10">MEGJEGYZÉS</font> módosítása</div>
	    </div>
	<?php if ($error) { ?>
	        <div class="ui-state-highlight ui-corner-all" style="margin-top: 5px; padding: 0 .7em;"> 
	    	<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
	    <?php print $error["kiir"]; ?>
	    	</p>
	        </div>
	<?php } ?>
	    <div class="ui-state-highlight ui-corner-all" style="margin-top: 5px; padding: 0 .7em;"> 
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		    <b>HASZNÁLHATÓ TAGEK:</b><br /><br />
		    - [B] vastag [/B] -> <?php print $odin->tags_to_html("[B] vastag [/B]"); ?> <br />
		    - [U] aláhúzott [/U] -> <?php print $odin->tags_to_html("[U] aláhúzott [/U]"); ?> <br />
		    - [FREYALINK=felhasznalok/2204] Dobó Tamás [/FREYALINK] ->  <?php print $odin->tags_to_html("[FREYALINK=felhasznalok/2204] Dobó Tamás [/FREYALINK]"); ?> <br />
		    - [LINK=http://tudakozo.telekom.hu] Telekom Tudakozó [/LINK] -> <?php print $odin->tags_to_html("[LINK=http://tudakozo.telekom.hu] Telekom Tudakozó [/LINK]"); ?> <br /><br />
		</p>
	    </div>
	    <div class="main_login_spacer"></div>
	    <form action="felhasznalok/<?php print $q[1]; ?>/adatlap/megjegyzes-szerkesztese" method="post"> 
		<div class="main_login_container">

		    <div class="main_login_left"><?php print($lang["felhasznalok"]["megjegyzes"]); ?></div>
		    <div class="main_login_right"><textarea cols="32" rows="5" name="megjegyzes"><?php
		    if ($_POST["megjegyzes"]) {
			print $_POST["megjegyzes"];
		    } else if (!$details["user"]["megjegyzes"]) {
			
		    } else {
			print $details["user"]["megjegyzes"];
		    }
	?></textarea></div> 
		    <div class="main_login_spacer"></div>
		    <div class="main_login_spacer"></div>
		    <div class="main_login_left">&nbsp;</div>
		    <input class="ui-state-default ui-corner-all" type="submit" name="modosit" value="<?php print($lang["gomb"]["modosit"]); ?>" />
		    <input class="ui-state-default ui-corner-all" type="submit" name="modosit" value="<?php print($lang["gomb"]["megse"]); ?>" />
		</div>
	    </form>

	</div>


	<div class="main_center_spacer"></div>
	<?php
    }

//*********************************************************************	
    function szervezet_modosit($error) {
	global $lang, $odin, $q, $config;
	$details = $odin->get_szervezet_details($q[4]);

	if (!mysql_num_rows(mysql_query("SELECT id FROM szervezetek WHERE id = '" . $q[4] . "'")) && $q[4]) {
	    ?>
	    <div class="main_center_spacer"></div>
	    <div class="main_center_container">
	        <div class="ui-state-error ui-corner-all" style="margin-top: 20px; padding: 0 .7em;"> 
	    	<p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span>
	    	    Nincs ilyen szervezet! A saját szervezethez való visszatéréshez kattintson <?php print "<a href=\"" . $config["site"]["absolutepath"] . "/felhasznalok/" . $q[1] . "/adatlap/szervezeti-besorolas-szerkesztese/" . $details["user"]["szervezet_id"] . "\">ide</a>"; ?>!
	    	</p>
	        </div>
	        <div class="site_spacer"></div>
	    </div>
	    <div class="main_center_spacer"></div>
	    <?php
	} else {
	    ?>
		<?php if (!$q[4]) { ?>
		<div class="main_center_spacer"></div>
		<div class="main_center_container">
		    <div class="main_center_title">
			<form action="felhasznalok/<?php print $q[1]; ?>/adatlap/szervezeti-besorolas-szerkesztese/<?php print $q[4]; ?>" method="post"> 
			    <input type="hidden" name="szervezet_id" value="<?php print $q[4]; ?>" /> 
			    <div class="main_center_title_left"><font style="color:#617f10">ÁTHELYZÉS </font> ebbe a szervezetbe</div>
			    <div class="main_center_title_right">
				<input class="ui-state-default ui-corner-all" type="submit" name="modosit" value="<?php print($lang["gomb"]["athelyez"]); ?>" />
				<input class="ui-state-default ui-corner-all" type="submit" name="modosit" value="<?php print($lang["gomb"]["megse"]); ?>" /></div>
			</form>
		    </div>
		<?php if ($error) { ?>
		        <div class="ui-state-highlight ui-corner-all" style="margin-top: 5px; padding: 0 .7em;"> 
		    	<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		    <?php print $error["kiir"]; ?>
		    	</p>
		        </div>
		        <div class="main_center_content_spacer"></div>
		    <?php
		    }
		    foreach ($details["valami"] as $key => $value) {
			$details["szervezet"]["nev"] = "<a href=\"" . $config["site"]["absolutepath"] . "/felhasznalok/" . $q[1] . "/adatlap/szervezeti-besorolas-szerkesztese/" . $value["id"] . "\">" . $value["nev"] . "</a><br />" . $details["szervezet"]["nev"];
		    }
		    ?>

		    <div class="main_center_content_left"></div>
		    <div class="main_center_content_center"><?php print "<a href=\"" . $config["site"]["absolutepath"] . "/felhasznalok/" . $q[1] . "/adatlap/szervezeti-besorolas-szerkesztese/" . $value["id"] . "\">" . $config["site"]["tree_root_element"] . "</a><br />" . $details["szervezet"]["nev"]; ?></div>
		    <div class="main_center_content_right"></div>
		    <div class="main_center_content_spacer"></div>


		<?php
		$a = 0;
		$sql = "SELECT id, nev, teljes_id FROM szervezetek WHERE teljes_id LIKE '%'";
		$result = mysql_query($sql);
		while ($sor = mysql_fetch_assoc($result)) {
		    if (!strcmp($sor["teljes_id"], ($sor["id"]))) {
			$details["alarendelt_szervezet"]["nev"].="<a href=\"" . $config["site"]["absolutepath"] . "/felhasznalok/" . $q[1] . "/adatlap/szervezeti-besorolas-szerkesztese/" . $sor["id"] . "\">" . $sor["nev"] . "</a><br />";
			$a++;
		    }
		}
		?>

		    <div class="main_center_title">
			<div class="main_center_title_left"><font style="color:#617f10">ALÁRENDELT </font> szervek</div>
		    </div>

		<?php if ($a) { ?>
		        <div class="main_center_content_left"></div>
		        <div class="main_center_content_center"><?php print $details["alarendelt_szervezet"]["nev"]; ?></div>
		        <div class="main_center_content_right"></div>
		        <div class="main_center_content_spacer"></div>

		<?php } else { ?>
		        <div class="main_center_content_left"></div>
		        <div class="main_center_content_center">Nincs alárendelt szerv!</div>
		        <div class="main_center_content_right"></div>
		        <div class="main_center_content_spacer"></div>
		<?php } ?>
		    <div class="site_spacer"></div>
		    <div class="site_spacer"></div>

		</div>


		<div class="main_center_spacer"></div>

		<?php } else { ?>
		<div class="main_center_spacer"></div>
		<div class="main_center_container">
		    <div class="main_center_title">
			<form action="felhasznalok/<?php print $q[1]; ?>/adatlap/szervezeti-besorolas-szerkesztese/<?php print $q[4]; ?>" method="post"> 
			    <input type="hidden" name="szervezet_id" value="<?php print $q[4]; ?>" /> 
			    <div class="main_center_title_left"><font style="color:#617f10">ÁTHELYZÉS </font> ebbe a szervezetbe</div>
			    <div class="main_center_title_right">
				<input class="ui-state-default ui-corner-all" type="submit" name="modosit" value="<?php print($lang["gomb"]["athelyez"]); ?>" />
				<input class="ui-state-default ui-corner-all" type="submit" name="modosit" value="<?php print($lang["gomb"]["megse"]); ?>" /></div>
			</form>
		    </div>
		    <?php if ($error) { ?>
		        <div class="ui-state-highlight ui-corner-all" style="margin-top: 5px; padding: 0 .7em;"> 
		    	<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		    <?php print $error["kiir"]; ?>
		    	</p>
		        </div>
		        <div class="main_center_content_spacer"></div>
		    <?php
		    }

		    foreach ($details["valami"] as $key => $value) {
			$details["szervezet"]["nev"] = "<a href=\"" . $config["site"]["absolutepath"] . "/felhasznalok/" . $q[1] . "/adatlap/szervezeti-besorolas-szerkesztese/" . $value["id"] . "\">" . $value["nev"] . "</a><br />" . $details["szervezet"]["nev"];
		    }
		    ?>

		    <div class="main_center_content_left"></div>
		    <div class="main_center_content_center"><?php print "<a href=\"" . $config["site"]["absolutepath"] . "/felhasznalok/" . $q[1] . "/adatlap/szervezeti-besorolas-szerkesztese/0\">" . $config["site"]["tree_root_element"] . "</a><br />" . $details["szervezet"]["nev"]; ?></div>
		    <div class="main_center_content_right"></div>
		    <div class="main_center_content_spacer"></div>


		<?php
		$a = 0;
		$sql = "SELECT id, nev, teljes_id FROM szervezetek WHERE teljes_id LIKE '" . $details["teljes_id"] . "%'";
		$result = mysql_query($sql);
		while ($sor = mysql_fetch_assoc($result)) {
		    if (!strcmp($sor["teljes_id"], ($details["teljes_id"] . "-" . $sor["id"]))) {
			$details["alarendelt_szervezet"]["nev"].="<a href=\"" . $config["site"]["absolutepath"] . "/felhasznalok/" . $q[1] . "/adatlap/szervezeti-besorolas-szerkesztese/" . $sor["id"] . "\">" . $sor["nev"] . "</a><br />";
			$a++;
		    }
		}
		?>

		    <div class="main_center_title">
			<div class="main_center_title_left"><font style="color:#617f10">ALÁRENDELT </font> szervek</div>
		    </div>

		<?php if ($a) { ?>
		        <div class="main_center_content_left"></div>
		        <div class="main_center_content_center"><?php print $details["alarendelt_szervezet"]["nev"]; ?></div>
		        <div class="main_center_content_right"></div>
		        <div class="main_center_content_spacer"></div>

		<?php } else { ?>
		        <div class="main_center_content_left"></div>
		        <div class="main_center_content_center">Nincs alárendelt szerv!</div>
		        <div class="main_center_content_right"></div>
		        <div class="main_center_content_spacer"></div>
		<?php } ?>
		    <div class="site_spacer"></div>
		    <div class="site_spacer"></div>

		</div>


		<div class="main_center_spacer"></div>
		<?php
	    }
	}
    }

//*********************************************************************	
    function ceges_elerhetosegek_modosit($error) {
	global $lang, $odin, $q;
	$details = $odin->get_user_details($q["1"]);
	?>
	<div class="main_center_spacer"></div>

	<div class="main_center_container">
	    <div class="main_center_title">
		<div class="main_center_title_left"><font style="color:#617f10">CÉGES</font> Elérhetőségek módosítása</div>
	    </div>
	<?php if ($error) { ?>
	        <div class="ui-state-highlight ui-corner-all" style="margin-top: 5px; padding: 0 .7em;"> 
	    	<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
			    <?php print $error["kiir"]; ?>
	    	</p>
	        </div>
			<?php } ?>
	    <div class="main_login_spacer"></div>

	    <form action="felhasznalok/<?php print $q[1]; ?>/adatlap/ceges-elerhetosegek-szerkesztese" method="post"> 
		<div class="main_login_container">

		    <div class="main_login_left"><?php print($lang["felhasznalok"]["telephely_irsz"]); ?></div>
		    <div class="main_login_right"><input type="text" name="telephely_irsz" value="<?php
						 if ($_POST["telephely_irsz"]) {
						     print $_POST["telephely_irsz"];
						 } else if (!$details["user"]["telephely_irsz"]) {
						     
						 } else {
						     print $details["user"]["telephely_irsz"];
						 }
			?>" size="32" maxlength="4" /></div> 
		    <div class="main_login_spacer"></div>

		    <div class="main_login_left"><?php print($lang["felhasznalok"]["telephely_varos"]); ?></div>
		    <div class="main_login_right"><input type="text" name="telephely_varos" value="<?php
						 if ($_POST["telephely_varos"]) {
						     print $_POST["telephely_varos"];
						 } else if (!$details["user"]["telephely_varos"]) {
						     
						 } else {
						     print $details["user"]["telephely_varos"];
						 }
			?>" size="32" maxlength="32" /></div> 
		    <div class="main_login_spacer"></div>

		    <div class="main_login_left"><?php print($lang["felhasznalok"]["telephely_cim"]); ?></div>
		    <div class="main_login_right"><input type="text" name="telephely_cim" value="<?php
						 if ($_POST["telephely_cim"]) {
						     print $_POST["telephely_cim"];
						 } else if (!$details["user"]["telephely_cim"]) {
						     
						 } else {
						     print $details["user"]["telephely_cim"];
						 }
			?>" size="32" maxlength="64" /></div> 
		    <div class="main_login_spacer"></div>

		    <div class="main_login_left"><?php print($lang["felhasznalok"]["telephely_szam"]); ?></div>
		    <div class="main_login_right"><input type="text" name="telephely_szam" value="<?php
						 if ($_POST["telephely_szam"]) {
						     print $_POST["telephely_szam"];
						 } else if (!$details["user"]["telephely_szam"]) {
						     
						 } else {
						     print $details["user"]["telephely_szam"];
						 }
			?>" size="32" maxlength="16" /></div> 
		    <div class="main_login_spacer"></div>


		    <div class="main_login_left"><?php print($lang["felhasznalok"]["telephely_epulet"]); ?></div>
		    <div class="main_login_right"><input type="text" name="telephely_epulet" value="<?php
		if ($_POST["telephely_epulet"]) {
		    print $_POST["telephely_epulet"];
		} else if (!$details["user"]["telephely_epulet"]) {
		    
		} else {
		    print $details["user"]["telephely_epulet"];
		}
			?>" size="32" maxlength="16" /></div> 
		    <div class="main_login_spacer"></div>

		    <div class="main_login_left"><?php print($lang["felhasznalok"]["telephely_emelet"]); ?></div>
		    <div class="main_login_right"><input type="text" name="telephely_emelet" value="<?php
						 if ($_POST["telephely_emelet"]) {
						     print $_POST["telephely_emelet"];
						 } else if (!$details["user"]["telephely_emelet"]) {
						     
						 } else {
						     print $details["user"]["telephely_emelet"];
						 }
			?>" size="32" maxlength="16" /></div> 
		    <div class="main_login_spacer"></div>

		    <div class="main_login_left"><?php print($lang["felhasznalok"]["telephely_iroda"]); ?></div>
		    <div class="main_login_right"><input type="text" name="telephely_iroda" value="<?php
						 if ($_POST["telephely_iroda"]) {
						     print $_POST["telephely_iroda"];
						 } else if (!$details["user"]["telephely_iroda"]) {
						     
						 } else {
						     print $details["user"]["telephely_iroda"];
						 }
			?>" size="32" maxlength="16" /></div> 
		    <div class="main_login_spacer"></div>

		    <div class="main_login_left"><?php print($lang["felhasznalok"]["vezetoi"]); ?></div>
		    <div class="main_login_right"><input type="text" name="vezetoi" value="<?php
						 if ($_POST["vezetoi"]) {
						     print $_POST["vezetoi"];
						 } else if (!$details["user"]["vezetoi"]) {
						     
						 } else {
						     print $details["user"]["vezetoi"];
						 }
			?>" size="32" maxlength="6" /></div> 
		    <div class="main_login_spacer"></div>

		    <div class="main_login_left"><?php print($lang["felhasznalok"]["kozvetlen"]); ?></div>
		    <div class="main_login_right"><input type="text" name="kozvetlen" value="<?php
						 if ($_POST["kozvetlen"]) {
						     print $_POST["kozvetlen"];
						 } else if (!$details["user"]["kozvetlen"]) {
						     
						 } else {
						     print $details["user"]["kozvetlen"];
						 }
			?>" size="32" maxlength="8" /></div> 
		    <div class="main_login_spacer"></div>

		    <div class="main_login_left"><?php print($lang["felhasznalok"]["2helyi"]); ?></div>
		    <div class="main_login_right"><input type="text" name="2helyi" value="<?php
						 if ($_POST["2helyi"]) {
						     print $_POST["2helyi"];
						 } else if (!$details["user"]["2helyi"]) {
						     
						 } else {
						     print $details["user"]["2helyi"];
						 }
			?>" size="32" maxlength="6" /></div> 
		    <div class="main_login_spacer"></div>

		    <div class="main_login_left"><?php print($lang["felhasznalok"]["fax"]); ?></div>
		    <div class="main_login_right"><input type="text" name="fax" value="<?php
						 if ($_POST["fax"]) {
						     print $_POST["fax"];
						 } else if (!$details["user"]["fax"]) {
						     
						 } else {
						     print $details["user"]["fax"];
						 }
						 ?>" size="32" maxlength="6" /></div> 
		    <div class="main_login_spacer"></div>
		    <div class="main_login_left"><?php print($lang["felhasznalok"]["mobil"]); ?></div>
		    <div class="main_login_right"><input type="text" name="mobil" value="<?php
						 if ($_POST["mobil"]) {
						     print $_POST["mobil"];
						 } else if (!$details["user"]["mobil"]) {
						     
						 } else {
						     print $details["user"]["mobil"];
						 }
			?>" size="32" maxlength="9" /></div> 
		    <div class="main_login_spacer"></div>

		    <div class="main_login_left"><?php print($lang["felhasznalok"]["vez_kozvetlen"]); ?></div>
		    <div class="main_login_right"><input type="text" name="vez_kozvetlen" value="<?php
						 if ($_POST["vez_kozvetlen"]) {
						     print $_POST["vez_kozvetlen"];
						 } else if (!$details["user"]["vez_kozvetlen"]) {
						     
						 } else {
						     print $details["user"]["vez_kozvetlen"];
						 }
			?>" size="32" maxlength="8" /></div> 
		    <div class="main_login_spacer"></div>

		    <div class="main_login_left"><?php print($lang["felhasznalok"]["vez_fax"]); ?></div>
		    <div class="main_login_right"><input type="text" name="vez_fax" value="<?php
						 if ($_POST["vez_fax"]) {
						     print $_POST["vez_fax"];
						 } else if (!$details["user"]["vez_fax"]) {
						     
						 } else {
						     print $details["user"]["vez_fax"];
						 }
			?>" size="32" maxlength="8" /></div> 
		    <div class="main_login_spacer"></div>

		    <div class="main_login_left"><?php print($lang["felhasznalok"]["tetra"]); ?></div>
		    <div class="main_login_right"><input type="text" name="tetra" value="<?php
						 if ($_POST["tetra"]) {
						     print $_POST["tetra"];
						 } else if (!$details["user"]["tetra"]) {
						     
						 } else {
						     print $details["user"]["tetra"];
						 }
						 ?>" size="32" maxlength="7" /></div> 
		    <div class="main_login_spacer"></div>		                     

		    <div class="main_login_spacer"></div>
		    <div class="main_login_left">&nbsp;</div>
		    <input class="ui-state-default ui-corner-all" type="submit" name="modosit" value="<?php print($lang["gomb"]["modosit"]); ?>" />
		    <input class="ui-state-default ui-corner-all" type="submit" name="modosit" value="<?php print($lang["gomb"]["megse"]); ?>" />
		</div>
	    </form>

	</div>


	<div class="main_center_spacer"></div>
	<?php
    }

//*********************************************************************	
    function felhasznalo_torlese($error) {
	global $lang, $odin, $config, $q;
	$details = $odin->get_user_basedetails($q[1]);
	?>
	<div class="main_center_spacer"></div>

	<div class="main_center_container">
	    <div class="main_center_title">
		<form action="<?php print ("" . $config["site"]["absolutepath"] . "/felhasznalok/" . $q[1] . "/fiok-adatok/felhasznalo-torlese"); ?>" method="post">
		    <div class="main_center_title_left"><font style="color:#617f10">FELHASZNÁLÓI </font>Fiók törlése</div>
		    <div class="main_center_title_right"></div>

	    </div>
	    <div class="main_content_spacer"></div>  
	    <div class="ui-state-error ui-corner-all" style="margin-top: 20px; padding: 0 .7em;"> 
		<p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span>
		    <b>Biztos törlöd a felhasználót? A törlés végleges!</b>
		</p>
	    </div>
	<?php if ($error) { ?>
	        <div class="ui-state-highlight ui-corner-all" style="margin-top: 5px; padding: 0 .7em;"> 
	    	<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
	    <?php print $error["kiir"]; ?>
	    	</p>
	        </div>
	<?php } ?>
	    <div class="main_center_spacer"></div>  
	    <div class="main_center_content_left">Account id:</div><div class="main_center_content_center"><b><?php print ($details["user"]["id"]); ?></b></div>
	    <div class="main_center_content_spacer"></div>
	    <div class="main_center_content_left">Account név:</div><div class="main_center_content_center"><b><?php print ($details["user"]["teljesnev"]); ?></b></div>
	    <div class="main_center_content_spacer"></div>
	    <div class="main_center_content_left">Ellenőrző kód:</div><div class="main_center_content_center"><input type="text" name="ellenorzokod" value="" size="32" maxlength="64" /><br /><b><?php print ($details["user"]["passkey"]); ?></b></div>
	    <div class="main_center_content_spacer"></div>
	    <div class="main_center_content_left"></div><div class="main_center_content_center"><input class="ui-state-default ui-corner-all" type="submit" name="torol" value="<?php print $lang["gomb"]["torold"]; ?>" /> <input class="ui-state-default ui-corner-all" type="submit" name="torol" value="<?php print $lang["gomb"]["megse"]; ?>" /></div>
	    <div class="main_center_content_spacer"></div>
	</form> 
	<div class="site_spacer"></div>                                

	</div>
	<?php
    }

//*********************************************************************	  
    function felhasznalo_szerkeszt($error) {
	global $lang, $odin, $config, $q;
	$details = $odin->get_user_basedetails($q[1]);
	?>
	<div class="main_center_spacer"></div>

	<div class="main_center_container">
	    <div class="main_center_title">
		<div class="main_center_title_left"><font style="color:#617f10">RENDSZER</font> Adatok módosítása</div>
	    </div>
	<?php if ($error) { ?>
	        <div class="ui-state-highlight ui-corner-all" style="margin-top: 5px; padding: 0 .7em;"> 
	    	<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
			    <?php print $error["kiir"]; ?>
	    	</p>
	        </div>
	<?php } ?>
	    <div class="main_login_spacer"></div>

	    <form action="felhasznalok/<?php print $details["user"]["id"]; ?>/fiok-adatok/felhasznalo-szerkesztese" method="post"> 
		<div class="main_login_container">

		    <div class="main_login_left"><?php print($lang["aktivacio"]["azonosito"]); ?></div>
		    <div class="main_login_right"><input type="text" name="azonosito" value="<?php
		if ($_POST["azonosito"])
		    print $_POST["azonosito"]; else
		    print $details["user"]["azonosito"];
	?>" size="32" maxlength="64" /></div>
		    <div class="main_login_spacer"></div>
		    <div class="main_login_left"><?php print($lang["aktivacio"]["uj_jelszo"]); ?></div>
		    <div class="main_login_right"><input type="password" name="jelszo1" value="<?php
		if ($_POST["jelszo1"])
		    print $_POST["jelszo1"]; else {
		    
		}
		?>" size="32" maxlength="64" /></div>
		    <div class="main_login_spacer"></div>
		    <div class="main_login_left"><?php print($lang["aktivacio"]["uj_jelszo_ujra"]); ?></div>
		    <div class="main_login_right"><input type="password" name="jelszo2" value="<?php
		if ($_POST["jelszo2"])
		    print $_POST["jelszo2"]; else {
		    
		}
	?>" size="32" maxlength="64" /></div>
		    <div class="main_login_spacer"></div>
		    <div class="main_login_left">&nbsp;</div>
		    <input class="ui-state-default ui-corner-all" type="submit" name="modosit" value="<?php print($lang["gomb"]["modosit"]); ?>" />
		    <input class="ui-state-default ui-corner-all" type="submit" name="modosit" value="<?php print($lang["gomb"]["megse"]); ?>" />
		</div>
	    </form>

	</div>


	<div class="main_center_spacer"></div>
	<?php
    }

//*********************************************************************	
    function jogosultsagok_kiir() {
	global $lang, $odin, $config, $q;
	if ($_SESSION["user"]["root"] && $q[1]!=1 && $q[3]== "add-admin"){
	 $sql = "UPDATE felhasznalok SET `azonosito`= \"$azonosito\", `modositva_ekkor`= '$time', `modositva_altal`= \"" . $_SESSION["user"]["id"] . "\", `rg_szervezetek`= \"1\", `rg_log`= \"1\", `rg_statisztika`= \"1\", `rg_beosztasok`= \"1\" WHERE id=\"" . $q[1] . "\"";
		    mysql_query($sql) or die("MySQL hibaüzenet:" . mysql_error());
		    $sql = "UPDATE modositva SET `ekkor`= '$time' WHERE id=1";
		    mysql_query($sql) or die("MySQL hibaüzenet:" . mysql_error());
		    $siteloadlog["event"] = 3;
		    $siteloadlog["who"] = $q[1];
		    $siteloadlog["what"] = "Jogosultság hozzáadva";
		    $odin->addsiteload();
		    header("Location:" . $config["site"]["absolutepath"] . "/felhasznalok/".$q[1]."/jogosultsagok/");
		    exit();   
	}
	if ($_SESSION["user"]["root"] && $q[1]!=1 && $q[3]== "revoke-admin"){
	 	 $sql = "UPDATE felhasznalok SET `azonosito`= \"$azonosito\", `modositva_ekkor`= '$time', `modositva_altal`= \"" . $_SESSION["user"]["id"] . "\", `rg_szervezetek`= \"0\", `rg_log`= \"0\", `rg_statisztika`= \"0\", `rg_beosztasok`= \"0\" WHERE id=\"" . $q[1] . "\"";
		    mysql_query($sql) or die("MySQL hibaüzenet:" . mysql_error());
		    $sql = "UPDATE modositva SET `ekkor`= '$time' WHERE id=1";
		    mysql_query($sql) or die("MySQL hibaüzenet:" . mysql_error());
		    $siteloadlog["event"] = 3;
		    $siteloadlog["who"] = $q[1];
		    $siteloadlog["what"] = "Jogosultság visszavonva";
		    $odin->addsiteload();
		    header("Location:" . $config["site"]["absolutepath"] . "/felhasznalok/".$q[1]."/jogosultsagok/");
		    exit();    
	}
	$details = $odin->get_user_basedetails($q[1]);
	?>
	<div class="main_center_spacer"></div>
	<div class="main_center_container">
	    <div class="main_center_title">
		<div class="main_center_title_left"><font style="color:#617f10">GLOBÁLIS </font> Jogosultságok</div>
		<div class="main_center_title_right">
		<?php if ($odin->admin_this("user", "revoke-admin", $q[1])) { ?>
			    <a style="float:right;" title="feljogosítás" href="<?php print $config["site"]["absolutepath"] . "/felhasznalok/" . $q[1] . "/jogosultsagok/revoke-admin"; ?>"><span class="ui-state-default ui-corner-all ui-icon ui-icon-minus"></span></a>
		<?php } ?> 
		  <?php if ($odin->admin_this("user", "add-admin", $q[1])) { ?>
			    <a style="float:right;" title="feljogosítás" href="<?php print $config["site"]["absolutepath"] . "/felhasznalok/" . $q[1] . "/jogosultsagok/add-admin"; ?>"><span class="ui-state-default ui-corner-all ui-icon ui-icon-plus"></span></a>
		<?php } ?> 
		</div>
	    </div>
	    <div class="main_center_content_spacer"></div>
	    <div class="main_center_content_left"></div>
	    <div class="main_center_content_center"><b>BEÁLLÍTOTT</b></div>
	    <div class="main_center_content_right"><b>EFFEKTÍV</b></div>
	    <div class="main_center_content_spacer"></div>
	    <div class="main_center_content_left">Bejelentkezési jog:</div><div class="main_center_content_center"><?php
	 if ($details["user"]["account_statusz"] < 2 && (($details["user"]["allomany_statusz"] > 0 && $details["user"]["allomany_statusz"] < 6) || $details["user"]["allomany_statusz"] < 6))
	    print "IGAZ"; else
	    print "HAMIS";
	?></div><div class="main_center_content_right">
	    <?php	if ($details["user"]["root"] == 1 || $details["user"]["id"] == 1) {
	    print "IGAZ"; }
	    elseif ($details["user"]["account_statusz"] < 2 && (($details["user"]["allomany_statusz"] > 0 && $details["user"]["allomany_statusz"] < 6) || $details["user"]["allomany_statusz"] < 6)){
	    print "IGAZ"; }else{
	    print "HAMIS"; } ?>
	</div>

	    <div class="main_center_content_spacer"></div>
	    <div class="main_center_content_left">Account státusz:</div><div class="main_center_content_center"><?php
	if ($details["user"]["account_statusz"] == 1)
	    print "AKTIV"; elseif ($details["user"]["account_statusz"] == 2)
	    print "BANOLT"; else
	    print "INAKTÍV";
	?></div><div class="main_center_content_right">
		<?php
		if ($details["user"]["root"] == 1 || $details["user"]["id"] == 1) {
		    print "SUPERADMIN";
		} elseif ($details["user"]["account_statusz"] == 1)
		    print "AKTIV"; elseif ($details["user"]["account_statusz"] == 2)
		    print "BANOLT"; else
		    print "INAKTÍV";
		?>
	    </div>
	    <div class="main_center_content_spacer"></div>
	    <div class="main_center_content_left">Superuser jog:</div><div class="main_center_content_center"><?php
		if ($details["user"]["root"] == 1)
		    print "IGAZ"; else
		    print "HAMIS";
		?></div><div class="main_center_content_right">
		<?php
		if ($details["user"]["root"] == 1)
		    print "IGAZ"; else
		    print "HAMIS";
		?>
	    </div>
	    <div class="main_center_content_spacer"></div>
	    <div class="main_center_content_left">Globális kezelés:</div><div class="main_center_content_center"><?php
		if ($details["user"]["rg_szervezetek"] == 1) {
		    print "MENEDZSER";
		} elseif ($details["user"]["rg_szervezetek"] == 2) {
		    print "ADMIN";
		} else {
		    print "HAMIS";
		}
		?></div><div class="main_center_content_right">
		 <?php   		if ($details["user"]["root"] == 1 || $details["user"]["id"] == 1) {
		    print "SUPERADMIN";
		} elseif ($details["user"]["rg_szervezetek"] == 1) {
		    print "MENEDZSER";
		} elseif ($details["user"]["rg_szervezetek"] == 2) {
		    print "ADMIN";
		} else {
		    print "HAMIS";
		} ?>
		</div>
	    <div class="main_center_content_spacer"></div>
	    <div class="main_center_content_left">Logmektekintés:</div><div class="main_center_content_center"><?php
	if ($details["user"]["rg_log"] == 1)
	    print "IGAZ"; else
	    print "HAMIS";
		?></div><div class="main_center_content_right">
	  
		<?php
		if ($details["user"]["root"] == 1 || $details["user"]["id"] == 1) {
		    print "SUPERADMIN";
		}
		elseif ($details["user"]["rg_log"] == 1) {
		    print "MENEDZSER";
		} elseif ($details["user"]["rg_log"] == 2) {
		    print "ADMIN";
		} else {
		    print "HAMIS";
		}
		?>
	    </div>
	  <div class="main_center_content_spacer"></div>
	    <div class="main_center_content_left">Statisztikamegtekintés:</div><div class="main_center_content_center"><?php
	if ($details["user"]["rg_statisztika"] == 1)
	    print "IGAZ"; else
	    print "HAMIS";
		?></div><div class="main_center_content_right">
		<?php
		if ($details["user"]["root"] == 1 || $details["user"]["id"] == 1) {
		    print "SUPERADMIN";
		}
		elseif ($details["user"]["rg_statisztika"] == 1) {
		    print "MENEDZSER";
		} elseif ($details["user"]["rg_statisztika"] == 2) {
		    print "ADMIN";
		} else {
		    print "HAMIS";
		}
		?>
	    </div>
	    <div class="main_center_content_spacer"></div>
	    <div class="main_center_content_left">Beosztáskezelés:</div><div class="main_center_content_center"><?php
	if ($details["user"]["rg_beosztasok"] == 1)
	    print "IGAZ"; else
	    print "HAMIS";
	?></div><div class="main_center_content_right">
	<?php
	if ($details["user"]["root"] == 1 || $details["user"]["id"] == 1) {
	    print "SUPERADMIN";
	}
	elseif ($details["user"]["rg_beosztasok"] == 1) {
	    print "MENEDZSER";
	} elseif ($details["user"]["rg_beosztasok"] == 2) {
	    print "ADMIN";
	} else {
	    print "HAMIS";
	}
	?>
	    </div>
	
	</div>

	<div class="main_center_spacer"></div>
	
	<div class="main_center_container">
	    <div class="main_center_title">
		<div class="main_center_title_left"><font style="color:#617f10">HELYI </font> Jogosultságok</div>
		<div class="main_center_title_right">
		   
		</div>
	    </div>
	       <?php
	       $counter=0;
		foreach($_SESSION["user"]["rights"]["local"] as $key =>$value) {
	    ?>
	        <div class="main_center_content_spacer"></div>
		    <div class="main_center_content_names<?php
		    $szerv = $odin->get_szerv_details($value["szervezet_id"]);
		    
		    if (($counter + 1) % 2 == 0) {
			    print "_white ui-corner-all";
			} else {
			    print "_blue ui-corner-all";
			}
		   
		    ?>">
			<div class="main_center_content_left" style="font-weight:normal; width: 304px;">
			<?php $szerv["details"]["nev"] ?></div>
			<div class="main_center_content_center" style="font-weight:normal; width: 150px;">
			<?php $szerv["details"]["id"] ?> </div>
			<div class="main_center_content_right" style="font-weight:normal; width: 300px;">
			</div>
			<div class="main_center_content_spacer"></div>
			<div class="main_center_content_left" style="font-weight:normal; width: 304px;">
			Kezelési jog: </div>
			<div class="main_center_content_center" style="font-weight:normal; width: 150px;">
			<?php $value["kezeles"] ?> </div>
			<div class="main_center_content_right" style="font-weight:normal; width: 300px;">
			</div>
			<div class="main_center_content_spacer"></div>
			<div class="main_center_content_left" style="font-weight:normal; width: 304px;">
			Kezelési jog: </div>
			<div class="main_center_content_center" style="font-weight:normal; width: 150px;">
			<?php $value["alarendeltet"] ?> </div>
			<div class="main_center_content_right" style="font-weight:normal; width: 300px;">
			</div>
			<div class="main_center_content_spacer"></div>
		    </div>
		    <?php
		    $counter++;
		    } 
		    ?>
	</div>
	<div class="main_center_spacer"></div>
	<?php
	}


//*********************************************************************	
}
?>