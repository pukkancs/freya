<?php

class illetekesseg_content {

//*********************************************************************	
    function nyito() {
	global $lang, $config, $q;
	?>
	<div class="main_center_spacer"></div>

	<div class="main_center_container">
	    <div class="main_center_title">
		<div class="main_center_title_left"><font style="color:#617f10">ILLETÉKESSÉG</font> Kereső</div>
		<div class="main_center_title_right"></div>
	    </div>
	    <form action="<?php print ("" . $config["site"]["absolutepath"] . "/illetekesseg/kereses/"); ?>" method="get">
		<div class="main_login_container">
		    <input type="hidden" name="illetekesseg" value="illetekesseg" />
		    <?php
		    if (isset($_POST["keresd"])) {
			if (strlen($_POST["iranyitoszam"]) < 2 && strlen($_POST["telepules"]) < 2) {
			    ?>
			    <div class="ui-state-highlight ui-corner-all" style="margin-top: 5px; padding: 0 .7em;"> 
				<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
				    <?php print($lang["illetekesseg"]["min2kar"]); ?></p>
			    </div>
			    <div class="main_login_spacer"></div>
			    <?php
			}
                        else if ($_POST["iranyitoszam"] && !is_numeric($_POST["iranyitoszam"])) {
                            ?>
                            <div class="ui-state-highlight ui-corner-all" style="margin-top: 5px; padding: 0 .7em;"> 
                                <p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
                <?php print($lang["illetekesseg"]["hibas_iranyitoszam"]); ?></p>
                            </div>
                            <div class="main_login_spacer"></div>
                <?php
            }
		    }
		    ?>
		    <div class="main_login_left"><?php print($lang["illetekesseg"]["iranyitoszam"]); ?></div>
		    <div class="main_login_right"><input type="text" name="iranyitoszam" value="<?php print $_POST["iranyitoszam"]; ?>" size="32" maxlength="4" /></div>
		    <div class="main_login_spacer"></div>
		    <div class="main_login_left"><?php print($lang["illetekesseg"]["telepules"]); ?></div>
		    <div class="main_login_right"><input type="text" name="telepules" value="<?php print $_POST["telepules"]; ?>" size="32" maxlength="64" /></div>
		    <div class="main_login_spacer"></div>
		    <div class="main_login_left"><?php print($lang["illetekesseg"]["szokezdet"]); ?></div>
		    <div class="main_login_right"><input type="checkbox" name="szokezdet"<?php if ($_POST["szokezdet"])
		print (" checked=\"checked\""); ?> value="1" /></div>
		    <div class="main_login_spacer"></div>
		    <div class="main_login_left"><?php print($lang["illetekesseg"]["limit"]); ?></div>
		    <div class="main_login_right">
			<select name="limit">
			    <?php
			    if ($_POST["limit"] && $_POST["limit"]!=10 && $_POST["limit"]!=25 && $_POST["limit"]!=50 && $_POST["limit"]!=100){
			    ?>
			    <option value="1-<?php print ($_POST["limit"]); ?>"><?php print ($_POST["limit"]); ?></option>
			    <?php
			    }
			    ?>
			    <option value="1-10" <?php if ($_POST["limit"]==10) print "selected=\"selected\"" ?>>10</option>
			    <option value="1-25" <?php if ($_POST["limit"]==25) print "selected=\"selected\"" ?>>25</option>
			    <option value="1-50" <?php if ($_POST["limit"]==50) print "selected=\"selected\"" ?>>50</option>
			    <option value="1-100" <?php if ($_POST["limit"]==100) print "selected=\"selected\"" ?>>100</option>
			</select>
		    </div>
		    <div class="main_login_spacer"></div>
		    <div class="main_login_left">&nbsp;</div>
		    <div class="main_login_right"><input class="ui-state-default ui-corner-all" type="submit" name="keresd" value="<?php print($lang["gomb"]["keresd"]); ?>" />
		   
		    </div>
		     <div class="main_center_content_spacer"></div>


		</div>
	    </form> 
	    <div class="main_center_spacer"></div>
	</div>
	<?php
    }

//*********************************************************************	
    function listazas() {
	global $lang, $config, $q, $odin;
	$i = 0;
	$limit = $_POST["limit"];
	$counter = 0;
	$from = ($_POST["from"]);
	$to = $_POST["to"];


	
	    if ($_POST["telepules"] && $_POST["iranyitoszam"]) {
		$sql = "SELECT * FROM varosok WHERE (`telepulesnev` LIKE '";
		if ($_POST["szokezdet"] != 1)
		    $sql.="%";
		$sql.="" . $_POST["telepules"] . "%'";
		$sql.=" OR `telepulesresz` LIKE '";
		if ($_POST["szokezdet"] != 1)
		    $sql.="%";
		$sql.="" . $_POST["telepules"] . "%')";
		$sql.=" AND `iranyitoszam` LIKE '";
		if ($_POST["szokezdet"] != 1)
		    $sql.="%";
		$sql.="" . $_POST["iranyitoszam"] . "%'";
	    }
	    if ($_POST["telepules"] && !$_POST["iranyitoszam"]) {
		$sql = "SELECT * FROM varosok WHERE `telepulesnev` LIKE '";
		if ($_POST["szokezdet"] != 1)
		    $sql.="%";
		$sql.="" . $_POST["telepules"] . "%'";
		$sql.=" OR `telepulesresz` LIKE '";
		if ($_POST["szokezdet"] != 1)
		    $sql.="%";
		$sql.="" . $_POST["telepules"] . "%'";
	    }
	    if (!$_POST["telepules"] && $_POST["iranyitoszam"]) {
		$sql = "SELECT * FROM varosok WHERE `iranyitoszam` LIKE '";
		if ($_POST["szokezdet"] != 1)
		    $sql.="%";
		$sql.="" . $_POST["iranyitoszam"] . "%'";
	    }
            $sql.=" ORDER BY telepulesnev, telepulesresz, iranyitoszam";


	    $result = mysql_query($sql);
	    $sql_query_count++;
	    while ($sor = mysql_fetch_assoc($result)) {
		$talalat[$i]["szervezet"]=$odin->get_szervezet_details($sor["szervezet_id"]);
		$talalat[$i]["iranyitoszam"] = $sor["iranyitoszam"];
		$talalat[$i]["telepulesnev"] = $sor["telepulesnev"];
		$talalat[$i]["telepulesresz"] = $sor["telepulesresz"];
		$talalat[$i]["szervezet_id"] = $sor["szervezet_id"];
		$result2 = mysql_query("SELECT * FROM szervezetek WHERE `id`='" . $sor["szervezet_id"] . "'");
		$sql_query_count++;
		while ($sor2 = mysql_fetch_assoc($result2)) {
		    $talalat[$i]["szervezet_nev"] = $sor2["nev"];
		    $talalat[$i]["szervezet_telephely"] = $sor2["telephely_irsz"] . " " . $sor2["telephely_varos"] . ", " . $sor2["telephely_cim"] . " " . $sor2["telephely_szam"];
		    $talalat[$i]["szervezet_levelezesi"] = $sor2["levelezesi_irsz"] . " " . $sor2["levelezesi_varos"] . ", " . $sor2["levelezesi_cim"];
		    if (!str_replace(",","",$talalat[$i]["telephely"])) {
			$talalat[$i]["telephely"] = "NA / NT";
		    }
		    if (!!str_replace(",", "",$talalat[$i]["levelezes"])) {
			$talalat[$i]["levelezesi"] = "NA / NT";
		    }
		    $talalat[$i]["szervezet_email"] = $sor2["email"];
		    $talalat[$i]["szervezet_fax"] = $sor2["fax"];
		    if (!$talalat[$i]["szervezet_email"]) {
			$talalat[$i]["szervezet_email"] = "NA / NT";
		    }
		    
		}

		$i++;
	    }

	$pagecount = (int) ($i / $limit);
	if ($i % $limit != 0)
	    $pagecount++;
	if ($to>$i) $to=$i;
	?>
	<div class="main_center_spacer"></div>
	<div class="main_center_container">
	    <div class="main_center_title">
		<div class="main_center_title_left"><font style="color:#617f10">TALÁLATI</font> Lista</div>
		<div class="main_center_title_right"><?php if ($i) print ("Találatok:".$from." - ".$to." / ".$i.""); ?></div>
	    </div><?php
	while ($counter + ($from-1) < $i && $counter < $limit) {
	    ?>
	        <div class="main_center_content_spacer"></div>
	        <div class="main_center_content_names<?php if (($counter + 1) % 2 == 0)
		print "_white ui-corner-all"; else
		print "_blue ui-corner-all"; ?>">
		<div class="main_center_content_left" style="font-weight:normal; width:304px;"><a href="<?php print $config["site"]["absolutepath"]."/szervezetek/".$talalat[$counter + ($from-1)]["szervezet_id"]; ?>"><?php print ($talalat[$counter + ($from-1)]["szervezet_nev"] . "</a><br /><font style=\"font-size:9px;\">" . $talalat[$counter + ($from-1)]["szervezet_telephely"] . "<br />" . $talalat[$counter + ($from-1)]["szervezet_levelezesi"]."</font>"); ?></div>
	    	<div class="main_center_content_center" style="font-weight:normal; width: 150px;"><?php print ($talalat[$counter + ($from-1)]["iranyitoszam"] . " " . $talalat[$counter + ($from-1)]["telepulesnev"]);
	    if ($talalat[$counter + ($from-1)]["telepulesresz"] != "")
		print (", " . $talalat[$counter + ($from-1)]["telepulesresz"]); ?></div>
                <div class="main_center_content_right" style="font-weight:normal; width: 304px;">
	    <?php 
	    print $talalat[$counter + ($from - 1)]["szervezet"]["rp"]; 
	    ?>
		</div>
	    	<div class="main_center_content_spacer"></div>
	        </div>
	    <?php
	    $counter++;
	}
	if ($pagecount>1)
	{?>
	     <div class="main_center_content_spacer"></div>
	        <div class="main_center_content_names<?php if (($counter + 1) % 2 == 0)
		print "_white ui-corner-all"; else
		print "_blue ui-corner-all"; ?>">
		<form style=" min-height: 22px; padding-left:5px; padding-right:5px;text-align: center;" action="<?php print $config["site"]["absolutepath"] . "/illetekesseg/kereses/"; ?>" method="get">
   		    <input type="hidden" name="iranyitoszam" value="<?php print $_POST["iranyitoszam"]; ?>" />
		    <input type="hidden" name="telepules" value="<?php print $_POST["telepules"]; ?>" />
		    <input type="hidden" name="szokezdet" value="<?php print $_POST["szokezdet"]; ?>" />
		    Találatok:
		    <select name="limit">
			 <option value="<?php print "1 - ".$limit ?>"><?php print "1 - ".$limit." / $i" ?></option>
			 <?php $counter=1; 
			 while ($counter!=$pagecount) { ?>
			<option value="<?php print (($limit*$counter)+1)." - ".($limit*($counter+1)) ?>" <?php if (((($limit*$counter)+1)."-".($limit*($counter+1))==($_POST["from"]."-".$_POST["to"]))) { print "selected=\"selected\""; } ?>><?php print "".(($limit*$counter)+1)." - "; if (($limit*($counter+1)<$i)) print "".($limit*($counter+1))." / $i"; else print ("$i / $i"); ?></option>
		    <?php $counter++; }
		    ?>
		    </select>
		     <input class="ui-state-default ui-corner-all" type="submit" name="mehet" value="Mehet" />
		</form> 
	    	<div class="main_center_content_spacer"></div>
		</div>
	<?php
			
		}

	if ($i == 0 || $from>$i) {
	    ?>
	        <div class="ui-state-highlight ui-corner-all" style="margin-top: 5px; padding: 0 .7em;"> 
	    	<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
	    <?php print($lang["illetekesseg"]["nincstalalat"]); ?></p></div>
	    <?php
	}
	?>


	</div>
	<?php
    }

//*********************************************************************
}
?>
