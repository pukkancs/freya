<?php

class telefonkonyv_content {

//*********************************************************************	
    function nyito() {
	global $lang, $config, $q;
	?>
	<div class="main_center_spacer"></div>

	<div class="main_center_container">

	    <div class="main_center_title">
		<div class="main_center_title_left"><font style="color:#617f10">TELEFONKÖNYV</font> Kereső</div>
		<div class="main_center_title_right"></div>
	    </div>
	    <form action="<?php print ("" . $config["site"]["absolutepath"] . "/telefonkonyv/kereses/"); ?>" method="get">
		<div class="main_login_container">
		    <?php
		    if (isset($_POST["keresd"])) {
			if (strlen($_POST["nev"]) < 2 && strlen($_POST["beosztas"]) < 2 && strlen($_POST["szervezet"]) < 2 && strlen($_POST["kozvetlen"]) < 2) {
			    ?>
			    <div class="ui-state-highlight ui-corner-all" style="margin-top: 5px; padding: 0 .7em;"> 
				<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
				    <?php print($lang["telefonkonyv"]["min2kar"]); ?></p>
			    </div>
			    <div class="main_login_spacer"></div>
			    <?php
			}
		    }
		    ?>
		    <div class="main_login_left"><?php print($lang["telefonkonyv"]["itt_keresd"]); ?></div>
		    <div class="main_login_right">
			<select name="ittkeresd">
			    <option value="">Minden szervezetben</option>
			    <?php
			    $check = mysql_query("SELECT nev, teljes_id FROM szervezetek WHERE szervezet_tipus = '5' OR id<8 ORDER BY teljes_id");
			    $sql_query_count++;
			    while ($sor = mysql_fetch_assoc($check)) {
				$countchar = substr_count($sor["teljes_id"], "-");
				if (strlen($sor["nev"]) > 30) {
				    $sor["nev"] = str_replace("Gazdasági Főigazgatóság", "GF", $sor["nev"]);
				    $sor["nev"] = str_replace("Rendőr-Főkapitányságok", "RFK-k", $sor["nev"]);
				}
				?>
	    		    <option value=<?php
		    print "\"" . $sor["teljes_id"] . "\"";
		    if ($_POST["ittkeresd"] == $sor["teljes_id"] || (!$_POST["ittkeresd"] && $_SESSION["user"]["foszerv"] == $sor["teljes_id"] ))
			print " selected=\"selected\""
				    ?> ><?php print $sor["nev"] . " és alárendelt szerveiben"; ?></option>
				    <?php } ?>
			</select>
		    </div>

		    <div class="main_login_spacer"></div>    
		    <div class="main_login_left"><?php print($lang["telefonkonyv"]["nev"]); ?></div>
		    <div class="main_login_right"><input type="text" name="nev" value="<?php print $_POST["nev"]; ?>" size="32" maxlength="64" /></div>
		    <div class="main_login_spacer"></div>
		    <div class="main_login_left"><?php print($lang["telefonkonyv"]["beosztas"]); ?></div>
		    <div class="main_login_right"><input type="text" name="beosztas" value="<?php print $_POST["beosztas"]; ?>" size="32" maxlength="64" /></div>
		    <div class="main_login_spacer"></div>
		    <div class="main_login_left"><?php print($lang["telefonkonyv"]["szervezet"]); ?></div>
		    <div class="main_login_right"><input type="text" name="szervezet" value="<?php print $_POST["szervezet"]; ?>" size="32" maxlength="64" /></div>
		    <div class="main_login_spacer"></div>
		    <div class="main_login_left"><?php print($lang["telefonkonyv"]["kozvetlen"]); ?></div>
		    <div class="main_login_right"><input type="text" name="kozvetlen" value="<?php print $_POST["kozvetlen"]; ?>" size="32" maxlength="64" /></div>
		    <div class="main_login_spacer"></div>
		    <div class="main_login_left"><?php print($lang["telefonkonyv"]["szokezdet"]); ?></div>
		    <div class="main_login_right"><input type="checkbox" name="szokezdet"<?php if ($_POST["szokezdet"])
				print (" checked=\"checked\""); ?> value="1" /></div>
		    <div class="main_login_spacer"></div>
		    <div class="main_login_left"><?php print($lang["telefonkonyv"]["fonetikus"]); ?></div>
		    <div class="main_login_right"><input type="checkbox" name="fonetikus"<?php if ($_POST["fonetikus"])
						     print (" checked=\"checked\""); ?> value="1" />(lassú)</div>
		    <div class="main_login_spacer"></div>
		    <div class="main_login_left"><?php print($lang["telefonkonyv"]["limit"]); ?></div>
		    <div class="main_login_right">
			<select name="limit">
			    <?php
			    if ($_POST["limit"] && $_POST["limit"] != 10 && $_POST["limit"] != 25 && $_POST["limit"] != 50 && $_POST["limit"] != 100) {
				?>
	    		    <option value="1-<?php print ($_POST["limit"]); ?>"><?php print ($_POST["limit"]); ?></option>
				<?php
			    }
			    ?>
			    <option value="1-10" <?php if ($_POST["limit"] == 10)
			print "selected=\"selected\"" ?>>10</option>
			    <option value="1-25" <?php if ($_POST["limit"] == 25)
			    print "selected=\"selected\"" ?>>25</option>
			    <option value="1-50" <?php if ($_POST["limit"] == 50)
			    print "selected=\"selected\"" ?>>50</option>
			    <option value="1-100" <?php if ($_POST["limit"] == 100)
			    print "selected=\"selected\"" ?>>100</option>
			</select>
		    </div>
		    <div class="main_login_spacer"></div>
		    <div class="main_login_left">&nbsp;</div>
		    <div class="main_login_right"><input class="ui-state-default ui-corner-all" type="submit" name="keresd" value="<?php print($lang["gomb"]["keresd"]); ?>" />
		    </div>


		</div>
	    </form> 
	    <div class="main_center_spacer"></div>
	</div>
	<?php
    }

//*********************************************************************	
    function listazas() {
	global $lang, $config, $q, $sql_query_count, $odin;
	$i = 0;
	$limit = $_POST["limit"];
	$counter = 0;
	$from = ($_POST["from"]);
	$to = $_POST["to"];

	if (!$_POST["fonetikus"]) {
	    if ($_POST["nev"] || $_POST["beosztas"] || $_POST["kozvetlen"]) {
		$type = 1;
		$szemelynevek = explode(" ", $_POST["nev"]);
		$szervnevek = explode(" ", $_POST["szervezet"]);
		$beonevek = explode(" ", $_POST["beosztas"]);
		$kozvszamok = explode(" ", $_POST["kozvetlen"]);

		$szamalalo = 0;
		$sql = "SELECT felhasznalok.id, szervezetek.teljes_nev as teljes_nev FROM felhasznalok LEFT JOIN (szervezetek, beosztasok) ON (felhasznalok.szervezet_id = szervezetek.id AND felhasznalok.beosztas_id=beosztasok.id) WHERE";
		foreach ($szemelynevek as $key => $value) {
		    if ($szamlalo > 0)
			($sql.=" AND");
		    $sql.= " (IFNULL(felhasznalok.vezeteknev,'') LIKE '";
		    if (!$_POST["szokezdet"])
			$sql.= "%";
		    $sql.= "$value%' OR IFNULL(felhasznalok.nicknev,'') LIKE '";
		    if (!$_POST["szokezdet"])
			$sql.= "%";
		    $sql.= "$value%' OR IFNULL(felhasznalok.kozepsonev,'') LIKE '";
		    if (!$_POST["szokezdet"])
			$sql.= "%";
		    $sql.= "$value%' OR IFNULL(felhasznalok.keresztnev,'') LIKE '";
		    if (!$_POST["szokezdet"])
			$sql.= "%";
		    $sql.= "$value%' OR IFNULL(felhasznalok.titulus,'') LIKE '";
		    if (!$_POST["szokezdet"])
			$sql.= "%";
		    $sql.= "$value%')";

		    $szamlalo++;
		}

		foreach ($szervnevek as $key => $value) {
		    $sql.=" AND";
		    $sql.=" (IFNULL(szervezetek.teljes_nev,'') LIKE '";
		    $sql.= "%$value%')";
		}
		foreach ($beonevek as $key => $value) {
		    $sql.=" AND";
		    $sql.=" (IFNULL(beosztasok.beosztasnev,'') LIKE '";
		    if (!$_POST["szokezdet"])
			$sql.= "%";
		    $sql.= "$value%'";
		    $sql.= " OR IFNULL(felhasznalok.megjegyzes,'') LIKE '";
		    if (!$_POST["szokezdet"])
			$sql.= "%";
		    $sql.= "$value%'";
		    $sql.= " OR IFNULL(felhasznalok.beosztas_megjegyzes,'') LIKE '";
		    if (!$_POST["szokezdet"])
			$sql.= "%";
		    $sql.= "$value%')";
		}
		foreach ($kozvszamok as $key => $value) {
		    $sql.=" AND";
		    $sql.=" (IFNULL(felhasznalok.kozvetlen,'') LIKE '";
		    $sql.= "%$value%' OR IFNULL(felhasznalok.vezetoi,'') LIKE '";
		    $sql.= "%$value%' OR IFNULL(felhasznalok.2helyi,'') LIKE '";
		    $sql.= "%$value%')";
		}

		$sql.=" AND felhasznalok.id>'1'";
		if ($_POST["ittkeresd"]) {
		    $sql.=" AND ( szervezetek.teljes_id LIKE '" . $_POST["ittkeresd"] . "%' OR  szervezetek.teljes_id LIKE '%" . $_POST["ittkeresd"] . "-%')";
		}
		$sql.=" ORDER BY vezeteknev,keresztnev";

		$result = mysql_query($sql) or die("MySQL hibaüzenet:" . mysql_error());
		while ($sor = mysql_fetch_assoc($result)) {
		    unset($names);
		    $names = explode("/", $sor["teljes_nev"]);
		    unset($match);
		    foreach ($names as $key => $value) {
			if ($_POST["szokezdet"]) {
			    foreach ($szervnevek as $key2 => $value2) {
				if (!strcasecmp(substr($value, 0, strlen($value2)), $value2)) {
				    $match = 1;
				    break;
				}
			    }
			} else {
			    $match = 1;
			    break;
			}
		    }
		    if ($match) {
			$talalat[$i]["id"] = $sor["id"];
			$i++;
		    }
		}
	    }

	    if (!$_POST["nev"] && !$_POST["beosztas"] && !$_POST["kozvetlen"] && $_POST["szervezet"]) {
		$type = 2;
		$szervnevek = explode(" ", $_POST["szervezet"]);
		$szamalalo = 0;
		$sql = "SELECT id, teljes_nev FROM szervezetek WHERE ";
		foreach ($szervnevek as $key => $value) {
		    if ($szamlalo > 0)
			($sql.=" AND ");
		    $sql.="`teljes_nev` LIKE '";
		    $sql.= "%$value%' ";
		    $szamlalo++;
		}
		if ($_POST["ittkeresd"]) {
		    $sql.=" AND ( teljes_id LIKE '%" . $_POST["ittkeresd"] . "%' OR  teljes_id LIKE '%" . $_POST["ittkeresd"] . "-%')";
		}
		$sql.=" AND szervezet_tipus>1 ORDER BY nev";
		$result = mysql_query($sql) or die("MySQL hibaüzenet:" . mysql_error());
		while ($sor = mysql_fetch_assoc($result)) {
		    unset($names);
		    $names = explode("/", $sor["teljes_nev"]);
		    unset($match);
		    foreach ($names as $key => $value) {
			if ($_POST["szokezdet"]) {
			    foreach ($szervnevek as $key2 => $value2) {
				if (!strcasecmp(substr($value, 0, strlen($value2)), $value2)) {
				    $match = 1;
				    break;
				}
			    }
			} else {
			    $match = 1;
			    break;
			}
		    }
		    if ($match) {
			$talalat[$i]["szervezet"]["id"] = $sor["id"];
			$i++;
		    }
		}
	    }
	} else {
	    if ($_POST["nev"] || $_POST["beosztas"] || $_POST["kozvetlen"]) {
		$type = 1;
		$szemelynevek = explode(" ", $_POST["nev"]);
		$szervnevek = explode(" ", $_POST["szervezet"]);
		$beonevek = explode(" ", $_POST["beosztas"]);
		$kozvszamok = explode(" ", $_POST["kozvetlen"]);

		$szamalalo = 0;
		$sql = "SELECT felhasznalok.*, beosztasok.beosztasnev as beosztas_nev, szervezetek.teljes_nev as teljes_nev FROM felhasznalok LEFT JOIN (szervezetek, beosztasok) ON (felhasznalok.szervezet_id = szervezetek.id AND felhasznalok.beosztas_id=beosztasok.id) WHERE 1";

		foreach ($kozvszamok as $key => $value) {
		    $sql.=" AND";
		    $sql.=" (IFNULL(felhasznalok.kozvetlen,'') LIKE '";
		    $sql.= "%$value%' OR IFNULL(felhasznalok.vezetoi,'') LIKE '";
		    $sql.= "%$value%' OR IFNULL(felhasznalok.2helyi,'') LIKE '";
		    $sql.= "%$value%')";
		}

		$sql.=" AND felhasznalok.id>'1'";
		if ($_POST["ittkeresd"]) {
		    $sql.=" AND ( szervezetek.teljes_id LIKE '%" . $_POST["ittkeresd"] . "%' OR  szervezetek.teljes_id LIKE '%" . $_POST["ittkeresd"] . "-%')";
		}
		$sql.=" ORDER BY vezeteknev,keresztnev";
		$result = mysql_query($sql) or die("MySQL hibaüzenet:" . mysql_error());
		while ($sor = mysql_fetch_assoc($result)) {
		    unset($match);
		    if (!$_POST["szokezdet"]) {
			$match = 1;
			if ($_POST["nev"] && $match) {
			    foreach ($szemelynevek as $key => $value) {
				$value = $odin->text_to_fonetic($value);
				if (!stristr($odin->text_to_fonetic($sor["titulus"]) . $odin->text_to_fonetic($sor["vezeteknev"]) . $odin->text_to_fonetic($sor["kozepsonev"]) . $odin->text_to_fonetic($sor["keresztnev"]), $value)) {
				    $match = 0;
				    break;
				}
			    }
			}
			if ($_POST["szervezet"] && $match) {
			    foreach ($szervnevek as $key => $value) {
				$value = $odin->text_to_fonetic($value);
				if (!stristr($odin->text_to_fonetic($sor["teljes_nev"]), $value)) {
				    $match = 0;
				    break;
				}
			    }
			}
			if ($_POST["beosztas"] && $match) {
			    foreach ($beonevek as $key => $value) {
				$value = $odin->text_to_fonetic($value);
				if (!stristr($odin->text_to_fonetic($sor["beosztas_nev"]), $value) && !stristr($odin->text_to_fonetic($sor["megjegyzes"]), $value) && !stristr($odin->text_to_fonetic($sor["beosztas_megjegyzes"]), $value)) {
				    $match = 0;
				    break;
				}
			    }
			}
		    } else {
			$match = 1;
			if ($_POST["nev"] && $match) {
			    foreach ($szemelynevek as $key => $value) {
				$value = $odin->text_to_fonetic($value);
				if (strcasecmp(substr($odin->text_to_fonetic($sor["titulus"]), 0, strlen($value)), $value) != 0 && strcasecmp(substr($odin->text_to_fonetic($sor["vezeteknev"]), 0, strlen($value)), $value) != 0 && strcasecmp(substr($odin->text_to_fonetic($sor["kozepsonev"]), 0, strlen($value)), $value) != 0 && strcasecmp(substr($odin->text_to_fonetic($sor["keresztnev"]), 0, strlen($value)), $value) != 0) {
				    $match = 0;
				    break;
				}
			    }
			}
			if ($_POST["szervezet"] && $match) {
			    foreach ($szervnevek as $key => $value) {
				$value = $odin->text_to_fonetic($value);
				unset($names);
				$names = explode("/", $sor["teljes_nev"]);
				unset($match2);    
				    foreach ($names as $key2 => $value2) {
					$names2 = explode(" ", $value2);
					foreach ($names2 as $key3 => $value3) {
					if (strcasecmp(substr($odin->text_to_fonetic($value3), 0, strlen($value)), $value) == 0) {
					$match2 = 1;
					break;
					}
					
					}
				}
				if (!$match2){ 
				    $match=0;
				    break;
				}
			    }
			}
			if ($_POST["beosztas"] && $match) {
			    foreach ($beonevek as $key => $value) {
				$value = $odin->text_to_fonetic($value);
				if (strcasecmp(substr($odin->text_to_fonetic($sor["beosztas_nev"]), 0, strlen($value)), $value) != 0 && strcasecmp(substr($odin->text_to_fonetic($sor["megjegyzes"]), 0, strlen($value)), $value) != 0 && strcasecmp(substr($odin->text_to_fonetic($sor["beosztas_megjegyzes"]), 0, strlen($value)), $value) != 0) {
				    $match = 0;
				    break;
				}
			    }
			}
		    }
		    if ($match) {
			$talalat[$i]["id"] = $sor["id"];
			$i++;
		    }
		}
	    }

	    if (!$_POST["nev"] && !$_POST["beosztas"] && !$_POST["kozvetlen"] && $_POST["szervezet"]) {
		$type = 2;
		$szervnevek = explode(" ", $_POST["szervezet"]);
		$szamalalo = 0;
		$sql = "SELECT id, teljes_nev FROM szervezetek WHERE ";
		foreach ($szervnevek as $key => $value) {
		    if ($szamlalo > 0)
			($sql.=" AND ");
		    $sql.="`teljes_nev` LIKE '";
		    $sql.= "%$value%' ";
		    $szamlalo++;
		}
		if ($_POST["ittkeresd"]) {
		    $sql.=" AND ( teljes_id LIKE '%" . $_POST["ittkeresd"] . "%' OR  teljes_id LIKE '%" . $_POST["ittkeresd"] . "-%')";
		}
		$sql.=" AND szervezet_tipus>1 ORDER BY nev";
		$result = mysql_query($sql) or die("MySQL hibaüzenet:" . mysql_error());
		while ($sor = mysql_fetch_assoc($result)) {
		    if (!$_POST["szokezdet"]) {
			$match = 1;
			if ($_POST["szervezet"] && $match) {
			    foreach ($szervnevek as $key => $value) {
				$value = $odin->text_to_fonetic($value);
				if (!stristr($odin->text_to_fonetic($sor["teljes_nev"]), $value)) {
				    $match = 0;
				    break;
				}
			    }
			}
		    } else {
			$match = 1;
			if ($_POST["szervezet"] && $match) {
			    foreach ($szervnevek as $key => $value) {
				$value = $odin->text_to_fonetic($value);
				unset($names);
				$names = explode("/", $sor["teljes_nev"]);
				unset($match2);    
				    foreach ($names as $key2 => $value2) {
					$names2 = explode(" ", $value2);
					foreach ($names2 as $key3 => $value3) {
					if (strcasecmp(substr($odin->text_to_fonetic($value3), 0, strlen($value)), $value) == 0) {
					$match2 = 1;
					break;
					}
					
					}
				}
				if (!$match2){ 
				    $match=0;
				    break;
				}
			    }
			}
		    }
		    if ($match) {
			$talalat[$i]["szervezet"]["id"] = $sor["id"];
			$i++;
		    }
		}
	    }
	}


	$pagecount = (int) ($i / $limit);
	if ($i % $limit != 0)
	    $pagecount++;
	if ($to > $i)
	    $to = $i;
	?>
	<div class="main_center_spacer"></div>
	<div class="main_center_container">
	    <div class="main_center_title">
		<div class="main_center_title_left"><font style="color:#617f10">TALÁLATI</font> Lista</div>
		<div class="main_center_title_right"><?php if ($i)
	    print ("Találatok:" . $from . " - " . $to . " / " . $i . ""); ?></div>
	    </div><?php
	while ($counter + ($from - 1) < $i && $counter < $limit) {
	    ?>
	        <div class="main_center_content_spacer"></div>
		<?php if ($type == 1) { ?>
		    <div class="main_center_content_names<?php
		    $talalat[$counter + ($from - 1)] = $odin->get_user_details($talalat[$counter + ($from - 1)]["id"]);
		    if ($talalat[$counter + ($from - 1)]["user"]["allomany_statusz"] != 6 && $talalat[$counter + ($from - 1)]["user"]["allomany_statusz"] != 7 && $talalat[$counter + ($from - 1)]["user"]["allomany_statusz"] != 8) {
			if (($counter + 1) % 2 == 0) {
			    print "_white";
			} else {
			    print "_blue ui-corner-all";
			}
		    } else {
			print "_gray ui-corner-all";
		    }
		    ?>">
			<div class="main_center_content_left" style="font-weight:normal; width: 304px;">
			    <?php
			    $szamlalo = 0;
			    $showid = 0;
			    foreach ($talalat[$counter + ($from - 1)]["user"]["szervezetek"] as $key => $value) {
				if ($value["szervezet_tipus"] == 5 && !$showid)
				    $showid = $value["id"];
			    }
			    $show = 0;
			    foreach ($talalat[$counter + ($from - 1)]["user"]["szervezetek"] as $key => $value) {
				if ($showid == $talalat[$counter + ($from - 1)]["user"]["szervezetek"][$szamlalo]["id"])
				    $show = 1;
				if ($show == 1 && $talalat[$counter + ($from - 1)]["user"]["szervezetek"][$szamlalo]["szervezet_tipus"] > 1) {
				    ?>
				    <a href="<?php print $config["site"]["absolutepath"] . "/szervezetek/" . $talalat[$counter + ($from - 1)]["user"]["szervezetek"][$szamlalo]["id"] . "" ?>">
					<?php print $talalat[$counter + ($from - 1)]["user"]["szervezetek"][$szamlalo]["nev"]; ?></a><br />
					<?php
				    }
				    $szamlalo++;
				} print ("<font style=\"font-size:9px;\">" . $odin->fancy_text($talalat[$counter + ($from - 1)]["user"]["telephely"]) . "<br />" . $odin->fancy_text($talalat[$counter + ($from - 1)]["user"]["levelezesi"]) . "</font>");
				?></div>
			<div class="main_center_content_center" style="font-weight:normal; width: 150px;"><?php
		print ("<b><a href=\"" . $config["site"]["absolutepath"] . "/felhasznalok/" . $talalat[$counter + ($from - 1)]["user"]["id"] . "\">" . $talalat[$counter + ($from - 1)]["user"]["teljesnev"] . "</a></b><br /><font  style=\"font-size:9px;\">" . $talalat[$counter + ($from - 1)]["user"]["rendfokozat"] . "<br />");
		if ($talalat[$counter + ($from - 1)]["user"]["titulus_2"])
		    print $talalat[$counter + ($from - 1)]["user"]["titulus_2"] . "<br />";
		print $talalat[$counter + ($from - 1)]["user"]["beosztas"];
		if ($talalat[$counter + ($from - 1)]["user"]["beosztas_megjegyzes"])
		    print "<br />(" . $talalat[$counter + ($from - 1)]["user"]["beosztas_megjegyzes"] . ")";
		print ("</font>");
		if ($talalat[$counter + ($from - 1)]["telepulesresz"] != "")
		    print (", " . $talalat[$counter + ($from - 1)]["telepulesresz"]);
				?></div>
			<div class="main_center_content_right" style="font-weight:normal; width: 300px;"><?php
		if ($talalat[$counter + ($from - 1)]["user"]["email"] != "NA / NT")
		    print "<a href=\"mailto:" . $talalat[$counter + ($from - 1)]["user"]["email"] . "\">" . $talalat[$counter + ($from - 1)]["user"]["email"] . "</a>";
		else
		    print "e-mail: NA /NT";
		print ("<br /><font style=\"font-size:9px;\">közvetlen: <a href=\"" . $config["site"]["absolutepath"] . "/telefonkonyv/kereses/kereses-helye=/nev=/beosztas=/szervezet=/kozvetlenszam=" . $talalat[$counter + ($from - 1)]["user"]["kozvetlen"] . "/szokezdet=/fonetikus=/lista=1-10" . "\">" . $odin->fancy_phone_number($talalat[$counter + ($from - 1)]["user"]["kozvetlen"]) . "</a><br />fax: " . $odin->fancy_phone_number($talalat[$counter + ($from - 1)]["user"]["fax"]) . "<br />titkárság: " . $talalat[$counter + ($from - 1)]["user"]["titkarsag_link"] . "</font>");
				?></div>
			<div class="main_center_content_spacer"></div>
		    </div>
		    <?php
		    $counter++;
		}
//SZERVEZETKERESŐ
		if ($type == 2) {
		    $talalat[$counter + ($from - 1)]["szervezet"] = $odin->get_szervezet_details($talalat[$counter + ($from - 1)]["szervezet"]["id"]);
		    ?>
		    <div class="main_center_content_names<?php
		if (($counter + 1) % 2 == 0)
		    print "_white ui-corner-all"; else
		    print "_blue ui-corner-all";
		    ?>">
			<div class="main_center_content_left" style="font-weight:normal; width: 304px;">	    <?php
		$szamlalo = 0;
		$showid = 0;
		foreach ($talalat[$counter + ($from - 1)]["szervezet"]["valami"] as $key => $value) {
		    if ($value["szervezet_tipus"] == 5 && !$showid) {
			$showid = $value["id"];
		    }
		}
		$show = 0;
		foreach ($talalat[$counter + ($from - 1)]["szervezet"]["valami"] as $key => $value) {
		    if ($showid == $talalat[$counter + ($from - 1)]["szervezet"]["valami"][$szamlalo]["id"])
			$show = 1;
		    if ($show == 1 && $talalat[$counter + ($from - 1)]["szervezet"]["valami"][$szamlalo]["szervezet_tipus"] > 1) {
			    ?>
				    <a href="<?php print $config["site"]["absolutepath"] . "/szervezetek/" . $talalat[$counter + ($from - 1)]["szervezet"]["valami"][$szamlalo]["id"] . "" ?>">
					<?php print $talalat[$counter + ($from - 1)]["szervezet"]["valami"][$szamlalo]["nev"]; ?></a><br />
					<?php
				    }
				    $szamlalo++;
				} print ("<font style=\"font-size:9px;\">" . $odin->fancy_text($talalat[$counter + ($from - 1)]["szervezet"]["telephely"]) . "<br />" . $odin->fancy_text($talalat[$counter + ($from - 1)]["szervezet"]["levelezesi"]) . "</font>");
				?></div>
			<div class="main_center_content_center" style="font-weight:normal; width: 150px;"><?php print "<a href=\"" . $config["site"]["absolutepath"] . "/szervezetek/" . $talalat[$counter + ($from - 1)]["szervezet"]["id"] . "\">" . $talalat[$counter + ($from - 1)]["szervezet"]["nev"] . "</a>"; ?></div>
			<div class="main_center_content_right" style="font-weight:normal; width: 300px;"><?php print $talalat[$counter + ($from - 1)]["szervezet"]["rp"]; ?></div>
			<div class="main_center_content_spacer"></div>
		    </div>
		    <?php
		    $counter++;
		}
	    }

	    if ($pagecount > 1) {
		?>
	        <div class="main_center_content_spacer"></div>
	        <div class="main_center_content_names<?php
	    if (($counter + 1) % 2 == 0)
		print "_white ui-corner-all"; else
		print "_blue ui-corner-all";
		?>">
	    	<form style=" min-height: 22px; padding-left:5px; padding-right:5px;text-align: center;" action="<?php print $config["site"]["absolutepath"] . "/telefonkonyv/kereses/"; ?>" method="get">
	    	    <input type="hidden" name="kereses-helye" value="<?php print $_POST["ittkeresd"]; ?>" />
	    	    <input type="hidden" name="nev" value="<?php print $_POST["nev"]; ?>" />
	    	    <input type="hidden" name="beosztas" value="<?php print $_POST["beosztas"]; ?>" />
	    	    <input type="hidden" name="szervezet" value="<?php print $_POST["szervezet"]; ?>" />
	    	    <input type="hidden" name="kozvetlen" value="<?php print $_POST["kozvetlen"]; ?>" />
	    	    <input type="hidden" name="szokezdet" value="<?php if ($_POST["szokezdet"])
		print "1"; ?>" />
	    	    Találatok:
	    	    <select name="limit">
	    		<option value="<?php print "1 - " . $limit ?>"><?php print "1 - " . $limit . " / $i" ?></option>
			    <?php $counter = 1;
			    while ($counter != $pagecount) { ?>
				<option value="<?php print (($limit * $counter) + 1) . " - " . ($limit * ($counter + 1)) ?>" <?php
		if (((($limit * $counter) + 1) . "-" . ($limit * ($counter + 1)) == ($_POST["from"] . "-" . $_POST["to"]))) {
		    print "selected=\"selected\"";
		}
				?>><?php
		print "" . (($limit * $counter) + 1) . " - ";
		if (($limit * ($counter + 1) < $i))
		    print "" . ($limit * ($counter + 1)) . " / $i"; else
		    print ("$i / $i");
				?></option>
				    <?php
				    $counter++;
				}
				?>
	    	    </select>
	    	    <input class="ui-state-default ui-corner-all" type="submit" name="mehet" value="Mehet" />
	    	</form>   
	    	<div class="main_center_content_spacer"></div>
	        </div>
		<?php
	    }
	    if ($i == 0 || $from > $i) {
		?>
	        <div class="ui-state-highlight ui-corner-all" style="margin-top: 5px; padding: 0 .7em;"> 
	    	<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
			<?php print($lang["illetekesseg"]["nincstalalat"]); ?></p></div>
			<?php
		    }
		    ?>


	</div>
	<?php
    }

//*********************************************************************
}
?>
