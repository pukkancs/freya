<div class="main_left_spacer"></div>

<?php
/* * ****************************************************** */
if (isset($_SESSION["user"]["id"]) && $q[0] == "sajat_vilag") {
    ?>
    <div class="main_left_container">
        <div class="main_left_title"><?php print $lang["side_menu"]["titles"]["sajat_vilag"]; ?></div>
        <div class="main_left_text">&minus; <a class="main_left_menu" href="sajat-vilag/adatlapom">Adatlapom</a></div>
	<div class="main_left_text">&minus; <a class="main_left_menu" href="sajat-vilag/fiok-logom">Fiók Logom</a></div>
        <div class="main_left_text">&minus; <a class="main_left_menu" href="sajat-vilag/fiok-adataim">Fiók Adataim</a></div>
        <div class="main_left_text">&minus; <a class="main_left_menu" href="sajat-vilag/jogosultsagaim">Jogosultságaim</a></div>
    </div>

    <div class="main_left_spacer"></div>
    <?php
}
/* * ****************************************************** */
if (isset($_SESSION["user"]["id"]) && $q[0] == "hirek") {
    ?>
    <div class="main_left_container">
        <div class="main_left_title"><?php print $lang["side_menu"]["titles"]["hirek"]; ?></div>
        <div class="main_left_text">&minus; <a class="main_left_menu" href="hirek/rendszer">Freya hírek</a></div>
	<div class="main_left_text">&minus; <a class="main_left_menu" href="hirek/verzionaplo">Verziónapló</a></div>
	<?php if ($_SESSION["user"]["root"]) { ?><div class="main_left_text">&minus; <a class="main_left_menu" href="hirek/emlekeztetok">Fejlesztői emlékeztetők</a></div> <?php } ?>
    </div>

    <div class="main_left_spacer"></div>
    <?php
}
/* * ****************************************************** */
if (isset($_SESSION["user"]["id"]) && ($q[0] == "telefonkonyv" || $q[0] == "illetekesseg" || $q[0] == "szervezetek")) {
    ?>
    <div class="main_left_container">
        <div class="main_left_title"><?php print $lang["side_menu"]["titles"]["telefonkonyv"]; ?></div>
        <div class="main_left_text">&minus; <a class="main_left_menu" href="telefonkonyv">Gyors keresés</a></div>
	<div class="main_left_text">&minus; <a class="main_left_menu" href="illetekesseg">Illetékesség keresés</a></div>
        <div class="main_left_text">&minus; <a class="main_left_menu" href="szervezetek">Szervezet lista</a></div>
    </div>

    <div class="main_left_spacer"></div>
    <?php
}
/* * ****************************************************** */
if ($_SESSION["user"]["id"] && $q[0] == "felhasznalok") {
    ?>
    <div class="main_left_container">
        <div class="main_left_title"><?php print $lang["side_menu"]["titles"]["felhasznalok"]; ?></div>
        <div class="main_left_text">&minus; <a class="main_left_menu" href="felhasznalok/<?php print $q[1]; ?>/adatlap">Adatlap</a></div>
        <div class="main_left_text">&minus; <a class="main_left_menu" href="felhasznalok/<?php print $q[1]; ?>/privat-adatlap">Privát adatok</a></div>
        <?php if ($_SESSION["user"]["root"] || $_SESSION["user"]["rg_szervezetek"] == 2) { ?> <div class="main_left_text">&minus; <a class="main_left_menu" href="felhasznalok/<?php print $q[1]; ?>/fiok-adatok">Fiók Adatok</a></div> <?php } ?>
        <?php if ($_SESSION["user"]["root"] || $_SESSION["user"]["rg_szervezetek"] == 2) { ?> <div class="main_left_text">&minus; <a class="main_left_menu" href="felhasznalok/<?php print $q[1]; ?>/jogosultsagok">Jogosultságok</a></div> <?php } ?>
    </div>

    <div class="main_left_spacer"></div>
    <?php
}
/* * ****************************************************** */
?>


<?php
/* * ****************************************************** */
if (isset($_SESSION["user"]["id"]) && $q[0] == "admin") {
    
    if ($_SESSION["user"]["root"] || $_SESSION["user"]["rg_log"] || $_SESSION["user"]["rg_beosztasok"])
    {
?>
    <div class="main_left_container">
        <div class="main_left_title"><?php print $lang["side_menu"]["titles"]["admin"]; ?></div>
        <?php if ($_SESSION["user"]["root"] || $_SESSION["user"]["rg_log"]) { ?><div class="main_left_text">&minus; <a class="main_left_menu" href="admin/rendszer-log">Rendszer log</a></div><?php } ?>
	<?php if ($_SESSION["user"]["root"] || $_SESSION["user"]["rg_beosztasok"]) { ?><div class="main_left_text">&minus; <a class="main_left_menu" href="admin/beosztasok-kezelese">Beosztások kezelése</a></div><?php } ?>
	<?php if ($_SESSION["user"]["root"]) { ?><div class="main_left_text">&minus; <a class="main_left_menu" href="admin/hianyzo-rendfokozatok">Hiányzó rendfokozatok</a></div><?php } ?>
    </div>
    <div class="main_left_spacer"></div>
    <?php } 
    if ($_SESSION["user"]["root"] || $_SESSION["user"]["rg_log"] || $_SESSION["user"]["rg_staisztika"])
    { ?>
    <div class="main_left_container">
        <div class="main_left_title"><?php print $lang["side_menu"]["titles"]["statisztikak"]; ?></div>
        <?php if ($_SESSION["user"]["root"] || $_SESSION["user"]["rg_statisztika"]) { ?> <div class="main_left_text">&minus; <a class="main_left_menu" href="admin/oldal-statisztika">Oldal statisztika</a></div><?php } ?>
        <?php if ($_SESSION["user"]["root"] || $_SESSION["user"]["rg_statisztika"]) { ?> <div class="main_left_text">&minus; <a class="main_left_menu" href="admin/grafikus-statisztika">Grafikus statisztika</a></div><?php } ?>
	<?php if ($_SESSION["user"]["root"] || $_SESSION["user"]["rg_statisztika"]) { ?> <div class="main_left_text">&minus; <a class="main_left_menu" href="admin/verzio-statisztika">Verzió statisztika</a></div><?php } ?>
	<?php if ($_SESSION["user"]["root"] || $_SESSION["user"]["rg_log"]) { ?><div class="main_left_text">&minus; <a class="main_left_menu" href="admin/online-felhasznalok">Online felhasználók</a></div><?php } ?>
    </div>


    <div class="main_left_spacer"></div>
    <?php } 

}
/* * ****************************************************** */
?>

<?php
/* * ****************************************************** */
if (isset($_SESSION["user"]["id"]) && $q[0] == "beosztasok") {
    global $odin;
    ?>
    <?php if ($odin->admin_this("beosztas","edit",$q[1])){ ?>
    <div class="main_left_container">
        <div class="main_left_title"><?php print $lang["side_menu"]["titles"]["beosztasok"]; ?></div>
	<div class="main_left_text">&minus; <a class="main_left_menu" href="admin/beosztasok-kezelese">Beosztások kezelése</a></div>
    </div>
   <div class="main_left_spacer"></div><?php } ?>
    <div class="main_left_container">
        <div class="main_left_title"><?php print $lang["side_menu"]["titles"]["telefonkonyv"]; ?></div>
        <div class="main_left_text">&minus; <a class="main_left_menu" href="telefonkonyv">Gyors keresés</a></div>
	<div class="main_left_text">&minus; <a class="main_left_menu" href="illetekesseg">Illetékesség keresés</a></div>
        <div class="main_left_text">&minus; <a class="main_left_menu" href="szervezetek">Szervezet lista</a></div>
    </div>


    <div class="main_left_spacer"></div>
    <?php
}
/* * ****************************************************** */
?>
<?php
include ("cache/sidetree.php");
/* * ****************************************************** */
?>
<?php

if (1) {
    ?>
    <div class="main_left_container">
        <div class="main_left_title"><?php print $lang["side_menu"]["titles"]["egyeb_telefonkonyvek"]; ?></div>
        <div class="main_left_text">&minus; <a class="main_left_menu" target="_blank" href="http://tudakozo.telekom.hu">Telekom tudakozó</a></div>
	<div class="main_left_text">&minus; <a class="main_left_menu" target="_blank" href="http://cimtar.police.hu/eGuide/servlet/eGuide">eGuide kereső</a></div>
	<div class="main_left_text">&minus; <a class="main_left_menu" target="_blank" href="http://budnl02.police.hu/tk/tk.php">ORFK-BRFK telefonkönyv</a></div>
	<div class="main_left_text">&minus; <a class="main_left_menu" target="_blank" href="http://10.72.0.10/tk/">Készenlétei Rendőrség</a></div>
	<div class="main_left_text">&minus; <a class="main_left_menu" target="_blank" href="http://10.48.0.9/tk/freya/downloads/mkf.xls">Baranya Megyei &nbsp;&nbsp;&nbsp;Katasztrófavédelem (xls)</a></div>
	
    </div>

    <div class="main_left_spacer"></div>
    <?php
}
/* * ****************************************************** */

if (!$q[0] || $q[0]=="hirek" || $q=="nyito" || !$_SESSION["user"]["id"]) {
    ?>
    <div class="main_left_container">
        <div class="main_left_title"><?php print $lang["side_menu"]["titles"]["megyei_telefonkonyvek"]; ?></div>
	<div class="main_left_text">&minus; <a class="main_left_menu" target="_blank" href="http://10.61.0.5/telefonkonyv/SMRFK201204.xls">Somogy telefonkönyv</a></div>
	<div class="main_left_text">&minus; <a class="main_left_menu" target="_blank" href="http://10.63.0.11/ptel/pteledrfer.php">Tolna telefonkönyv</a></div>
    </div>

    <div class="main_left_spacer"></div>
    <?php
}
/* * ****************************************************** */

if (!$q[0] || $q[0]=="hirek" || $q=="nyito" || !$_SESSION["user"]["id"]) {
    ?>
    <div class="main_left_container">
        <div class="main_left_title"><?php print $lang["side_menu"]["titles"]["dokumentumok"]; ?></div>
	<div class="main_left_text">&minus; <a class="main_left_menu" target="_blank" href="downloads/1_ne.xls">Nemzetközi előhívószámok</a></div>
	<div class="main_left_text">&minus; <a class="main_left_menu" target="_blank" href="downloads/2_mk.xls">Magyar körzetszámok</a></div>
	<div class="main_left_text">&minus; <a class="main_left_menu" target="_blank" href="downloads/3_bk.xls">BM körzetszámok</a></div>
	<div class="main_left_text">&minus; <a class="main_left_menu" target="_blank" href="downloads/4_bo.xls">Baranyai okospapír</a></div>
	<div class="main_left_text">&minus; <a class="main_left_menu" target="_blank" href="downloads/5_bmo.xls">Baranya MRFK összesítő</a></div>
	<div class="main_left_text">&minus; <a class="main_left_menu" target="_blank" href="downloads/6_bso.xls">Baranyai szervek összesítő</a></div>
    </div>

    <div class="main_left_spacer"></div>
    <?php
}
/* * ****************************************************** */

if ($_SESSION["user"]["root"] == 1) {
    ?>
    <div class="main_left_container">
        <div class="main_left_title"><?php print $lang["side_menu"]["titles"]["szerver_info"]; ?></div>
        <div class="main_left_text">&minus; PHP VERZIÓ: <?php print(phpversion()); ?></div>
        <div class="main_left_text">&minus; MAGIC QUOTES:  <?php print(get_magic_quotes_gpc()); ?></div>
    </div>

    <div class="main_left_spacer"></div>
    <?php
}
/*********************************************************/
if ($_SESSION["user"]["root"] == 1) {
    ?>


<div class="main_left_container">
    <div class="main_left_title"><?php print $lang["side_menu"]["titles"]["kliens_info"]; ?></div>
    <div class="main_left_text">&minus; IP:  <?php print($_SERVER["REMOTE_ADDR"]); ?></div>
    <div class="main_left_text">&minus; OS:  <?php print($ua->Platform); ?> </div>
    <div class="main_left_text">&minus; BÖNGÉSZŐ:  <?php print($ua->Browser . " " . $ua->Version); ?> </div>
    <div class="main_left_text">&minus; JAVASCRIPT: <noscript>Letiltva</noscript><script type="text/javascript">document.write('Engedélyezve');</script></div>
</div>

<div class="main_left_spacer"></div>

<?php
}
/*********************************************************/
?>
