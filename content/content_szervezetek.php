<?php

class szervezetek_content {

//*********************************************************************
    function adatlap_kiir() {
	global $lang, $odin, $q, $config;
	$details = $odin->get_szervezet_details($q["1"]);
	if (strstr($_SERVER['HTTP_REFERER'], $config["site"]["absolutepath"] . "/felhasznalok/") && strstr($_SERVER['HTTP_REFERER'], "fiok-adatok/felhasznalo-torlese")) {
	    ?>
	    <script type="text/javascript">
	        $(function(){
	    	$('#dialog').dialog({
	    	    autoOpen: true,
	    	    width: 600,
	    	    buttons: {
	    		"Ok": function() { 
	    		    $(this).dialog("close"); 
	    		} 
	    	    }
	    	});

	        }); </script>
	<div id="dialog">A felhasználó sikeresen törölve!</div><?php
	}
	if (!$details && $q[1]) {
	     
	    ?>
	    <div class="main_center_container">
	        <div class="main_center_spacer"></div>
	        <div class="ui-widget">
	    	<div class="ui-state-highlight ui-corner-all">
	    	    <p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
	    		Nincs ilyen azonosítójú szervezet!
	    	</div>
	        </div>
	    </div>
	    <div class="site_spacer"></div>
	    <?php
	} elseif ($details && $q[1]) {
	    ?>
	    <div class="main_center_spacer"></div>

	    <div class="main_center_container">

		<?php if ($details["szervezet_tipus"] != 1) { ?>

		    <div class="main_center_title">
			<div class="main_center_title_left"><font style="color:#617f10"><?php print mb_strtoupper($odin->fancy_text($details["nev"]), 'UTF-8'); ?></font> Elérhetőségei</div>
			<div class="main_center_title_right">
			    <?php if ($odin->admin_this("szervezet", "delete", $details["id"])) { ?>
		    	    <a style="float:right;" title="szervezet törlése" href="<?php print $config["site"]["absolutepath"] . "/szervezetek/" . $q[1] . "/szervezet-torlese/"; ?>"><span class="ui-state-default ui-corner-all ui-icon ui-icon-close"></span></a>
			    <?php } ?>	
			    <?php if ($odin->admin_this("szervezet", "edit", $details["id"])) { ?>
		    	    <a style="float:right;" title="szervezet szerkesztése" href="<?php print $config["site"]["absolutepath"] . "/szervezetek/" . $q[1] . "/szervezet-elerhetosegek-szerkesztese/"; ?>"><span class="ui-state-default ui-corner-all ui-icon ui-icon-pencil"></span></a>
			    <?php } ?>
			</div>
		    </div>

		    <div class="main_center_content_spacer"></div>
		    <div class="main_center_content_left">Vezető:</div>
		    <div class="main_center_content_center"><?php print $odin->fancy_text($details["vezeto_tel"]); ?></div>
		    <div class="main_center_content_right"><?php print $odin->fancy_text($details["vezeto"]); ?></div>
		    <div class="main_center_content_spacer"></div>
		    <?php if ($details["vezeto-helyettes"]) { ?>
		        <div class="main_center_content_left">Vezető-helyettes:</div>
		        <div class="main_center_content_center"><?php print $odin->fancy_text($details["vezeto-helyettes_tel"]); ?></div>
		        <div class="main_center_content_right"><?php print $odin->fancy_text($details["vezeto-helyettes"]); ?></div>
		        <div class="main_center_content_spacer"></div>
		    <?php } ?>
		    <div class="main_center_content_left">Saját titkárság:</div>
		    <div class="main_center_content_center"><?php print $odin->fancy_text($details["titkarno_tel"]); ?></div>
		    <div class="main_center_content_right"><?php print $odin->fancy_text($details["titkarno"]); ?></div>
		    <div class="main_center_content_spacer"></div>
		    <?php
		    if (!$details["titkarno"] && $details["titkarsag"]){
		    ?>
		    <div class="main_center_content_left">Felettes szerv titkársága:</div>
		    <div class="main_center_content_center"><?php print $details["titkarsag"]; ?></div>
		    <div class="main_center_content_right"><?php print $details["titkarsag_nevek"]; ?></div>
		    <div class="main_center_content_spacer"></div>
		    <?php
		    }
		    ?>
		    <div class="main_center_content_left">Fax:</div><div class="main_center_content_center"><?php print $odin->fancy_phone_number($details["fax"]); ?></div>
		    <div class="main_center_content_spacer"></div>
		    <div class="main_center_content_left">Elektronikus levelezési cím:</div><div class="main_center_content_center"><?php print $odin->fancy_text($details["email"]); ?></div>
		    <div class="main_center_content_spacer"></div>
		    <div class="main_center_content_left">Külső telefonszám:</div><div class="main_center_content_center"><?php print $odin->fancy_phone_number($details["vez_kozvetlen"]); ?></div>
		    <div class="main_center_content_spacer"></div>
		    <div class="main_center_content_left">Külső faxszám:</div><div class="main_center_content_center"><?php print $odin->fancy_phone_number($details["vez_fax"]); ?></div>
		    <div class="main_center_content_spacer"></div>
		    <div class="main_center_content_left">Cím:</div><div class="main_center_content_center"><?php print $odin->fancy_text($details["telephely"]); ?></div>
		    <div class="main_center_content_spacer"></div>
		    <div class="main_center_content_left">Levelezési cím:</div><div class="main_center_content_center"><?php print $odin->fancy_text($details["levelezesi"]); ?></div>
		    <div class="main_center_content_spacer"></div>

		    <div class="main_center_title">
			<div class="main_center_title_left"><font style="color:#617f10">ÁLTALÁNOS</font> Adatok</div>
			<div class="main_center_title_right">
			    <?php if ($odin->admin_this("szervezet", "edit", $details["id"])) { ?>
		    	    <a style="float:right;" title="szervezet általános adatainak szerkesztése" href="<?php print $config["site"]["absolutepath"] . "/szervezetek/" . $q[1] . "/szervezet-altalanos-adatainak-szerkesztese/"; ?>"><span class="ui-state-default ui-corner-all ui-icon ui-icon-pencil"></span></a>
			    <?php } ?>
			</div>
		    </div>
		    <div class="main_center_content_spacer"></div>
		    <div class="main_center_content_left">Hírközpont:</div><div class="main_center_content_center"><a href="<?php print $config["site"]["absolutepath"] . "/telefonkonyv/kereses/kereses-helye=/nev=/beosztas=/szervezet=/kozvetlenszam=" . $details["hirkozpont"] . "/szokezdet=/fonetikus=/lista=1-10"; ?>"><?php print $odin->fancy_phone_number($details["hirkozpont"]); ?></a></div>
		    <div class="main_center_content_spacer"></div>
		    <div class="main_center_content_left">Ügyelet:</div><div class="main_center_content_center"><a href="<?php print $config["site"]["absolutepath"] . "/telefonkonyv/kereses/kereses-helye=/nev=/beosztas=/szervezet=/kozvetlenszam=" . $details["ugyelet"] . "/szokezdet=/fonetikus=/lista=1-10"; ?>"><?php print $odin->fancy_phone_number($details["ugyelet"]); ?></a></div>
		    <div class="main_center_content_spacer"></div>

		<?php }
		else if ($details["szervezet_tipus"]==1){ ?>
		    	<div class="main_center_title">
			<div class="main_center_title_left"><font style="color:#617f10"><?php print mb_strtoupper($odin->fancy_text($details["nev"]), 'UTF-8'); ?></font></div>
			<div class="main_center_title_right">
			    <?php if ($odin->admin_this("szervezet", "delete", $details["id"])) { ?>
		    	    <a style="float:right;" title="szervezet törlése" href="<?php print $config["site"]["absolutepath"] . "/szervezetek/" . $q[1] . "/szervezet-torlese/"; ?>"><span class="ui-state-default ui-corner-all ui-icon ui-icon-close"></span></a>
			    <?php } ?>	
			    <?php if ($odin->admin_this("szervezet", "edit", $details["id"])) { ?>
		    	    <a style="float:right;" title="szervezet szerkesztése" href="<?php print $config["site"]["absolutepath"] . "/szervezetek/" . $q[1] . "/szervezet-elerhetosegek-szerkesztese/"; ?>"><span class="ui-state-default ui-corner-all ui-icon ui-icon-pencil"></span></a>
			    <?php } ?>
			</div>
		    </div>
		    <div class="main_center_content_left">Szervezet típusa</div>
		    <div class="main_center_content_center"><?php print $lang["szervezet"]["tipus"][$details["szervezet_tipus"]]; ?></div>
		    <div class="main_center_content_right"></div>
		    <div class="main_center_content_spacer"></div>
		<?php }
?>

	        <div class="main_center_title">
	    	<div class="main_center_title_left"><font style="color:#617f10">SZERVEZETI</font> Besorolás</div>
	    	<div class="main_center_title_right">
			<?php if ($odin->admin_this("szervezet", "edit", $details["id"])) { ?>
			    <a style="float:right;" title="szervezeti besorolás szerkesztése" href="<?php print $config["site"]["absolutepath"] . "/szervezetek/" . $q[1] . "/szervezeti-besorolas-szerkesztese/" . $q[1] . ""; ?>"><span class="ui-state-default ui-corner-all ui-icon ui-icon-pencil"></span></a>
			<?php } ?>
	    	</div>
	        </div>
		<?php
		foreach ($details["valami"] as $key => $value) {
		    $details["szervezet"]["nev"] = "<a href=\"" . $config["site"]["absolutepath"] . "/szervezetek/" . $value["id"] . "\">" . $value["nev"] . "</a><br />" . $details["szervezet"]["nev"];
		}
		$details["szervezet"]["nev"] = "<a href=\"" . $config["site"]["absolutepath"] . "/szervezetek\">".$config["site"]["tree_root_element"]."</a><br />" . $details["szervezet"]["nev"];
		?>

	        <div class="main_center_content_left"></div>
	        <div class="main_center_content_center"><?php print $details["szervezet"]["nev"]; ?></div>
	        <div class="main_center_content_right"></div>
	        <div class="main_center_content_spacer"></div>


	        <div class="main_center_title">
	    	<div class="main_center_title_left"><font style="color:#617f10">ALÁRENDELT</font> Szervek</div>
	    	<div class="main_center_title_right">
			<?php if ($odin->admin_this("szervezet", "new", $details["id"])) { ?>
			    <a style="float:right;" title="új alárendelt szerv hozzáadása" href="<?php print $config["site"]["absolutepath"] . "/szervezetek/" . $q[1] . "/uj-alarendelt-szerv-hozzaadasa/"; ?>"><span class="ui-state-default ui-corner-all ui-icon ui-icon-plus"></span></a>
			<?php } ?>
	    	</div>
	        </div>
		<?php
		$a = 0;
		$sql = "SELECT id, nev, teljes_id FROM szervezetek WHERE teljes_id LIKE '" . $details["teljes_id"] . "%' ORDER BY nev";
		$result = mysql_query($sql);
		while ($sor = mysql_fetch_assoc($result)) {
		    if (!strcmp($sor["teljes_id"], ($details["teljes_id"] . "-" . $sor["id"]))) {
			$details["alarendelt_szervezet"]["nev"].="<a href=\"" . $config["site"]["absolutepath"] . "/szervezetek/" . $sor["id"] . "\">" . $sor["nev"] . "</a><br />";
			$a++;
		    }
		}
		?>

		<?php if ($a) { ?>
		    <div class="main_center_content_left"></div>
		    <div class="main_center_content_center"><?php print $details["alarendelt_szervezet"]["nev"]; ?></div>
		    <div class="main_center_content_right"></div>
		    <div class="main_center_content_spacer"></div>

		<?php } else { ?>
		    <div class="main_center_content_left"></div>
		    <div class="main_center_content_center">Nincs alárendelt szerv!</div>
		    <div class="main_center_content_right"></div>
		    <div class="main_center_content_spacer"></div>
		<?php } ?>
		<?php if ($details["szervezet_tipus"] != 1) { ?>
		    <div class="main_center_title">
			<div class="main_center_title_left"><font style="color:#617f10">ALKALMAZOTTAK</font> Listája</div>
			<div class="main_center_title_right">
			    <?php if ($odin->admin_this("user", "new", $q[1])) { ?>
		    	    <a title="új felhasználó hozzáadása" href="<?php print $config["site"]["absolutepath"] . "/szervezetek/" . $q[1] . "/uj-felhasznalo-hozzaadasa"; ?>"><span class="ui-state-default ui-corner-all ui-icon ui-icon-plus"></span></a>
			    <?php } ?>
			</div>
		    </div>
		    <?php
		    $a = 0;
		    $sql = "SELECT id FROM felhasznalok WHERE id > 1 AND szervezet_id='" . $details["id"] . "%' AND (`allomany_statusz`<6 OR `allomany_statusz`>8) ORDER BY allomany_statusz ASC, vezeteknev";
		    $result = mysql_query($sql);
		    while ($sor = mysql_fetch_assoc($result)) {
			$a++;
			$details = $odin->get_user_details($sor["id"]);
			?>
		        <div class="main_center_spacer"></div>
		        <div class="main_center_content_names<?php
		    if (($a) % 2 == 0)
			print "_white ui-corner-all"; else
			print "_blue ui-corner-all";
			?>">
		    	<div class="main_center_content_left" style="font-weight:normal; width: 304px;">
				<?php
				print "<b>" . $details["user"]["beosztas"] . "</b><font  style=\"font-size:9px;\">";
				if ($details["user"]["beosztas_megjegyzes"])
				    print "<br />(" . $details["user"]["beosztas_megjegyzes"] . ")";

				print "</font>";
				?>
		    	</div>
		    	<div class="main_center_content_center" style="font-weight:normal; width: 150px;"><?php
		    print ("<b><a href=\"" . $config["site"]["absolutepath"] . "/felhasznalok/" . $details["user"]["id"] . "\">" . $details["user"]["teljesnev"] . "</a></b><br /><font  style=\"font-size:9px;\">" . $details["user"]["rendfokozat"] . "<br />");
		    if ($details["user"]["titulus_2"])
			print $details["user"]["titulus_2"] . "<br />";
		    print ("</font>");
				?></div>
		    	<div class="main_center_content_right" style="font-weight:normal; width: 300px;"><?php
		    if ($details["user"]["email"] != "NA / NT")
			print "<a href=\"mailto:" . $details["user"]["email"] . "\">" . $details["user"]["email"] . "</a>";
		    else
			print "e-mail: NA /NT";
		    print ("<br /><font style=\"font-size:9px;\">közvetlen: <a href=\"" . $config["site"]["absolutepath"] . "/telefonkonyv/kereses/kereses-helye=/nev=/beosztas=/szervezet=/kozvetlenszam=" . $details["user"]["kozvetlen"] . "/szokezdet=/fonetikus=/lista=1-10" . "\">" . $odin->fancy_phone_number($details["user"]["kozvetlen"]) . "</a><br />fax: " . $odin->fancy_phone_number($details["user"]["fax"]) . "</font>");
				?></div>
		    	<div class="main_center_content_spacer"></div>
		        </div>
			<?php
		    }
		    if (!$a) {
			?>
		        <div class="main_center_content_left"></div>
		        <div class="main_center_content_center">Nincsenek alkalmazottak felvíve!</div>
		        <div class="main_center_content_right"></div>
		        <div class="main_center_content_spacer"></div>
		    <?php } ?>

		<?php } ?>
	        <div class="main_center_spacer"></div>
		<?php if ($details["szervezet_tipus"] != 1) { ?>
		    <div class="main_center_title">
			<div class="main_center_title_left"><font style="color:#617f10">INAKTÍV</font> Alkalmazottak listája</div>
			<div class="main_center_title_right"></div>
		    </div>
		    <?php
		    $a = 0;
		    $sql = "SELECT id FROM felhasznalok WHERE id > 1 AND szervezet_id='" . $q[1] . "' AND `allomany_statusz`>5 and `allomany_statusz`<9 ORDER BY allomany_statusz ASC, vezeteknev";
		    $result = mysql_query($sql);
		    while ($sor = mysql_fetch_assoc($result)) {
			$a++;
			$details = $odin->get_user_details($sor["id"]);
			?>
		        <div class="main_center_spacer"></div>
		        <div class="main_center_content_names<?php
		    if (($a) % 2 == 0)
			print "_gray ui-corner-all"; else
			print "_gray ui-corner-all";
			?>">
		    	<div class="main_center_content_left" style="font-weight:normal; width: 304px;">
				<?php
				print "<b>" . $details["user"]["beosztas"] . "</b><font  style=\"font-size:9px;\">";
				if ($details["user"]["beosztas_megjegyzes"])
				    print "<br />(" . $details["user"]["beosztas_megjegyzes"] . ")";

				print "</font>";
				?>
		    	</div>
		    	<div class="main_center_content_center" style="font-weight:normal; width: 150px;"><?php
		    print ("<b><a href=\"" . $config["site"]["absolutepath"] . "/felhasznalok/" . $details["user"]["id"] . "\">" . $details["user"]["teljesnev"] . "</a></b><br /><font  style=\"font-size:9px;\">" . $details["user"]["rendfokozat"] . "<br />");
		    if ($details["user"]["titulus_2"])
			print $details["user"]["titulus_2"] . "<br />";
		    print ("</font>");
				?></div>
		    	<div class="main_center_content_right" style="font-weight:normal; width: 300px;"><?php
		    if ($details["user"]["email"] != "NA / NT")
			print "<a href=\"mailto:" . $details["user"]["email"] . "\">" . $details["user"]["email"] . "</a>";
		    else
			print "e-mail: NA /NT";
		    print ("<br /><font style=\"font-size:9px;\">közvetlen: <a href=\"" . $config["site"]["absolutepath"] . "/telefonkonyv/kereses/kereses-helye=/nev=/beosztas=/szervezet=/kozvetlenszam=" . $details["user"]["kozvetlen"] . "/szokezdet=/fonetikus=/lista=1-10" . "\">" . $odin->fancy_phone_number($details["user"]["kozvetlen"]) . "</a><br />fax: " . $odin->fancy_phone_number($details["user"]["fax"]) . "</font>");
				?></div>
		    	<div class="main_center_content_spacer"></div>
		        </div>
			<?php
		    }
		    if (!$a) {
			?>
		        <div class="main_center_content_left"></div>
		        <div class="main_center_content_center">Nincsenek alkalmazottak felvíve!</div>
		        <div class="main_center_content_right"></div>
		        <div class="main_center_content_spacer"></div>
		    <?php } ?>

		<?php } ?>
	    </div>
	    <div class="main_center_spacer"></div>
	<?php } else { ?>
	    <div class="main_center_spacer"></div>

	    <div class="main_center_container">


	        <div class="main_center_title">
	    	<div class="main_center_title_left"><font style="color:#617f10">SZERVEZETI</font> Besorolás</div>
	    	<div class="main_center_title_right"></div>
	        </div>
		<?php
		$details["szervezet"]["nev"] = "<a href=\"" . $config["site"]["absolutepath"] . "/szervezetek\">".$config["site"]["tree_root_element"]."</a><br />" . $details["szervezet"]["nev"];
		?>

	        <div class="main_center_content_left"></div>
	        <div class="main_center_content_center"><?php print $details["szervezet"]["nev"]; ?></div>
	        <div class="main_center_content_right"></div>
	        <div class="main_center_content_spacer"></div>


	        <div class="main_center_title">
	    	<div class="main_center_title_left"><font style="color:#617f10">ALÁRENDELT</font> SZERVEK</div>
	    	<div class="main_center_title_right">
			<?php if ($odin->admin_this("szervezet", "new", $details["id"])) { ?>
			    <a style="float:right;" title="új alárendelt szerv hozzáadása" href="<?php print $config["site"]["absolutepath"] . "/szervezetek/0/uj-alarendelt-szerv-hozzaadasa/"; ?>"><span class="ui-state-default ui-corner-all ui-icon ui-icon-plus"></span></a>
			<?php } ?>
	    	</div>
	        </div>
		<?php
		$a = 0;
		$sql = "SELECT id, nev, teljes_id FROM szervezetek WHERE teljes_id LIKE '%' ORDER BY nev";
		$result = mysql_query($sql);
		while ($sor = mysql_fetch_assoc($result)) {
		    if (!strcmp($sor["teljes_id"], ($sor["id"]))) {
			$details["alarendelt_szervezet"]["nev"].="<a href=\"" . $config["site"]["absolutepath"] . "/szervezetek/" . $sor["id"] . "\">" . $sor["nev"] . "</a><br />";
			$a++;
		    }
		}
		?>

		<?php if ($a) { ?>
		    <div class="main_center_content_left"></div>
		    <div class="main_center_content_center"><?php print $details["alarendelt_szervezet"]["nev"]; ?></div>
		    <div class="main_center_content_right"></div>
		    <div class="main_center_content_spacer"></div>

		<?php } else { ?>
		    <div class="main_center_content_left"></div>
		    <div class="main_center_content_center">Nincs alárendelt szerv!</div>
		    <div class="main_center_content_right"></div>
		    <div class="main_center_content_spacer"></div>
		<?php } ?>

            </div>
	        <div class="main_center_spacer"></div>
                
		<?php
	    }
	}

//*********************************************************************
	function uj_felhasznalo_hozzaad($error) {
	    global $lang, $odin, $q, $config;
	    $details["user"] = $odin->get_szervezet_details($q[1]);
	    ?>
	    <div class="main_center_spacer"></div>

	    <div class="main_center_container">
		<?php if ($error) { ?>
	    	<div class="ui-state-highlight ui-corner-all" style="margin-top: 5px; padding: 0 .7em;">
	    	    <p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
			    <?php print $error["kiir"]; ?>
	    	    </p>
	    	</div>
		<?php } ?>
		<div class="main_center_title">
		    <div class="main_center_title_left"><font style="color:#617f10">SZEMÉLYES</font> Adatok</div>
		</div>

		<div class="main_login_spacer"></div>

		<form action="szervezetek/<?php print $q[1]; ?>/uj-felhasznalo-hozzaadasa" method="post">
		    <div class="main_login_container">

			<div class="main_login_left"><?php print($lang["felhasznalok"]["titulus"]); ?></div>
			<div class="main_login_right"><input type="text" name="titulus" value="<?php
	if ($_POST["titulus"])
	    print $_POST["titulus"]; else
	    print $details["user"]["titulus"];
		?>" size="32" maxlength="32" /></div>
			<div class="main_login_spacer"></div>
			<div class="main_login_left"><?php print($lang["felhasznalok"]["vezeteknev"]); ?></div>
			<div class="main_login_right"><input type="text" name="vezeteknev" value="<?php
						     if ($_POST["vezeteknev"])
							 print $_POST["vezeteknev"]; else
							 print $details["user"]["vezeteknev"];
		?>" size="32" maxlength="64" /></div>
			<div class="main_login_spacer"></div>
			<div class="main_login_left"><?php print($lang["felhasznalok"]["kozepsonev"]); ?></div>
			<div class="main_login_right"><input type="text" name="kozepsonev" value="<?php
						     if ($_POST["kozepsonev"])
							 print $_POST["kozepsonev"]; else
							 print $details["user"]["kozepsonev"];
		?>" size="32" maxlength="64" /></div>
			<div class="main_login_spacer"></div>
			<div class="main_login_left"><?php print($lang["felhasznalok"]["keresztnev"]); ?></div>
			<div class="main_login_right"><input type="text" name="keresztnev" value="<?php
						     if ($_POST["keresztnev"])
							 print $_POST["keresztnev"]; else
							 print $details["user"]["keresztnev"];
		?>" size="32" maxlength="64" /></div>
			<div class="main_login_spacer"></div>
			<div class="main_login_left"><?php print($lang["felhasznalok"]["nicknev"]); ?></div>
			<div class="main_login_right"><input type="text" name="nicknev" value="<?php
						     if ($_POST["nicknev"])
							 print $_POST["nicknev"];
		?>" size="32" maxlength="64" /></div>
			<div class="main_login_spacer"></div>
			<div class="main_login_left"><?php print($lang["aktivacio"]["email"]); ?></div>
			<div class="main_login_right"><input type="text" name="email" value="<?php
						     if ($_POST["email"])
							 print $_POST["email"];
		?>" size="32" maxlength="64" /></div>
			<div class="main_login_spacer"></div>
			<?php if (!$error["rendfokozat"]) { ?>

	    		<div class="main_login_left"><?php print($lang["felhasznalok"]["rendfokozat"]); ?></div>
	    		<div class="main_login_right">
	    		    <select name="rendfokozat_id">
				    <?php if ($details["user"]["rendfokozat_id"]) { ?>
					<option value="<?php print $details["user"]["rendfokozat_id"]; ?>"><?php print $details["user"]["rendfokozat"]; ?></option>
				    <?php } ?>
	    			<option value="0">Nem szerepel a listában</option>
				    <?php
				    $sql = "SELECT * FROM rendfokozatok ORDER BY id";
				    $result = mysql_query($sql);
				    $sql_query_count++;
				    while ($sor = mysql_fetch_assoc($result)) {
					?>
					<option value="<?php print $sor["id"]; ?>" <?php if ($_POST["rendfokozat_id"] == $sor["id"])
			    print "selected=\"selected\"" ?>><?php print $sor["megnevezes"]; ?></option>
					    <?php } ?>
	    			<option value="0">Nem szerepel a listában</option>
	    		    </select>
	    		</div>
			<?php }else { ?>
	    		<div class="main_login_left"><?php print($lang["felhasznalok"]["rendfokozat"]); ?></div>
	    		<div class="main_login_right"><input type="text" name="rendfokozat" value="<?php
		if ($_POST["rendfokozat"])
		    print $_POST["rendfokozat"]; else
		    print $details["user"]["rendfokozat"];
			    ?>" size="32" maxlength="64" /></div>
				<?php
			    }
			    ?>
			<div class="main_login_spacer"></div>
			<div class="main_login_left"><?php print($lang["felhasznalok"]["titulus_2"]); ?></div>
			<div class="main_login_right"><input type="text" name="titulus_2" value="<?php
		    if ($_POST["titulus_2"])
			print $_POST["titulus_2"];
			    ?>" size="32" maxlength="64" /></div>
			<div class="main_login_spacer"></div>
			<?php if (!$error["beosztas"]) { ?>
	    		<div class="main_login_left"><?php print($lang["felhasznalok"]["beosztas"]); ?></div>
	    		<div class="main_login_right">
	    		    <select name="beosztas_id">
				    <?php if ($details["user"]["beosztas_id"]) { ?>
					<option value="<?php print $details["user"]["beosztas_id"]; ?>"><?php print $details["user"]["beosztas"]; ?></option>
				    <?php } ?>
	    			<option value="0">Nem szerepel a listában</option>
				    <?php
				    $sql = "SELECT * FROM beosztasok ORDER BY beosztasnev";
				    $result = mysql_query($sql);
				    $sql_query_count++;
				    while ($sor = mysql_fetch_assoc($result)) {
					?>
					<option value="<?php print $sor["id"]; ?>"<?php if ($_POST["beosztas_id"] == $sor["id"])
			    print "selected=\"selected\"" ?>><?php print $sor["beosztasnev"]; ?></option>
					    <?php } ?>
	    			<option value="0">Nem szerepel a listában</option>
	    		    </select>
	    		</div>
			<?php }else { ?>
	    		<div class="main_login_left"><?php print($lang["felhasznalok"]["beosztas"]); ?></div>
	    		<div class="main_login_right"><input type="text" name="beosztas" value="<?php
		if ($_POST["beosztas"])
		    print $_POST["beosztas"]; else
		    print $details["user"]["beosztas"];
			    ?>" size="32" maxlength="64" /></div>
				<?php
			    }
			    ?>
			<div class="main_login_spacer"></div>


			<div class="main_login_left"><?php print($lang["felhasznalok"]["beosztas_megjegyzes"]); ?></div>
			<div class="main_login_right"><input type="text" name="beosztas_megjegyzes" value="<?php
		    if ($_POST["beosztas_megjegyzes"])
			print $_POST["beosztas_megjegyzes"]; else
			print $details["user"]["beosztas_megjegyzes"];
			    ?>" size="32" maxlength="64" /></div>
			<div class="main_login_spacer"></div>
			<div class="main_login_left"><?php print($lang["felhasznalok"]["beosztas_jellege"]); ?></div>
			<div class="main_login_right">
			    <select name="allomany_statusz">
				<?php
				if (!$_POST["allomany_statusz"])
				    $_POST["allomany_statusz"] = 5;
				foreach ($lang["beosztas_jellege"] as $key => $value) {
				    ?>
	    			<option value="<?php print $key; ?>" <?php if ($key == $_POST["allomany_statusz"])
			    print "selected=\"selected\""; ?>><?php print $value; ?></option>
					<?php } ?>
			    </select>
			</div>                        
		    </div>

		    <div class="main_login_spacer"></div>
		    <div class="main_center_title">
			<div class="main_center_title_left"><font style="color:#617f10">CÉGES</font> Elérhetőségek</div>
		    </div>

		    <div class="main_login_container">

			<div class="main_login_left"><?php print($lang["felhasznalok"]["telephely_irsz"]); ?></div>
			<div class="main_login_right"><input type="text" name="telephely_irsz" value="<?php
				if ($_POST["telephely_irsz"]) {
				    print $_POST["telephely_irsz"];
				} else if (!$details["user"]["telephely_irsz"]) {
				    
				} else {
				    print $details["user"]["telephely_irsz"];
				}
					?>" size="32" maxlength="4" /></div>
			<div class="main_login_spacer"></div>

			<div class="main_login_left"><?php print($lang["felhasznalok"]["telephely_varos"]); ?></div>
			<div class="main_login_right"><input type="text" name="telephely_varos" value="<?php
						     if ($_POST["telephely_varos"]) {
							 print $_POST["telephely_varos"];
						     } else if (!$details["user"]["telephely_varos"]) {
							 
						     } else {
							 print $details["user"]["telephely_varos"];
						     }
					?>" size="32" maxlength="32" /></div>
			<div class="main_login_spacer"></div>

			<div class="main_login_left"><?php print($lang["felhasznalok"]["telephely_cim"]); ?></div>
			<div class="main_login_right"><input type="text" name="telephely_cim" value="<?php
						     if ($_POST["telephely_cim"]) {
							 print $_POST["telephely_cim"];
						     } else if (!$details["user"]["telephely_cim"]) {
							 
						     } else {
							 print $details["user"]["telephely_cim"];
						     }
					?>" size="32" maxlength="64" /></div>
			<div class="main_login_spacer"></div>

			<div class="main_login_left"><?php print($lang["felhasznalok"]["telephely_szam"]); ?></div>
			<div class="main_login_right"><input type="text" name="telephely_szam" value="<?php
						     if ($_POST["telephely_szam"]) {
							 print $_POST["telephely_szam"];
						     } else if (!$details["user"]["telephely_szam"]) {
							 
						     } else {
							 print $details["user"]["telephely_szam"];
						     }
					?>" size="32" maxlength="16" /></div>
			<div class="main_login_spacer"></div>


			<div class="main_login_left"><?php print($lang["felhasznalok"]["telephely_epulet"]); ?></div>
			<div class="main_login_right"><input type="text" name="telephely_epulet" value="<?php
						     if ($_POST["telephely_epulet"]) {
							 print $_POST["telephely_epulet"];
						     } else if (!$details["user"]["telephely_epulet"]) {
							 
						     } else {
							 print $details["user"]["telephely_epulet"];
						     }
					?>" size="32" maxlength="16" /></div>
			<div class="main_login_spacer"></div>

			<div class="main_login_left"><?php print($lang["felhasznalok"]["telephely_emelet"]); ?></div>
			<div class="main_login_right"><input type="text" name="telephely_emelet" value="<?php
						     if ($_POST["telephely_emelet"]) {
							 print $_POST["telephely_emelet"];
						     } else if (!$details["user"]["telephely_emelet"]) {
							 
						     } else {
							 print $details["user"]["telephely_emelet"];
						     }
					?>" size="32" maxlength="16" /></div>
			<div class="main_login_spacer"></div>

			<div class="main_login_left"><?php print($lang["felhasznalok"]["telephely_iroda"]); ?></div>
			<div class="main_login_right"><input type="text" name="telephely_iroda" value="<?php
						     if ($_POST["telephely_iroda"]) {
							 print $_POST["telephely_iroda"];
						     } else if (!$details["user"]["telephely_iroda"]) {
							 
						     } else {
							 print $details["user"]["telephely_iroda"];
						     }
					?>" size="32" maxlength="16" /></div>
			<div class="main_login_spacer"></div>

			<div class="main_login_left"><?php print($lang["felhasznalok"]["vezetoi"]); ?></div>
			<div class="main_login_right"><input type="text" name="vezetoi" value="<?php
						     if ($_POST["vezetoi"]) {
							 print $_POST["vezetoi"];
						     } else if (!$details["user"]["vezetoi"]) {
							 
						     } else {
							 print $details["user"]["vezetoi"];
						     }
					?>" size="32" maxlength="6" /></div>
			<div class="main_login_spacer"></div>

			<div class="main_login_left"><?php print($lang["felhasznalok"]["kozvetlen"]); ?></div>
			<div class="main_login_right"><input type="text" name="kozvetlen" value="<?php
						     if ($_POST["kozvetlen"]) {
							 print $_POST["kozvetlen"];
						     } else if (!$details["user"]["kozvetlen"]) {
							 
						     } else {
							 print $details["user"]["kozvetlen"];
						     }
					?>" size="32" maxlength="8" /></div>
			<div class="main_login_spacer"></div>

			<div class="main_login_left"><?php print($lang["felhasznalok"]["2helyi"]); ?></div>
			<div class="main_login_right"><input type="text" name="2helyi" value="<?php
						     if ($_POST["2helyi"]) {
							 print $_POST["2helyi"];
						     } else if (!$details["user"]["2helyi"]) {
							 
						     } else {
							 print $details["user"]["2helyi"];
						     }
					?>" size="32" maxlength="6" /></div>
			<div class="main_login_spacer"></div>

			<div class="main_login_left"><?php print($lang["felhasznalok"]["fax"]); ?></div>
			<div class="main_login_right"><input type="text" name="fax" value="<?php
						     if ($_POST["fax"]) {
							 print $_POST["fax"];
						     } else if (!$details["user"]["fax"]) {
							 
						     } else {
							 print $details["user"]["fax"];
						     }
					?>" size="32" maxlength="6" /></div>
			<div class="main_login_spacer"></div>
			<div class="main_login_left"><?php print($lang["felhasznalok"]["mobil"]); ?></div>
			<div class="main_login_right"><input type="text" name="mobil" value="<?php
						     if ($_POST["mobil"]) {
							 print $_POST["mobil"];
						     } else if (!$details["user"]["mobil"]) {
							 
						     } else {
							 print $details["user"]["mobil"];
						     }
					?>" size="32" maxlength="9" /></div>
			<div class="main_login_spacer"></div>

			<div class="main_login_left"><?php print($lang["felhasznalok"]["vez_kozvetlen"]); ?></div>
			<div class="main_login_right"><input type="text" name="vez_kozvetlen" value="<?php
						     if ($_POST["vez_kozvetlen"]) {
							 print $_POST["vez_kozvetlen"];
						     }
					?>" size="32" maxlength="8" /></div>
			<div class="main_login_spacer"></div>

			<div class="main_login_left"><?php print($lang["felhasznalok"]["vez_fax"]); ?></div>
			<div class="main_login_right"><input type="text" name="vez_fax" value="<?php
						     if ($_POST["vez_fax"]) {
							 print $_POST["vez_fax"];
						     }
					?>" size="32" maxlength="8" /></div>
			<div class="main_login_spacer"></div>

			<div class="main_login_left"><?php print($lang["felhasznalok"]["tetra"]); ?></div>
			<div class="main_login_right"><input type="text" name="tetra" value="<?php
						     if ($_POST["tetra"]) {
							 print $_POST["tetra"];
						     } else if (!$details["user"]["tetra"]) {
							 
						     } else {
							 print $details["user"]["tetra"];
						     }
					?>" size="32" maxlength="7" /></div>
		    </div>
		    <div class="main_login_spacer"></div>

		    <div class="main_center_title">
			<div class="main_center_title_left"><font style="color:#617f10">MEGJEGYZÉS</font></div>
		    </div>
		    <div class="ui-state-highlight ui-corner-all" style="margin-top: 5px; padding: 0 .7em;">
			<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
			    <b>HASZNÁLHATÓ TAGEK:</b><br /><br />
			    - [B] vastag [/B] -> <?php print $odin->tags_to_html("[B] vastag [/B]"); ?> <br />
			    - [U] aláhúzott [/U] -> <?php print $odin->tags_to_html("[U] aláhúzott [/U]"); ?> <br />
			    - [FREYALINK=felhasznalok/2204] Dobó Tamás [/FREYALINK] ->  <?php print $odin->tags_to_html("[FREYALINK=felhasznalok/2204] Dobó Tamás [/FREYALINK]"); ?> <br />
			    - [LINK=http://tudakozo.telekom.hu] Telekom Tudakozó [/LINK] -> <?php print $odin->tags_to_html("[LINK=http://tudakozo.telekom.hu] Telekom Tudakozó [/LINK]"); ?> <br /><br />
			</p>
		    </div>
		    <div class="main_login_spacer"></div>
		    <div class="main_login_container">
			<div class="main_login_left"><?php print($lang["felhasznalok"]["megjegyzes"]); ?></div>
			<div class="main_login_right"><textarea cols="32" rows="5" name="megjegyzes"><?php
						     if ($_POST["megjegyzes"]) {
							 print $_POST["megjegyzes"];
						     } else if (!$details["user"]["megjegyzes"]) {
							 
						     } else {
							 print $details["user"]["megjegyzes"];
						     }
					?></textarea></div>



			<div class="main_login_left">&nbsp;</div>
			<input class="ui-state-default ui-corner-all" type="submit" name="hozzaad" value="<?php print($lang["gomb"]["hozzaad"]); ?>" />
			<input class="ui-state-default ui-corner-all" type="submit" name="hozzaad" value="<?php print($lang["gomb"]["megse"]); ?>" />
		    </div>
		</form>

	    </div>


	    <div class="main_center_spacer"></div>
	    <?php
	}

//*********************************************************************
	function altalanos_adatok_szerkesztese($error) {
	    global $lang, $odin, $q;
	    $details = $odin->get_szervezet_details($q["1"]);
	    ?>
	    <div class="main_center_spacer"></div>

	    <div class="main_center_container">
		<div class="main_center_title">
		    <div class="main_center_title_left"><font style="color:#617f10"><?php print mb_strtoupper($odin->fancy_text($details["nev"]), 'UTF-8'); ?></font> Alapadatainak módosítása</div>
		</div>
		<?php if ($error) { ?>
	    	<div class="ui-state-highlight ui-corner-all" style="margin-top: 5px; padding: 0 .7em;"> 
	    	    <p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
			    <?php print $error["kiir"]; ?>
	    	    </p>
	    	</div>
		<?php } ?>
		<div class="main_login_spacer"></div>

		<form action="szervezetek/<?php print $q[1]; ?>/szervezet-altalanos-adatainak-szerkesztese" method="post"> 
		    <div class="main_login_container">

			<div class="main_login_left">Hírközpont: </div>
			<div class="main_login_right"><input type="text" name="hirkozpont" value="<?php
	if ($_POST["hirkozpont"]) {
	    print $_POST["hirkozpont"];
	} else if (!$details["hirkozpont"]) {
	    
	} else {
	    print $details["hirkozpont"];
	}
		?>" size="32" maxlength="6" /></div> 
			<div class="main_login_spacer"></div>

			<div class="main_login_left">Ügyelet: </div>
			<div class="main_login_right"><input type="text" name="ugyelet" value="<?php
						     if ($_POST["ugyelet"]) {
							 print $_POST["ugyelet"];
						     } else if (!$details["ugyelet"]) {
							 
						     } else {
							 print $details["ugyelet"];
						     }
		?>" size="32" maxlength="6" /></div> 
			<div class="main_login_spacer"></div>



			<div class="main_login_spacer"></div>
			<div class="main_login_left">&nbsp;</div>
			<input class="ui-state-default ui-corner-all" type="submit" name="modosit" value="<?php print($lang["gomb"]["modosit"]); ?>" />
			<input class="ui-state-default ui-corner-all" type="submit" name="modosit" value="<?php print($lang["gomb"]["megse"]); ?>" />
		    </div>
		</form>

	    </div>


	    <div class="main_center_spacer"></div>
	    <?php
	}

//*********************************************************************	
	function szervezet_elerhetosegek_szerkesztese($error) {
	    global $lang, $odin, $q;
	    $details = $odin->get_szervezet_details($q["1"]);
	    ?>
	    <script type="text/javascript">
		$(document).ready(function() {
		    if ( $('#sztipus').val() == 1 ){
			    $('#adatok').hide();
			}
		    $('#sztipus').change(function(){
			if ( $(this).val() == 1 ){
			    $('#adatok').slideUp(800);
			}
			if ( $(this).val() == 2 ){
			    $('#adatok').slideDown(800);
			}
			if ( $(this).val() == 3 ){
			    $('#adatok').slideDown(800);
			}
			if ( $(this).val() == 4 ){
			    $('#adatok').slideDown(800);
			}
			if ( $(this).val() == 5 ){
			    $('#adatok').slideDown(800);
			}
		    });
		});
	    </script>
	    <div class="main_center_spacer"></div>

	    <div class="main_center_container">
		<div class="main_center_title">
		    <div class="main_center_title_left"><font style="color:#617f10"><?php print mb_strtoupper($details["nev"], 'UTF-8'); ?></font> Elérhetőségeinek módosítása</div>
		</div>
		<?php if ($error) { ?>
	    	<div class="ui-state-highlight ui-corner-all" style="margin-top: 5px; padding: 0 .7em;"> 
	    	    <p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
			    <?php print $error["kiir"]; ?>
	    	    </p>
	    	</div>
		<?php } ?>
		<div class="main_login_spacer"></div>

		<form action="szervezetek/<?php print $q[1]; ?>/szervezet-elerhetosegek-szerkesztese" method="post"> 
		    <div class="main_login_container">

			<div class="main_login_left">Szervezet neve:</div>
			<div class="main_login_right"><input type="text" name="nev" value="<?php
	if ($_POST["nev"]) {
	    print $_POST["nev"];
	} else if (!$details["nev"]) {
	    
	} else {
	    print $details["nev"];
	}
		?>" size="32" maxlength="64" /></div> 
			<div class="main_login_spacer"></div>
			<div class="main_login_left">Szervezet típusa:</div>
			<div class="main_login_right">
			    <select id="sztipus" name="szervezet_tipus">
				<?php
				foreach ($lang["szervezet"]["tipus"] as $key => $value) {
				    ?>
	    			<option value="<?php print $key; ?>"<?php if ($key == $details["szervezet_tipus"])
			    print " selected=\"selected\"" ?>><?php print $value; ?></option>  
					<?php } ?>
			    </select>
			</div>
			<div class="main_login_spacer"></div>
			<div id="adatok">
			    <div class="main_login_left">Öröklődés: </div>
			    <div class="main_login_right" style="text-align:left;">
				<input type="radio" value="0" name="oroklodes" <?php
				if (!$_POST["oroklodes"] || is_null($details["oroklodes"])) {
				    print " checked=\"checked\"";
				}
					?> />öröklődés engedélyezve<br />
				<input type="radio" value="1" name="oroklodes" <?php
			       if ($_POST["oroklodes"]==1 || (!$_POST["oroklodes"] && $details["oroklodes"]==1)) {
				   print " checked=\"checked\"";
			       }
					?> />öröklődés csak erre a szervre tiltva<br />
				<input type="radio" value="2" name="oroklodes" <?php
			       if ($_POST["oroklodes"]==2 || (!$_POST["oroklodes"] && $details["oroklodes"]==2)) {
				   print " checked=\"checked\"";
			       }
					?> />öröklődés teljes tiltása
			    </div>
			    <div class="main_login_spacer"></div>
			<div class="main_login_left"><?php print($lang["felhasznalok"]["email"]); ?></div>
			<div class="main_login_right"><input type="text" name="email" value="<?php
			       if ($_POST["email"]) {
				   print $_POST["email"];
			       } else if (!$details["email"]) {
				   
			       } else {
				   print $details["email"];
			       }
					?>" size="32" maxlength="64" /></div> 
			<div class="main_login_spacer"></div>

			<div class="main_login_left"><?php print($lang["felhasznalok"]["telephely_irsz"]); ?></div>
			<div class="main_login_right"><input type="text" name="telephely_irsz" value="<?php
						     if ($_POST["telephely_irsz"]) {
							 print $_POST["telephely_irsz"];
						     } else if (!$details["telephely_irsz"]) {
							 
						     } else {
							 print $details["telephely_irsz"];
						     }
					?>" size="32" maxlength="4" /></div> 
			<div class="main_login_spacer"></div>

			<div class="main_login_left"><?php print($lang["felhasznalok"]["telephely_varos"]); ?></div>
			<div class="main_login_right"><input type="text" name="telephely_varos" value="<?php
						     if ($_POST["telephely_varos"]) {
							 print $_POST["telephely_varos"];
						     } else if (!$details["telephely_varos"]) {
							 
						     } else {
							 print $details["telephely_varos"];
						     }
					?>" size="32" maxlength="32" /></div> 
			<div class="main_login_spacer"></div>

			<div class="main_login_left"><?php print($lang["felhasznalok"]["telephely_cim"]); ?></div>
			<div class="main_login_right"><input type="text" name="telephely_cim" value="<?php
						     if ($_POST["telephely_cim"]) {
							 print $_POST["telephely_cim"];
						     } else if (!$details["telephely_cim"]) {
							 
						     } else {
							 print $details["telephely_cim"];
						     }
					?>" size="32" maxlength="64" /></div> 
			<div class="main_login_spacer"></div>

			<div class="main_login_left"><?php print($lang["felhasznalok"]["telephely_szam"]); ?></div>
			<div class="main_login_right"><input type="text" name="telephely_szam" value="<?php
						     if ($_POST["telephely_szam"]) {
							 print $_POST["telephely_szam"];
						     } else if (!$details["telephely_szam"]) {
							 
						     } else {
							 print $details["telephely_szam"];
						     }
					?>" size="32" maxlength="16" /></div> 
			<div class="main_login_spacer"></div>

			<div class="main_login_left"><?php print($lang["felhasznalok"]["levelezesi_irsz"]); ?></div>
			<div class="main_login_right"><input type="text" name="levelezesi_irsz" value="<?php
						     if ($_POST["levelezesi_irsz"]) {
							 print $_POST["levelezesi_irsz"];
						     } else if (!$details["levelezesi_irsz"]) {
							 
						     } else {
							 print $details["levelezesi_irsz"];
						     }
					?>" size="32" maxlength="4" /></div> 
			<div class="main_login_spacer"></div>

			<div class="main_login_left"><?php print($lang["felhasznalok"]["levelezesi_varos"]); ?></div>
			<div class="main_login_right"><input type="text" name="levelezesi_varos" value="<?php
						     if ($_POST["levelezesi_varos"]) {
							 print $_POST["levelezesi_varos"];
						     } else if (!$details["levelezesi_varos"]) {
							 
						     } else {
							 print $details["levelezesi_varos"];
						     }
					?>" size="32" maxlength="32" /></div> 
			<div class="main_login_spacer"></div>

			<div class="main_login_left"><?php print($lang["felhasznalok"]["levelezesi_cim"]); ?></div>
			<div class="main_login_right"><input type="text" name="levelezesi_cim" value="<?php
						     if ($_POST["levelezesi_cim"]) {
							 print $_POST["levelezesi_cim"];
						     } else if (!$details["levelezesi_cim"]) {
							 
						     } else {
							 print $details["levelezesi_cim"];
						     }
					?>" size="32" maxlength="64" /></div> 
			<div class="main_login_spacer"></div>

			<div class="main_login_left"><?php print($lang["felhasznalok"]["fax"]); ?></div>
			<div class="main_login_right"><input type="text" name="fax" value="<?php
						     if ($_POST["fax"]) {
							 print $_POST["fax"];
						     } else if (!$details["fax"]) {
							 
						     } else {
							 print $details["fax"];
						     }
					?>" size="32" maxlength="6" /></div> 
			<div class="main_login_spacer"></div>

			<div class="main_login_left"><?php print($lang["felhasznalok"]["vez_kozvetlen"]); ?></div>
			<div class="main_login_right"><input type="text" name="vez_kozvetlen" value="<?php
						     if ($_POST["vez_kozvetlen"]) {
							 print $_POST["vez_kozvetlen"];
						     } else if (!$details["vez_kozvetlen"]) {
							 
						     } else {
							 print $details["vez_kozvetlen"];
						     }
					?>" size="32" maxlength="8" /></div> 
			<div class="main_login_spacer"></div>

			<div class="main_login_left"><?php print($lang["felhasznalok"]["vez_fax"]); ?></div>
			<div class="main_login_right"><input type="text" name="vez_fax" value="<?php
						     if ($_POST["vez_fax"]) {
							 print $_POST["vez_fax"];
						     } else if (!$details["vez_fax"]) {
							 
						     } else {
							 if (strlen($details["vez_fax"]) < 9)
							     print $details["vez_fax"];
						     }
					?>" size="32" maxlength="8" /></div> 
			<div class="main_login_spacer"></div>
			</div>
			<div class="main_login_spacer"></div>
			<div class="main_login_left">&nbsp;</div>
			<input class="ui-state-default ui-corner-all" type="submit" name="modosit" value="<?php print($lang["gomb"]["modosit"]); ?>" />
			<input class="ui-state-default ui-corner-all" type="submit" name="modosit" value="<?php print($lang["gomb"]["megse"]); ?>" />
		    </div>
		</form>

	    </div>


	    <div class="main_center_spacer"></div>
	    <?php
	}

//*********************************************************************
    function szervezet_modosit($error) {
	global $lang, $odin, $q, $config;
	$details = $odin->get_szervezet_details($q[3]);
	
	if (!mysql_num_rows(mysql_query("SELECT id FROM szervezetek WHERE id = '" . $q[3] . "'")) && $q[3]) {
		?>
	<div class="main_center_spacer"></div>
	<div class="main_center_container">
	        <div class="ui-state-error ui-corner-all" style="margin-top: 20px; padding: 0 .7em;"> 
	    	<p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span>
		Nincs ilyen szervezet! A saját szervezethez való visszatéréshez kattintson <?php print "<a href=\"" . $config["site"]["absolutepath"] . "/szervezetek/".$q[1]."/szervezeti-besorolas-szerkesztese/" . $details["id"] . "\">ide</a>"; ?>!
	    	</p>
	        </div>
	<div class="site_spacer"></div>
	</div>
	<div class="main_center_spacer"></div>
	    <?php
	    } else {
	    ?>
	<?php if (!$q[3]) { ?>
		<div class="main_center_spacer"></div>
	<div class="main_center_container">
	    <div class="main_center_title">
		<form action="szervezetek/<?php print $q[1]; ?>/szervezeti-besorolas-szerkesztese/<?php print $q[3]; ?>" method="post"> 
		    <input type="hidden" name="id" value="<?php print $q[3]; ?>" /> 
		    <div class="main_center_title_left"><font style="color:#617f10">ÁTHELYZÉS </font> ebbe a szervezetbe</div>
		    <div class="main_center_title_right">
			<input class="ui-state-default ui-corner-all" type="submit" name="modosit" value="<?php print($lang["gomb"]["athelyez"]); ?>" />
			<input class="ui-state-default ui-corner-all" type="submit" name="modosit" value="<?php print($lang["gomb"]["megse"]); ?>" /></div>
		</form>
	    </div>
	    <?php 
	    	if ($error) { ?>
	        <div class="ui-state-highlight ui-corner-all" style="margin-top: 5px; padding: 0 .7em;"> 
	    	<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
			<?php print $error["kiir"]; ?>
	    	</p>
	        </div>
		<div class="main_center_content_spacer"></div>
	    <?php }
	    foreach ($details["valami"] as $key => $value) {
		$details["szervezet"]["nev"] = "<a href=\"" . $config["site"]["absolutepath"] . "/szervezetek/".$q[1]."/szervezeti-besorolas-szerkesztese/" . $value["id"] . "\">" . $value["nev"] . "</a><br />" . $details["szervezet"]["nev"];
	    }
	    ?>

	    <div class="main_center_content_left"></div>
	    <div class="main_center_content_center"><?php print "<a href=\"" . $config["site"]["absolutepath"] . "/szervezetek/".$q[1]."/szervezeti-besorolas-szerkesztese/" . $value["id"] . "\">".$config["site"]["tree_root_element"]."</a><br />".$details["szervezet"]["nev"]; ?></div>
	    <div class="main_center_content_right"></div>
	    <div class="main_center_content_spacer"></div>


	    <?php
	    $a = 0;
	    $sql = "SELECT id, nev, teljes_id FROM szervezetek WHERE teljes_id LIKE '%' ORDER BY nev";
	    $result = mysql_query($sql);
	    while ($sor = mysql_fetch_assoc($result)) {
		if (!strcmp($sor["teljes_id"], ($sor["id"]))) {
		    $details["alarendelt_szervezet"]["nev"].="<a href=\"" . $config["site"]["absolutepath"] . "/szervezetek/".$q[1]."/szervezeti-besorolas-szerkesztese/" . $sor["id"] . "\">" . $sor["nev"] . "</a><br />";
		    $a++;
		}
	    }
	    ?>

	    <div class="main_center_title">
		<div class="main_center_title_left"><font style="color:#617f10">ALÁRENDELT </font> szervek</div>
	    </div>

	    <?php if ($a) { ?>
	        <div class="main_center_content_left"></div>
	        <div class="main_center_content_center"><?php print $details["alarendelt_szervezet"]["nev"]; ?></div>
	        <div class="main_center_content_right"></div>
	        <div class="main_center_content_spacer"></div>

	    <?php } else { ?>
	        <div class="main_center_content_left"></div>
	        <div class="main_center_content_center">Nincs alárendelt szerv!</div>
	        <div class="main_center_content_right"></div>
	        <div class="main_center_content_spacer"></div>
	    <?php } ?>
	    <div class="site_spacer"></div>
	    <div class="site_spacer"></div>

	</div>


	<div class="main_center_spacer"></div>
	    
	<?php } else { ?>
	<div class="main_center_spacer"></div>
	<div class="main_center_container">
	    <div class="main_center_title">
		<form action="szervezetek/<?php print $q[1]; ?>/szervezeti-besorolas-szerkesztese/<?php print $q[3]; ?>" method="post"> 
		    <input type="hidden" name="id" value="<?php print $q[3]; ?>" /> 
		    <div class="main_center_title_left"><font style="color:#617f10">ÁTHELYZÉS </font> ebbe a szervezetbe</div>
		    <div class="main_center_title_right">
			<input class="ui-state-default ui-corner-all" type="submit" name="modosit" value="<?php print($lang["gomb"]["athelyez"]); ?>" />
			<input class="ui-state-default ui-corner-all" type="submit" name="modosit" value="<?php print($lang["gomb"]["megse"]); ?>" /></div>
		</form>
	    </div>
	    	    <?php 
	    	if ($error) { ?>
	        <div class="ui-state-highlight ui-corner-all" style="margin-top: 5px; padding: 0 .7em;"> 
	    	<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
			<?php print $error["kiir"]; ?>
	    	</p>
	        </div>
		<div class="main_center_content_spacer"></div>
	    <?php }

	    foreach ($details["valami"] as $key => $value) {
		$details["szervezet"]["nev"] = "<a href=\"" . $config["site"]["absolutepath"] . "/szervezetek/".$q[1]."/szervezeti-besorolas-szerkesztese/" . $value["id"] . "\">" . $value["nev"] . "</a><br />" . $details["szervezet"]["nev"];
	    }
	    ?>

	    <div class="main_center_content_left"></div>
	    <div class="main_center_content_center"><?php print "<a href=\"" . $config["site"]["absolutepath"] . "/szervezetek/".$q[1]."/szervezeti-besorolas-szerkesztese/0\">".$config["site"]["tree_root_element"]."</a><br />".$details["szervezet"]["nev"]; ?></div>
	    <div class="main_center_content_right"></div>
	    <div class="main_center_content_spacer"></div>


	    <?php
	    $a = 0;
	    $sql = "SELECT id, nev, teljes_id FROM szervezetek WHERE teljes_id LIKE '" . $details["teljes_id"] . "%' ORDER BY nev";
	    $result = mysql_query($sql);
	    while ($sor = mysql_fetch_assoc($result)) {
		if (!strcmp($sor["teljes_id"], ($details["teljes_id"] . "-" . $sor["id"]))) {
		    $details["alarendelt_szervezet"]["nev"].="<a href=\"" . $config["site"]["absolutepath"] . "/szervezetek/".$q[1]."/szervezeti-besorolas-szerkesztese/" . $sor["id"] . "\">" . $sor["nev"] . "</a><br />";
		    $a++;
		}
	    }
	    ?>

	    <div class="main_center_title">
		<div class="main_center_title_left"><font style="color:#617f10">ALÁRENDELT </font> szervek</div>
	    </div>

	    <?php if ($a) { ?>
	        <div class="main_center_content_left"></div>
	        <div class="main_center_content_center"><?php print $details["alarendelt_szervezet"]["nev"]; ?></div>
	        <div class="main_center_content_right"></div>
	        <div class="main_center_content_spacer"></div>

	    <?php } else { ?>
	        <div class="main_center_content_left"></div>
	        <div class="main_center_content_center">Nincs alárendelt szerv!</div>
	        <div class="main_center_content_right"></div>
	        <div class="main_center_content_spacer"></div>
	    <?php } ?>
	    <div class="site_spacer"></div>
	    <div class="site_spacer"></div>

	</div>


	<div class="main_center_spacer"></div>
	<?php
		}
	    }
    }
//*********************************************************************
     function szervezet_torlese($error) {
        global $lang, $odin, $config, $q;
        $details=$odin->get_szervezet_details($q[1]);
	?>
        <div class="main_center_spacer"></div>

        <div class="main_center_container">
            <div class="main_center_title">
                <div class="main_center_title_left"><font style="color:#617f10">SZERVEZET </font>törlése</div>
                <div class="main_center_title_right"></div>
            </div>
            <form action="<?php print ("" . $config["site"]["absolutepath"] . "/szervezetek/" . $q[1] . "/szervezet-torlese"); ?>" method="post">
	    <div class="main_content_spacer"></div>  
            <div class="ui-state-error ui-corner-all" style="margin-top: 20px; padding: 0 .7em;"> 
                <p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span>
		    <b>Biztos törlöd a szervezetet? A törlés végleges!</b>
                </p>
            </div>
	    	     <?php if ($error) { ?>
	        <div class="ui-state-highlight ui-corner-all" style="margin-top: 5px; padding: 0 .7em;"> 
	    	<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
			<?php print $error["kiir"]; ?>
	    	</p>
	        </div>
	    <?php } ?>
	    <div class="main_center_spacer"></div>  
	    <div class="main_center_content_left">Szervezet id:</div><div class="main_center_content_center"><b><?php print ($details["id"]); ?></b></div>
            <div class="main_center_content_spacer"></div>
		<?php
		foreach ($details["valami"] as $key => $value) {
		    $details["szervezet"]["nev"] = "<a href=\"" . $config["site"]["absolutepath"] . "/szervezetek/" . $value["id"] . "\">" . $value["nev"] . "</a><br />" . $details["szervezet"]["nev"];
		}
		$details["szervezet"]["nev"] = "<a href=\"" . $config["site"]["absolutepath"] . "/szervezetek\">".$config["site"]["tree_root_element"]."</a><br />" . $details["szervezet"]["nev"];
		?>

	        <div class="main_center_content_left">Szervezeti besorolás: </div>
	        <div class="main_center_content_center"><?php print $details["szervezet"]["nev"]; ?></div>
	        <div class="main_center_content_right"></div>
	        <div class="main_center_content_spacer"></div>
	    <div class="main_center_content_left"></div><div class="main_center_content_center"><input class="ui-state-default ui-corner-all" type="submit" name="torol" value="<?php print $lang["gomb"]["torold"]; ?>" /> <input class="ui-state-default ui-corner-all" type="submit" name="torol" value="<?php print $lang["gomb"]["megse"];?>" /></div>
            <div class="main_center_content_spacer"></div>
             </form> 
	    <div class="site_spacer"></div>                                
           
        </div>
        <?php
    }
    //*********************************************************************
    	function szervezet_hozzaadasa($error) {
	    global $lang, $odin, $q;
	    $details = $odin->get_szervezet_details($q["1"]);
	    ?>
	    <script type="text/javascript">
		$(document).ready(function() {
		    if ( $('#sztipus').val() == 1){
			    $('#adatok').hide();
			}
			
		    $('#sztipus').change(function(){
			if ( $(this).val() == 1 ){
			    $('#adatok').slideUp(800);
			}
			if ( $(this).val() == 2 ){
			    $('#adatok').slideDown(800);
			}
			if ( $(this).val() == 3 ){
			    $('#adatok').slideDown(800);
			}
			if ( $(this).val() == 4 ){
			    $('#adatok').slideDown(800);
			}
			if ( $(this).val() == 5 ){
			    $('#adatok').slideDown(800);
			}

		    });
		});
	    </script>
	    <div class="main_center_spacer"></div>

	    <div class="main_center_container">
		<div class="main_center_title">
		    <div class="main_center_title_left"><font style="color:#617f10">ÚJ ALÁRENDELT SZERVEZET </font>Hozzáadása</div>
		</div>
		<?php if ($error) { ?>
	    	<div class="ui-state-highlight ui-corner-all" style="margin-top: 5px; padding: 0 .7em;"> 
	    	    <p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
			    <?php print $error["kiir"]; ?>
	    	    </p>
	    	</div>
		<?php } ?>
		<div class="main_login_spacer"></div>

		<form name="ujszerv" action="szervezetek/<?php print $q[1]; ?>/uj-alarendelt-szerv-hozzaadasa" method="post"> 
		    <div class="main_login_container">

			<div class="main_login_left">Szervezet neve:</div>
			<div class="main_login_right"><input type="text" name="nev" value="<?php
	if ($_POST["nev"]) {
	    print $_POST["nev"];
	}
		?>" size="32" maxlength="64" /></div> 
			<div class="main_login_spacer"></div>
			<div class="main_login_left">Szervezet típusa:</div>
			<div class="main_login_right">
			    <select id="sztipus" name="szervezet_tipus">
				<?php
				foreach ($lang["szervezet"]["tipus"] as $key => $value) {
				    ?>
	    			<option value="<?php print $key; ?>"<?php if ($key == $_POST["szervezet_tipus"])
			    print " selected=\"selected\"" ?>><?php print $value; ?></option>  
					<?php } ?>
			    </select>
			</div>
			<div class="main_login_spacer"></div>
			<div id="adatok">
			    <div class="main_login_left">Öröklődés: </div>
			    <div class="main_login_right" style="text-align:left;">
				<input name="orokldes" type="radio" value="0" <?php
				if (!$_POST["oroklodes"] || is_null($_POST["oroklodes"])) {
				    print " checked=\"checked\"";
				}
					?> />öröklődés engedélyezve<br />
				<input name="orokldes" type="radio" value="1" <?php
			       if ($_POST["oroklodes"]==1) {
				   print " checked=\"checked\"";
			       }
					?> />öröklődés csak erre a szervre tiltva<br />
				<input name="orokldes" type="radio" value="2" <?php
			       if ($_POST["oroklodes"]==2) {
				   print " checked=\"checked\"";
			       }
					?> />öröklődés teljes tiltása
			    </div>
			    <div class="main_login_spacer"></div>

			<div class="main_login_left"><?php print($lang["felhasznalok"]["email"]); ?></div>
			<div class="main_login_right"><input type="text" name="email" value="<?php
			       if ($_POST["email"]) {
				   print $_POST["email"];
			       } else if (!$details["email"]) {
				   
			       } else {
				   print $details["email"];
			       }
					?>" size="32" maxlength="64" /></div> 
			<div class="main_login_spacer"></div>

			<div class="main_login_left"><?php print($lang["felhasznalok"]["telephely_irsz"]); ?></div>
			<div class="main_login_right"><input type="text" name="telephely_irsz" value="<?php
						     if ($_POST["telephely_irsz"]) {
							 print $_POST["telephely_irsz"];
						     } else if (!$details["telephely_irsz"]) {
							 
						     } else {
							 print $details["telephely_irsz"];
						     }
					?>" size="32" maxlength="4" /></div> 
			<div class="main_login_spacer"></div>

			<div class="main_login_left"><?php print($lang["felhasznalok"]["telephely_varos"]); ?></div>
			<div class="main_login_right"><input type="text" name="telephely_varos" value="<?php
						     if ($_POST["telephely_varos"]) {
							 print $_POST["telephely_varos"];
						     } else if (!$details["telephely_varos"]) {
							 
						     } else {
							 print $details["telephely_varos"];
						     }
					?>" size="32" maxlength="32" /></div> 
			<div class="main_login_spacer"></div>

			<div class="main_login_left"><?php print($lang["felhasznalok"]["telephely_cim"]); ?></div>
			<div class="main_login_right"><input type="text" name="telephely_cim" value="<?php
						     if ($_POST["telephely_cim"]) {
							 print $_POST["telephely_cim"];
						     } else if (!$details["telephely_cim"]) {
							 
						     } else {
							 print $details["telephely_cim"];
						     }
					?>" size="32" maxlength="64" /></div> 
			<div class="main_login_spacer"></div>

			<div class="main_login_left"><?php print($lang["felhasznalok"]["telephely_szam"]); ?></div>
			<div class="main_login_right"><input type="text" name="telephely_szam" value="<?php
						     if ($_POST["telephely_szam"]) {
							 print $_POST["telephely_szam"];
						     } else if (!$details["telephely_szam"]) {
							 
						     } else {
							 print $details["telephely_szam"];
						     }
					?>" size="32" maxlength="16" /></div> 
			<div class="main_login_spacer"></div>

			<div class="main_login_left"><?php print($lang["felhasznalok"]["levelezesi_irsz"]); ?></div>
			<div class="main_login_right"><input type="text" name="levelezesi_irsz" value="<?php
						     if ($_POST["levelezesi_irsz"]) {
							 print $_POST["levelezesi_irsz"];
						     } else if (!$details["levelezesi_irsz"]) {
							 
						     } else {
							 print $details["levelezesi_irsz"];
						     }
					?>" size="32" maxlength="4" /></div> 
			<div class="main_login_spacer"></div>

			<div class="main_login_left"><?php print($lang["felhasznalok"]["levelezesi_varos"]); ?></div>
			<div class="main_login_right"><input type="text" name="levelezesi_varos" value="<?php
						     if ($_POST["levelezesi_varos"]) {
							 print $_POST["levelezesi_varos"];
						     } else if (!$details["levelezesi_varos"]) {
							 
						     } else {
							 print $details["levelezesi_varos"];
						     }
					?>" size="32" maxlength="32" /></div> 
			<div class="main_login_spacer"></div>

			<div class="main_login_left"><?php print($lang["felhasznalok"]["levelezesi_cim"]); ?></div>
			<div class="main_login_right"><input type="text" name="levelezesi_cim" value="<?php
						     if ($_POST["levelezesi_cim"]) {
							 print $_POST["levelezesi_cim"];
						     } else if (!$details["levelezesi_cim"]) {
							 
						     } else {
							 print $details["levelezesi_cim"];
						     }
					?>" size="32" maxlength="64" /></div> 
			<div class="main_login_spacer"></div>

			<div class="main_login_left"><?php print($lang["felhasznalok"]["fax"]); ?></div>
			<div class="main_login_right"><input type="text" name="fax" value="<?php
						     if ($_POST["fax"]) {
							 print $_POST["fax"];
						     } else if (!$details["fax"]) {
							 
						     } else {
							 print $details["fax"];
						     }
					?>" size="32" maxlength="6" /></div> 
			<div class="main_login_spacer"></div>

			<div class="main_login_left"><?php print($lang["felhasznalok"]["vez_kozvetlen"]); ?></div>
			<div class="main_login_right"><input type="text" name="vez_kozvetlen" value="<?php
						     if ($_POST["vez_kozvetlen"]) {
							 print $_POST["vez_kozvetlen"];
						     } else if (!$details["vez_kozvetlen"]) {
							 
						     } else {
							 print $details["vez_kozvetlen"];
						     }
					?>" size="32" maxlength="8" /></div> 
			<div class="main_login_spacer"></div>

			<div class="main_login_left"><?php print($lang["felhasznalok"]["vez_fax"]); ?></div>
			<div class="main_login_right"><input type="text" name="vez_fax" value="<?php
						     if ($_POST["vez_fax"]) {
							 print $_POST["vez_fax"];
						     } else if (!$details["vez_fax"]) {
							 
						     } else {
							 if (strlen($details["vez_fax"]) < 9)
							     print $details["vez_fax"];
						     }
					?>" size="32" maxlength="8" /></div> 
			<div class="main_login_spacer"></div>
						<div class="main_login_left">Hírközpont: </div>
			<div class="main_login_right"><input type="text" name="hirkozpont" value="<?php
	if ($_POST["hirkozpont"]) {
	    print $_POST["hirkozpont"];
	} else if (!$details["hirkozpont"]) {
	    
	} else {
	    print $details["hirkozpont"];
	}
		?>" size="32" maxlength="6" /></div> 
			<div class="main_login_spacer"></div>

			<div class="main_login_left">Ügyelet: </div>
			<div class="main_login_right"><input type="text" name="ugyelet" value="<?php
						     if ($_POST["ugyelet"]) {
							 print $_POST["ugyelet"];
						     } else if (!$details["ugyelet"]) {
							 
						     } else {
							 print $details["ugyelet"];
						     }
		?>" size="32" maxlength="6" /></div> 
			<div class="main_login_spacer"></div>
			</div>
			<div class="main_login_spacer"></div>
			<div class="main_login_left">&nbsp;</div>
			<input class="ui-state-default ui-corner-all" type="submit" name="hozzaad" value="<?php print($lang["gomb"]["hozzaad"]); ?>" />
			<input class="ui-state-default ui-corner-all" type="submit" name="hozzaad" value="<?php print($lang["gomb"]["megse"]); ?>" />
		    </div>
		</form>

	    </div>


	    <div class="main_center_spacer"></div>
	    <?php
	}
    }
    ?>