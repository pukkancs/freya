<?php

// UTF-8asítás...

class output_html {

    function head_box() {
        global $config, $ua;
        global $lang;
        ?>
        <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
        <html xmlns="http://www.w3.org/1999/xhtml">

            <head>
                <title> <?php print($lang["head"]["title"]); ?></title>
                <base href="<?php print($config["site"]["absolutepath"]); ?>/" />
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                <link href="favicon.ico" type="image/x-icon" rel="shortcut icon" />
                <meta name="Keywords" content=" <?php print($lang["head"]["keywords"]); ?>"/>
                <meta name="Description" content=" <?php print($lang["head"]["description"]); ?>" />
                <link href="css/<?php print($ua->Style); ?>" rel="stylesheet" type="text/css" />
                <link href="css/jquery-ui.css" rel="stylesheet" type="text/css" />
                <script type="text/javascript" src="js/jquery.js"></script>
                <script type="text/javascript" src="js/jquery.ui.js"></script>
                <script type="text/javascript" src="js/jquery.ui.datepicker-hu.js"></script>
                <script type="text/javascript" src="js/jquery.simpletreeview.min.js"></script>
                <script type="text/javascript">
                    $(function(){
                        $( "#szuletesnap" ).datepicker({
                            yearRange: '1900:2000',
                            changeYear: 'true',
                            showMonthAfterYear:'true'});
                        $( "#kiemelt" ).datepicker({
                            yearRange: '2012:2050',
                            changeYear: 'true',
                            showMonthAfterYear:'true'});
                        $( "#nevnap" ).datepicker( {showMonthAfterYear:'true'});
                    });
                    $(document).ready(function() {
                        $('#main_login_miez').hide();
                        $('#main_login_lowsec').hide();
                        $('#lowsec-toogle').click(function() {$('#main_login_lowsec').slideToggle(400); return false;});
                        $('#main_login_login').hide();
                        $('#login-toogle').click(function() {$('#main_login_login').slideToggle(400); return false;});
                        $('#miez-toogle').click(function() {$('#main_login_miez').slideToggle(400); return false;});
                        $('#main_center_content_names').hide();
                        $('#names-toogle').click(function() {$('#main_center_content_names').slideToggle(400); return false;});
                        $('#main_center_content_privat_szuletesnap').hide();
                        $('#privat-szuletesnap-toogle').click(function() {$('#main_center_content_privat_szuletesnap').slideToggle(400); return false;});
                    });
                </script>  
            </head>


            <body>

                <div id="site">
                    <div id="top_spacer"></div>
                    <div id="head_container">
                        <div id="head">
                            <div id="head_logo"><a class="head_logo" href="<?php print($config["site"]["absolutepath"]); ?>"><?php print strtoupper($config["site"]["name"]); ?></a><font style="font-size:8px; font-weight:bold; vertical-align:top; color:#FFF"><?php print ("v." . $config["site"]["version"]); ?></font></div>
                            <div id="head_menu_container">
                                <div class="head_menu_spacer"></div>
                                <div class="head_menu">
                                    <?php if (!isset($_SESSION["user"]["id"])) { ?>
                                        <a class="head_menu" href="bejelentkezes"><?php print $lang["main_menu"]["bejelentkezes"]; ?></a>-
                                        <a class="head_menu" href="elfelejtett-jelszo"><?php print $lang["main_menu"]["elfelejtett_jelszo"]; ?></a>
                                    <?php } ?>
                                    <?php if (isset($_SESSION["user"]["id"]) && ($_SESSION["user"]["account_statusz"] == "1" || $_SESSION["user"]["root"])) { ?>
                                        <a class="head_menu" href="hirek"><?php print $lang["main_menu"]["hirek"]; ?></a>-
                                        <a class="head_menu" href="telefonkonyv"><?php print $lang["main_menu"]["telefonkonyv"]; ?></a>-
                                        <a class="head_menu" href="illetekesseg"><?php print $lang["main_menu"]["illetekesseg"]; ?></a>-
                                        <a class="head_menu" href="sajat-vilag"><?php print $lang["main_menu"]["sajat_vilag"]; ?></a>-
            <?php if ($_SESSION["user"]["root"] || $_SESSION["user"]["rg_beosztasok"] || $_SESSION["user"]["rg_statisztika"] || $_SESSION["user"]["rg_log"]) { ?><a class="head_menu" href="admin"><?php print $lang["main_menu"]["admin"]; ?></a>-<?php } ?>
                                        <a class="head_menu" href="kijelentkezes"><?php print $lang["main_menu"]["kijelentkezes"]; ?></a>
                                    <?php } ?>
                                    <?php if (isset($_SESSION["user"]["id"]) && ($_SESSION["user"]["account_statusz"] != "1" && !$_SESSION["user"]["root"])) { ?>
                                        <a class="head_menu" href="kijelentkezes"><?php print $lang["main_menu"]["kijelentkezes"]; ?></a>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>

        <?php
    }

    function left_box() {
        global $ua, $stats, $q, $lang, $config, $odin;
        ?>	

                    <div id="main">

                        <div id="main_left">
        <?php
        include("content/side_menu.php");
        ?>
                        </div>


                        <div id="main_center_big_container">
                            <div id="main_center">
        <?php
    }

    function footer_box() {
        global $start_time, $sql_query_count, $lang, $q;
        ?>	


                            </div>


                            <div class="main_closer_left"><font style="font-weight:bold">Copyright &copy; 2007-2012</font> &minus; <font style="color:#617f10">Dobó Tamás</font></div>
                            <div class="main_closer_center"><?php
                        print($lang["footer"]["1"] . "" . round(microtime(true) - $start_time, 2) . "" . $lang["footer"]["2"] . "" . $sql_query_count . "" . $lang["footer"]["3"]);
                        ?></div>
                            <div class="main_closer_right"></div>

                        </div>

                    </div>
                </div>
            </body>
        </html>
        <?php
    }

}

//end of obj

$output_html = new output_html();
?>
